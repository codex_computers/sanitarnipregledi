object frmPriem: TfrmPriem
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1088#1080#1077#1084
  ClientHeight = 789
  ClientWidth = 1201
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1201
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar11'
        end
        item
          Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab3: TdxRibbonTab
      Caption = #1048#1079#1074#1077#1096#1090#1072#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar10'
        end
        item
          ToolbarName = 'dxBarManager1Bar12'
        end>
      Index = 1
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end>
      Index = 2
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 766
    Width = 1201
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074' '#1087#1088#1077#1075#1083#1077#1076', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1087#1088#1077#1075#1083#1077#1076',  F8 - '#1041#1088#1080#1096#1080' '#1087#1088#1077#1075#1083#1077#1076', F9' +
          ' - '#1047#1072#1087#1080#1096#1080', Ctrl+F5 - '#1044#1086#1076#1072#1076#1080' '#1091#1089#1083#1091#1075#1072', Ctrl+ F6 - '#1041#1088#1080#1096#1080' '#1091#1089#1083#1091#1075#1072', Ctr' +
          'l+F - '#1055#1088#1077#1073#1072#1088#1072#1112', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1201
    Height = 373
    Align = alTop
    TabOrder = 2
    object cxPageControlMaster: TcxPageControl
      Left = 1
      Top = 1
      Width = 1199
      Height = 371
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelarenM
      Properties.CustomButtons.Buttons = <>
      ExplicitHeight = 300
      ClientRectBottom = 371
      ClientRectRight = 1199
      ClientRectTop = 24
      object cxTabSheetTabelarenM: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1087#1088#1080#1077#1084' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080
        ImageIndex = 0
        ExplicitHeight = 276
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1199
          Height = 347
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 276
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            FindPanel.DisplayMode = fpdmAlways
            FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPregledi
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = cxGrid1DBTableView1BROJ
              end
              item
                Format = '0.00,.'
                Kind = skSum
                Column = cxGrid1DBTableView1CENA
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
            OptionsView.Footer = True
            Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA'
              Visible = False
              Width = 74
            end
            object cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_PRIEM'
              Width = 101
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 101
            end
            object cxGrid1DBTableView1U_LISTA: TcxGridDBColumn
              DataBinding.FieldName = 'U_LISTA'
              Width = 129
            end
            object cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PACIENT_ID'
              Visible = False
              Width = 96
            end
            object cxGrid1DBTableView1EMBG: TcxGridDBColumn
              DataBinding.FieldName = 'EMBG'
              Width = 117
            end
            object cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PACIENT_NAZIV'
              Width = 165
            end
            object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNO_MESTO'
              Visible = False
              Width = 126
            end
            object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNOMESTONAZIV'
              Width = 137
            end
            object cxGrid1DBTableView1TP: TcxGridDBColumn
              DataBinding.FieldName = 'TP'
              Visible = False
              Width = 83
            end
            object cxGrid1DBTableView1P: TcxGridDBColumn
              DataBinding.FieldName = 'P'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER_NAZIV'
              Width = 203
            end
            object cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn
              DataBinding.FieldName = 'KATEGORIJA_RM'
              Visible = False
              Width = 139
            end
            object cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'KATEGORIJANAZIV'
              Width = 169
            end
            object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PAKET_ID'
              Width = 168
            end
            object cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn
              DataBinding.FieldName = 'NACINPLAKANJE'
              Width = 127
            end
            object cxGrid1DBTableView1BR_KVITANCIJA: TcxGridDBColumn
              DataBinding.FieldName = 'BR_KVITANCIJA'
              Width = 108
            end
            object cxGrid1DBTableView1CENA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              Width = 94
            end
            object cxGrid1DBTableView1PLACANJE: TcxGridDBColumn
              DataBinding.FieldName = 'PLACANJE'
              Visible = False
              Width = 145
            end
            object cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_ZAVERKA'
              Width = 118
            end
            object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_VAZENJE'
              Width = 98
            end
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 168
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 213
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 201
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 237
            end
            object cxGrid1DBTableView1BRFAKTURI: TcxGridDBColumn
              DataBinding.FieldName = 'BRFAKTURI'
              Visible = False
            end
            object cxGrid1DBTableView1KONTROLEN_PREGLED: TcxGridDBColumn
              Caption = #1050#1086#1085#1090#1088#1086#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076' ('#1076#1072'/'#1085#1077')'
              DataBinding.FieldName = 'KONTROLEN_PREGLED'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1DOGOVOR_ID: TcxGridDBColumn
              DataBinding.FieldName = 'DOGOVOR_ID'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS_VKUPNO'
              Width = 80
            end
            object cxGrid1DBTableView1FISKALNA: TcxGridDBColumn
              DataBinding.FieldName = 'FISKALNA'
              Width = 57
            end
            object cxGrid1DBTableView1STORNA: TcxGridDBColumn
              DataBinding.FieldName = 'STORNA'
              Width = 49
            end
            object cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_PRESMETKA'
              Width = 102
            end
            object cxGrid1DBTableView1PRATI_MAIL_B: TcxGridDBColumn
              DataBinding.FieldName = 'PRATI_MAIL_B'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxSmallImages
              Properties.Items = <
                item
                  Description = #1044#1072
                  ImageIndex = 6
                  Value = 1
                end
                item
                  Description = #1053#1077
                  ImageIndex = 21
                  Value = 0
                end
                item
                  Description = #1053#1077
                  ImageIndex = 21
                end>
              Width = 215
            end
            object cxGrid1DBTableView1PRATI_MAIL_PB: TcxGridDBColumn
              DataBinding.FieldName = 'PRATI_MAIL_PB'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxSmallImages
              Properties.Items = <
                item
                  Description = #1044#1072
                  ImageIndex = 6
                  Value = 1
                end
                item
                  Description = #1053#1077
                  ImageIndex = 21
                  Value = 0
                end
                item
                  Description = #1053#1077
                  ImageIndex = 21
                end>
              Width = 288
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheetDeatlenM: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1088#1080#1077#1084
        ImageIndex = 1
        ExplicitHeight = 276
        object dPanel: TPanel
          Left = 0
          Top = 0
          Width = 1199
          Height = 347
          Align = alClient
          Enabled = False
          TabOrder = 0
          ExplicitHeight = 276
          object Label4: TLabel
            Left = 5
            Top = 0
            Width = 52
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1064#1080#1092#1088#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object ID: TcxDBTextEdit
            Left = 42
            Top = 240
            Hint = #1048#1084#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
            TabStop = False
            BeepOnEnter = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dm.dsPregledi
            ParentFont = False
            Properties.BeepOnError = True
            Style.Shadow = False
            TabOrder = 1
            Visible = False
            Width = 98
          end
          object cxGroupBox1: TcxGroupBox
            Left = 14
            Top = 0
            Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1077#1075#1083#1077#1076
            TabOrder = 0
            Height = 174
            Width = 283
            object Label3: TLabel
              Left = 99
              Top = 30
              Width = 52
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1043#1086#1076#1080#1085#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label5: TLabel
              Left = 35
              Top = 57
              Width = 116
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label6: TLabel
              Left = 51
              Top = 84
              Width = 100
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label11: TLabel
              Left = 13
              Top = 111
              Width = 138
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label12: TLabel
              Left = 28
              Top = 138
              Width = 122
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object GODINA: TcxDBComboBox
              Tag = 1
              Left = 157
              Top = 27
              Hint = #1043#1086#1076#1080#1085#1072' '#1074#1086' '#1082#1086#1112#1072' '#1089#1077' '#1080#1079#1074#1088#1096#1091#1074#1072' '#1087#1088#1077#1075#1083#1077#1076#1086#1090
              DataBinding.DataField = 'GODINA'
              DataBinding.DataSource = dm.dsPregledi
              Properties.Items.Strings = (
                '2012'
                '2013'
                '2014'
                '2015'
                '2016'
                '2017')
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 98
            end
            object DATUM_PRIEM: TcxDBDateEdit
              Tag = 1
              Left = 157
              Top = 54
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1086#1112' '#1089#1077' '#1080#1079#1074#1088#1096#1091#1074#1072' '#1087#1088#1080#1077#1084#1086#1090
              DataBinding.DataField = 'DATUM_PRIEM'
              DataBinding.DataSource = dm.dsPregledi
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 98
            end
            object BROJ: TcxDBTextEdit
              Tag = 1
              Left = 157
              Top = 81
              Hint = #1041#1088#1086#1112' '#1085#1072' '#1083#1072#1073#1086#1088#1072#1090#1086#1088#1080#1089#1082#1080' '#1076#1085#1077#1074#1085#1080#1082
              BeepOnEnter = False
              DataBinding.DataField = 'BROJ'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Style.Shadow = False
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 98
            end
            object DATUM_ZAVERKA: TcxDBDateEdit
              Left = 157
              Top = 108
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1090#1074#1088#1076#1072
              TabStop = False
              DataBinding.DataField = 'DATUM_ZAVERKA'
              DataBinding.DataSource = dm.dsPregledi
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 97
            end
            object DATUM_VAZENJE: TcxDBDateEdit
              Left = 156
              Top = 135
              Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1087#1086#1074#1090#1086#1088#1085#1086' '#1090#1088#1077#1073#1072' '#1076#1072' '#1087#1088#1072#1074#1080' '#1089#1072#1085#1080#1090#1072#1088#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
              TabStop = False
              DataBinding.DataField = 'DATUM_VAZENJE'
              DataBinding.DataSource = dm.dsPregledi
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 98
            end
          end
          object cxGroupBox4: TcxGroupBox
            Left = 303
            Top = 145
            Caption = #1055#1072#1082#1077#1090' '#1080' '#1085#1072#1095#1080#1085#1080' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
            TabOrder = 3
            DesignSize = (
              698
              118)
            Height = 118
            Width = 698
            object Label8: TLabel
              Left = 79
              Top = 30
              Width = 53
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1055#1072#1082#1077#1090' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label10: TLabel
              Left = 14
              Top = 57
              Width = 118
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label9: TLabel
              Left = 74
              Top = 84
              Width = 58
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1062#1077#1085#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label2: TLabel
              Left = 455
              Top = 57
              Width = 120
              Height = 19
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1112#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object PLACANJE_NAZIV: TcxDBLookupComboBox
              Tag = 1
              Left = 181
              Top = 54
              Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' '#1085#1072' '#1091#1089#1083#1091#1075#1072' - '#1085#1072#1079#1080#1074
              Anchors = [akLeft, akTop, akRight, akBottom]
              BeepOnEnter = False
              DataBinding.DataField = 'PLACANJE'
              DataBinding.DataSource = dm.dsPregledi
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  Width = 100
                  FieldName = 'ID'
                end
                item
                  Width = 300
                  FieldName = 'NAZIV'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsTipPlakanje
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 268
            end
            object PLACANJE: TcxDBTextEdit
              Tag = 1
              Left = 138
              Top = 54
              Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' '#1085#1072' '#1091#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'PLACANJE'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 44
            end
            object CENA: TcxDBTextEdit
              Left = 138
              Top = 81
              Hint = #1048#1079#1085#1086#1089' '#1079#1072' '#1085#1072#1087#1083#1072#1090#1072
              BeepOnEnter = False
              DataBinding.DataField = 'CENA'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Style.Shadow = False
              TabOrder = 5
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 98
            end
            object Paket_ID: TcxTextEdit
              Left = 138
              Top = 27
              Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1077#1090
              TabOrder = 0
              Visible = False
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 44
            end
            object cbPaketNaziv: TcxLookupComboBox
              Left = 181
              Top = 27
              Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1082#1077#1090
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'NAZIV'
                end
                item
                  FieldName = 'CENA'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dm.dsPaketi
              Properties.OnChange = cbPaketNazivPropertiesChange
              Properties.OnCloseUp = cbPaketNazivPropertiesCloseUp
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 497
            end
            object PAKET_ID_TEXT: TcxDBTextEdit
              Left = 181
              Top = 27
              TabStop = False
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataBinding.DataField = 'PAKET_ID'
              DataBinding.DataSource = dm.dsPregledi
              TabOrder = 6
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 497
            end
            object BR_KVITANCIJA: TcxDBTextEdit
              Left = 581
              Top = 54
              Hint = #1041#1088#1086#1112' '#1085#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1112#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1087#1083#1072#1116#1072#1114#1077#1090' '#1077' '#1074#1086' '#1075#1086#1090#1086#1074#1086
              BeepOnEnter = False
              DataBinding.DataField = 'BR_KVITANCIJA'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Style.Shadow = False
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 97
            end
          end
          object cxGroupBox3: TcxGroupBox
            Left = 303
            Top = 0
            Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1072#1094#1080#1077#1085#1090
            TabOrder = 2
            DesignSize = (
              698
              146)
            Height = 146
            Width = 698
            object Label7: TLabel
              Left = 74
              Top = 30
              Width = 58
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1055#1072#1094#1080#1077#1085#1090' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label14: TLabel
              Left = 59
              Top = 84
              Width = 73
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1055#1072#1088#1090#1085#1077#1088' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label13: TLabel
              Left = 27
              Top = 57
              Width = 105
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label1: TLabel
              Left = 14
              Top = 111
              Width = 118
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object PACIENT_EMBG: TcxDBTextEdit
              Tag = 1
              Left = 138
              Top = 27
              Hint = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
              BeepOnEnter = False
              DataBinding.DataField = 'EMBG'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 94
            end
            object PACIENT_NAZIV: TcxDBExtLookupComboBox
              Tag = 1
              Left = 232
              Top = 27
              Hint = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataBinding.DataField = 'PACIENT_ID'
              DataBinding.DataSource = dm.dsPregledi
              Properties.ClearKey = 46
              Properties.DropDownSizeable = True
              Properties.View = cxGridViewRepository1DBTableView1
              Properties.KeyFieldNames = 'ID'
              Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 446
            end
            object R_MESTO: TcxDBTextEdit
              Tag = 1
              Left = 138
              Top = 54
              Hint = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'RABOTNO_MESTO'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 44
            end
            object R_MESTO_NAZIV: TcxDBLookupComboBox
              Tag = 1
              Left = 182
              Top = 54
              Hint = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
              Anchors = [akLeft, akTop, akRight, akBottom]
              BeepOnEnter = False
              DataBinding.DataField = 'RABOTNO_MESTO'
              DataBinding.DataSource = dm.dsPregledi
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  Width = 100
                  FieldName = 'ID'
                end
                item
                  Width = 300
                  FieldName = 'NAZIV'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsRabotnoMesto
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 495
            end
            object KATEGORIJA_RM: TcxDBTextEdit
              Tag = 1
              Left = 138
              Top = 108
              Hint = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'KATEGORIJA_RM'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 7
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 44
            end
            object KATEGORIJA_RM_NAZIV: TcxDBLookupComboBox
              Tag = 1
              Left = 182
              Top = 108
              Hint = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1085#1072#1079#1080#1074
              Anchors = [akLeft, akTop, akRight, akBottom]
              BeepOnEnter = False
              DataBinding.DataField = 'KATEGORIJA_RM'
              DataBinding.DataSource = dm.dsPregledi
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  Width = 100
                  FieldName = 'ID'
                end
                item
                  Width = 300
                  FieldName = 'NAZIV'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dm.dsKategorijaRM
              TabOrder = 8
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 495
            end
            object TIP_PARTNER: TcxDBTextEdit
              Tag = 1
              Left = 138
              Top = 81
              Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1090#1080#1087
              BeepOnEnter = False
              DataBinding.DataField = 'TP'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 34
            end
            object PARTNER: TcxDBTextEdit
              Tag = 1
              Left = 171
              Top = 81
              Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1096#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'P'
              DataBinding.DataSource = dm.dsPregledi
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 5
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 40
            end
            object PARTNERNAZIV: TcxExtLookupComboBox
              Left = 210
              Top = 81
              Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1085#1072#1079#1080#1074
              Anchors = [akLeft, akTop, akRight]
              Properties.ClearKey = 46
              Properties.DropDownSizeable = True
              Properties.OnCloseUp = PARTNERNAZIVPropertiesCloseUp
              Properties.OnEditValueChanged = PARTNERNAZIVPropertiesEditValueChanged
              TabOrder = 6
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 468
            end
          end
          object ZapisiButton: TcxButton
            Left = 1007
            Top = 223
            Width = 75
            Height = 25
            Action = aZapisi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 4
          end
          object OtkaziButton: TcxButton
            Left = 1088
            Top = 223
            Width = 75
            Height = 25
            Action = aOtkazi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 5
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 499
    Width = 1201
    Height = 267
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    ExplicitTop = 428
    ExplicitHeight = 338
    object cxPageControlDetail: TcxPageControl
      Left = 1
      Top = 1
      Width = 1199
      Height = 265
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelarenD
      Properties.CustomButtons.Buttons = <>
      ExplicitTop = -4
      ExplicitHeight = 336
      ClientRectBottom = 265
      ClientRectRight = 1199
      ClientRectTop = 24
      object cxTabSheetTabelarenD: TcxTabSheet
        Caption = #1059#1089#1083#1091#1075#1080' '#1074#1086' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1087#1088#1080#1077#1084
        ImageIndex = 0
        ExplicitHeight = 312
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1199
          Height = 241
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 312
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid2DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsPreglediStavki
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 72
            end
            object cxGrid2DBTableView1PREGLED_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PREGLED_ID'
              Visible = False
              Width = 97
            end
            object cxGrid2DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
              Width = 143
            end
            object cxGrid2DBTableView1RENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV'
              Width = 197
            end
            object cxGrid2DBTableView1USLUGA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'USLUGA_ID'
              Visible = False
              Width = 85
            end
            object cxGrid2DBTableView1ULUGANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ULUGANAZIV'
              Width = 256
            end
            object cxGrid2DBTableView1CENA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              Visible = False
              Width = 115
            end
            object cxGrid2DBTableView1DOGOVOR_ID: TcxGridDBColumn
              DataBinding.FieldName = 'DOGOVOR_ID'
              Visible = False
              Width = 100
            end
            object cxGrid2DBTableView1M_DOGOVORI_ID: TcxGridDBColumn
              DataBinding.FieldName = 'M_DOGOVORI_ID'
              Visible = False
              Width = 224
            end
            object cxGrid2DBTableView1ZAVRSENA: TcxGridDBColumn
              DataBinding.FieldName = 'ZAVRSENA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  Description = #1053#1045#1047#1040#1042#1056#1064#1045#1053#1040
                  ImageIndex = 3
                  Value = 0
                end
                item
                  Description = #1047#1040#1042#1056#1064#1045#1053#1040
                  ImageIndex = 2
                  Value = 1
                end
                item
                  ImageIndex = 4
                  Value = 2
                end>
              Styles.OnGetContentStyle = cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle
              Width = 105
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 170
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              Caption = #1042#1088#1077#1084#1077
              DataBinding.FieldName = 'TS_UPD'
              Width = 112
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1NAOD: TcxGridDBColumn
              DataBinding.FieldName = 'NAOD'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.ImageAlign = iaRight
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  Description = #1059#1056#1045#1044#1045#1053' '#1053#1040#1054#1044
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = #1048#1052#1040' '#1053#1040#1054#1044' '#1042#1054
                  ImageIndex = 1
                  Value = 1
                end
                item
                  ImageIndex = 4
                  Value = 2
                end
                item
                  Value = 3
                end>
              Styles.OnGetContentStyle = cxGrid2DBTableView1NAODStylesGetContentStyle
              Width = 128
            end
            object cxGrid2DBTableView1BR_MRSA: TcxGridDBColumn
              DataBinding.FieldName = 'BR_MRSA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  ImageIndex = 1
                  Value = 1
                end>
              Width = 28
              IsCaptionAssigned = True
            end
            object cxGrid2DBTableView1TIP: TcxGridDBColumn
              DataBinding.FieldName = 'TIP'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
    end
  end
  object PanelIzborPartner: TPanel
    Left = 128
    Top = 581
    Width = 642
    Height = 140
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    DesignSize = (
      642
      140)
    object cxGroupBoxIzberiPartner: TcxGroupBox
      Left = 0
      Top = 0
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      DesignSize = (
        642
        137)
      Height = 137
      Width = 642
      object lbl1: TLabel
        Left = 39
        Top = 60
        Width = 112
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076#1075#1086#1074#1086#1088#1077#1085' '#1083#1077#1082#1072#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl2: TLabel
        Left = 88
        Top = 33
        Width = 62
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1072#1088#1090#1085#1077#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object I_PARTNER_NAZIV: TcxExtLookupComboBox
        Left = 240
        Top = 30
        Hint = #1060#1080#1088#1084#1072' '#1076#1086' '#1082#1086#1112#1072' '#1089#1077' '#1080#1089#1087#1088#1072#1116#1072' '#1080#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077#1090#1086' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight]
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        Properties.OnCloseUp = I_PARTNER_NAZIVPropertiesCloseUp
        Properties.OnEditValueChanged = I_PARTNER_NAZIVPropertiesEditValueChanged
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 382
      end
      object btnaOK_PIzvestuvanjeBacilonositelstvoPrekin: TcxButton
        Left = 248
        Top = 97
        Width = 274
        Height = 25
        Action = actOK_PIzvestuvanjeBacilonositelstvoPrekin
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 4
      end
      object btnOtkazi: TcxButton
        Left = 528
        Top = 97
        Width = 90
        Height = 25
        Action = aOtkazi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 6
      end
      object I_TIP_PARTNER: TcxTextEdit
        Left = 156
        Top = 30
        TabOrder = 0
        Text = 'I_TIP_PARTNER'
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 33
      end
      object I_PARTNER: TcxTextEdit
        Left = 188
        Top = 30
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 53
      end
      object btnaOK_PIzvestuvanjeBacilonositelstvo: TcxButton
        Left = 24
        Top = 97
        Width = 217
        Height = 25
        Action = aOK_PIzvestuvanjeBacilonositelstvo
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 5
      end
      object LekarNaziv: TcxLookupComboBox
        Left = 157
        Top = 57
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsLekari
        TabOrder = 3
        Width = 465
      end
      object btn_MailBacilonositelstvo: TcxButton
        Left = 104
        Top = 96
        Width = 178
        Height = 25
        Action = actMailZaBacilonositelstvo
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 7
      end
      object btn_MailBacilonositelstvoPrekin: TcxButton
        Left = 299
        Top = 96
        Width = 223
        Height = 25
        Action = actMailZaPrekinNaBacilonositelstvo
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 8
      end
    end
  end
  object PanelIzborDatum: TPanel
    Left = 337
    Top = 525
    Width = 313
    Height = 129
    TabOrder = 5
    Visible = False
    DesignSize = (
      313
      129)
    object cxGroupBoxIzborDatum: TcxGroupBox
      Left = 20
      Top = 16
      Caption = #1048#1079#1073#1077#1088#1080' '#1076#1072#1090#1091#1084
      TabOrder = 0
      Height = 57
      Width = 277
      object Label15: TLabel
        Left = 3
        Top = 27
        Width = 34
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 139
        Top = 27
        Width = 34
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object fiskalOd: TcxDateEdit
        Left = 43
        Top = 24
        TabOrder = 0
        Width = 81
      end
      object fiskalDo: TcxDateEdit
        Left = 179
        Top = 24
        TabOrder = 1
        Width = 81
      end
    end
    object cxButton3: TcxButton
      Left = 96
      Top = 88
      Width = 93
      Height = 25
      Action = aOtkaziPeriodicen
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
    end
    object cxButton4: TcxButton
      Left = 205
      Top = 87
      Width = 93
      Height = 25
      Action = aPecatiPeriodicen
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 1104
    Top = 448
  end
  object PopupMenu1: TPopupMenu
    Left = 1120
    Top = 256
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 896
    Top = 120
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1080#1077#1084
      CaptionButtons = <>
      DockedLeft = 263
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 988
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton37'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1141
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1055#1088#1080#1077#1084#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1055#1088#1080#1077#1084#1080
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1045
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 89
          Visible = True
          ItemName = 'cbBarGodina'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 94
          Visible = True
          ItemName = 'cxBarDatum'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'RadioStatus'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedLeft = 415
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton33'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedLeft = 921
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = ' '
      CaptionButtons = <>
      DockedLeft = 879
      DockedTop = 0
      FloatLeft = 1266
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton35'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton34'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar10: TdxBar
      Caption = '    '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1417
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton36'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar11: TdxBar
      Caption = #1060#1080#1089#1082#1072#1083#1077#1085' '#1087#1088#1080#1085#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 653
      DockedTop = 0
      FloatLeft = 1409
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton39'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton40'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar12: TdxBar
      Caption = '        '
      CaptionButtons = <>
      DockedLeft = 135
      DockedTop = 0
      FloatLeft = 1214
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton43'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017')
      Properties.OnChange = cxBarEditItem2PropertiesChange
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object cbBarGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbBarGodinaChange
      Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      ItemIndex = -1
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aDodadiUsluga
      Category = 0
      ScreenTip = tipDodadiUloga
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiUsluga
      Category = 0
      ScreenTip = tipBrisiUloga
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 65
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end>
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Category = 0
      Visible = ivAlways
      ImageIndex = 9
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aSpustiSoberi2
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aZacuvajExcel2
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiTabela2
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPodesuvanjePecatenje2
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aPageSetup2
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aSnimiPecatenje2
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje2
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSnimiIzgled2
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisiIzgled2
      Category = 0
    end
    object cxBarDatum: TcxBarEditItem
      Caption = #1044#1072#1090#1091#1084
      Category = 0
      Hint = #1044#1072#1090#1091#1084
      Visible = ivAlways
      OnChange = cxBarDatumChange
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.AssignedValues.DisplayFormat = True
      Properties.ImmediatePost = True
      Properties.InputKind = ikRegExpr
      Properties.SaveTime = False
      Properties.ShowTime = False
      InternalEditValue = 0d
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1091#1089#1083#1091#1075#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Caption = #1057#1048#1058#1045
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 15
    end
    object dxBarButton6: TdxBarButton
      Caption = #1047#1040#1042#1056#1064#1045#1053#1048
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
    end
    object dxBarButton7: TdxBarButton
      Caption = #1053#1045#1047#1040#1042#1056#1064#1045#1053#1048
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
    end
    object RadioStatus: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.DefaultValue = 0
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1048#1058#1045
          Value = 0
        end
        item
          Caption = #1047#1040#1042#1056#1064#1045#1053#1048
          Value = 1
        end
        item
          Caption = #1053#1045#1047#1040#1042#1056#1064#1045#1053#1048
          Value = 2
        end>
      Properties.OnEditValueChanged = RadioStatusPropertiesEditValueChanged
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Action = aPregledajPecati
      Category = 0
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = aDodadiSanitarnaKniska
      Category = 0
    end
    object dxBarLargeButton34: TdxBarLargeButton
      Action = aUpatnica
      Category = 0
    end
    object dxBarLargeButton35: TdxBarLargeButton
      Action = aPotvrda
      Category = 0
    end
    object dxBarLargeButton36: TdxBarLargeButton
      Action = aPIzvestuvanjeBacilonositelstvo
      Category = 0
    end
    object dxBarLargeButton37: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton38: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = #1048#1079#1074#1077#1096#1090#1072#1080' '#1086#1076' '#1092#1080#1089#1082#1072#1083#1077#1085' '#1087#1088#1080#1085#1090#1077#1088
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      LargeImageIndex = 98
      Images = dmRes.cxSmallImages
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end>
    end
    object dxBarButton8: TdxBarButton
      Action = aKontrolenDnevenPregled
      Category = 0
    end
    object dxBarButton9: TdxBarButton
      Action = aDnevenPregledSoNuliranje
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = aSkratenPeriodicenIzvestaj
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = aDetalenPeriodicenIzvestaj
      Category = 0
    end
    object dxBarLargeButton39: TdxBarLargeButton
      Action = aPecatiFiskalnaSmetka
      Category = 0
    end
    object dxBarLargeButton40: TdxBarLargeButton
      Action = aStornirajFiskalnaSmetka
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = aDetalenPeriodicen
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = aSkratenPeriodicen
      Category = 0
    end
    object dxBarLargeButton41: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object aPratiRezMailPacient: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 45
      SyncImageIndex = False
      ImageIndex = -1
    end
    object dxBarLargeButton42: TdxBarLargeButton
      Action = actPratiRezMailPartner
      Category = 0
    end
    object dxBarLargeButton43: TdxBarLargeButton
      Action = actPratiRezMailPartner
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 1008
    Top = 280
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrebarajPacient: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
    end
    object aIsprazniPacient: TAction
      Caption = #1048#1089#1087#1088#1072#1079#1085#1080
      ImageIndex = 23
    end
    object aDodadiUsluga: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      ShortCut = 16500
      OnExecute = aDodadiUslugaExecute
    end
    object aBrisiUsluga: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
      ShortCut = 16503
      OnExecute = aBrisiUslugaExecute
    end
    object aSpustiSoberi2: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072' '#1079#1072' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 65
      OnExecute = aSpustiSoberi2Execute
    end
    object aZacuvajExcel2: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcel2Execute
    end
    object aPecatiTabela2: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 30
      OnExecute = aPecatiTabela2Execute
    end
    object aPodesuvanjePecatenje2: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenje2Execute
    end
    object aPageSetup2: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetup2Execute
    end
    object aSnimiPecatenje2: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenje2Execute
    end
    object aBrisiPodesuvanjePecatenje2: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenje2Execute
    end
    object aSnimiIzgled2: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgled2Execute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aBrisiIzgled2: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgled2Execute
    end
    object aPregledajPecati: TAction
      Caption = #1056#1077#1079#1091#1083#1090#1072#1090#1080' '#1086#1076' '#1040#1085#1072#1083#1080#1079#1072
      ImageIndex = 82
      ShortCut = 121
      OnExecute = aPregledajPecatiExecute
    end
    object aDizajnRezultati: TAction
      Caption = #1056#1077#1079#1091#1083#1090#1072#1090#1080' '#1086#1076' '#1040#1085#1072#1083#1080#1079#1072
      ImageIndex = 19
      OnExecute = aDizajnRezultatiExecute
    end
    object aDodadiSanitarnaKniska: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1057#1072#1085#1080#1090#1072#1088#1085#1072' '#1082#1085#1080#1096#1082#1072', '#1095#1072#1096#1082#1072',...'
      ImageIndex = 70
      OnExecute = aDodadiSanitarnaKniskaExecute
    end
    object aUpatnica: TAction
      Caption = #1059#1087#1072#1090#1085#1080#1094#1072' '#1079#1072' '#1084#1080#1082#1088#1086#1073#1080#1086#1083#1086#1096#1082#1080' '#1087#1088#1077#1075#1083#1077#1076
      ImageIndex = 16
      OnExecute = aUpatnicaExecute
    end
    object aPotvrda: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072
      ImageIndex = 83
      OnExecute = aPotvrdaExecute
    end
    object aDizajnPoupUp: TAction
      Caption = 'aDizajnPoupUp'
      ShortCut = 24697
      OnExecute = aDizajnPoupUpExecute
    end
    object aDUpatnica: TAction
      Caption = #1059#1087#1072#1090#1085#1080#1094#1072' '#1079#1072' '#1084#1080#1082#1088#1086#1073#1080#1086#1083#1086#1096#1082#1080' '#1087#1088#1077#1075#1083#1077#1076
      OnExecute = aDUpatnicaExecute
    end
    object aDPotvrda: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072
      OnExecute = aDPotvrdaExecute
    end
    object aPIzvestuvanjeBacilonositelstvo: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      ImageIndex = 87
      OnExecute = aPIzvestuvanjeBacilonositelstvoExecute
    end
    object aDIzvestuvanjeBacilonositelstvo: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      OnExecute = aDIzvestuvanjeBacilonositelstvoExecute
    end
    object aKontrolenDnevenPregled: TAction
      Caption = #1050#1086#1085#1090#1088#1086#1083#1077#1085' '#1076#1085#1077#1074#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
      ImageIndex = 19
      OnExecute = aKontrolenDnevenPregledExecute
    end
    object aDnevenPregledSoNuliranje: TAction
      Caption = #1044#1085#1077#1074#1077#1085' '#1087#1088#1077#1075#1083#1077#1076' '#1089#1086' '#1085#1091#1083#1080#1088#1072#1114#1077
      ImageIndex = 19
      OnExecute = aDnevenPregledSoNuliranjeExecute
    end
    object aSkratenPeriodicenIzvestaj: TAction
      Caption = #1057#1082#1088#1072#1090#1077#1085' '#1087#1077#1088#1080#1086#1076#1080#1095#1077#1085' '#1080#1079#1074#1077#1096#1090#1072#1112
      ImageIndex = 19
      OnExecute = aSkratenPeriodicenIzvestajExecute
    end
    object aOK_PIzvestuvanjeBacilonositelstvo: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      Hint = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080' - '#1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      ImageIndex = 87
      OnExecute = aOK_PIzvestuvanjeBacilonositelstvoExecute
    end
    object aDetalenPeriodicenIzvestaj: TAction
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1077#1088#1080#1086#1076#1080#1095#1077#1085' '#1080#1079#1074#1077#1096#1090#1072#1112
      ImageIndex = 19
      OnExecute = aDetalenPeriodicenIzvestajExecute
    end
    object aPecatiFiskalnaSmetka: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1092#1080#1089#1082#1072#1083#1085#1072' '#1089#1084#1077#1090#1082#1072
      ImageIndex = 98
      OnExecute = aPecatiFiskalnaSmetkaExecute
    end
    object aStornirajFiskalnaSmetka: TAction
      Caption = #1057#1090#1086#1088#1085#1080#1088#1072#1112' '#1092#1080#1089#1082#1072#1083#1085#1072' '#1089#1084#1077#1090#1082#1072
      ImageIndex = 37
      OnExecute = aStornirajFiskalnaSmetkaExecute
    end
    object aSkratenPeriodicen: TAction
      Caption = #1057#1082#1088#1072#1090#1077#1085' '#1087#1077#1088#1080#1086#1076#1080#1095#1077#1085' '#1080#1079#1074#1077#1096#1090#1072#1112
      ImageIndex = 19
      OnExecute = aSkratenPeriodicenExecute
    end
    object aDetalenPeriodicen: TAction
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1077#1088#1080#1086#1076#1080#1095#1077#1085' '#1080#1079#1074#1077#1096#1090#1072#1112
      ImageIndex = 19
      OnExecute = aDetalenPeriodicenExecute
    end
    object aPecatiPeriodicen: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 98
      OnExecute = aPecatiPeriodicenExecute
    end
    object aOtkaziPeriodicen: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 37
      OnExecute = aOtkaziPeriodicenExecute
    end
    object actFind: TAction
      Caption = 'actFind'
      ShortCut = 16454
      OnExecute = actFindExecute
    end
    object actOK_PIzvestuvanjeBacilonositelstvoPrekin: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      ImageIndex = 19
      OnExecute = actOK_PIzvestuvanjeBacilonositelstvoPrekinExecute
    end
    object actDIzvestuvanjeBacilonositelstvoPrekin: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      OnExecute = actDIzvestuvanjeBacilonositelstvoPrekinExecute
    end
    object actPratiRezMailPartner: TAction
      Caption = #1055#1088#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090' '#1087#1086' eMail '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 45
      OnExecute = actPratiRezMailPartnerExecute
    end
    object actMailZaBacilonositelstvo: TAction
      Caption = 'eMail '#1079#1072' '#1041#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      ImageIndex = 45
      OnExecute = actMailZaBacilonositelstvoExecute
    end
    object actMailZaPrekinNaBacilonositelstvo: TAction
      Caption = 'eMail '#1079#1072' '#1055#1088#1077#1082#1080#1085' '#1085#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      ImageIndex = 45
      OnExecute = actMailZaPrekinNaBacilonositelstvoExecute
    end
    object actPratiMail: TAction
      Caption = 'actPratiMail'
      OnExecute = actPratiMailExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 1072
    Top = 336
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 45593.438724618060000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 1048
    Top = 312
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipDodadiUloga: TdxScreenTip
      Header.Text = #1044#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1091#1089#1083#1091#1075#1072' '#1074#1086' '#1087#1088#1080#1077#1084
      Description.Text = 
        #1048#1079#1073#1086#1088' '#1085#1072' '#1091#1089#1083#1091#1075#1072' '#1086#1076' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1059#1089#1083#1091#1075#1080' '#1080' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1074#1086' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080 +
        #1086#1090' '#1087#1088#1080#1077#1084
    end
    object tipBrisiUloga: TdxScreenTip
      Header.Text = #1041#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      Description.Text = #1041#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1091#1089#1083#1091#1075#1072' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1087#1088#1080#1077#1084
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 1264
    Top = 256
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 1048
    Top = 136
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 952
    Top = 264
  end
  object PopupMenu2: TPopupMenu
    Left = 176
    Top = 312
    object aDizajnRezultati1: TMenuItem
      Action = aDizajnRezultati
    end
    object N2: TMenuItem
      Action = aDPotvrda
    end
    object N3: TMenuItem
      Action = aDUpatnica
    end
    object N4: TMenuItem
      Action = aDIzvestuvanjeBacilonositelstvo
    end
    object N5: TMenuItem
      Action = actDIzvestuvanjeBacilonositelstvoPrekin
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 1128
    Top = 296
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsPacienti
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView1EMBG: TcxGridDBColumn
        DataBinding.FieldName = 'EMBG'
        Width = 85
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 343
      end
    end
    object cxGridViewRepository1DBTableView2: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsAktivniPartneri
      DataController.KeyFieldNames = 'PAR_TIP;PAR_SIF'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView2DBColumn: TcxGridDBColumn
        DataBinding.FieldName = #1058#1080#1087
        Width = 38
      end
      object cxGridViewRepository1DBTableView2DBColumn1: TcxGridDBColumn
        DataBinding.FieldName = #1064#1080#1092#1088#1072
        Width = 52
      end
      object cxGridViewRepository1DBTableView2DBColumn2: TcxGridDBColumn
        DataBinding.FieldName = #1053#1072#1079#1080#1074
        Width = 241
      end
      object cxGridViewRepository1DBTableView2DBColumn3: TcxGridDBColumn
        DataBinding.FieldName = #1052#1077#1089#1090#1086
        Width = 100
      end
      object cxGridViewRepository1DBTableView2DBColumn4: TcxGridDBColumn
        DataBinding.FieldName = #1040#1076#1088#1077#1089#1072
        Width = 135
      end
    end
    object cxGridViewRepository1DBTableView3: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.KeyFieldNames = 'PAR_TIP;PAR_SIF'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView3DBColumn: TcxGridDBColumn
        DataBinding.FieldName = #1058#1080#1087
        Width = 46
      end
      object cxGridViewRepository1DBTableView3DBColumn1: TcxGridDBColumn
        DataBinding.FieldName = #1064#1080#1092#1088#1072
        Width = 47
      end
      object cxGridViewRepository1DBTableView3DBColumn2: TcxGridDBColumn
        DataBinding.FieldName = #1053#1072#1079#1080#1074
        Width = 201
      end
      object cxGridViewRepository1DBTableView3DBColumn3: TcxGridDBColumn
        DataBinding.FieldName = #1052#1077#1089#1090#1086
        Width = 89
      end
      object cxGridViewRepository1DBTableView3DBColumn4: TcxGridDBColumn
        DataBinding.FieldName = #1040#1076#1088#1077#1089#1072
        Width = 89
      end
    end
  end
end
