unit sifAntibiogram;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMemo, cxBlobEdit;

type
  TfrmAntibiogramSifrarnik = class(TfrmMaster)
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1LEGENDA: TcxGridDBColumn;
    OPIS: TcxDBMemo;
    Label3: TLabel;
    Label4: TLabel;
    LEGENDA: TcxDBTextEdit;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aDefAntibiogram: TAction;
    tipDefAntibiogram: TdxScreenTip;
    procedure aNovExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure prefrli;
    procedure aDefAntibiogramExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAntibiogramSifrarnik: TfrmAntibiogramSifrarnik;

implementation

{$R *.dfm}

uses dmUnit, sifDefAntibiogram;

procedure TfrmAntibiogramSifrarnik.aDefAntibiogramExecute(Sender: TObject);
begin
  inherited;
   frmSifDefAntibiogram:=TfrmSifDefAntibiogram.Create(self,true);
   frmSifDefAntibiogram.ShowModal();
   frmSifDefAntibiogram.Free;
end;

procedure TfrmAntibiogramSifrarnik.aNovExecute(Sender: TObject);
begin
  inherited;
  dm.qMaxAntibiogram.Close;
  dm.qMaxAntibiogram.ExecQuery;
  dm.tblAntibiogramID.Value:=dm.qMaxAntibiogram.FldByName['id'].Value;
  NAZIV.SetFocus;
end;

procedure TfrmAntibiogramSifrarnik.prefrli;
begin
    inherited;
    if(not inserting) then
      begin
         ModalResult := mrOk;
         SetSifra(0,IntToStr(dm.tblAntibiogramID.Value));
      end;
end;


procedure TfrmAntibiogramSifrarnik.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

procedure TfrmAntibiogramSifrarnik.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmAntibiogramSifrarnik.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblAntibiogram.Open;
end;

end.
