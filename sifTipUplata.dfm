inherited frmTipUplata: TfrmTipUplata
  Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
  ClientHeight = 507
  ExplicitWidth = 749
  ExplicitHeight = 545
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 228
    ExplicitHeight = 228
    inherited cxGrid1: TcxGrid
      Height = 224
      ExplicitHeight = 224
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsTipPlakanje
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 84
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 445
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 354
    Height = 130
    ExplicitTop = 354
    ExplicitHeight = 130
    inherited Label1: TLabel
      Left = 17
      Top = 22
      ExplicitLeft = 17
      ExplicitTop = 22
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 69
      Top = 19
      Hint = #1058#1080#1087' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTipPlakanje
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 69
      ExplicitTop = 19
    end
    inherited OtkaziButton: TcxButton
      Top = 90
      TabOrder = 3
      ExplicitTop = 90
    end
    inherited ZapisiButton: TcxButton
      Top = 90
      TabOrder = 2
      ExplicitTop = 90
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 46
      Hint = #1058#1080#1087' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1085#1072#1079#1080#1074
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipPlakanje
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 468
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 484
    ExplicitTop = 484
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41333.627836053240000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
