program SanitarniPregledi;

uses
  Vcl.Forms,
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  Master in '..\Share2010\Master.pas' {frmMaster},
  Login in '..\Share2010\Login.pas' {frmLogin},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  Utils in '..\Share2010\Utils.pas',
  Main in 'Main.pas' {frmMain},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  sifUslugi in 'sifUslugi.pas' {frmUslugi},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  MK in '..\Share2010\MK.pas' {frmMK},
  sifPaketi in 'sifPaketi.pas' {frmPaketi},
  sifPaketiUslugi in 'sifPaketiUslugi.pas' {frmPaketiUslugi},
  sifRabotnoMesto in 'sifRabotnoMesto.pas' {frmRabotnoMesto},
  Partner in '..\Share2010\Partner.pas' {frmPartner},
  Pacienti in 'Pacienti.pas' {frmPacienti},
  PreglediMikro in 'PreglediMikro.pas' {frmPreglediMikro},
  sifTipUplata in 'sifTipUplata.pas' {frmTipUplata},
  sifMikroorganizmi in 'sifMikroorganizmi.pas' {frmMikroorganizmi},
  sifMikroorganizmiUslugi in 'sifMikroorganizmiUslugi.pas' {frmMikroorganizmiUslugi},
  Priem in 'Priem.pas' {frmPriem},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  sifKategorijaRM in 'sifKategorijaRM.pas' {frmKategorijaRM},
  sifLekovi in 'sifLekovi.pas' {frmLekovi},
  Analizi in 'Analizi.pas' {frmAnaliza},
  Rezultat in 'Rezultat.pas' {frmRezultat},
  sifAntibiogram in 'sifAntibiogram.pas' {frmAntibiogramSifrarnik},
  sifDefAntibiogram in 'sifDefAntibiogram.pas' {frmSifDefAntibiogram},
  sifDefNaodi in 'sifDefNaodi.pas' {frmDefNaodi},
  sifLekari in 'sifLekari.pas' {frmLekari},
  RabotniEdinici in '..\Share2010\RabotniEdinici.pas' {frmRE},
  sifPopust in 'sifPopust.pas' {frmPopust},
  Fakturiranje in 'Fakturiranje.pas' {frmFakturiranje},
  GrupnoPecatenjeFakturi in 'GrupnoPecatenjeFakturi.pas' {frmGrupnoPecatenjeFakturi},
  PominatRok in 'PominatRok.pas' {frmPominatRok},
  IzvestaiGrupni in 'IzvestaiGrupni.pas' {frmGrupniIzvestai},
  ListaPrimeniPacienti in 'ListaPrimeniPacienti.pas' {frmListaPrimeniPacienti},
  PratiIZvestuvanjePoMail in 'PratiIZvestuvanjePoMail.pas' {frmPratiIzvestuvanjePoMail},
  DogovoriCeni in 'DogovoriCeni.pas' {frmDogovoriCeni},
  NurkoRepository in '..\Share2010\NurkoRepository.pas' {frmNurkoRepository},
  cxFiscalInterface in 'cxFiscalInterface.pas',
  StringUtils in '..\Share2010\StringUtils.pas',
  Firma in '..\Share2010\Firma.pas' {frmFirma},
  FirmaInit in '..\Share2010\FirmaInit.pas' {frmFirmaInit},
  FormConfig in '..\Share2010\FormConfig.pas' {frmFormConfig},
  Pateka in '..\Share2010\Pateka.pas' {frmPateka},
  Error in '..\Share2010\Error.pas' {frmError},
  AboutBox in '..\Share2010\AboutBox.pas' {frmAboutBox},
  ZabeleskaKontakt in '..\Share2010\ZabeleskaKontakt.pas' {frmZabeleskaKontakt},
  Notepad in '..\Share2010\Notepad.pas' {frmNotepad},
  Aplikacii in '..\Share2010\Aplikacii.pas' {frmAplikacii},
  cxService1 in '..\Share2010\cxService1.pas',
  wsse in '..\Share2010\wsse.pas',
  PromeniLozinka in '..\Share2010\PromeniLozinka.pas' {frmPromeniLozinka},
  TipPartner in '..\Share2010\TipPartner.pas' {frmTipPartner},
  Drzava in '..\Share2010\Drzava.pas' {frmDrzava},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  Opstina in '..\Share2010\Opstina.pas' {frmOpstina},
  LocationMap in '..\Share2010\LocationMap.pas' {frmLocationMap},
  NurkoSelect in '..\Share2010\NurkoSelect.pas' {frmNurkoSelect},
  EmailSetup in '..\Share2010\EmailSetup.pas' {frmEmailSetup},
  LogThread in '..\Share2010\LogThread.pas',
  DCPbase64 in 'C:\VCL\dcpcrypt\Source\DCPbase64.pas',
  DCPblockciphers in 'C:\VCL\dcpcrypt\Source\DCPblockciphers.pas',
  DCPconst in 'C:\VCL\dcpcrypt\Source\DCPconst.pas',
  DCPcrypt2 in 'C:\VCL\dcpcrypt\Source\DCPcrypt2.pas',
  DCPreg in 'C:\VCL\dcpcrypt\Source\DCPreg.pas',
  DCPtypes in 'C:\VCL\dcpcrypt\Source\DCPtypes.pas',
  DCPsha1 in 'C:\VCL\dcpcrypt\Source\Hashes\DCPsha1.pas',
  LoginMZ in 'LoginMZ.pas' {frmLoginMZ};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := 'Санитарни прегледи';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='SAN';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.



