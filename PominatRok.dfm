object frmPominatRok: TfrmPominatRok
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 
    #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080' '#1079#1072' '#1082#1086#1112' '#1077' '#1087#1086#1084#1080#1085#1072#1090' '#1076#1072#1090#1091#1084#1086#1090' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1079#1072' '#1087#1086#1074#1090#1086#1088#1077#1085 +
    ' '#1087#1088#1077#1075#1083#1077#1076
  ClientHeight = 622
  ClientWidth = 964
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 964
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 599
    Width = 964
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 177
    Width = 964
    Height = 422
    Align = alClient
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
      DataController.DataSource = dm.dsPominatRok
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          Column = cxGrid1DBTableView1BROJ
        end
        item
          Format = '0.00,.'
          Kind = skSum
          Column = cxGrid1DBTableView1CENA
        end>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
      OptionsView.Footer = True
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'PRIEM'
        Visible = False
        Width = 60
      end
      object cxGrid1DBTableView1GODINA: TcxGridDBColumn
        DataBinding.FieldName = 'GODINA'
        Width = 59
      end
      object cxGrid1DBTableView1BROJ: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ'
        Width = 96
      end
      object cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_PRIEM'
        Width = 91
      end
      object cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_ZAVERKA'
        Width = 125
      end
      object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_VAZENJE'
        Width = 105
      end
      object cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn
        DataBinding.FieldName = 'PACIENT_ID'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1EMBG: TcxGridDBColumn
        DataBinding.FieldName = 'EMBG'
        Width = 100
      end
      object cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PACIENT_NAZIV'
        Width = 100
      end
      object cxGrid1DBTableView1TP: TcxGridDBColumn
        DataBinding.FieldName = 'TP'
        Width = 100
      end
      object cxGrid1DBTableView1P: TcxGridDBColumn
        DataBinding.FieldName = 'P'
        Width = 100
      end
      object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER_NAZIV'
        Width = 153
      end
      object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
        DataBinding.FieldName = 'PAKET_ID'
        Width = 161
      end
      object cxGrid1DBTableView1CENA: TcxGridDBColumn
        DataBinding.FieldName = 'CENA'
        Width = 71
      end
      object cxGrid1DBTableView1PLACANJE: TcxGridDBColumn
        DataBinding.FieldName = 'PLACANJE'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn
        DataBinding.FieldName = 'NACINPLAKANJE'
        Width = 100
      end
      object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
        DataBinding.FieldName = 'RABOTNO_MESTO'
        Width = 134
      end
      object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'RABOTNOMESTONAZIV'
        Width = 100
      end
      object cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn
        DataBinding.FieldName = 'KATEGORIJA_RM'
        Visible = False
        Width = 146
      end
      object cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'KATEGORIJANAZIV'
        Width = 100
      end
      object cxGrid1DBTableView1U_LISTA: TcxGridDBColumn
        DataBinding.FieldName = 'U_LISTA'
        Width = 100
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 964
    Height = 51
    Align = alTop
    TabOrder = 3
    DesignSize = (
      964
      51)
    object Label1: TLabel
      Left = 7
      Top = 20
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1072#1088#1090#1085#1077#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PrikaziNivaButton: TcxButton
      Left = 740
      Top = 14
      Width = 89
      Height = 25
      Action = aPrebaraj
      Anchors = [akTop, akRight]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
      OnKeyDown = EnterKakoTab
    end
    object TIP_PARTNER: TcxTextEdit
      Tag = 1
      Left = 115
      Top = 16
      Hint = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      BeepOnEnter = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object PARTNER: TcxTextEdit
      Tag = 1
      Left = 158
      Top = 16
      Hint = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object PARTNER_NAZIV: TcxExtLookupComboBox
      Left = 201
      Top = 16
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      Anchors = [akLeft, akTop, akRight]
      BeepOnEnter = False
      Properties.DropDownSizeable = True
      Properties.View = cxGridViewRepository1DBTableView1
      Properties.KeyFieldNames = 'TP;P'
      Properties.ListFieldItem = cxGridViewRepository1DBTableView1PARTNERNAZIV
      Properties.OnEditValueChanged = PARTNER_NAZIVPropertiesEditValueChanged
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 524
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 437
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 682
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 922
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 90
          Visible = True
          ItemName = 'cbDatum'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 142
      DockedTop = 0
      FloatLeft = 998
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = 'E-MAIL'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 437
      DockedTop = 0
      FloatLeft = 998
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbDatum: TcxBarEditItem
      Caption = #1044#1072#1090#1091#1084
      Category = 0
      Hint = #1044#1072#1090#1091#1084
      Visible = ivAlways
      OnChange = cbDatumChange
      PropertiesClassName = 'TcxDateEditProperties'
      Properties.SaveTime = False
      Properties.ShowTime = False
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPIstecenRok
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPPredIstecenRok
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPratiIzvestuvanjePoEmail
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 440
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrebaraj: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
      OnExecute = aPrebarajExecute
    end
    object aPIstecenRok: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1087#1086#1084#1080#1085#1072#1090' '#1088#1086#1082' '#1085#1072' '#1074#1072#1078#1114#1077
      ImageIndex = 19
      OnExecute = aPIstecenRokExecute
    end
    object aPPredIstecenRok: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1055#1056#1045#1044' '#1076#1072' '#1087#1086#1084#1080#1077' '#1088#1086#1082#1086#1090' '#1085#1072' '#1074#1072#1078#1114#1077
      ImageIndex = 19
      OnExecute = aPPredIstecenRokExecute
    end
    object aDIstecenRok: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1087#1086#1084#1080#1085#1072#1090' '#1088#1086#1082' '#1085#1072' '#1074#1072#1078#1114#1077
      ImageIndex = 19
      OnExecute = aDIstecenRokExecute
    end
    object aDPredIstecenRok: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1055#1056#1045#1044' '#1076#1072' '#1087#1086#1084#1080#1077' '#1088#1086#1082#1086#1090' '#1085#1072' '#1074#1072#1078#1114#1077
      ImageIndex = 19
      OnExecute = aDPredIstecenRokExecute
    end
    object aPoupUp: TAction
      Caption = 'aPoupUp'
      ShortCut = 24697
      OnExecute = aPoupUpExecute
    end
    object aPratiIzvestuvanjePoEmail: TAction
      Caption = #1055#1088#1072#1090#1080' '#1080#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1087#1086' Email'
      ImageIndex = 45
      OnExecute = aPratiIzvestuvanjePoEmailExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 480
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 520
    Top = 280
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 624
    Top = 304
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsPartneriOdPriem
      DataController.KeyFieldNames = 'TP;P'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView1TP: TcxGridDBColumn
        DataBinding.FieldName = 'TP'
        Width = 45
      end
      object cxGridViewRepository1DBTableView1P: TcxGridDBColumn
        DataBinding.FieldName = 'P'
        Width = 53
      end
      object cxGridViewRepository1DBTableView1PARTNERNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNERNAZIV'
        Width = 586
      end
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 712
    Top = 440
    object N2: TMenuItem
      Action = aDPredIstecenRok
    end
    object N3: TMenuItem
      Action = aDIstecenRok
    end
  end
end
