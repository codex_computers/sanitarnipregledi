inherited frmPopust: TfrmPopust
  Caption = #1055#1086#1087#1091#1089#1090
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 290
    ExplicitHeight = 290
    inherited cxGrid1: TcxGrid
      Height = 286
      ExplicitHeight = 286
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsPopust
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 48
        end
        object cxGrid1DBTableView1TP: TcxGridDBColumn
          DataBinding.FieldName = 'TP'
          Visible = False
          Width = 83
        end
        object cxGrid1DBTableView1P: TcxGridDBColumn
          DataBinding.FieldName = 'P'
          Width = 97
        end
        object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNERNAZIV'
          Width = 487
        end
        object cxGrid1DBTableView1POPUST: TcxGridDBColumn
          DataBinding.FieldName = 'POPUST'
          Width = 86
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Height = 114
    ExplicitTop = 416
    ExplicitHeight = 114
    inherited Label1: TLabel
      Left = 590
      Top = 23
      Visible = False
      ExplicitLeft = 590
      ExplicitTop = 23
    end
    object Label2: TLabel [1]
      Left = 41
      Top = 47
      Width = 48
      Height = 13
      Caption = #1055#1086#1087#1091#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel [2]
      Left = -1
      Top = 20
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1072#1088#1090#1085#1077#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 181
      Top = 47
      Width = 13
      Height = 13
      Caption = '%'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 642
      Top = 20
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPopust
      TabOrder = 2
      Visible = False
      ExplicitLeft = 642
      ExplicitTop = 20
    end
    inherited OtkaziButton: TcxButton
      Top = 74
      TabOrder = 6
      ExplicitTop = 74
    end
    inherited ZapisiButton: TcxButton
      Top = 74
      TabOrder = 5
      ExplicitTop = 74
    end
    object POPUST: TcxDBTextEdit
      Tag = 1
      Left = 95
      Top = 44
      Hint = #1055#1086#1087#1091#1089#1090' '#1080#1079#1088#1072#1079#1077#1085' '#1074#1086' '#1087#1088#1086#1094#1077#1085#1090#1080
      BeepOnEnter = False
      DataBinding.DataField = 'POPUST'
      DataBinding.DataSource = dm.dsPopust
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object TIP_PARTNER: TcxDBTextEdit
      Tag = 1
      Left = 95
      Top = 17
      Hint = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      BeepOnEnter = False
      DataBinding.DataField = 'TP'
      DataBinding.DataSource = dm.dsPopust
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 40
    end
    object PARTNER: TcxDBTextEdit
      Tag = 1
      Left = 134
      Top = 17
      Hint = #1055#1072#1088#1090#1085#1077#1088'  - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'P'
      DataBinding.DataSource = dm.dsPopust
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 40
    end
    object PARTNERNAZIV: TcxExtLookupComboBox
      Left = 173
      Top = 17
      Hint = #1055#1072#1088#1090#1085#1077#1088'  - '#1085#1072#1079#1080#1074
      Anchors = [akLeft, akTop, akRight]
      Properties.DropDownSizeable = True
      Properties.OnEditValueChanged = PARTNERNAZIVPropertiesEditValueChanged
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 364
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 264
    Top = 304
  end
  inherited PopupMenu1: TPopupMenu
    Top = 264
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 288
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41367.630402928240000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
