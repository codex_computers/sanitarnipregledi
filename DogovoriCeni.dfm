inherited frmDogovoriCeni: TfrmDogovoriCeni
  Caption = #1044#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1094#1077#1085#1080' '#1087#1086' '#1087#1072#1082#1077#1090
  ClientHeight = 671
  ClientWidth = 989
  ExplicitWidth = 1005
  ExplicitHeight = 710
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Top = 195
    Width = 989
    Height = 229
    ExplicitTop = 195
    ExplicitWidth = 989
    ExplicitHeight = 229
    inherited cxGrid1: TcxGrid
      Width = 985
      Height = 225
      ExplicitWidth = 985
      ExplicitHeight = 225
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmManual
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsDogvoriCeni
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 44
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
          Width = 79
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Width = 94
        end
        object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER_NAZIV'
          Width = 322
        end
        object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
          DataBinding.FieldName = 'PAKET_ID'
          Visible = False
          Width = 81
        end
        object cxGrid1DBTableView1PAKET_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PAKET_NAZIV'
          Width = 172
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 78
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 86
        end
        object cxGrid1DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Width = 91
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 424
    Width = 989
    Height = 224
    ExplicitTop = 424
    ExplicitWidth = 989
    ExplicitHeight = 224
    inherited Label1: TLabel
      Left = 839
      Top = 9
      Visible = False
      ExplicitLeft = 839
      ExplicitTop = 9
    end
    object lbl2: TLabel [1]
      Left = 41
      Top = 48
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1072#1082#1077#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl3: TLabel [2]
      Left = 21
      Top = 75
      Width = 70
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl4: TLabel [3]
      Left = 21
      Top = 102
      Width = 70
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl5: TLabel [4]
      Left = 41
      Top = 129
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1062#1077#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl6: TLabel [5]
      Left = 41
      Top = 153
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl7: TLabel [6]
      Left = 1
      Top = 21
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1072#1088#1090#1085#1077#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 891
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDogvoriCeni
      TabOrder = 2
      Visible = False
      ExplicitLeft = 891
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 898
      Top = 184
      TabOrder = 8
      ExplicitLeft = 898
      ExplicitTop = 184
    end
    inherited ZapisiButton: TcxButton
      Left = 817
      Top = 184
      TabOrder = 7
      ExplicitLeft = 817
      ExplicitTop = 184
    end
    object PAKET_ID: TcxDBTextEdit
      Tag = 1
      Left = 97
      Top = 45
      Hint = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'PAKET_ID'
      DataBinding.DataSource = dm.dsDogvoriCeni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBackground
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object PAKET_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 148
      Top = 45
      Hint = #1059#1089#1083#1091#1075#1072' - '#1085#1072#1079#1080#1074
      BeepOnEnter = False
      DataBinding.DataField = 'PAKET_ID'
      DataBinding.DataSource = dm.dsDogvoriCeni
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end
        item
          Width = 200
          FieldName = 'CENA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsPaketi
      StyleDisabled.TextColor = clBackground
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 369
    end
    object DATUM_OD: TcxDBDateEdit
      Tag = 1
      Left = 97
      Top = 72
      DataBinding.DataField = 'DATUM_OD'
      DataBinding.DataSource = dm.dsDogvoriCeni
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object DATUM_DO: TcxDBDateEdit
      Tag = 1
      Left = 97
      Top = 99
      DataBinding.DataField = 'DATUM_DO'
      DataBinding.DataSource = dm.dsDogvoriCeni
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object CENA: TcxDBTextEdit
      Tag = 1
      Left = 97
      Top = 126
      BeepOnEnter = False
      DataBinding.DataField = 'CENA'
      DataBinding.DataSource = dm.dsDogvoriCeni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBackground
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object OPIS: TcxDBMemo
      Left = 97
      Top = 153
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsDogvoriCeni
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 40
      Width = 420
    end
    object PARTNER_NAZIV: TcxDBTextEdit
      Left = 135
      Top = 18
      DataBinding.DataField = 'PARTNER_NAZIV'
      DataBinding.DataSource = dm.dsDogvoriCeni
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 9
      Width = 382
    end
    object PARTNER: TcxDBTextEdit
      Left = 97
      Top = 18
      DataBinding.DataField = 'PARTNER'
      DataBinding.DataSource = dm.dsDogvoriCeni
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 10
      Width = 38
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 989
    ExplicitWidth = 989
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 648
    Width = 989
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', Ctrl+F - '#1055#1088#1077#1073#1072#1088 +
          #1072#1112', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 648
    ExplicitWidth = 989
  end
  object pnl1: TPanel [4]
    Left = 0
    Top = 126
    Width = 989
    Height = 69
    Align = alTop
    Caption = 'pnl1'
    TabOrder = 4
    object cxGroupBox1: TcxGroupBox
      Left = 18
      Top = 6
      Caption = #1048#1079#1073#1077#1088#1080' '#1092#1080#1088#1084#1072
      ParentColor = False
      TabOrder = 0
      DesignSize = (
        703
        57)
      Height = 57
      Width = 703
      object lbl1: TLabel
        Left = -17
        Top = 25
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1072#1088#1090#1085#1077#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PN: TcxExtLookupComboBox
        Left = 160
        Top = 22
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight]
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 528
      end
      object P: TcxTextEdit
        Left = 111
        Top = 22
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1096#1080#1092#1088#1072
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object T: TcxTextEdit
        Left = 79
        Top = 22
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1090#1080#1087
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 32
      end
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 872
  end
  inherited PopupMenu1: TPopupMenu
    Left = 376
    Top = 312
  end
  inherited dxBarManager1: TdxBarManager
    Left = 504
    Top = 288
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 116
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 299
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 619
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1015
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'Status'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object Status: TcxBarEditItem
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1080
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.DefaultValue = 1
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1040#1050#1058#1048#1042#1053#1048
          Value = 1
        end
        item
          Caption = #1057#1048#1058#1045
          Value = 0
        end>
      Properties.OnChange = cxBarEditItem2PropertiesChange
    end
  end
  inherited ActionList1: TActionList
    Left = 888
    Top = 136
    object actFind: TAction
      Caption = 'actFind'
      ShortCut = 16454
      OnExecute = actFindExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41627.508854907410000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
