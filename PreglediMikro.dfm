object frmPreglediMikro: TfrmPreglediMikro
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1040#1085#1072#1083#1080#1079#1080' '#1080' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 710
  ClientWidth = 1143
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1143
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 687
    Width = 1143
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Enter - '#1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1040#1085#1072#1083#1080#1079#1072', F10 - '#1055#1077#1095#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090' '#1086#1076' '#1040#1085#1072#1083#1080#1079#1072',' +
          ' Ctrl+F - '#1055#1088#1077#1073#1072#1088#1072#1112
        Width = 340
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1143
    Height = 314
    Align = alClient
    TabOrder = 2
    object cxPageControlMaster: TcxPageControl
      Left = 1
      Top = 1
      Width = 1141
      Height = 312
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelarenM
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 312
      ClientRectRight = 1141
      ClientRectTop = 24
      object cxTabSheetTabelarenM: TcxTabSheet
        Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1077#1084' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080
        ImageIndex = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1141
          Height = 288
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid1DBTableView1KeyDown
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            FindPanel.DisplayMode = fpdmAlways
            FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPriemLab
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = cxGrid1DBTableView1BROJ
              end
              item
                Format = '0.00,.'
                Kind = skAverage
                Column = cxGrid1DBTableView1CENA
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
            OptionsView.Footer = True
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 54
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 93
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA'
              Visible = False
              Width = 83
            end
            object cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_PRIEM'
              Width = 91
            end
            object cxGrid1DBTableView1U_LISTA: TcxGridDBColumn
              DataBinding.FieldName = 'U_LISTA'
              Width = 163
            end
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
              Width = 143
            end
            object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV'
              Width = 194
            end
            object cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PACIENT_ID'
              Visible = False
              Width = 98
            end
            object cxGrid1DBTableView1EMBG: TcxGridDBColumn
              DataBinding.FieldName = 'EMBG'
              Width = 113
            end
            object cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PACIENT_NAZIV'
              Width = 217
            end
            object cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn
              DataBinding.FieldName = 'KATEGORIJA_RM'
              Visible = False
              Width = 200
            end
            object cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'KATEGORIJANAZIV'
              Width = 200
            end
            object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNO_MESTO'
              Visible = False
              Width = 127
            end
            object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNOMESTONAZIV'
              Width = 200
            end
            object cxGrid1DBTableView1TP: TcxGridDBColumn
              DataBinding.FieldName = 'TP'
              Width = 80
            end
            object cxGrid1DBTableView1P: TcxGridDBColumn
              DataBinding.FieldName = 'P'
              Width = 90
            end
            object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER_NAZIV'
              Width = 200
            end
            object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PAKET_ID'
              Width = 140
            end
            object cxGrid1DBTableView1CENA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              Width = 135
            end
            object cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_ZAVERKA'
              Width = 137
            end
            object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_VAZENJE'
              Width = 128
            end
            object cxGrid1DBTableView1PLACANJE: TcxGridDBColumn
              DataBinding.FieldName = 'PLACANJE'
              Visible = False
              Width = 138
            end
            object cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn
              DataBinding.FieldName = 'NACINPLAKANJE'
              Width = 200
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 172
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 226
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 216
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 236
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 448
    Width = 1143
    Height = 239
    Align = alBottom
    Caption = 'Panel2'
    TabOrder = 3
    object cxPageControlDetail: TcxPageControl
      Left = 1
      Top = 1
      Width = 1141
      Height = 237
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelarenD
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 237
      ClientRectRight = 1141
      ClientRectTop = 24
      object cxTabSheetTabelarenD: TcxTabSheet
        Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1080' '#1079#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1072#1094#1080#1077#1085#1090
        ImageIndex = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1141
          Height = 213
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid2DBTableView1KeyDown
            OnKeyPress = cxGrid2DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsPriemLabStavki
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Options.Editing = False
            end
            object cxGrid2DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
            end
            object cxGrid2DBTableView1RENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV'
              Width = 195
            end
            object cxGrid2DBTableView1PREGLED_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PREGLED_ID'
              Visible = False
              Options.Editing = False
              Width = 99
            end
            object cxGrid2DBTableView1USLUGA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'USLUGA_ID'
              Visible = False
              Options.Editing = False
              Width = 93
            end
            object cxGrid2DBTableView1ULUGANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ULUGANAZIV'
              Options.Editing = False
              Width = 298
            end
            object cxGrid2DBTableView1CENA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              Visible = False
            end
            object cxGrid2DBTableView1ZAVRSENA: TcxGridDBColumn
              DataBinding.FieldName = 'ZAVRSENA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  Description = #1053#1045#1047#1040#1042#1056#1064#1045#1053#1040
                  ImageIndex = 3
                  Value = 0
                end
                item
                  Description = #1047#1040#1042#1056#1064#1045#1053#1040
                  ImageIndex = 2
                  Value = 1
                end>
              Options.Editing = False
              Styles.OnGetContentStyle = cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle
              Width = 106
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              Caption = #1042#1088#1077#1084#1077
              DataBinding.FieldName = 'TS_UPD'
              Options.Editing = False
              Width = 109
            end
            object cxGrid2DBTableView1NAOD: TcxGridDBColumn
              DataBinding.FieldName = 'naodStatus'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  Description = #1059#1056#1045#1044#1045#1053' '#1053#1040#1054#1044
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = #1048#1052#1040' '#1053#1040#1054#1044' '#1042#1054
                  ImageIndex = 1
                  Value = 1
                end
                item
                  Value = 3
                end>
              Options.Editing = False
              Styles.OnGetContentStyle = cxGrid2DBTableView1NAODStylesGetContentStyle
              Width = 102
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Options.Editing = False
              Width = 161
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Options.Editing = False
              Width = 205
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Options.Editing = False
              Width = 240
            end
            object cxGrid2DBTableView1NAOD1: TcxGridDBColumn
              DataBinding.FieldName = 'NAOD'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 440
    Width = 1143
    Height = 8
    AlignSplitter = salBottom
    Control = Panel2
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 440
    Top = 296
  end
  object PopupMenu1: TPopupMenu
    Left = 1008
    Top = 280
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 1024
    Top = 344
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 357
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 611
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1055#1088#1080#1077#1084#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1055#1088#1080#1077#1084#1080
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1045
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 65
          Visible = True
          ItemName = 'cbBarGodina'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 122
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1059#1089#1083#1091#1075#1080
      CaptionButtons = <>
      DockedLeft = 921
      DockedTop = 0
      FloatLeft = 1169
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = ' '#1040#1085#1072#1083#1080#1079#1072
      CaptionButtons = <>
      DockedLeft = 192
      DockedTop = 0
      FloatLeft = 1177
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 357
      DockedTop = 0
      FloatLeft = 1177
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017')
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object cbBarGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbBarGodinaChange
      Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      ItemIndex = -1
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aDodadiUsluga
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiUsluga
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 65
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end>
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Category = 0
      Visible = ivAlways
      ImageIndex = 9
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aSpustiSoberi2
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aZacuvajExcel2
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiTabela2
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPodesuvanjePecatenje2
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aPageSetup2
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aSnimiPecatenje2
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje2
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSnimiIzgled2
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisiIzgled2
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aVnesNaRezultat
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
      Category = 0
      ScreenTip = tipAnaliza
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aStatus
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Action = aPecatiRezultatiAnaliza
      Caption = #1055#1077#1095#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1048#1079#1073#1086#1088' '#1085#1072' '#1072#1085#1072#1083#1080#1079#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end>
    end
    object dxBarButton6: TdxBarButton
      Action = aSiteAnalizi
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Action = aZavrseniAnalizi
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Action = aNezavrseniAnalizi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 352
    Top = 288
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1088#1080#1077#1084#1080
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrebarajPacient: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
    end
    object aIsprazniPacient: TAction
      Caption = #1048#1089#1087#1088#1072#1079#1085#1080
      ImageIndex = 23
    end
    object aDodadiUsluga: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiUslugaExecute
    end
    object aBrisiUsluga: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
    end
    object aSpustiSoberi2: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072' '#1079#1072' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 65
      OnExecute = aSpustiSoberi2Execute
    end
    object aZacuvajExcel2: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcel2Execute
    end
    object aPecatiTabela2: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 30
      OnExecute = aPecatiTabela2Execute
    end
    object aPodesuvanjePecatenje2: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenje2Execute
    end
    object aPageSetup2: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetup2Execute
    end
    object aSnimiPecatenje2: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenje2Execute
    end
    object aBrisiPodesuvanjePecatenje2: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenje2Execute
    end
    object aSnimiIzgled2: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgled2Execute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aBrisiIzgled2: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgled2Execute
    end
    object aVnesNaRezultat: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1088#1077#1079#1091#1083#1090#1072#1090#1080' '#1086#1076' '#1040#1085#1072#1083#1080#1079#1072
      ImageIndex = 63
      OnExecute = aVnesNaRezultatExecute
    end
    object aStatus: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089
      ImageIndex = 92
      OnExecute = aStatusExecute
    end
    object aPecatiRezultatiAnaliza: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090#1080' '#1086#1076' '#1040#1085#1072#1083#1080#1079#1072
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aPecatiRezultatiAnalizaExecute
    end
    object aDizajnRezultatAnaliza: TAction
      Caption = 'aDizajnRezultatAnaliza'
      ImageIndex = 19
      ShortCut = 24697
      OnExecute = aDizajnRezultatAnalizaExecute
    end
    object aZavrseniAnalizi: TAction
      Caption = #1047#1040#1042#1056#1064#1045#1053#1048
      ImageIndex = 15
      OnExecute = aZavrseniAnaliziExecute
    end
    object aNezavrseniAnalizi: TAction
      Caption = #1053#1045#1047#1040#1042#1056#1064#1045#1053#1048
      ImageIndex = 15
      OnExecute = aNezavrseniAnaliziExecute
    end
    object aSiteAnalizi: TAction
      Caption = #1057#1048#1058#1045
      ImageIndex = 15
      OnExecute = aSiteAnaliziExecute
    end
    object actFind: TAction
      Caption = 'actFind'
      ShortCut = 16454
      OnExecute = actFindExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 984
    Top = 320
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44287.323632638890000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 120
    Top = 264
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipAnaliza: TdxScreenTip
      Header.Text = #1040#1085#1072#1083#1080#1079#1072
      Description.Text = 
        #1055#1088#1077#1075#1083#1077#1076' '#1080' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' ('#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1086#1074', '#1072#1078#1091#1088#1080#1088#1072#1114#1077', '#1073#1088#1080#1096#1077#1114#1077') '#1085#1072' '#1088#1077#1079#1091#1083 +
        #1090#1072#1090#1080' '#1086#1076' '#1072#1085#1072#1083#1080#1079#1072' '#1079#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1055#1072#1094#1080#1077#1085#1090' '#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1059#1089#1083#1091#1075#1072
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 696
    Top = 96
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 44
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 63
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        SortIndex = 0
        SortOrder = soDescending
        Width = 258
      end
      object cxGridViewRepository1DBTableView1ADRESA: TcxGridDBColumn
        DataBinding.FieldName = 'ADRESA'
        Width = 158
      end
      object cxGridViewRepository1DBTableView1MESTONAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MESTONAZIV'
        Width = 109
      end
      object cxGridViewRepository1DBTableView1MB: TcxGridDBColumn
        DataBinding.FieldName = 'MB'
        Width = 120
      end
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 576
    Top = 384
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 480
    Top = 352
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 904
    Top = 88
  end
end
