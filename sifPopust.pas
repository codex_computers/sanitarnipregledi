unit sifPopust;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, dmResources;

type
  TfrmPopust = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POPUST: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    POPUST: TcxDBTextEdit;
    Label2: TLabel;
    Label14: TLabel;
    TIP_PARTNER: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    PARTNERNAZIV: TcxExtLookupComboBox;
    Label3: TLabel;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aNovExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPopust: TfrmPopust;
  rData : TRepositoryData;
implementation

{$R *.dfm}

uses dmUnit, dmMaticni, MK, NurkoRepository;

procedure TfrmPopust.aNovExecute(Sender: TObject);
begin
  inherited;
  PARTNERNAZIV.Clear;
end;

procedure TfrmPopust.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
   if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
            begin
               ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
           end
    end
end;

procedure TfrmPopust.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  ZemiImeDvoenKluc(TIP_PARTNER,PARTNER,PARTNERNAZIV);
end;

procedure TfrmPopust.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var kom : TWinControl;
begin
  inherited;
  kom := Sender as TWinControl;
  case Key of
       VK_INSERT:
        begin
         if (kom = PARTNERNAZIV) then
              begin
                frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
                frmNurkoRepository.kontrola_naziv := kom.Name;
                frmNurkoRepository.ShowModal;

                if (frmNurkoRepository.ModalResult = mrOk) then
                    PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);

                frmNurkoRepository.Free;
              end;
        end;
   end;
end;

procedure TfrmPopust.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dmRes.FreeRepository(rData);
end;

procedure TfrmPopust.FormCreate(Sender: TObject);
begin
  inherited;
  rData := TRepositoryData.Create();
end;

procedure TfrmPopust.FormShow(Sender: TObject);
begin
  inherited;
  dmMat.tblNurko.Open;
  dm.tblPopust.Open;
  ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);

  PARTNERNAZIV.RepositoryItem := dmRes.InitRepository(-1, 'PARTNERNAZIV', Name, rData);
end;

procedure TfrmPopust.PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       if PARTNERNAZIV.Text <> '' then
          begin
            dm.tblPopustTP.Value:=PARTNERNAZIV.EditValue[0];
            dm.tblPopustP.Value:=PARTNERNAZIV.EditValue[1];
          end
        else
         begin
            PARTNER.Clear;
            TIP_PARTNER.Clear;
         end;
     end;
end;

procedure TfrmPopust.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

end.
