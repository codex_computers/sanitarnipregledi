unit dmUnit;

interface

uses
  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, Variants, Controls, FIBDatabase, pFIBDatabase, pFIBStoredProc,
  frxClass, frxDBSet, cxCustomPivotGrid, cxTL, cxGridCardView,
  cxGridBandedTableView, cxStyles, cxGridTableView, cxClasses, XMLIntf, XMLDoc,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  frxExportMail, frxExportPDF, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdSSL, IdSSLOpenSSL, Vcl.Menus, IdMessage, IdGlobal, System.Bluetooth,
  System.Bluetooth.Components, ExtCtrls, xmldom, msxmldom,
  comobj,  Windows, Messages,   Graphics, Forms,
  Dialogs, DCPcrypt2, DCPsha1, DCPblockciphers, DCPrijndael, StdCtrls, DCPrc6,
 MSXML;



type
  Tdm = class(TDataModule)
    dsUslugi: TDataSource;
    tblUslugi: TpFIBDataSet;
    tblUslugiID: TFIBIntegerField;
    tblUslugiNAZIV: TFIBStringField;
    tblUslugiCENA: TFIBBCDField;
    tblUslugiTS_INS: TFIBDateTimeField;
    tblUslugiTS_UPD: TFIBDateTimeField;
    tblUslugiUSR_INS: TFIBStringField;
    tblUslugiUSR_UPD: TFIBStringField;
    qMaxUslugiSifra: TpFIBQuery;
    qMaxPaketiSifra: TpFIBQuery;
    tblPaketi: TpFIBDataSet;
    dsPaketi: TDataSource;
    tblPaketiID: TFIBIntegerField;
    tblPaketiNAZIV: TFIBStringField;
    tblPaketiCENA: TFIBBCDField;
    tblPaketiTS_INS: TFIBDateTimeField;
    tblPaketiTS_UPD: TFIBDateTimeField;
    tblPaketiUSR_INS: TFIBStringField;
    tblPaketiUSR_UPD: TFIBStringField;
    dsPaketUsluga: TDataSource;
    tblPaketUsluga: TpFIBDataSet;
    tblPaketUslugaPAKET_ID: TFIBIntegerField;
    tblPaketUslugaPAKETNAZIV: TFIBStringField;
    tblPaketUslugaUSLUGA_ID: TFIBIntegerField;
    tblPaketUslugaUSLUGANAZIV: TFIBStringField;
    tblPaketUslugaTS_INS: TFIBDateTimeField;
    tblPaketUslugaTS_UPD: TFIBDateTimeField;
    tblPaketUslugaUSR_INS: TFIBStringField;
    tblPaketUslugaUSR_UPD: TFIBStringField;
    tblRabotnoMesto: TpFIBDataSet;
    dsRabotnoMesto: TDataSource;
    tblRabotnoMestoID: TFIBIntegerField;
    tblRabotnoMestoNAZIV: TFIBStringField;
    tblRabotnoMestoOPIS: TFIBStringField;
    tblRabotnoMestoROK: TFIBSmallIntField;
    tblRabotnoMestoTS_INS: TFIBDateTimeField;
    tblRabotnoMestoTS_UPD: TFIBDateTimeField;
    tblRabotnoMestoUSR_INS: TFIBStringField;
    tblRabotnoMestoUSR_UPD: TFIBStringField;
    qMaxRabotnoMestoID: TpFIBQuery;
    tblPacienti: TpFIBDataSet;
    dsPacienti: TDataSource;
    tblPacientiID: TFIBIntegerField;
    tblPacientiBROJ_KARTON: TFIBStringField;
    tblPacientiEMBG: TFIBStringField;
    tblPacientiIME: TFIBStringField;
    tblPacientiPREZIME: TFIBStringField;
    tblPacientiTATKOVO: TFIBStringField;
    tblPacientiNAZIV: TFIBStringField;
    tblPacientiDATUM_RAGJANJE: TFIBDateField;
    tblPacientiADRESA: TFIBStringField;
    tblPacientiTEL: TFIBStringField;
    tblPacientiEMAIL: TFIBStringField;
    tblPacientiMESTO_ZIVEENJE: TFIBIntegerField;
    tblPacientiMESTOZIVEENJE: TFIBStringField;
    tblPacientiMESTO_RAGJANJE: TFIBIntegerField;
    tblPacientiMESTORAGJANJE: TFIBStringField;
    tblPacientiR_MESTO: TFIBIntegerField;
    tblPacientiRABOTNOMESTONAZIV: TFIBStringField;
    tblPacientiTIP_PARTNER: TFIBIntegerField;
    tblPacientiPARTNER: TFIBIntegerField;
    tblPacientiPARTNERNAZIV: TFIBStringField;
    tblPacientiTS_INS: TFIBDateTimeField;
    tblPacientiTS_UPD: TFIBDateTimeField;
    tblPacientiUSR_INS: TFIBStringField;
    tblPacientiUSR_UPD: TFIBStringField;
    dsPartner: TDataSource;
    tblPartner: TpFIBDataSet;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerNAZIV_SKRATEN: TFIBStringField;
    tblPartnerRE: TFIBIntegerField;
    tblPartnerADRESA: TFIBStringField;
    tblPartnerMESTONAZIV: TFIBStringField;
    tblPartnerMB: TFIBStringField;
    tblPregledi: TpFIBDataSet;
    dsPregledi: TDataSource;
    tblPreglediID: TFIBIntegerField;
    tblPreglediGODINA: TFIBSmallIntField;
    tblPreglediDATUM_PRIEM: TFIBDateField;
    tblPreglediPACIENT_ID: TFIBIntegerField;
    tblPreglediEMBG: TFIBStringField;
    tblPreglediTP: TFIBIntegerField;
    tblPreglediP: TFIBIntegerField;
    tblPreglediPAKET_ID: TFIBStringField;
    tblPreglediCENA: TFIBBCDField;
    tblPreglediDATUM_ZAVERKA: TFIBDateField;
    tblPreglediDATUM_VAZENJE: TFIBDateField;
    tblPreglediPLACANJE: TFIBIntegerField;
    tblPreglediPACIENT_NAZIV: TFIBStringField;
    tblPreglediPARTNER_NAZIV: TFIBStringField;
    tblPreglediNACINPLAKANJE: TFIBStringField;
    tblTipPlakanje: TpFIBDataSet;
    dsTipPlakanje: TDataSource;
    tblTipPlakanjeID: TFIBIntegerField;
    tblTipPlakanjeNAZIV: TFIBStringField;
    qMaxTipPlakanje: TpFIBQuery;
    dsPreglediStavki: TDataSource;
    tblPreglediStavki: TpFIBDataSet;
    tblPreglediStavkiID: TFIBBCDField;
    tblPreglediStavkiPREGLED_ID: TFIBIntegerField;
    tblPreglediStavkiUSLUGA_ID: TFIBIntegerField;
    tblPreglediStavkiULUGANAZIV: TFIBStringField;
    tblPreglediStavkiNAOD: TFIBSmallIntField;
    tblPreglediStavkiZAVRSENA: TFIBSmallIntField;
    tblPreglediStavkiTS_INS: TFIBDateTimeField;
    tblPreglediStavkiTS_UPD: TFIBDateTimeField;
    tblPreglediStavkiUSR_INS: TFIBStringField;
    tblPreglediStavkiUSR_UPD: TFIBStringField;
    PROC_SAN_INSERTUSLUGA: TpFIBStoredProc;
    TransakcijaP: TpFIBTransaction;
    qDeleteUslugiOdPregled: TpFIBQuery;
    pInsDelSelektiraniUslugi: TpFIBStoredProc;
    tblMikroorganizmi: TpFIBDataSet;
    dsMikroorganizmi: TDataSource;
    tblMikroorganizmiID: TFIBIntegerField;
    tblMikroorganizmiNAZIV: TFIBStringField;
    tblMikroorganizmiTS_INS: TFIBDateTimeField;
    tblMikroorganizmiTS_UPD: TFIBDateTimeField;
    tblMikroorganizmiUSR_INS: TFIBStringField;
    tblMikroorganizmiUSR_UPD: TFIBStringField;
    tblMikroUslugi: TpFIBDataSet;
    dsMikroUslugi: TDataSource;
    tblMikroUslugiUSLUGA_ID: TFIBIntegerField;
    tblMikroUslugiUSLUGANAZIV: TFIBStringField;
    tblMikroUslugiMIKRO_ID: TFIBIntegerField;
    tblMikroUslugiMIKRONAZIV: TFIBStringField;
    tblMikroUslugiTS_INS: TFIBDateTimeField;
    tblMikroUslugiTS_UPD: TFIBDateTimeField;
    tblMikroUslugiUSR_INS: TFIBStringField;
    tblMikroUslugiUSR_UPD: TFIBStringField;
    qMaxMikro: TpFIBQuery;
    tblPreglediRE: TFIBIntegerField;
    qFirmaNaPacient: TpFIBQuery;
    tblKategorijaRM: TpFIBDataSet;
    dsKategorijaRM: TDataSource;
    tblKategorijaRMID: TFIBIntegerField;
    tblKategorijaRMNAZIV: TFIBStringField;
    tblKategorijaRMTS_INS: TFIBDateTimeField;
    tblKategorijaRMTS_UPD: TFIBDateTimeField;
    tblKategorijaRMUSR_INS: TFIBStringField;
    tblKategorijaRMUSR_UPD: TFIBStringField;
    qMaxKategorijaRM: TpFIBQuery;
    tblPreglediRABOTNO_MESTO: TFIBIntegerField;
    tblPreglediRABOTNOMESTONAZIV: TFIBStringField;
    tblPreglediKATEGORIJA_RM: TFIBIntegerField;
    tblPreglediKATEGORIJANAZIV: TFIBStringField;
    tblPreglediTS_INS: TFIBDateTimeField;
    tblPreglediTS_UPD: TFIBDateTimeField;
    tblPreglediUSR_INS: TFIBStringField;
    tblPreglediUSR_UPD: TFIBStringField;
    tblLekovi: TpFIBDataSet;
    dsLekovi: TDataSource;
    tblLekoviID: TFIBIntegerField;
    tblLekoviNAZIV: TFIBStringField;
    tblLekoviTS_INS: TFIBDateTimeField;
    tblLekoviTS_UPD: TFIBDateTimeField;
    tblLekoviUSR_INS: TFIBStringField;
    tblLekoviUSR_UPD: TFIBStringField;
    qMaxLekovi: TpFIBQuery;
    tblAntibiogram: TpFIBDataSet;
    dsAntibiogram: TDataSource;
    tblAntibiogramID: TFIBBCDField;
    tblAntibiogramTS_INS: TFIBDateTimeField;
    tblAntibiogramTS_UPD: TFIBDateTimeField;
    tblAntibiogramUSR_INS: TFIBStringField;
    tblAntibiogramUSR_UPD: TFIBStringField;
    qMaxAntibiogram: TpFIBQuery;
    tblAntibiogramNAZIV: TFIBStringField;
    tblDefAntibiogram: TpFIBDataSet;
    dsDefAntibiogram: TDataSource;
    tblDefAntibiogramANTIBIOGRAM_ID: TFIBBCDField;
    tblDefAntibiogramANAZIV: TFIBStringField;
    tblDefAntibiogramLEK_ID: TFIBIntegerField;
    tblDefAntibiogramLNAZIV: TFIBStringField;
    tblDefAntibiogramTS_INS: TFIBDateTimeField;
    tblDefAntibiogramTS_UPD: TFIBDateTimeField;
    tblDefAntibiogramUSR_INS: TFIBStringField;
    tblDefAntibiogramUSR_UPD: TFIBStringField;
    tblDefNaodi: TpFIBDataSet;
    dsDefNaodi: TDataSource;
    tblDefNaodiID: TFIBIntegerField;
    tblDefNaodiOPIS: TFIBStringField;
    tblDefNaodiTS_INS: TFIBDateTimeField;
    tblDefNaodiTS_UPD: TFIBDateTimeField;
    tblDefNaodiUSR_INS: TFIBStringField;
    tblDefNaodiUSR_UPD: TFIBStringField;
    qMaxDefNaodi: TpFIBQuery;
    tblAnaliza: TpFIBDataSet;
    dsAnaliza: TDataSource;
    tblAntibiogramOPIS: TFIBStringField;
    tblAntibiogramLEGENDA: TFIBStringField;
    qPacientNAziv: TpFIBQuery;
    qPacientEMBG: TpFIBQuery;
    qPaketNaziv: TpFIBQuery;
    tblLekari: TpFIBDataSet;
    dsLekari: TDataSource;
    tblLekariID: TFIBSmallIntField;
    tblLekariTITULA: TFIBStringField;
    tblLekariSPECIJALNOST: TFIBStringField;
    tblLekariTS_INS: TFIBDateTimeField;
    tblLekariTS_UPD: TFIBDateTimeField;
    tblLekariUSR_INS: TFIBStringField;
    tblLekariUSR_UPD: TFIBStringField;
    qMaxLekariID: TpFIBQuery;
    tblUslugiRE: TFIBIntegerField;
    tblUslugiRENAZIV: TFIBStringField;
    tblUslugiSTATUS: TFIBSmallIntField;
    tblAnalizaID: TFIBIntegerField;
    tblAnalizaPREGLED: TFIBIntegerField;
    tblAnalizaPREGLED_S: TFIBBCDField;
    tblAnalizaMIKROORGANIZAM: TFIBIntegerField;
    tblAnalizaMIKRO_NAZIV: TFIBStringField;
    tblAnalizaANTIBIOGRAM: TFIBBCDField;
    tblAnalizaANTIBIOGRAM_OPIS: TFIBStringField;
    tblAnalizaLEKAR: TFIBIntegerField;
    tblAnalizaLEKARNAZIV: TFIBStringField;
    tblAnalizaDEF_NAOD: TFIBIntegerField;
    tblAnalizaNAOD_OPIS: TFIBStringField;
    tblAnalizaOPIS: TFIBBlobField;
    tblAnalizaTS_INS: TFIBDateTimeField;
    tblAnalizaTS_UPD: TFIBDateTimeField;
    tblAnalizaUSR_INS: TFIBStringField;
    tblAnalizaUSR_UPD: TFIBStringField;
    tblAnalizaLAB_BR: TFIBStringField;
    tblLekariPREZIME: TFIBStringField;
    tblLekariIME: TFIBStringField;
    tblLekariPREZIME_IME: TFIBStringField;
    tblLekariNAZIV: TFIBStringField;
    tblAnalizaBR_MIKROORGANIZAM: TFIBSmallIntField;
    qCountAnaliza: TpFIBQuery;
    tblRezultat: TpFIBDataSet;
    dsRezultat: TDataSource;
    tblRezultatID: TFIBIntegerField;
    tblRezultatPREGLED_ID: TFIBIntegerField;
    tblRezultatPREGLED_S_ID: TFIBBCDField;
    tblRezultatANALIZA_ID: TFIBIntegerField;
    tblRezultatLEK_ID: TFIBIntegerField;
    tblRezultatVREDNOST: TFIBStringField;
    tblRezultatTS_INS: TFIBDateTimeField;
    tblRezultatTS_UPD: TFIBDateTimeField;
    tblRezultatUSR_INS: TFIBStringField;
    tblRezultatUSR_UPD: TFIBStringField;
    tblRezultatLEKNAZIV: TFIBStringField;
    qCountRezultat: TpFIBQuery;
    PROC_SAN_INSERTREZULTAT: TpFIBStoredProc;
    qDeleteAnaliza: TpFIBQuery;
    qDeleterezultat: TpFIBQuery;
    frxDBAntibiogram: TfrxDBDataset;
    tblPreglediStavkiCENA: TFIBBCDField;
    tblPriemLab: TpFIBDataSet;
    dsPriemLab: TDataSource;
    tblPriemLabID: TFIBIntegerField;
    tblPriemLabGODINA: TFIBSmallIntField;
    tblPriemLabDATUM_PRIEM: TFIBDateField;
    tblPriemLabPACIENT_ID: TFIBIntegerField;
    tblPriemLabEMBG: TFIBStringField;
    tblPriemLabPACIENT_NAZIV: TFIBStringField;
    tblPriemLabTP: TFIBIntegerField;
    tblPriemLabP: TFIBIntegerField;
    tblPriemLabPARTNER_NAZIV: TFIBStringField;
    tblPriemLabPAKET_ID: TFIBStringField;
    tblPriemLabCENA: TFIBBCDField;
    tblPriemLabDATUM_ZAVERKA: TFIBDateField;
    tblPriemLabDATUM_VAZENJE: TFIBDateField;
    tblPriemLabPLACANJE: TFIBIntegerField;
    tblPriemLabNACINPLAKANJE: TFIBStringField;
    tblPriemLabRE: TFIBIntegerField;
    tblPriemLabRENAZIV: TFIBStringField;
    tblPriemLabRABOTNO_MESTO: TFIBIntegerField;
    tblPriemLabRABOTNOMESTONAZIV: TFIBStringField;
    tblPriemLabKATEGORIJA_RM: TFIBIntegerField;
    tblPriemLabKATEGORIJANAZIV: TFIBStringField;
    tblPriemLabTS_INS: TFIBDateTimeField;
    tblPriemLabTS_UPD: TFIBDateTimeField;
    tblPriemLabUSR_INS: TFIBStringField;
    tblPriemLabUSR_UPD: TFIBStringField;
    tblPriemLabStavki: TpFIBDataSet;
    dsPriemLabStavki: TDataSource;
    tblPriemLabStavkiID: TFIBBCDField;
    tblPriemLabStavkiPREGLED_ID: TFIBIntegerField;
    tblPriemLabStavkiUSLUGA_ID: TFIBIntegerField;
    tblPriemLabStavkiULUGANAZIV: TFIBStringField;
    tblPriemLabStavkiCENA: TFIBBCDField;
    tblPriemLabStavkiRE: TFIBIntegerField;
    tblPriemLabStavkiNAOD: TFIBSmallIntField;
    tblPriemLabStavkiZAVRSENA: TFIBSmallIntField;
    tblPriemLabStavkiTS_INS: TFIBDateTimeField;
    tblPriemLabStavkiTS_UPD: TFIBDateTimeField;
    tblPriemLabStavkiUSR_INS: TFIBStringField;
    tblPriemLabStavkiUSR_UPD: TFIBStringField;
    tblPriemLabStavkiRENAZIV: TFIBStringField;
    tblPreglediStavkiRE: TFIBIntegerField;
    tblPreglediStavkiRENAZIV: TFIBStringField;
    qCountPravaRe: TpFIBQuery;
    tblPopust: TpFIBDataSet;
    dsPopust: TDataSource;
    tblPopustID: TFIBIntegerField;
    tblPopustP: TFIBIntegerField;
    tblPopustTP: TFIBIntegerField;
    tblPopustPARTNERNAZIV: TFIBStringField;
    tblPopustPOPUST: TFIBIntegerField;
    tblPopustTS_INS: TFIBDateTimeField;
    tblPopustTS_UPD: TFIBDateTimeField;
    tblPopustUSR_INS: TFIBStringField;
    tblPopustUSR_UPD: TFIBStringField;
    tblNefakturirani: TpFIBDataSet;
    dsNefakturirani: TDataSource;
    tblNefakturiraniPREGLED_ID_OUT: TFIBIntegerField;
    tblNefakturiraniIZNOS_OUT: TFIBBCDField;
    tblNefakturiraniDATUM_PRIEM: TFIBDateField;
    tblNefakturiraniDATUM_ZAVERKA: TFIBDateField;
    tblNefakturiraniDATUM_VAZENJE: TFIBDateField;
    tblNefakturiraniNACINPLAKANJE: TFIBStringField;
    tblNefakturiraniEMBG: TFIBStringField;
    tblNefakturiraniPACIENTNAZIV: TFIBStringField;
    tblNefakturiraniTP: TFIBIntegerField;
    tblNefakturiraniP: TFIBIntegerField;
    tblNefakturiraniPARTNERNAZIV: TFIBStringField;
    tblNefakturiraniRMNAZIV: TFIBStringField;
    tblNefakturiraniKRMNAZIV: TFIBStringField;
    qUpdatePriemFlagFaktura: TpFIBQuery;
    tblFakturi: TpFIBDataSet;
    dsFakturi: TDataSource;
    tblFakturiPRIEM_ID: TFIBIntegerField;
    tblFakturiFAKTURA_ID: TFIBIntegerField;
    tblFakturiTS_INS: TFIBDateTimeField;
    tblFakturiTS_UPD: TFIBDateTimeField;
    tblFakturiUSR_INS: TFIBStringField;
    tblFakturiUSR_UPD: TFIBStringField;
    PROC_FAKTURIRANJE: TpFIBStoredProc;
    tblFakturiBRFAKTURA: TFIBIntegerField;
    tblFakturiPACIENT_ID: TFIBIntegerField;
    tblFakturiP: TFIBIntegerField;
    tblFakturiTP: TFIBIntegerField;
    tblFakturiPARTNERNAZIV: TFIBStringField;
    tblFakturiEMBG: TFIBStringField;
    tblFakturiNAZIV: TFIBStringField;
    qDeleteFaktura: TpFIBQuery;
    qDeleteFakturaPriem: TpFIBQuery;
    tblFakturiPOPUST: TFIBIntegerField;
    tblFakturiIZNOS: TFIBBCDField;
    tblFakturiIZNOS_POPUST: TFIBBCDField;
    tblFakturiDATUM: TFIBDateField;
    tblFakturiGODINA: TFIBIntegerField;
    tblFakturiRE: TFIBIntegerField;
    tblFrxAntibiogram: TpFIBDataSet;
    frxDBRezultatiAnaliza: TfrxDBDataset;
    tblRezultatAnaliza: TpFIBDataSet;
    dsRezultatAnaliza: TDataSource;
    tblKategorijaRMROK: TFIBSmallIntField;
    tblPacientiKATEGORIJARMMAZIV: TFIBStringField;
    tblPacientiKATEGORIJA_RM: TFIBSmallIntField;
    tblPreglediU_LISTA: TFIBStringField;
    tblPriemLabU_LISTA: TFIBStringField;
    PROC_SAN_LISTAUSLUGI: TpFIBStoredProc;
    tblFakturiPoBroj: TpFIBDataSet;
    tblFakturiPoData: TpFIBDataSet;
    dsFakturiPoBroj: TDataSource;
    dsFakturiPoData: TDataSource;
    tblFakturiPoBrojBRFAKTURA: TFIBIntegerField;
    tblFakturiPoBrojPOPUST: TFIBIntegerField;
    tblFakturiPoBrojIZNOS: TFIBBCDField;
    tblFakturiPoBrojIZNOS_POPUST: TFIBBCDField;
    tblFakturiPoBrojDATUM: TFIBDateField;
    tblFakturiPoBrojGODINA: TFIBIntegerField;
    tblFakturiPoBrojRE: TFIBIntegerField;
    tblFakturiPoBrojPARTNERNAZIV: TFIBStringField;
    tblFakturiPoBrojFAKTURA_ID: TFIBIntegerField;
    tblFakturiPoDataBRFAKTURA: TFIBIntegerField;
    tblFakturiPoDataPOPUST: TFIBIntegerField;
    tblFakturiPoDataIZNOS: TFIBBCDField;
    tblFakturiPoDataIZNOS_POPUST: TFIBBCDField;
    tblFakturiPoDataDATUM: TFIBDateField;
    tblFakturiPoDataGODINA: TFIBIntegerField;
    tblFakturiPoDataRE: TFIBIntegerField;
    tblFakturiPoDataPARTNERNAZIV: TFIBStringField;
    tblFakturiPoDataFAKTURA_ID: TFIBIntegerField;
    tblPreglediBR_KVITANCIJA: TFIBStringField;
    tblFakturiPoBrojPARTNER: TFIBIntegerField;
    tblFakturiPoBrojTIP_PARTNER: TFIBIntegerField;
    tblFakturiPoDataPARTNER: TFIBIntegerField;
    tblFakturiPoDataTIP_PARTNER: TFIBIntegerField;
    qDatumVazenje: TpFIBQuery;
    tblPominatRok: TpFIBDataSet;
    dsPominatRok: TDataSource;
    tblPominatRokGODINA: TFIBSmallIntField;
    tblPominatRokDATUM_PRIEM: TFIBDateField;
    tblPominatRokPACIENT_ID: TFIBIntegerField;
    tblPominatRokEMBG: TFIBStringField;
    tblPominatRokPACIENT_NAZIV: TFIBStringField;
    tblPominatRokTP: TFIBIntegerField;
    tblPominatRokP: TFIBIntegerField;
    tblPominatRokPARTNER_NAZIV: TFIBStringField;
    tblPominatRokPAKET_ID: TFIBStringField;
    tblPominatRokCENA: TFIBBCDField;
    tblPominatRokDATUM_ZAVERKA: TFIBDateField;
    tblPominatRokDATUM_VAZENJE: TFIBDateField;
    tblPominatRokPLACANJE: TFIBIntegerField;
    tblPominatRokNACINPLAKANJE: TFIBStringField;
    tblPominatRokRABOTNO_MESTO: TFIBIntegerField;
    tblPominatRokRABOTNOMESTONAZIV: TFIBStringField;
    tblPominatRokKATEGORIJA_RM: TFIBIntegerField;
    tblPominatRokKATEGORIJANAZIV: TFIBStringField;
    tblPominatRokU_LISTA: TFIBStringField;
    tblLekariPOTPIS: TFIBBlobField;
    tblPreglediStavkiTIP: TFIBSmallIntField;
    qUpdatePriemFlagFakturaNULL: TpFIBQuery;
    qCountEMBGPacient: TpFIBQuery;
    qMaxBrojPriem: TpFIBQuery;
    tblPreglediBROJ: TFIBIntegerField;
    tblListaPrimeniPacientiFirst: TpFIBDataSet;
    dsListaPrimeniPacientiFirst: TDataSource;
    tblListaPrimeniPacientiFirstID: TFIBIntegerField;
    tblListaPrimeniPacientiFirstGODINA: TFIBSmallIntField;
    tblListaPrimeniPacientiFirstDATUM_PRIEM: TFIBDateField;
    tblListaPrimeniPacientiFirstBROJ: TFIBIntegerField;
    tblListaPrimeniPacientiFirstPACIENT_ID: TFIBIntegerField;
    tblListaPrimeniPacientiFirstEMBG: TFIBStringField;
    tblListaPrimeniPacientiFirstPACIENT_NAZIV: TFIBStringField;
    tblListaPrimeniPacientiFirstTP: TFIBIntegerField;
    tblListaPrimeniPacientiFirstP: TFIBIntegerField;
    tblListaPrimeniPacientiFirstPARTNER_NAZIV: TFIBStringField;
    tblListaPrimeniPacientiFirstPAKET_ID: TFIBStringField;
    tblListaPrimeniPacientiFirstCENA: TFIBBCDField;
    tblListaPrimeniPacientiFirstDATUM_ZAVERKA: TFIBDateField;
    tblListaPrimeniPacientiFirstDATUM_VAZENJE: TFIBDateField;
    tblListaPrimeniPacientiFirstPLACANJE: TFIBIntegerField;
    tblListaPrimeniPacientiFirstNACINPLAKANJE: TFIBStringField;
    tblListaPrimeniPacientiFirstRE: TFIBIntegerField;
    tblListaPrimeniPacientiFirstRENAZIV: TFIBStringField;
    tblListaPrimeniPacientiFirstRABOTNO_MESTO: TFIBIntegerField;
    tblListaPrimeniPacientiFirstRABOTNOMESTONAZIV: TFIBStringField;
    tblListaPrimeniPacientiFirstKATEGORIJA_RM: TFIBIntegerField;
    tblListaPrimeniPacientiFirstKATEGORIJANAZIV: TFIBStringField;
    tblListaPrimeniPacientiFirstTS_INS: TFIBDateTimeField;
    tblListaPrimeniPacientiFirstTS_UPD: TFIBDateTimeField;
    tblListaPrimeniPacientiFirstUSR_INS: TFIBStringField;
    tblListaPrimeniPacientiFirstUSR_UPD: TFIBStringField;
    tblListaPrimeniPacientiFirstU_LISTA: TFIBStringField;
    tblListaPrimeniPacientiFirstBR_KVITANCIJA: TFIBStringField;
    tblPriemLabBROJ: TFIBIntegerField;
    tblPominatRokBROJ: TFIBIntegerField;
    tblNefakturiraniBROJ_DNEVNIK: TFIBIntegerField;
    tblPartneriOdPriem: TpFIBDataSet;
    dsPartneriOdPriem: TDataSource;
    tblPartneriOdPriemTP: TFIBIntegerField;
    tblPartneriOdPriemP: TFIBIntegerField;
    tblPartneriOdPriemPARTNERNAZIV: TFIBStringField;
    dsIzvestuvanjePoMail: TDataSource;
    tblIzvestuvanjePoMail: TpFIBDataSet;
    tblIzvestuvanjePoMailID: TFIBIntegerField;
    tblIzvestuvanjePoMailDATUM: TFIBDateTimeField;
    tblIzvestuvanjePoMailTIP_PARTNER: TFIBIntegerField;
    tblIzvestuvanjePoMailPARTNER: TFIBIntegerField;
    tblIzvestuvanjePoMailKONTAKT: TFIBIntegerField;
    tblIzvestuvanjePoMailNASLOV: TFIBStringField;
    tblIzvestuvanjePoMailTEXT: TFIBBlobField;
    tblIzvestuvanjePoMailMAIL_SENDER: TFIBStringField;
    tblIzvestuvanjePoMailMAIL_RECIPIENT: TFIBStringField;
    tblIzvestuvanjePoMailNAZIV: TFIBStringField;
    tblIzvestuvanjePoMailKONTAKTNAZIV: TFIBStringField;
    tblListaPrimeniPacientiStavkifirst: TpFIBDataSet;
    dsListaPrimeniPacientiStavkifirst: TDataSource;
    tblListaPrimeniPacientiStavkifirstID: TFIBBCDField;
    tblListaPrimeniPacientiStavkifirstPREGLED_ID: TFIBIntegerField;
    tblListaPrimeniPacientiStavkifirstUSLUGA_ID: TFIBIntegerField;
    tblListaPrimeniPacientiStavkifirstNAZIV: TFIBStringField;
    tblListaPrimeniPacientiStavkifirstCENA: TFIBBCDField;
    tblListaPrimeniPacientiStavkifirstRE: TFIBIntegerField;
    tblListaPrimeniPacientiStavkifirstSTATUS: TFIBSmallIntField;
    master: TpFIBDataSet;
    dsMaster: TDataSource;
    masterID: TFIBIntegerField;
    masterGODINA: TFIBSmallIntField;
    masterDATUM_PRIEM: TFIBDateField;
    masterBROJ: TFIBIntegerField;
    masterPACIENT_ID: TFIBIntegerField;
    masterEMBG: TFIBStringField;
    masterPACIENT_NAZIV: TFIBStringField;
    masterTP: TFIBIntegerField;
    masterP: TFIBIntegerField;
    masterPARTNER_NAZIV: TFIBStringField;
    masterPAKET_ID: TFIBStringField;
    masterCENA: TFIBBCDField;
    masterDATUM_ZAVERKA: TFIBDateField;
    masterDATUM_VAZENJE: TFIBDateField;
    masterPLACANJE: TFIBIntegerField;
    masterNACINPLAKANJE: TFIBStringField;
    masterRE: TFIBIntegerField;
    masterRENAZIV: TFIBStringField;
    masterRABOTNO_MESTO: TFIBIntegerField;
    masterRABOTNOMESTONAZIV: TFIBStringField;
    masterKATEGORIJA_RM: TFIBIntegerField;
    masterKATEGORIJANAZIV: TFIBStringField;
    masterTS_INS: TFIBDateTimeField;
    masterTS_UPD: TFIBDateTimeField;
    masterUSR_INS: TFIBStringField;
    masterUSR_UPD: TFIBStringField;
    masterU_LISTA: TFIBStringField;
    masterBR_KVITANCIJA: TFIBStringField;
    detail: TpFIBDataSet;
    dsdetail: TDataSource;
    tblPreglediBRFAKTURI: TFIBIntegerField;
    qCountZavrseni: TpFIBQuery;
    tblUslugiTIP: TFIBSmallIntField;
    pZatvoriDatum: TpFIBStoredProc;
    tblUslugiStringPriem: TpFIBDataSet;
    dsUslugiStringPriem: TDataSource;
    tblUslugiStringPriemU_LISTA: TFIBStringField;
    detailSTAVKA_ID: TFIBBCDField;
    detailPRIEM_ID: TFIBIntegerField;
    detailUSLUGA_ID: TFIBIntegerField;
    detailNAZIV: TFIBStringField;
    detailCENA: TFIBBCDField;
    detailRE: TFIBIntegerField;
    detailZAVRSENA: TFIBSmallIntField;
    detailNAOD: TFIBSmallIntField;
    detailBROJ: TFIBIntegerField;
    tblRezultatAnalizaPREGLED_S: TFIBBCDField;
    tblRezultatAnalizaUSLUGA: TFIBStringField;
    tblRezultatAnalizaSTATUS_NAOD: TFIBStringField;
    tblRezultatAnalizaANALIZA: TFIBIntegerField;
    tblRezultatAnalizaMIKRO: TFIBStringField;
    tblRezultatAnalizaANTIB: TFIBIntegerField;
    tblRezultatAnalizaLEKAR: TFIBStringField;
    tblRezultatAnalizaDEF_NAOD: TFIBStringField;
    tblRezultatAnalizaLAB_BR: TFIBStringField;
    tblRezultatAnalizaOPIS: TFIBBlobField;
    tblRezultatAnalizaBR_MIKROORGANIZAM: TFIBSmallIntField;
    tblRezultatAnalizaLEK: TFIBStringField;
    tblRezultatAnalizaVREDNOST: TFIBStringField;
    tblRezultatAnalizaUSLUGA_ID: TFIBIntegerField;
    tblRezultatAnalizaPOTPIS: TFIBBlobField;
    tblRezultatAnalizaANALIZA_TS_INS: TFIBDateField;
    tblRezultatAnalizaANALIZA_TS_UPD: TFIBDateField;
    tblRezultatAnalizaDATUM_PRIEM: TFIBDateField;
    tblRezultatAnalizaEMBG: TFIBStringField;
    tblRezultatAnalizaRE: TFIBIntegerField;
    tblRezultatAnalizaRENAZIV: TFIBStringField;
    tblRezultatAnalizaPRIEM_STAVKA_TS_UPD: TFIBDateField;
    StyleRepository: TcxStyleRepository;
    Sunny: TcxStyle;
    Dark: TcxStyle;
    Golden: TcxStyle;
    Summer: TcxStyle;
    Autumn: TcxStyle;
    Bright: TcxStyle;
    Cold: TcxStyle;
    Spring: TcxStyle;
    Light: TcxStyle;
    Winter: TcxStyle;
    Title: TcxStyle;
    Search: TcxStyle;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    zContent: TcxStyle;
    zHeader: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
  stlCheckBox: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    cxStyle40: TcxStyle;
    cxStyle41: TcxStyle;
    cxStyle42: TcxStyle;
    cxStyle43: TcxStyle;
    cxStyle44: TcxStyle;
    cxStyle45: TcxStyle;
    cxStyle46: TcxStyle;
    cxStyle47: TcxStyle;
    cxStyle48: TcxStyle;
    cxStyle49: TcxStyle;
    cxStyle50: TcxStyle;
    cxStyle51: TcxStyle;
    cxStyle52: TcxStyle;
    cxStyle53: TcxStyle;
    cxStyle54: TcxStyle;
    cxStyle55: TcxStyle;
    cxStyle56: TcxStyle;
    cxStyle57: TcxStyle;
    cxStyle58: TcxStyle;
    cxStyle59: TcxStyle;
    cxStyle60: TcxStyle;
    cxStyle61: TcxStyle;
    cxStyle62: TcxStyle;
    cxStyle63: TcxStyle;
    cxStyle64: TcxStyle;
    cxStyle65: TcxStyle;
    cxStyle66: TcxStyle;
    cxStyle67: TcxStyle;
    cxStyle68: TcxStyle;
    cxStyle69: TcxStyle;
    cxStyle70: TcxStyle;
    cxStyle71: TcxStyle;
    cxStyle72: TcxStyle;
    cxStyle73: TcxStyle;
    cxStyle74: TcxStyle;
    cxStyle75: TcxStyle;
    cxStyle76: TcxStyle;
    cxStyle77: TcxStyle;
    cxStyle78: TcxStyle;
    cxStyle79: TcxStyle;
    cxStyle80: TcxStyle;
    cxStyle81: TcxStyle;
    cxStyle82: TcxStyle;
    cxStyle83: TcxStyle;
    cxStyle84: TcxStyle;
    cxStyle85: TcxStyle;
    cxStyle86: TcxStyle;
    cxStyle87: TcxStyle;
    cxStyle88: TcxStyle;
    cxStyle89: TcxStyle;
    cxStyle90: TcxStyle;
    cxStyle91: TcxStyle;
    cxStyle92: TcxStyle;
    cxStyle93: TcxStyle;
    cxStyle94: TcxStyle;
    cxStyle95: TcxStyle;
    cxStyle96: TcxStyle;
    cxStyle97: TcxStyle;
    cxStyle98: TcxStyle;
    cxStyle99: TcxStyle;
    cxStyle100: TcxStyle;
    cxStyle101: TcxStyle;
    cxStyle102: TcxStyle;
    cxStyle103: TcxStyle;
    cxStyle104: TcxStyle;
    cxStyle105: TcxStyle;
    cxStyle106: TcxStyle;
    cxStyle107: TcxStyle;
    cxStyle108: TcxStyle;
    cxStyle109: TcxStyle;
    cxStyle110: TcxStyle;
    cxStyle111: TcxStyle;
    cxStyle112: TcxStyle;
    cxStyle113: TcxStyle;
    cxStyle114: TcxStyle;
    cxStyle115: TcxStyle;
    cxStyle116: TcxStyle;
    cxStyle117: TcxStyle;
    cxStyle118: TcxStyle;
    cxStyle119: TcxStyle;
    cxStyle120: TcxStyle;
    cxStyle121: TcxStyle;
    cxStyle122: TcxStyle;
    cxStyle123: TcxStyle;
    cxStyle124: TcxStyle;
    cxStyle125: TcxStyle;
    cxStyle126: TcxStyle;
    cxStyle127: TcxStyle;
    cxStyle128: TcxStyle;
    cxStyle129: TcxStyle;
    cxStyle130: TcxStyle;
    cxStyle131: TcxStyle;
    cxStyle132: TcxStyle;
    cxStyle133: TcxStyle;
    cxStyle134: TcxStyle;
    cxStyle135: TcxStyle;
    cxStyle136: TcxStyle;
    cxStyle137: TcxStyle;
    cxStyle138: TcxStyle;
    cxStyle139: TcxStyle;
    cxStyle140: TcxStyle;
    cxStyle141: TcxStyle;
    cxStyle142: TcxStyle;
    cxStyle143: TcxStyle;
    cxStyle144: TcxStyle;
    cxStyle145: TcxStyle;
    cxStyle146: TcxStyle;
    cxStyle147: TcxStyle;
    cxStyle148: TcxStyle;
    cxStyle149: TcxStyle;
    cxStyle150: TcxStyle;
    cxStyle151: TcxStyle;
    cxStyle152: TcxStyle;
    cxStyle153: TcxStyle;
    cxStyle154: TcxStyle;
    cxStyle155: TcxStyle;
    cxStyle156: TcxStyle;
    cxStyle157: TcxStyle;
    cxStyle158: TcxStyle;
    cxStyle159: TcxStyle;
    cxStyle160: TcxStyle;
    cxStyle161: TcxStyle;
    cxStyle162: TcxStyle;
    cxStyle163: TcxStyle;
    cxStyle164: TcxStyle;
    cxStyle165: TcxStyle;
    cxStyle166: TcxStyle;
    cxStyle167: TcxStyle;
    cxStyle168: TcxStyle;
    cxStyle169: TcxStyle;
    cxStyle170: TcxStyle;
    cxStyle171: TcxStyle;
    cxStyle172: TcxStyle;
    cxStyle173: TcxStyle;
    cxStyle174: TcxStyle;
    cxStyle175: TcxStyle;
    cxStyle176: TcxStyle;
    cxStyle177: TcxStyle;
    cxStyle178: TcxStyle;
    cxStyle179: TcxStyle;
    cxStyle180: TcxStyle;
    cxStyle181: TcxStyle;
    cxStyle182: TcxStyle;
    cxStyle183: TcxStyle;
    cxStyle184: TcxStyle;
    cxStyle185: TcxStyle;
    cxStyle186: TcxStyle;
    cxStyle187: TcxStyle;
    cxStyle188: TcxStyle;
    cxStyle189: TcxStyle;
    cxStyle190: TcxStyle;
    cxStyle191: TcxStyle;
    cxStyle192: TcxStyle;
    cxStyle193: TcxStyle;
    cxStyle194: TcxStyle;
    cxStyle195: TcxStyle;
    cxStyle196: TcxStyle;
    cxStyle197: TcxStyle;
    cxStyle198: TcxStyle;
    cxStyle199: TcxStyle;
    cxStyle200: TcxStyle;
    cxStyle201: TcxStyle;
    cxStyle202: TcxStyle;
    cxStyle203: TcxStyle;
    cxStyle204: TcxStyle;
    cxStyle205: TcxStyle;
    cxStyle206: TcxStyle;
    cxStyle207: TcxStyle;
    cxStyle208: TcxStyle;
    cxStyle209: TcxStyle;
    cxStyle210: TcxStyle;
    cxStyle211: TcxStyle;
    cxStyle212: TcxStyle;
    cxStyle213: TcxStyle;
    cxStyle227: TcxStyle;
    cxChartStyle: TcxStyle;
    cxStyle230: TcxStyle;
    RedLight: TcxStyle;
    MoneyGreen: TcxStyle;
    cxStyle232: TcxStyle;
    cxStyle233: TcxStyle;
    cxStyle234: TcxStyle;
    cxStyle235: TcxStyle;
    cxStyle236: TcxStyle;
    cxStyle237: TcxStyle;
    cxStyle238: TcxStyle;
    cxStyle239: TcxStyle;
    cxStyle240: TcxStyle;
    cxStyle241: TcxStyle;
    cxStyle242: TcxStyle;
    cxStyle243: TcxStyle;
    cxStyle244: TcxStyle;
    cxStyle245: TcxStyle;
    cxStyle246: TcxStyle;
    cxStyle247: TcxStyle;
    cxStyle248: TcxStyle;
    cxStyle249: TcxStyle;
    cxStyle250: TcxStyle;
    cxStyle251: TcxStyle;
    cxStyle252: TcxStyle;
    cxStyle253: TcxStyle;
    cxStyle254: TcxStyle;
    cxStyle255: TcxStyle;
    cxStyle256: TcxStyle;
    cxStyle257: TcxStyle;
    cxStyle258: TcxStyle;
    cxStyle259: TcxStyle;
    cxStyle260: TcxStyle;
    cxStyle261: TcxStyle;
    cxStyle262: TcxStyle;
    cxStyle263: TcxStyle;
    cxStyle264: TcxStyle;
    cxStyle265: TcxStyle;
    cxStyle266: TcxStyle;
    cxStyle267: TcxStyle;
    cxStyle268: TcxStyle;
    cxStyle269: TcxStyle;
    cxStyle270: TcxStyle;
    cxStyle271: TcxStyle;
    cxStyle272: TcxStyle;
    cxStyle273: TcxStyle;
    cxStyle274: TcxStyle;
    cxStyle275: TcxStyle;
    cxStyle276: TcxStyle;
    cxStyle277: TcxStyle;
    cxStyle278: TcxStyle;
    cxStyle279: TcxStyle;
    cxStyle280: TcxStyle;
    cxStyle281: TcxStyle;
    cxStyle282: TcxStyle;
    cxStyle283: TcxStyle;
    cxStyle284: TcxStyle;
    LightBlue: TcxStyle;
    BoldText: TcxStyle;
    UserStyleSheet: TcxGridTableViewStyleSheet;
    DetailStyleSheet: TcxGridTableViewStyleSheet;
    MasterStyleSheet: TcxGridTableViewStyleSheet;
    ssPrekuEdno: TcxGridTableViewStyleSheet;
    Banded: TcxGridBandedTableViewStyleSheet;
    TableViewDefaultStyle: TcxGridTableViewStyleSheet;
    GridBandedTableViewStyleSheetDevExpress: TcxGridBandedTableViewStyleSheet;
    GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet;
    TreeListStyleSheetDevExpress: TcxTreeListStyleSheet;
    PivotGridStyleSheetDevExpress: TcxPivotGridStyleSheet;
    GridTableViewStyleSheetLogin: TcxGridTableViewStyleSheet;
    cxStylePortokalova: TcxStyle;
    cxStyleZelono: TcxStyle;
    cxStyleZolto: TcxStyle;
    tblPriemLabStavkinaodStatus: TFIBIntegerField;
    qGetParamPartner: TpFIBQuery;
    tblDogvoriCeni: TpFIBDataSet;
    dsDogvoriCeni: TDataSource;
    tblDogvoriCeniID: TFIBIntegerField;
    tblDogvoriCeniTIP_PARTNER: TFIBIntegerField;
    tblDogvoriCeniPARTNER: TFIBIntegerField;
    tblDogvoriCeniDATUM_OD: TFIBDateField;
    tblDogvoriCeniDATUM_DO: TFIBDateField;
    tblDogvoriCeniCENA: TFIBBCDField;
    tblDogvoriCeniTS_INS: TFIBDateTimeField;
    tblDogvoriCeniTS_UPD: TFIBDateTimeField;
    tblDogvoriCeniPARTNER_NAZIV: TFIBStringField;
    tblDogvoriCeniOPIS: TFIBStringField;
    tblDogvoriCeniUSR_INS: TFIBStringField;
    tblDogvoriCeniUSR_UPD: TFIBStringField;
    tblDogvoriCeniCUSTOM1: TFIBStringField;
    tblDogvoriCeniCUSTOM2: TFIBStringField;
    tblDogvoriCeniCUSTOM3: TFIBStringField;
    qDatumBTW: TpFIBQuery;
    tblPreglediStavkiDOGOVOR_ID: TFIBIntegerField;
    tblSifUslugi: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBBCDField1: TFIBBCDField;
    FIBDateTimeField1: TFIBDateTimeField;
    FIBDateTimeField2: TFIBDateTimeField;
    FIBStringField2: TFIBStringField;
    FIBStringField3: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBStringField4: TFIBStringField;
    FIBSmallIntField1: TFIBSmallIntField;
    FIBSmallIntField2: TFIBSmallIntField;
    dsSifUslugi: TDataSource;
    tblDogvoriCeniPAKET_ID: TFIBIntegerField;
    tblDogvoriCeniPAKET_NAZIV: TFIBStringField;
    tblPaketiKONTROLEN_PREGLED: TFIBSmallIntField;
    tblPreglediKONTROLEN_PREGLED: TFIBSmallIntField;
    tblPominatRokPRIEM: TFIBIntegerField;
    tblPreglediStavkiBR_MRSA: TFIBIntegerField;
    qUpdatePriemDogovorIDNULL: TpFIBQuery;
    tblAktivniPartneri: TpFIBDataSet;
    dsAktivniPartneri: TDataSource;
    tblAktivniPartneri���: TFIBIntegerField;
    tblAktivniPartneri�����: TFIBIntegerField;
    tblAktivniPartneri�����: TFIBStringField;
    tblAktivniPartneri�����: TFIBStringField;
    tblAktivniPartneri������: TFIBStringField;
    tblAktivniPartneriPAR_TIP: TFIBIntegerField;
    tblAktivniPartneriPAR_SIF: TFIBIntegerField;
    tblAktivniPartneriPAR_NAZ: TFIBStringField;
    tblAktivniPartneriPAR_TIPSIF: TFIBStringField;
    tblAktivniPartneriMESTO_NAZIV: TFIBStringField;
    tblAktivniPartneriPAR_ADR: TFIBStringField;
    tblPreglediDOGOVOR_ID: TFIBIntegerField;
    tblPreglediNAPLATENO: TFIBSmallIntField;
    tblPreglediFISKALNA: TFIBSmallIntField;
    tblPreglediSTORNA: TFIBSmallIntField;
    tblPreglediDATUM_PRESMETKA: TFIBDateField;
    tblPreglediIZNOS_VKUPNO: TFIBBCDField;
    qSetUpReBroj: TpFIBQuery;
    qSetUpRePregled: TpFIBQuery;
    frxMailExport: TfrxMailExport;
    IdSMTP_Mail: TIdSMTP;
    qSetupMail: TpFIBQuery;
    frxRezultatMail: TfrxReport;
    masterEMAIL: TFIBStringField;
    frxPDFExport1: TfrxPDFExport;
    MainMenu1: TMainMenu;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    SMTP: TIdSMTP;
    SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
    EmailMessage: TIdMessage;
    Email: TIdMessage;
    qUpdatePregledMail: TpFIBQuery;
    tblPratiMail: TpFIBDataSet;
    tblPratiMailStavki: TpFIBDataSet;
    tblPratiMailStavkiID: TFIBIntegerField;
    tblPratiMailStavkiBROJ: TFIBIntegerField;
    tblPratiMailStavkiPACIENT_NAZIV: TFIBStringField;
    tblPratiMailStavkiGODINA: TFIBSmallIntField;
    qUpdatePregledMail_NulL: TpFIBQuery;
    tblPratiMailTIP_PARTNER: TFIBIntegerField;
    tblPratiMailPARTNER: TFIBIntegerField;
    qUpdateMailPotvrda: TpFIBQuery;
    masterPRATI_MAIL_POTVRDA: TFIBSmallIntField;
    qUpdateMailPotvrdaPartner: TpFIBQuery;
    masterPRATI_MAIL_POTVRDA_PARTNER: TFIBSmallIntField;
    tblPratiMailPARTNER_NAZIV: TFIBStringField;
    tblPratiMailEMAIL: TFIBStringField;
    masterPARTNER_EMAIL: TFIBStringField;
    tblSetup: TpFIBDataSet;
    tblSetupPARAMETAR: TFIBStringField;
    tblSetupPARAM_VREDNOST: TFIBStringField;
    tblSetupVREDNOST: TFIBIntegerField;
    tblSetupP1: TFIBStringField;
    tblSetupP2: TFIBStringField;
    tblSetupV1: TFIBStringField;
    tblSetupV2: TFIBStringField;
    DCP_rc61: TDCP_rc6;
    DCP_sha11: TDCP_sha1;
    TimerToken: TTimer;
    qMestoID: TpFIBQuery;
    tblPreglediLEKAR: TFIBIntegerField;
    tblPreglediPRATI_MAIL_B: TFIBSmallIntField;
    tblPreglediPRATI_MAIL_PB: TFIBSmallIntField;
    qCenaUslugaPoDogovor: TpFIBQuery;
    tblPreglediStavkiM_DOGOVORI_ID: TFIBIntegerField;
    tblPaketiSTATUS: TFIBSmallIntField;

    procedure insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    procedure tblRezultatVREDNOSTSetText(Sender: TField; const Text: string);
    function zemiString3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
    function zemi3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
    function PecatiFiskalnaDll(iznos, stavka: string; tip: byte;  tip_placanje, tip_presmetka: integer): boolean;
    procedure tblPreglediStavkiBeforeDelete(DataSet: TDataSet);
    function decrypt(instr:AnsiString):AnsiString;
    procedure TimerTokenTimer(Sender: TObject);
    function GetSessionToken(username,passw : string):string;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }    re_broj,re_pregled,mail_setup, mail, mail_smtp, mail_pass, mail_pateka, token_codex, session_token_form, session_token, username_st, password_st, osiguruvanje, get_datum_aktivacija, osiguruvanje_old:String;
                               mail_smtp_port:Integer;
  end;

var
  dm: Tdm;
  re_broj,re_pregled:String;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmKonekcija, Utils, DaNe, dmResources, cxFiscalInterface;

{$R *.dfm}
procedure Tdm.tblPreglediStavkiBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort
    else
      begin
        dm.insert6(dm.pInsDelSelektiraniUslugi, 'PREGLED_ID','USLUGA_ID','TIP_DEL_INS', Null, Null,Null,dm.tblPreglediID.Value, dm.tblPreglediStavkiUSLUGA_ID.Value,0,Null,Null,Null);
      end;
end;

procedure Tdm.tblRezultatVREDNOSTSetText(Sender: TField; const Text: string);
begin
     tblRezultat.Edit;
     if Text <> '' then
        tblRezultatVREDNOST.Value:=Text;
     tblRezultat.Post;
end;

procedure Tdm.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.TimerTokenTimer(Sender: TObject);
begin
   session_token:=GetSessionToken(username_st, password_st);
end;

function Tdm.GetSessionToken(username,passw : string):string;

var pass:boolean;  sstoken:string;
 xml: IXMLDOMDocument2;
 xmlNode: IXMLDomNode;
 xmlListNode: IXMLDomNodeList;
 load_url:String;
 i, broj:integer;
begin
    try
        xml := CreateOleObject('Msxml2.DOMDocument.6.0') as IXMLDOMDocument2;
        xml.async := False;
        xml.setProperty('ServerHTTPRequest', true);
        dm.tblSetup.Open;
        dm.tblSetup.Locate('P2','GETSESSIONTOKEN',[])  ;

        pass:=true;

        if dm.token_codex<>'' then
         load_url:=dm.tblSetupV1.asstring+username+'&password='+passw+'&token='+dm.token_codex
        else
         load_url:=dm.tblSetupV1.asstring+username+'&password='+passw;

        xml.load(load_url);
        if xml.parseError.errorCode <> 0 then
        begin
         raise Exception.Create('XML Load error:' + xml.parseError.reason)   ;
         pass:=false;
        end;

        xmlListNode := xml.selectNodes('/session_token');

    except on Exception do   pass:=false
    end;
    if (pass=true) then
    begin
        try

          sstoken:=xmlListNode.item[0].text;

         except on Exception do
        end;
    end
    else sstoken:='';
    GetSessionToken:=sstoken;
end;



function Tdm.decrypt(instr:AnsiString):AnsiString;
   var
    i: integer;
    Cipher: TDCP_rc6;
    KeyStr, temp, rev: AnsiString;
  begin
    KeyStr:= '2ferwgvq34Cv334T3434tfweq34SDFVhjfghmhj.5780p.ll;poitui584bvbl/>M<';
    Cipher:= TDCP_rc6.Create(Self);
    Cipher.InitStr(KeyStr,TDCP_sha1);
    for i:=length(instr) downto 1 do
       temp:=temp+instr[i];
    result:= Cipher.DecryptString(temp);
    Cipher.Burn;
    Cipher.Free;

end;

procedure Tdm.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
    end;

function Tdm.zemi3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         ret := Proc.ParamByName(maxbr).Value;
         result :=ret;
end;

function Tdm.zemiString3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :string;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         ret := Proc.ParamByName(maxbr).Value;
         result :=ret;
end;

function Tdm.PecatiFiskalnaDll(iznos, stavka: string; tip: byte;
  tip_placanje, tip_presmetka: integer): boolean;
var strf :TMemoryStream;
    XMLDok1: IXMLDocument;
    fisk, telo, istavka, iart, ikol, icena, iddv, imkd, imin: IXMLNode;
    inapl, inac, itip, iiznos :IXMLNode;
    pom_fiskalna:Integer;
begin

    xmldok1:= TXMLDocument.Create(nil);
//  xmldok.LoadFromFile('c:\cxcache\cxxml.xml');
    xmldok1.Active:=true;
    xmldok1.Version:='1.0';
    xmldok1.Encoding:='utf-8';
    xmldok1.StandAlone:='yes';

    fisk:=xmldok1.DocumentElement;
    fisk:= xmldok1.AddChild('fiskalna');
    fisk.Attributes['smetkabroj']:='0';
    telo:=fisk.AddChild('telo');
    istavka:=telo.AddChild('stavka');
    iart:=istavka.AddChild('artikal');
    iart.Text:=stavka;
    ikol:=istavka.AddChild('kolicina');
    ikol.Text:='1.000';
    icena:=istavka.AddChild('cena');
    icena.Text:=iznos;
    iddv:=istavka.AddChild('ddv');
    iddv.Text:='0.00';
    imkd:=istavka.AddChild('mkdart');
    imkd.Text:='0';
    imin:=istavka.AddChild('mineral');
    imin.Text:='0';
    inapl:=fisk.AddChild('naplata');
    inac:=inapl.AddChild('nacin');
    itip:=inac.AddChild('tip');
    itip.Text:=IntToStr(tip_placanje);
    iiznos:=inac.AddChild('iznos');
    iiznos.Text:=iznos;

  //  xmldok1.SaveToFile('c:\partic.xml');


    try
       strf:=TMemoryStream.Create;
       xmldok1.SaveToStream(strf);
       xmldok1.Active:=false;
       xmldok1:=nil;

    // fiskalna
    if(tip=0) then
    begin
        if(fpFiskalna(strf)>0) then
          begin
            PecatiFiskalnaDll:=true;
            pom_fiskalna:=1;
          end
        else
          begin
            PecatiFiskalnaDll:=false;
            pom_fiskalna:=0;
          end
    end
    else
    begin
        if(fpStorna(strf)>0) then
          begin
            PecatiFiskalnaDll:=true;
            pom_fiskalna:=1;
          end
        else
          begin
            PecatiFiskalnaDll:=false;
            pom_fiskalna:=0;
          end
    end;
    finally
       //strf.Free;
       FreeAndNil(strf);
    end;

    if pom_fiskalna = 1 then
    begin
      if tip_presmetka=1 then
          begin
             dm.tblPregledi.Edit;
             if tip = 0 then
                begin
                  dm.tblPreglediFISKALNA.Value:=dm.tblPreglediFISKALNA.Value+1;
                  dm.tblPreglediDATUM_PRESMETKA.Value:=Now;
                 // dm.tblPreglediNAPLATENO.Value:=1;
                end
             else
                begin
                  dm.tblPreglediSTORNA.Value:=dm.tblPreglediSTORNA.Value+1; //ova mi kazuva deka e ispe;atena fiskalna.
                //  dm.tblPreglediNAPLATENO.Value:=0;
                end;
             dm.tblPregledi.Post;
          end
    end

end;


end.
