unit PratiIZvestuvanjePoMail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxPC, cxControls,
  RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar, dxRibbonStatusBar, ExtCtrls,
  cxGraphics, StdCtrls, cxDBEdit, cxDropDownEdit, cxCalendar, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ActnList, ToolWin, ActnMan,
  ActnCtrls, RibbonActnCtrls, ActnMenus, RibbonActnMenus, cxGridExportLink, cxExport,
  RibbonSilverStyleActnCtrls, RibbonObsidianStyleActnCtrls, ComCtrls,
  cxDBExtLookupComboBox, DBCtrls, cxMemo, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  dxRibbon, dxBar, cxBarEditItem, Menus, cxButtons, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  PlatformDefaultStyleActnCtrls, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, dxRibbonCustomizationForm,
  System.Actions;

type
  TfrmPratiIzvestuvanjePoMail = class(TForm)
    Panel2: TPanel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView2: TcxGridDBTableView;
    ActionManager1: TActionManager;
    aIspratiPoMail: TAction;
    aPregledajGoBaraweto: TAction;
    aIzlez: TAction;
    GroupBox1: TGroupBox;
    Tekst: TcxDBMemo;
    Label5: TLabel;
    Naslov: TcxDBMemo;
    Label4: TLabel;
    Label6: TLabel;
    PKontaktNaziv: TcxExtLookupComboBox;
    PKontaktID: TcxDBTextEdit;
    Label3: TLabel;
    PPartner: TcxExtLookupComboBox;
    PPartnerID: TcxDBTextEdit;
    PTipPartner: TcxDBTextEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Firma: TcxTextEdit;
    ImePrezime: TcxTextEdit;
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    aPomos: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    aPreprati: TAction;
    aBrisi: TAction;
    aEdit: TAction;
    MailSender: TcxDBTextEdit;
    MailRecipient: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aZacuvajGoIzgledot: TAction;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    Action1: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action2: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    Zacuvaj: TcxButton;
    Otkazi: TcxButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TIP_NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ADRESA: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TEL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1FAX: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DANOCEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTO: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1IME: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1PREZIME: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TATKO: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1LOGO: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1RE: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1LOGOTEXT: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1STATUS: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MAIL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DRZAVA_NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ADRESA_LATITUDE: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ADRESA_LONGITUDE: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1PRAVNO_LICE: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1CENOVNIK: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2PARTNER_NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MAIL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MOBILEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TEL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1NASLOV: TcxGridDBColumn;
    cxGrid1DBTableView1TEXT: TcxGridDBColumn;
    cxGrid1DBTableView1MAIL_SENDER: TcxGridDBColumn;
    cxGrid1DBTableView1MAIL_RECIPIENT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKTNAZIV: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aIspratiPoMailExecute(Sender: TObject);
    procedure aPregledajGoBarawetoExecute(Sender: TObject);
    procedure PTipPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure PPartnerIDPropertiesEditValueChanged(Sender: TObject);
    procedure PKontaktIDPropertiesEditValueChanged(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure aPrepratiExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aEditExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aZacuvajGoIzgledotExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    protected
    prva, posledna :TWinControl;
  end;

var
  frmPratiIzvestuvanjePoMail: TfrmPratiIzvestuvanjePoMail;
  StateActive:TDataSetState;

implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, TipPartner,
  Utils, Partner, Kontakt, dmResources;

{$R *.dfm}

procedure TfrmPratiIzvestuvanjePoMail.aPrepratiExecute(Sender: TObject);
var RptStream :TStream;
    pid, ptip, kid: integer;  nas, par, psender, precipient:string; tek:Extended;
begin
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//        begin
//           Panel2.Enabled:=True;
//           Panel1.Enabled:=False;
//           dxRibbon1.Enabled:=False;
//
//           pid:=dm.tblNabavkaMailPARTNER.Value;
//           ptip:=dm.tblNabavkaMailTIP_PARTNER.Value;
//           psender:=dm.tblNabavkaMailMAIL_SENDER.Value;
//           precipient:=dm.tblNabavkaMailMAIL_RECIPIENT.Value;
//
//           if dm.tblNabavkaMailKONTAKT.AsVariant <> Null then
//              kid:=dm.tblNabavkaMailKONTAKT.Value
//           else
//              kid:=0;
//           nas:=dm.tblNabavkaMailNASLOV.Value;
//           par:=dm.tblNabavkaMailNAZIV.Value;
//
//           Firma.Text:=dm.FirmiNAZIV.Value;
//           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//           ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
//
//           dm.tblNabavkaMail.Insert;
//           StateActive:=dsInsert;
//
//           dm.tblNabavkaMailTIP_PARTNER.Value:=ptip;
//           dm.tblNabavkaMailPARTNER.Value:=pid;
//           dm.tblNabavkaMailMAIL_SENDER.Value:= psender;
//           dm.tblNabavkaMailMAIL_RECIPIENT.Value:= precipient;
//           if kid <> 0 then
//             dm.tblNabavkaMailKONTAKT.Value:=kid;
//           dm.tblNabavkaMailNASLOV.Value:=nas;
//           PPartner.Text:=par;
//           MailRecipient.SetFocus;
//        end
//     else ShowMessage('����������� ������');
end;

procedure TfrmPratiIzvestuvanjePoMail.aSnimiIzgledExecute(Sender: TObject);
begin
        zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
        ZacuvajFormaIzgled(self);
end;

procedure TfrmPratiIzvestuvanjePoMail.aSnimiPecatenjeExecute(Sender: TObject);
begin
      zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPratiIzvestuvanjePoMail.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPratiIzvestuvanjePoMail.aZacuvajGoIzgledotExecute(Sender: TObject);
begin
      zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmPratiIzvestuvanjePoMail.aZapisiExecute(Sender: TObject);
var RptStream :TStream;
begin
//      if StateActive in [dsEdit] then
//        begin
//           if (dm.Validacija(panel2) = false) then
//              begin
//                 dm.tblNabavkaMail.post;
//                 StateActive:=dsBrowse;
//                 dm.tblNabavkaMail.FullRefresh;
//                 Panel2.Enabled:=False;
//                 dxRibbon1.Enabled:=True;
//                 Panel1.Enabled:=True;
//                 cxGrid1.SetFocus;
//                end;
//        end;
//
//      if (dm.Validacija(panel2) = false) and (StateActive in [dsInsert]) then
//         begin
//            dm.tblReportDizajn.ParamByName('id').Value:=4013;
//            dm.tblReportDizajn.FullRefresh;
//
//            dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
//            dm.tblSpecifikacijaNaNabavka.FullRefresh;
//
//            RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
//            dm.frxReport.LoadFromStream(RptStream) ;
//
//            dm.frxReport.PrepareReport();
//
//            dm.frxMailExport2.Login:=MailSender.Text;
//            dm.frxMailExport2.Password:=smpt_pass;
//            dm.frxMailExport2.SmtpHost:=smpt_mail;
//            dm.frxMailExport2.FromMail:=MailSender.Text; //from
//            dm.frxMailExport2.FromName:=ImePrezime.Text;
//            dm.frxMailExport2.FromCompany:=Firma.Text;
//            dm.frxMailExport2.Address:=MailRecipient.Text; //to
//            dm.frxMailExport2.Subject:=Naslov.Text;
//            dm.frxMailExport2.Lines.Text:=Tekst.Text;
//            dm.frxMailExport2.ExportFilter:=dm.frxJPEGExport1;
//
//            dm.frxReport.Export(dm.frxMailExport2);
//
//            dm.tblNabavkaMailID.Value:=dm.zemiPonudaStavkaID(dm.NabavkaMailID,Null, Null, Null, Null, Null, Null, 'MAXBR');
//            dm.tblNabavkaMailNABAVKA_ID.Value:=dm.tblNabaviID.Value;
//            dm.tblNabavkaMail.post;
//            StateActive:=dsBrowse;
//            dm.tblNabavkaMail.FullRefresh;
//            dm.RestoreControls(panel2);
//
//            Panel2.Enabled:=False;
//            dxRibbon1.Enabled:=True;
//            Panel1.Enabled:=True;
//            cxGrid1.SetFocus;
//         end;
end;

procedure TfrmPratiIzvestuvanjePoMail.aBrisiExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//        begin
//           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
//           if frmDaNe.ShowModal =mrYes then
//             cxGrid1DBTableView1.DataController.DataSet.Delete();
//        end
//     else ShowMessage('����������� ������ !!!');
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then
//       begin
//          panel2.Enabled:=true;
//          Firma.Text:=dm.FirmiNAZIV.Value;
//          dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//          PPartner.Text:= '';
//          PKontaktNaziv.Text:= '';
//          panel2.Enabled:=false;
//       end;
end;

procedure TfrmPratiIzvestuvanjePoMail.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPratiIzvestuvanjePoMail.aEditExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//        begin
//           Panel1.Enabled:=False;
//           Panel2.Enabled:=True;
//           dxRibbon1.Enabled:=False;
//           //GroupBox2.Enabled:=False;
//           PTipPartner.SetFocus;
//
//           Firma.Text:=dm.FirmiNAZIV.Value;
//           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//           ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
//
//           dm.tblNabavkaMail.Edit;
//           StateActive:=dsEdit;
//        end
//     else ShowMessage('����������� ������ !!!');
end;

procedure TfrmPratiIzvestuvanjePoMail.aIspratiPoMailExecute(Sender: TObject);
begin
//     Panel2.Enabled:=True;
//     Panel1.Enabled:=False;
//     dxRibbon1.Enabled:=False;
//
//     Firma.Text:='';
//     ImePrezime.Text:='';
//
//     dm.tblNabavkaMail.Insert;
//     StateActive:=dsInsert;
//     PTipPartner.SetFocus;
//
//     Firma.Text:=dm.FirmiNAZIV.Value;
//     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//     ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
//     dm.tblNabavkaMailMAIL_SENDER.Value:=dm.tblPartnerMAIL.Value;
//
//     PPartner.Text:='';
//     PKontaktNaziv.Text:= '';
end;

procedure TfrmPratiIzvestuvanjePoMail.aIzlezExecute(Sender: TObject);
begin
//     if Panel2.Enabled = true then
//        begin
//           dm.tblNabavkaMail.Cancel;
//           dm.RestoreControls(panel2);
//           StateActive:=dsBrowse;
//
//           //GroupBox2.Enabled:=true;
//           Panel2.Enabled:=False;
//           Panel1.Enabled:=True;
//           dxRibbon1.Enabled:=True;
//
//           if (PTipPartner.Text = '') and (PPartnerID.text = '') then
//            begin
//               PPartner.Text := '';
//               PKontaktNaziv.Text := '';
//            end;
//
//           if PKontaktNaziv.Text = '' then
//            begin
//               PKontaktNaziv.Text := '';
//            end;
//
//           Firma.Text:=dm.FirmiNAZIV.Value;
//           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//           cxGrid1.SetFocus;
//        end
//      else
//        Close;
end;

procedure TfrmPratiIzvestuvanjePoMail.aOtkaziExecute(Sender: TObject);
begin
//     dm.tblNabavkaMail.Cancel;
//     StateActive:=dsBrowse;
//     dm.RestoreControls(panel2);
//
//     //GroupBox2.Enabled:=true;
//     Panel2.Enabled:=False;
//     Panel1.Enabled:=True;
//     dxRibbon1.Enabled:=True;
//
//     if (PTipPartner.Text = '') and (PPartnerID.text = '') then
//      begin
//         PPartner.Text := '';
//         PKontaktNaziv.Text := '';
//      end;
//
//      if PKontaktNaziv.Text = '' then
//      begin
//         PKontaktNaziv.Text := '';
//      end;
//
//     Firma.Text:=dm.FirmiNAZIV.Value;
//     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//     cxGrid1.SetFocus;
end;

procedure TfrmPratiIzvestuvanjePoMail.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPratiIzvestuvanjePoMail.aPecatiTabelaExecute(Sender: TObject);
begin
//  dxComponentPrinter1Link1.ReportTitle.Text := Caption;
//
//  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
//  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
//  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
//
//  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
//  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);
//
//  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPratiIzvestuvanjePoMail.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPratiIzvestuvanjePoMail.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(129);
end;

procedure TfrmPratiIzvestuvanjePoMail.aPregledajGoBarawetoExecute(Sender: TObject);
begin
//if frmPominatRok.cbDatum.EditValue <> Null then
//   if frmPominatRok.PARTNER_NAZIV.Text <> '' then
//      begin
//        try
//          dmRes.Spremi('SAN',7);
//          dmKon.tblSqlReport.ParamByName('tp').Value:=frmPominatRok.TIP_PARTNER.EditValue;
//          dmKon.tblSqlReport.ParamByName('p').Value:=frmPominatRok.PARTNER.EditValue;
//          dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
//          dmKon.tblSqlReport.ParamByName('datum').Value:=frmPominatRok.cbDatum.EditValue;
//          dmKon.tblSqlReport.ParamByName('godina').Value:=dmKon.godina;
//          dmKon.tblSqlReport.ParamByName('param').Value:=0;
//          dmKon.tblSqlReport.Open;
//
//          dmRes.frxReport1.Variables.AddVariable('VAR', 'datum', QuotedStr(dateToStr(frmPominatRok.bDatum.EditValue)));
//          dmRes.frxReport1.ShowReport();
//        except
//          ShowMessage('�� ���� �� �� ������� ���������!');
//        end
//      end
//    else ShowMessage('�������� ������� !')
//   else ShowMessage('�������� ����� !');
end;

procedure TfrmPratiIzvestuvanjePoMail.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//     dm.tblNabavkaMail.Close;
end;

procedure TfrmPratiIzvestuvanjePoMail.FormCreate(Sender: TObject);
begin
//     dm.KontaktPartner.Open;
//     dm.tblPartner.Close;
//     dm.tblPartner.ParamByName('tipPartner').Value:='%';
//     dm.tblPartner.Open;
//     dm.tblNabavkaMail.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
//     dm.tblNabavkaMail.Open;
end;

procedure TfrmPratiIzvestuvanjePoMail.FormShow(Sender: TObject);
begin
       dmRes.GenerateFormReportsAndActions(dxRibbon1,dxRibbon1Tab1,dxBarManager1,cxGrid1DBTableView1, Name);
//     dm.frxReport.Script.Clear;
//     dm.frxReport.Clear;
//     dm.PrvPosledenTab(panel2,posledna,prva);
//     Firma.Text:=dm.FirmiNAZIV.Value;
//     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
//     ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
//     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
end;

procedure TfrmPratiIzvestuvanjePoMail.PKontaktIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if PKontaktID.Text <> '' then
        begin
           PKontaktNaziv.EditValue:=StrToInt(PKontaktID.Text);
        end;
end;

procedure TfrmPratiIzvestuvanjePoMail.PPartnerIDPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
//    if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
//       begin
//          pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
//          if pom = 1  then
//            begin
//               dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
//               PPartner.Text:= dm.tblPartnerNAZIV.Value;
//            end
//         else PPartner.Text:= '';
//    end;
end;

procedure TfrmPratiIzvestuvanjePoMail.PPartnerPropertiesEditValueChanged(
  Sender: TObject);
begin
//     if PPartner.Text <> '' then
//        begin
//           PTipPartner.Text :=IntToStr(dm.tblPartnerTIP_PARTNER.Value);
//           PPartnerID.Text:=IntToStr(dm.tblPartnerID.Value);
//        end;
//     PKontaktNaziv.Text:='';
//     PKontaktID.Text:='';
//     dm.KontaktPartner.ParamByName('tippartner').Value:=dm.tblPartnerTIP_PARTNER.Value;
//     dm.KontaktPartner.ParamByName('partner').Value:=dm.tblPartnerID.Value;
//     dm.KontaktPartner.FullRefresh;
end;

procedure TfrmPratiIzvestuvanjePoMail.PTipPartnerPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
//     if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
//        begin
//           pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
//           if pom = 1  then
//             begin
//                dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
//                PPartner.Text:= dm.tblPartnerNAZIV.Value;
//             end
//           else PPartner.Text:= '';
//        end;
end;

procedure TfrmPratiIzvestuvanjePoMail.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
//    kom := Sender as TWinControl;
//    case Key of
//        VK_DOWN:
//          begin
//            PostMessage(Handle,WM_NextDlgCtl,0,0);
//          end;
//        VK_UP:
//          begin
//            PostMessage(Handle,WM_NextDlgCtl,1,0);
//          end;
//        VK_RETURN:
//          begin
//            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
//          end;
//        VK_INSERT:begin
//                   if (sender = PPartner) or (Sender = PPartnerID )then
//                      begin
//                         frmPartner:=TfrmPartner.Create(Application);
//                         frmPartner.Tag:=4;
//                         frmPartner.ShowModal;
//                         frmPartner.Free;
//                         PPartner.Text:=dm.tblPartnerNAZIV.Value;
//                         dm.tblNabavkaMailPARTNER.Value:=dm.tblPartnerID.Value;
//                         dm.tblNabavkaMailTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
//                         PKontaktID.SetFocus;
//                         dm.tblPartner.ParamByName('tipPartner').Value:='%';
//                         dm.tblPartner.FullRefresh;
//                      end;
//                   if (sender = PTipPartner) then
//                      begin
//                         frmTipPartner:=TfrmTipPartner.Create(Application);
//                         frmTipPartner.Tag:=1;
//                         frmTipPartner.ShowModal;
//                         frmTipPartner.Free;
//                         dm.tblNabavkaMailTIP_PARTNER.Value:=dmMat.tblTipPartnerID.Value;
//                         PPartnerID.SetFocus;
//                      end;
//                   if (Sender = PKontaktID) or (Sender =PKontaktNaziv) then
//                      begin
//                         if (PPartner.Text <> '') and (PPartnerID.Text <> '') and (PTipPartner.Text <> '') then
//                            begin
//                               dm.tblKontakt.close;
//                               dm.tblKontakt.ParamByName('tipPartner').Value:=StrToInt(PTipPartner.Text);
//                               dm.tblKontakt.ParamByName('Partner').Value:=StrToInt(PPartnerID.Text);
//                               dm.tblKontakt.Open;
//                               frmKontakt:=TfrmKontakt.Create(Application);
//                               dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
//                               frmKontakt.Tag:= 1;
//                               frmKontakt.ShowModal;
//                               frmKontakt.Free;
//                               dm.KontaktPartner.ParamByName('tippartner').Value:=StrToInt(PTipPartner.Text);
//                               dm.KontaktPartner.ParamByName('partner').Value:=StrToInt(PPartnerID.Text);
//                               dm.KontaktPartner.FullRefresh;
//                               dm.tblNabavkaMailKONTAKT.Value:=dm.tblKontaktID.Value;
//                               PKontaktNaziv.EditValue:=dm.tblKontaktID.Value;
//                            end;
//                      end;
//          end;
//
//    end;
end;

procedure TfrmPratiIzvestuvanjePoMail.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPratiIzvestuvanjePoMail.cxDBTextEditAllExit(Sender: TObject);
begin
//    TEdit(Sender).Color:=clWhite;
//
//    if Sender = PKontaktNaziv then
//      begin
//        if (PKontaktNaziv.Text <> '') and (StateActive in [dsInsert, dsEdit]) and (panel2.Enabled = true)then
//        begin
//           PKontaktID.Text:=IntToStr(dm.KontaktPartnerID.Value);
//           if dm.KontaktPartnerMAIL.AsVariant <> Null then
//              dm.tblNabavkaMailMAIL_RECIPIENT.Value:= dm.KontaktPartnerMAIL.Value;
//        end;
//      end;
end;

end.
