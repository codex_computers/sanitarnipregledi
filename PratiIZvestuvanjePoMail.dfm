object frmPratiIzvestuvanjePoMail: TfrmPratiIzvestuvanjePoMail
  Left = 0
  Top = 0
  ClientHeight = 620
  ClientWidth = 867
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 126
    Width = 867
    Height = 254
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Enabled = False
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      867
      254)
    object GroupBox1: TGroupBox
      Left = 12
      Top = 6
      Width = 429
      Height = 235
      Caption = #1044#1086
      TabOrder = 0
      object Label5: TLabel
        Left = 26
        Top = 170
        Width = 34
        Height = 13
        Caption = #1058#1077#1082#1089#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 18
        Top = 126
        Width = 42
        Height = 13
        Caption = #1053#1072#1089#1083#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 30
        Top = 99
        Width = 30
        Height = 13
        Caption = 'eMail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 11
        Top = 59
        Width = 49
        Height = 13
        Caption = #1050#1086#1085#1090#1072#1082#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 10
        Top = 32
        Width = 50
        Height = 13
        Caption = #1055#1072#1088#1090#1085#1077#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Tekst: TcxDBMemo
        Left = 66
        Top = 167
        DataBinding.DataField = 'TEXT'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 58
        Width = 352
      end
      object Naslov: TcxDBMemo
        Tag = 1
        Left = 66
        Top = 123
        DataBinding.DataField = 'NASLOV'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 38
        Width = 352
      end
      object PKontaktNaziv: TcxExtLookupComboBox
        Left = 108
        Top = 56
        BeepOnEnter = False
        Properties.View = cxGridViewRepository1DBTableView2
        Properties.KeyFieldNames = 'ID'
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 309
      end
      object PKontaktID: TcxDBTextEdit
        Left = 66
        Top = 56
        BeepOnEnter = False
        DataBinding.DataField = 'KONTAKT'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        Properties.OnEditValueChanged = PKontaktIDPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clBackground
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 41
      end
      object PPartner: TcxExtLookupComboBox
        Tag = 1
        Left = 150
        Top = 29
        BeepOnEnter = False
        Properties.DropDownAutoSize = True
        Properties.View = cxGridViewRepository1DBTableView1
        Properties.KeyFieldNames = 'TIP_PARTNER;ID'
        Properties.OnEditValueChanged = PPartnerPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 267
      end
      object PPartnerID: TcxDBTextEdit
        Tag = 1
        Left = 108
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'PARTNER'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        Properties.OnEditValueChanged = PPartnerIDPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clBackground
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 41
      end
      object PTipPartner: TcxDBTextEdit
        Tag = 1
        Left = 66
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'TIP_PARTNER'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        Properties.OnEditValueChanged = PTipPartnerPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clBackground
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 41
      end
      object MailRecipient: TcxDBTextEdit
        Tag = 1
        Left = 66
        Top = 96
        BeepOnEnter = False
        DataBinding.DataField = 'MAIL_RECIPIENT'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        Properties.OnEditValueChanged = PTipPartnerPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clBackground
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 351
      end
    end
    object GroupBox2: TGroupBox
      Left = 447
      Top = 65
      Width = 410
      Height = 101
      Caption = #1054#1076
      TabOrder = 1
      object Label1: TLabel
        Left = 17
        Top = 46
        Width = 83
        Height = 13
        Caption = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 63
        Top = 19
        Width = 37
        Height = 13
        Caption = #1060#1080#1088#1084#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 70
        Top = 73
        Width = 30
        Height = 13
        Caption = 'eMail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Firma: TcxTextEdit
        Tag = 1
        Left = 106
        Top = 16
        BeepOnEnter = False
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 295
      end
      object ImePrezime: TcxTextEdit
        Left = 106
        Top = 43
        BeepOnEnter = False
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 295
      end
      object MailSender: TcxDBTextEdit
        Tag = 1
        Left = 106
        Top = 70
        BeepOnEnter = False
        DataBinding.DataField = 'MAIL_SENDER'
        DataBinding.DataSource = dm.dsIzvestuvanjePoMail
        Properties.OnEditValueChanged = PTipPartnerPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clBackground
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 295
      end
    end
    object Zacuvaj: TcxButton
      Left = 640
      Top = 194
      Width = 127
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Caption = #1048#1089#1087#1088#1072#1090#1080'/'#1047#1072#1095#1091#1074#1072#1112
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
    end
    object Otkazi: TcxButton
      Left = 773
      Top = 194
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 380
    Width = 867
    Height = 217
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 863
      Height = 213
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzvestuvanjePoMail
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 70
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 75
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 92
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Width = 99
        end
        object cxGrid1DBTableView1KONTAKT: TcxGridDBColumn
          DataBinding.FieldName = 'KONTAKT'
          Width = 100
        end
        object cxGrid1DBTableView1NASLOV: TcxGridDBColumn
          DataBinding.FieldName = 'NASLOV'
          Width = 200
        end
        object cxGrid1DBTableView1TEXT: TcxGridDBColumn
          DataBinding.FieldName = 'TEXT'
          Width = 200
        end
        object cxGrid1DBTableView1MAIL_SENDER: TcxGridDBColumn
          DataBinding.FieldName = 'MAIL_SENDER'
          Width = 200
        end
        object cxGrid1DBTableView1MAIL_RECIPIENT: TcxGridDBColumn
          DataBinding.FieldName = 'MAIL_RECIPIENT'
          Width = 200
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1KONTAKTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'KONTAKTNAZIV'
          Width = 200
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 597
    Width = 867
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080
        Width = 140
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Text = 'Ctrl+Alt+S - '#1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
        Width = 170
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F1 - '#1055#1086#1084#1086#1096', Esc - '#1054#1090#1082#1072#1078#1080'/'#1048#1079#1083#1077#1079
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 867
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 3
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 504
    Top = 152
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
      end
      object cxGridViewRepository1DBTableView1TIP_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_NAZIV'
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
      end
      object cxGridViewRepository1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV_SKRATEN'
      end
      object cxGridViewRepository1DBTableView1ADRESA: TcxGridDBColumn
        DataBinding.FieldName = 'ADRESA'
      end
      object cxGridViewRepository1DBTableView1TEL: TcxGridDBColumn
        DataBinding.FieldName = 'TEL'
      end
      object cxGridViewRepository1DBTableView1FAX: TcxGridDBColumn
        DataBinding.FieldName = 'FAX'
      end
      object cxGridViewRepository1DBTableView1DANOCEN: TcxGridDBColumn
        DataBinding.FieldName = 'DANOCEN'
      end
      object cxGridViewRepository1DBTableView1MESTO: TcxGridDBColumn
        DataBinding.FieldName = 'MESTO'
      end
      object cxGridViewRepository1DBTableView1MESTO_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MESTO_NAZIV'
      end
      object cxGridViewRepository1DBTableView1IME: TcxGridDBColumn
        DataBinding.FieldName = 'IME'
      end
      object cxGridViewRepository1DBTableView1PREZIME: TcxGridDBColumn
        DataBinding.FieldName = 'PREZIME'
      end
      object cxGridViewRepository1DBTableView1TATKO: TcxGridDBColumn
        DataBinding.FieldName = 'TATKO'
      end
      object cxGridViewRepository1DBTableView1LOGO: TcxGridDBColumn
        DataBinding.FieldName = 'LOGO'
      end
      object cxGridViewRepository1DBTableView1RE: TcxGridDBColumn
        DataBinding.FieldName = 'RE'
      end
      object cxGridViewRepository1DBTableView1LOGOTEXT: TcxGridDBColumn
        DataBinding.FieldName = 'LOGOTEXT'
      end
      object cxGridViewRepository1DBTableView1STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'STATUS'
      end
      object cxGridViewRepository1DBTableView1MAIL: TcxGridDBColumn
        DataBinding.FieldName = 'MAIL'
      end
      object cxGridViewRepository1DBTableView1DRZAVA_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'DRZAVA_NAZIV'
      end
      object cxGridViewRepository1DBTableView1AKTIVEN: TcxGridDBColumn
        DataBinding.FieldName = 'AKTIVEN'
      end
      object cxGridViewRepository1DBTableView1ADRESA_LATITUDE: TcxGridDBColumn
        DataBinding.FieldName = 'ADRESA_LATITUDE'
      end
      object cxGridViewRepository1DBTableView1ADRESA_LONGITUDE: TcxGridDBColumn
        DataBinding.FieldName = 'ADRESA_LONGITUDE'
      end
      object cxGridViewRepository1DBTableView1PRAVNO_LICE: TcxGridDBColumn
        DataBinding.FieldName = 'PRAVNO_LICE'
      end
      object cxGridViewRepository1DBTableView1CENOVNIK: TcxGridDBColumn
        DataBinding.FieldName = 'CENOVNIK'
      end
    end
    object cxGridViewRepository1DBTableView2: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmMat.dsKontakt
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView2ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
      end
      object cxGridViewRepository1DBTableView2NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
      end
      object cxGridViewRepository1DBTableView2TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
      end
      object cxGridViewRepository1DBTableView2PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER'
      end
      object cxGridViewRepository1DBTableView2PARTNER_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER_NAZIV'
      end
      object cxGridViewRepository1DBTableView2MAIL: TcxGridDBColumn
        DataBinding.FieldName = 'MAIL'
      end
      object cxGridViewRepository1DBTableView2MOBILEN: TcxGridDBColumn
        DataBinding.FieldName = 'MOBILEN'
      end
      object cxGridViewRepository1DBTableView2TEL: TcxGridDBColumn
        DataBinding.FieldName = 'TEL'
      end
      object cxGridViewRepository1DBTableView2OPIS: TcxGridDBColumn
        DataBinding.FieldName = 'OPIS'
      end
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = aPregledajGoBaraweto
            ImageIndex = 19
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            Action = aIspratiPoMail
            ImageIndex = 17
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aPreprati
            ImageIndex = 14
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            Action = aZacuvajGoIzgledot
            ImageIndex = 9
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aPomos
            ImageIndex = 24
            ShortCut = 112
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aIzlez
            ImageIndex = 20
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            Action = aEdit
            ImageIndex = 12
            ShortCut = 117
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aBrisi
            ImageIndex = 11
            ShortCut = 119
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            ChangesAllowed = [caModify]
            Items = <
              item
                Caption = '&ActionClientItem0'
              end>
            Caption = '&ActionClientItem0'
            KeyTip = 'F'
          end>
        AutoSize = False
      end>
    Left = 624
    Top = 176
    StyleName = 'Platform Default'
    object aPregledajGoBaraweto: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1075#1086' '#1073#1072#1088#1072#1114#1077#1090#1086
      ImageIndex = 19
      OnExecute = aPregledajGoBarawetoExecute
    end
    object aIspratiPoMail: TAction
      Caption = #1048#1089#1087#1088#1072#1090#1080
      ImageIndex = 17
      OnExecute = aIspratiPoMailExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 20
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      ShortCut = 112
      OnExecute = aPomosExecute
    end
    object aPreprati: TAction
      Caption = #1055#1088#1077#1087#1088#1072#1090#1080
      ImageIndex = 14
      OnExecute = aPrepratiExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aEdit: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aEditExecute
    end
    object aZacuvajGoIzgledot: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 9
      SecondaryShortCuts.Strings = (
        'Ctrl+Alt+S')
      OnExecute = aZacuvajGoIzgledotExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 760
    Top = 200
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 125
    Top = 65217
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object Action1: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object Action2: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 413
    Top = 65201
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1087#1086#1085#1091#1076#1072
      CaptionButtons = <>
      DockedLeft = 232
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 365
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 120
      FloatClientHeight = 130
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 572
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 141
      FloatClientHeight = 216
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 901
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = 'eMail'
      CaptionButtons = <>
      DockedLeft = 94
      DockedTop = 0
      FloatLeft = 901
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aEdit
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = Action2
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPregledajGoBaraweto
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aIspratiPoMail
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1085#1086#1074
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPreprati
      Category = 0
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 480
    Top = 296
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44287.332915000000000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
end
