inherited frmAntibiogramSifrarnik: TfrmAntibiogramSifrarnik
  Caption = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
  ClientHeight = 587
  ClientWidth = 975
  ExplicitWidth = 991
  ExplicitHeight = 625
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 975
    Height = 274
    ExplicitWidth = 975
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Width = 971
      Height = 270
      ExplicitWidth = 971
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsAntibiogram
        OptionsData.Editing = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Options.Editing = False
          Width = 66
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 178
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 397
        end
        object cxGrid1DBTableView1LEGENDA: TcxGridDBColumn
          DataBinding.FieldName = 'LEGENDA'
          Options.Editing = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 163
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 215
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 203
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 233
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Width = 975
    Height = 164
    ExplicitTop = 400
    ExplicitWidth = 975
    ExplicitHeight = 164
    object Label2: TLabel [1]
      Left = 37
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 44
      Top = 75
      Width = 33
      Height = 13
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 20
      Top = 114
      Width = 57
      Height = 13
      Caption = #1051#1077#1075#1077#1085#1076#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsAntibiogram
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
    end
    inherited OtkaziButton: TcxButton
      Left = 884
      Top = 124
      TabOrder = 5
      ExplicitLeft = 884
      ExplicitTop = 124
    end
    inherited ZapisiButton: TcxButton
      Left = 803
      Top = 124
      TabOrder = 4
      ExplicitLeft = 803
      ExplicitTop = 124
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsAntibiogram
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 606
    end
    object OPIS: TcxDBMemo
      Left = 83
      Top = 72
      Hint = #1054#1087#1080#1089' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsAntibiogram
      Properties.WantReturns = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 33
      Width = 606
    end
    object LEGENDA: TcxDBTextEdit
      Left = 83
      Top = 111
      Hint = #1051#1077#1075#1077#1085#1076#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      BeepOnEnter = False
      DataBinding.DataField = 'LEGENDA'
      DataBinding.DataSource = dm.dsAntibiogram
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 606
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 975
    ExplicitWidth = 975
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 564
    Width = 975
    ExplicitTop = 564
    ExplicitWidth = 975
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 288
  end
  inherited PopupMenu1: TPopupMenu
    Left = 336
    Top = 312
  end
  inherited dxBarManager1: TdxBarManager
    Left = 424
    Top = 272
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 291
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 536
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = ' '
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 1009
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDefAntibiogram
      Category = 0
      ScreenTip = tipDefAntibiogram
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 272
    object aDefAntibiogram: TAction
      Caption = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      ImageIndex = 66
      OnExecute = aDefAntibiogramExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41354.601716006950000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 592
    Top = 232
    object tipDefAntibiogram: TdxScreenTip
      Header.Text = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      Description.Text = #1044#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
    end
  end
end
