unit sifPaketiUslugi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfrmPaketiUslugi = class(TfrmMaster)
    Label2: TLabel;
    USLUGA_ID: TcxDBTextEdit;
    PAKET: TcxDBLookupComboBox;
    USLUGA: TcxDBLookupComboBox;
    cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PAKETNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLUGA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1USLUGANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPaketiUslugi: TfrmPaketiUslugi;

implementation

{$R *.dfm}

uses dmUnit, sifUslugi, sifPaketi;

procedure TfrmPaketiUslugi.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    USLUGA_ID.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');

end;

procedure TfrmPaketiUslugi.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblPaketUslugaPAKET_ID.Value:=Dm.tblPaketiID.Value;
    USLUGA_ID.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPaketiUslugi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
       VK_INSERT:
        begin
          if ((Sender = USLUGA_ID) or (Sender = USLUGA)) then
             begin
               frmUslugi:=TfrmUslugi.Create(self,false);
               frmUslugi.ShowModal;
               if (frmUslugi.ModalResult = mrOK) then
                    dm.tblPaketUslugaUSLUGA_ID.Value := StrToInt(frmUslugi.GetSifra(0));
                frmUslugi.Free;
             end;
          if ((Sender = PAKET) or (Sender = Sifra)) then
             begin
               frmPaketi:=TfrmPaketi.Create(self,false);
               frmPaketi.ShowModal;
               if (frmPaketi.ModalResult = mrOK) then
                    dm.tblPaketUslugaPAKET_ID.Value := StrToInt(frmPaketi.GetSifra(0));
                frmPaketi.Free;
             end;
        end;
   end;
end;

procedure TfrmPaketiUslugi.FormShow(Sender: TObject);
begin
  inherited;
  dxBarManager1Bar1.Caption:='���������';
  dm.tblUslugi.Close;
  dm.tblUslugi.ParamByName('param').Value:=0;
  dm.tblUslugi.ParamByName('tip').Value:=0;
  dm.tblUslugi.Open;

  dm.tblPaketUsluga.Close;
  dm.tblPaketUsluga.ParamByName('paket').Value:=dm.tblPaketiID.Value;
  dm.tblPaketUsluga.Open;
end;

end.
