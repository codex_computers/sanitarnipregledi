unit Analizi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxBlobEdit, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmAnaliza = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel3: TPanel;
    PopupMenu2: TPopupMenu;
    Panel2: TPanel;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    ANTIB_ID: TcxDBTextEdit;
    MIKRO_ID: TcxDBTextEdit;
    Label6: TLabel;
    BROJ: TcxDBTextEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    OPIS: TcxDBMemo;
    Label1: TLabel;
    Label7: TLabel;
    DEF_NAOD: TcxDBTextEdit;
    MIKRO_NAZIV: TcxDBLookupComboBox;
    ANTIB_NAZIV: TcxDBLookupComboBox;
    DEF_NAOD_OPIS: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1PREGLED: TcxGridDBColumn;
    cxGrid1DBTableView1PREGLED_S: TcxGridDBColumn;
    cxGrid1DBTableView1MIKROORGANIZAM: TcxGridDBColumn;
    cxGrid1DBTableView1MIKRO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ANTIBIOGRAM: TcxGridDBColumn;
    cxGrid1DBTableView1ANTIBIOGRAM_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1LEKAR: TcxGridDBColumn;
    cxGrid1DBTableView1LEKARNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DEF_NAOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAOD_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Panel1: TPanel;
    Label4: TLabel;
    LAB_BR: TcxDBTextEdit;
    Label14: TLabel;
    LEKAR: TcxDBTextEdit;
    LEKAR_NAZIV: TcxDBLookupComboBox;
    BarNaod: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPozitiven: TAction;
    aNegativen: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aRezultati: TAction;
    cxGrid1DBTableView1LAB_BR: TcxGridDBColumn;
    cxGrid1DBTableView1BR_MIKROORGANIZAM: TcxGridDBColumn;
    Label5: TLabel;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    dxBarLargeButton21: TdxBarLargeButton;
    ZapisiButtonNeg: TcxButton;
    OtkaziButtonNeg: TcxButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aPozitivenExecute(Sender: TObject);
    procedure aNegativenExecute(Sender: TObject);
    procedure aRezultatiExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmAnaliza: TfrmAnaliza;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit,
  sifAntibiogram, Rezultat;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmAnaliza.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmAnaliza.aNegativenExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.RecordCount = 0) then
    begin
      dm.tblPriemLabStavki.Edit;
      dm.tblPriemLabStavkiNAOD.Value:=0;
      dm.tblPriemLabStavki.Post;

      dm.tblPriemLabStavki.Edit;
      dm.tblPriemLabStavkiZAVRSENA.Value:=0;
      dm.tblPriemLabStavki.Post;

      BarNaod.Caption:='������ � ���������';
      aNegativen.Enabled:=False;
      aPozitiven.Enabled:=True;

      ZapisiButtonNeg.Visible:=true;
      OtkaziButtonNeg.Visible:=true;
      Panel3.Visible:=false;
      frmAnaliza.Height:=frmAnaliza.Height - Panel3.Height;
    end
  else
    begin
       frmDaNe := TfrmDaNe.Create(self, '������ �� ����', '���� ��� ������� ���� ������ �� �� ��������� ��������, ��������� �������� �� ����� ��������� ?', 1);
        if (frmDaNe.ShowModal = mrYes) then
          begin
            dm.tblPriemLabStavki.Edit;
            dm.tblPriemLabStavkiNAOD.Value:=0;
            dm.tblPriemLabStavki.Post;

            dm.tblPriemLabStavki.Edit;
            dm.tblPriemLabStavkiZAVRSENA.Value:=0;
            dm.tblPriemLabStavki.Post;

            dm.qDeleterezultat.Close;
            dm.qDeleterezultat.ParamByName('pregled_s').Value:=dm.tblPriemLabStavkiID.Value;
            dm.qDeleterezultat.ExecQuery;

            dm.qDeleteAnaliza.Close;
            dm.qDeleteAnaliza.ParamByName('pregled_s').Value:=dm.tblPriemLabStavkiID.Value;
            dm.qDeleteAnaliza.ExecQuery;

            //
            BarNaod.Caption:='������ � ���������';
            aNegativen.Enabled:=False;
            aPozitiven.Enabled:=True;

            ZapisiButtonNeg.Visible:=true;
            OtkaziButtonNeg.Visible:=true;
            Panel3.Visible:=false;
            frmAnaliza.Height:=frmAnaliza.Height - Panel3.Height;
            //

            dm.tblAnaliza.FullRefresh;
          end;
    end;
end;

procedure TfrmAnaliza.aNovExecute(Sender: TObject);
var pom_lab_br:String;
    pom_lekar:Integer;
begin

  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
     Panel1.Enabled:=true;
     Panel2.Enabled:=true;
     Panel3.Enabled:=true;

     dm.qCountAnaliza.Close;
     dm.qCountAnaliza.ParamByName('pregled_s').Value:=dm.tblPriemLabStavkiID.Value;
     dm.qCountAnaliza.ExecQuery;

     if dm.qCountAnaliza.FldByName['br'].Value > 0 then
        begin
          if (not dm.tblAnalizaLAB_BR.IsNull) then
             pom_lab_br:=dm.tblAnalizaLAB_BR.Value
          else
             pom_lab_br:= '-999';
          if (not dm.tblAnalizaLEKAR.IsNull) then
             pom_lekar:=cxGrid1DBTableView1LEKAR.EditValue
          else
             pom_lekar:= -999;
        end;

     cxGrid1DBTableView1.DataController.DataSet.Insert;
     dm.tblAnalizaPREGLED.Value:=dm.tblPriemLabID.Value;
     dm.tblAnalizaPREGLED_S.Value:=dm.tblPriemLabStavkiID.Value;
     if dm.qCountAnaliza.FldByName['br'].Value > 0 then
        begin
     //     cxGrid1DBTableView1.DataController.DataSet.Insert;
          if pom_lab_br <> '-999' then
             dm.tblAnalizaLAB_BR.Value:=pom_lab_br;
          if pom_lekar <> - 999 then
             dm.tblAnalizaLEKAR.Value:=pom_lekar;

             DEF_NAOD.SetFocus

        end
     else
        begin
          LAB_BR.SetFocus;
        end;

  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmAnaliza.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
     Panel1.Enabled:=true;
     Panel2.Enabled:=true;
     Panel3.Enabled:=true;
     LAB_BR.SetFocus;
     cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmAnaliza.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
     begin
        frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
        if (frmDaNe.ShowModal = mrYes) then
           begin
            dm.tblPriemLabStavki.Edit;
            dm.tblPriemLabStavkiZAVRSENA.Value:=0;
            dm.tblPriemLabStavkiNAOD.Value:=0;
            dm.tblPriemLabStavki.Post;
            cxGrid1DBTableView1.DataController.DataSet.Delete();
           end
     end;

end;

procedure TfrmAnaliza.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmAnaliza.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmAnaliza.aRezultatiExecute(Sender: TObject);
begin
     if (not dm.tblAnalizaANTIBIOGRAM.IsNull) then
        begin
          frmrezultat:=TfrmRezultat.Create(Self, false);
          dm.qCountRezultat.close;
          dm.qCountRezultat.ParamByName('analiza').Value:=dm.tblAnalizaID.value;
          dm.qCountRezultat.ExecQuery;
          if dm.qCountRezultat.FldByName['br'].Value = 0 then
             begin
               dm.insert6(dm.PROC_SAN_INSERTREZULTAT, 'ANTIBIOGRAM_ID', 'PREGLED_ID', 'PREGLED_S_ID', 'MIKRO_ID','ANALIZA_ID', Null, dm.tblAnalizaANTIBIOGRAM.Value, dm.tblAnalizaPREGLED.Value,dm.tblAnalizaPREGLED_S.Value, dm.tblAnalizaMIKROORGANIZAM.value,dm.tblAnalizaID.Value, Null);
             end;
          frmrezultat.ShowModal;
          frmrezultat.Free;
        end;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmAnaliza.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmAnaliza.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmAnaliza.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmAnaliza.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmAnaliza.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmAnaliza.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmAnaliza.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmAnaliza.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmAnaliza.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmAnaliza.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmAnaliza.prefrli;
begin
end;

procedure TfrmAnaliza.FormClose(Sender: TObject; var Action: TCloseAction);
var tag, local_tag:Integer;
begin
    local_tag:=0;
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dmRes.FreeRepository(rData);
            Action := caFree;
        end
        else
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dmRes.FreeRepository(rData);
            Action := caFree;
          end
    end
  else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then dmRes.FreeRepository(rData);
    begin
      dmRes.FreeRepository(rData);
    end;

  if (dm.tblPriemLabStavkiZAVRSENA.Value = 0) and (cxGrid1DBTableView1.DataController.RecordCount > 0) then
      begin
        frmDaNe := TfrmDaNe.Create(self, '����� �� ������', '���� ���� ������� �� �������� ?', 0);
        if (frmDaNe.ShowModal = mrYes) then
           begin
             dm.tblPriemLabStavki.Edit;
             dm.tblPriemLabStavkiZAVRSENA.Value:=1;
             dm.tblPriemLabStavki.Post;
             // Zatvoranje na datum na zaverka i datum na vazenje
             tag:=dm.zemi3(dm.pZatvoriDatum,'PREGLED_ID', Null, Null, dm.tblPriemLabID.Value, Null, Null,'TAG');
             if tag = 1 then ShowMessage('������� ����������� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!')
             else if tag = 0 then  ShowMessage('������� ����Ō� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!') ;
           end;
       end
  else
       begin
        tag:=dm.zemi3(dm.pZatvoriDatum,'PREGLED_ID', Null, Null, dm.tblPriemLabID.Value, Null, Null,'TAG');
        if tag = 0 then  ShowMessage('������� ����Ō� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!') ;
       end;
  dm.tblPriemLab.Refresh;

end;
procedure TfrmAnaliza.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmAnaliza.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

    dmRes.GenerateFormReportsAndActions(dxRibbon1,dxRibbon1Tab1,dxBarManager1,cxGrid1DBTableView1, Name);

    if ((dm.tblPriemLabStavkiNAOD.Value = 0) or (dm.tblPriemLabStavkiNAOD.Value = 3)) then
       begin
         BarNaod.Caption:='������ � ���������';
         aNegativen.Enabled:=False;
         Panel3.Visible:=false;
         frmAnaliza.Height:=frmAnaliza.Height - Panel3.Height;
       end
    else if dm.tblPriemLabStavkiNAOD.Value = 1 then
       begin
         BarNaod.Caption:='������ � ���������';
         aPozitiven.Enabled:=False;
         ZapisiButtonNeg.Visible:=false;
         OtkaziButtonNeg.Visible:=false;
       end;
    dm.tblDefNaodi.Open;
    dm.tblLekari.Open;
    dm.tblMikroorganizmi.Open;
    dm.tblAntibiogram.Open;
    dm.tblAnaliza.Open;

    dm.tblMikroUslugi.Close;
    dm.tblMikroUslugi.ParamByName('usluga').Value:=dm.tblPriemLabStavkiUSLUGA_ID.Value;
    dm.tblMikroUslugi.Open;

    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    cxGrid1.SetFocus;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmAnaliza.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmAnaliza.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmAnaliza.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    cxGrid1DBTableView1.DataController.DataSet.Post;
    if (ANTIB_ID.Text <> '') and (ANTIB_NAZIV.Text <> '') and (dm.tblPriemLabStavkiNAOD.Value = 1) then
        begin
          frmrezultat:=TfrmRezultat.Create(Self, false);
          dm.qCountRezultat.close;
          dm.qCountRezultat.ParamByName('analiza').Value:=dm.tblAnalizaID.value;
          dm.qCountRezultat.ExecQuery;
          if dm.qCountRezultat.FldByName['br'].Value = 0 then
             begin
               dm.insert6(dm.PROC_SAN_INSERTREZULTAT, 'ANTIBIOGRAM_ID', 'PREGLED_ID', 'PREGLED_S_ID', 'MIKRO_ID','ANALIZA_ID', Null, dm.tblAnalizaANTIBIOGRAM.Value, dm.tblAnalizaPREGLED.Value,dm.tblAnalizaPREGLED_S.Value, dm.tblAnalizaMIKROORGANIZAM.value,dm.tblAnalizaID.Value, Null);
             end;
          frmrezultat.ShowModal;
          frmrezultat.Free;
        end;
    cxGrid1.SetFocus;
    Panel1.Enabled:=false;
    Panel2.Enabled:=false;
    Panel3.Enabled:=false;
  end;

end;

//	����� �� ���������� �� �������
procedure TfrmAnaliza.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      Panel1.Enabled:=false;
      Panel2.Enabled:=false;
      Panel3.Enabled:=false;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmAnaliza.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmAnaliza.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmAnaliza.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmAnaliza.aPozitivenExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.RecordCount = 0) then
    begin
      dm.tblPriemLabStavki.Edit;
      dm.tblPriemLabStavkiNAOD.Value:=1;
      dm.tblPriemLabStavki.Post;

      dm.tblPriemLabStavki.Edit;
      dm.tblPriemLabStavkiZAVRSENA.Value:=0;
      dm.tblPriemLabStavki.Post;

      BarNaod.Caption:='������ � ���������';
      aPozitiven.Enabled:=False;
      aNegativen.Enabled:=True;

      ZapisiButtonNeg.Visible:=false;
      OtkaziButtonNeg.Visible:=false;
      frmAnaliza.Height:=frmAnaliza.Height + Panel3.Height;
      Panel3.Visible:=True;
    end
  else
    begin
       frmDaNe := TfrmDaNe.Create(self, '������ �� ����', '���� ��� ������� ���� ������ �� �� ��������� ��������, ��������� �������� �� ����� ��������� ?', 1);
        if (frmDaNe.ShowModal = mrYes) then
          begin
            dm.tblPriemLabStavki.Edit;
            dm.tblPriemLabStavkiNAOD.Value:=1;
            dm.tblPriemLabStavki.Post;

            dm.tblPriemLabStavki.Edit;
            dm.tblPriemLabStavkiZAVRSENA.Value:=0;
            dm.tblPriemLabStavki.Post;

            dm.qDeleterezultat.Close;
            dm.qDeleterezultat.ParamByName('pregled_s').Value:=dm.tblPriemLabStavkiID.Value;
            dm.qDeleterezultat.ExecQuery;

            dm.qDeleteAnaliza.Close;
            dm.qDeleteAnaliza.ParamByName('pregled_s').Value:=dm.tblPriemLabStavkiID.Value;
            dm.qDeleteAnaliza.ExecQuery;
            //
            BarNaod.Caption:='������ � ���������';
            aPozitiven.Enabled:=False;
            aNegativen.Enabled:=True;

            ZapisiButtonNeg.Visible:=false;
            OtkaziButtonNeg.Visible:=false;
            frmAnaliza.Height:=frmAnaliza.Height + Panel3.Height;
            Panel3.Visible:=True;
            //
            dm.tblAnaliza.FullRefresh;
          end;
    end;
end;

procedure TfrmAnaliza.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmAnaliza.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmAnaliza.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmAnaliza.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmAnaliza.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmAnaliza.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
