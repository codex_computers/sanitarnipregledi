unit cxFiscalInterface;


interface
uses
  System.Classes;
  function fpFiskalna(strim:TMemoryStream):Integer; stdcall; external 'cxFiscal.dll';
  function fpStorna(strim:TMemoryStream):Integer; stdcall; external 'cxFiscal.dll';
  function fpVlez(iznos:integer):Integer; stdcall; external 'cxFiscal.dll';
  function fpIzlez(iznos:integer):Integer; stdcall; external 'cxFiscal.dll';
  function fpFinansiski():integer; stdcall; external 'cxFiscal.dll';//vrakja brojce - ako e negativno da se bara greskata
  function fpKontrolen():integer; stdcall; external 'cxFiscal.dll';//vrakja brojce - ako e negativno da se bara greskata
  function fpDetalenPoDatum(OdDatum:string; DoDatum:string):integer; stdcall; external 'cxFiscal.dll';//vrakja brojce - ako e negativno da se bara greskata
  function fpSkratenPoDatum(OdDatum:string; DoDatum:string):integer; stdcall; external 'cxFiscal.dll';//vrakja brojce - ako e negativno da se bara greskata


//  function KreirajPf500inFiskalna(strim:TMemoryStream):Boolean; stdcall; external 'cxFiscal.dll'; //vrakja brojce  // za accent pf500.in fajlot
  function dllPecatiFiskalna(strim:TMemoryStream):integer; stdcall; external 'cxFiscal.dll'; //vrakja brojce
  procedure DllMessage(); stdcall; external 'cxFiscal.dll';
  procedure CustomMessage(poraka:integer); stdcall; external 'cxFiscal.dll';
  function MyFunction(s:string):string; stdcall; external 'cxFiscal.dll';
  Procedure PratiNaFiskal32(SmetkaId:Integer; Wait: Boolean); stdcall; external 'cxFiscal.dll';
  procedure TestMessage(); STDCALL; external 'cxFiscal.dll';
//  function NapraviFiskalna(xmlDokument):boolean; stdcall; external 'cxFiscal.dll';
  function PecatiFiskalna(xmlPateka: string):Boolean; stdcall; external 'cxFiscal.dll';
  function PecatiFiskalnaXML(strim: Tstream):Boolean; stdcall; external 'cxFiscal.dll';
  procedure ExecNewProcess(ProgramName : String; Wait: Boolean); stdcall; external 'cxFiscal.dll';


implementation


end.
