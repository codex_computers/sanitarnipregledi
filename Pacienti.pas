unit Pacienti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit,
  cxCalendar, cxDBExtLookupComboBox, cxGroupBox, cxSplitter, dmResources,
  dxCustomHint, cxHint, dxSkinOffice2013White, cxNavigator, System.Actions,
  cxCheckBox, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, xmldom, Xml.XMLIntf,
  Xml.XMLDoc, ComObj, MSXML, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, Xml.Win.msxmldom;

type
  TfrmPacienti = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_KARTON: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RAGJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_ZIVEENJE: TcxGridDBColumn;
    cxGrid1DBTableView1MESTOZIVEENJE: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_RAGJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1MESTORAGJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1R_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    EMBG: TcxDBTextEdit;
    Label2: TLabel;
    BROJ_KARTON: TcxDBTextEdit;
    Label4: TLabel;
    IME: TcxDBTextEdit;
    PREZIME: TcxDBTextEdit;
    Label5: TLabel;
    Label6: TLabel;
    TATKOVO: TcxDBTextEdit;
    DATUM_RAGJANJE: TcxDBDateEdit;
    Label7: TLabel;
    Label8: TLabel;
    MESTO_RAGJANJE: TcxDBTextEdit;
    MESTO_RAGJANJE_NAZIV: TcxDBLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    Label9: TLabel;
    MESTO_ZIVEENJE: TcxDBTextEdit;
    MESTO_ZIVEENJE_NAZIV: TcxDBLookupComboBox;
    Label10: TLabel;
    ADRESA: TcxDBTextEdit;
    TEL: TcxDBTextEdit;
    Label11: TLabel;
    Label12: TLabel;
    EMAIL: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    Label13: TLabel;
    R_MESTO: TcxDBTextEdit;
    R_MESTO_NAZIV: TcxDBLookupComboBox;
    Label14: TLabel;
    cxSplitter1: TcxSplitter;
    TIP_PARTNER: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    PARTNERNAZIV: TcxExtLookupComboBox;
    cxHintStyleController1: TcxHintStyleController;
    Label15: TLabel;
    KATEGORIJA_RM: TcxDBTextEdit;
    KATEGORIJA_RM_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1KATEGORIJARMMAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1DBColumn: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1DBColumn4: TcxGridDBColumn;
    actFind: TAction;
    XMLDocument1: TXMLDocument;
    actget_upati_datumExecute: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    IdHTTP1: TIdHTTP;
    xml: TXMLDocument;
    actget_upati_datum_2Execute: TAction;
    actResponseActiveDate: TAction;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure FormShow(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EMBGPropertiesValidate(Sender: TObject; var DisplayValue: Variant;
      var ErrorText: TCaption; var Error: Boolean);
    function proverkaModul11(broj: Int64): boolean;
    procedure setDatumPol(matBr:String);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure prefrli;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure zemiPacientPodatoci_new(mb: String);
    procedure zemiPacientPodatoci_old(mb: String);
    procedure actFindExecute(Sender: TObject);
    procedure actget_upati_datumExecuteExecute(Sender: TObject);
   procedure aGet_datum_aktivacija(Sender: TObject);
    procedure actResponseActiveDateExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPacienti: TfrmPacienti;
  rData : TRepositoryData;
  activate_date:string;
implementation

{$R *.dfm}

uses  dmUnit, Mesto, dmMaticni, sifRabotnoMesto, Partner, NurkoRepository,
  Utils, sifKategorijaRM, DaNe, dmKonekcija;

procedure TfrmPacienti.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  BROJ_KARTON.SetFocus;
end;

procedure TfrmPacienti.actFindExecute(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.FindPanel.DisplayMode:=fpdmAlways;
end;

procedure TfrmPacienti.actget_upati_datumExecuteExecute(Sender: TObject);
var
Fmt: TFormatSettings;
  sss:TStringStream;
  trte:IXMLNode;
  spec: IXMLNode;
  vals, gen, dij, sp, errormessage:string;
  filename, erServisMZ:string;
  i:Integer;
  skripta:TStringList;
  da : Byte;

begin

        // erServisMZ := 'http://ws.prima-medica.net:8090/ch_health/get_upati_datum';
          erServisMZ := 'http://ws.prima-medica.net:8090/ch_health/get_upati_datum_temp';

       //   frmTokenator.tokenauer(IdHTTP2);
           da := 0;
           //da se prezeme od servisot specijalistot
           filename:='D:\Projects\dstojkova\'+'ServisMZ_datum.xml';
//           skripta:=TStringList.Create();
           sss := TStringStream.Create('', TEncoding.UTF8);
          try
            IdHTTP1.Request.Username := 'CXHEALTH';
            IdHTTP1.Request.Password := 'cxhadmin1@';
            IdHTTP1.Get(erServisMZ,sss);
            sss.SaveToFile(filename);
            xml.FileName:=filename;
            xml.Active := True;

            trte:=xml.DocumentElement;
                  ShowMessage('servis_mz_vazi_do'+trte.ChildNodes['vazi_od'].NodeValue);
          except
              // ShowMessage(' ����������� ������� �� �������� �� �������� ?!?!');
              errormessage := ' ����������� ������� �� �������� �� �������� ?!?!';
          end;
end;

procedure TfrmPacienti.actResponseActiveDateExecute(Sender: TObject);
var
    s:tmemorystream;
    response :IXMLNode;
    url,status:string;
    buf:array of byte;
begin


    url:=dm.get_datum_aktivacija;
    try
       s  := TMemoryStream.Create;
       IdHTTP1.get(url, s);
       SetLength(buf, s.Size);
       s.Position := 0;
       s.ReadBuffer(buf[0], s.Size);
       XML.LoadFromstream(s);
       response:=xml.DocumentElement;
       status:=response.ChildValues['vazi_od'];

      // ShowMessage(status);
       s.Free;
     except

     on E: Exception do
       begin
          status :='';
       end;
     end;

     activate_date:=status;
end;

procedure TfrmPacienti.aGet_datum_aktivacija(Sender: TObject);
var
  Fmt: TFormatSettings;
  sss:TStringStream;
  trte:IXMLNode;
  spec: IXMLNode;
  vals, gen, dij, sp, errormessage:string;
  filename, erServisMZ:string;
  i:Integer;
  skripta:TStringList;
  da : Byte;

begin
         erServisMZ := 'http://ws.prima-medica.net:8090/ch_health/get_upati_datum';
       //   erServisMZ := 'http://ws.prima-medica.net:8090/ch_health/get_upati_datum_temp';

       //   frmTokenator.tokenauer(IdHTTP2);
           da := 0;
           //da se prezeme od servisot specijalistot
           filename:='D:\Projects\dstojkova\'+'ServisMZ_datum.xml';
//           skripta:=TStringList.Create();
           sss := TStringStream.Create('', TEncoding.UTF8);
          try
            IdHTTP1.Request.Username := 'CXHEALTH';
            IdHTTP1.Request.Password := 'ccxhadmin1@';
            IdHTTP1.Get(erServisMZ,sss);
            sss.SaveToFile(filename);
            xml.FileName:=filename;
            xml.Active := True;

            trte:=xml.DocumentElement;
                  ShowMessage('servis_mz_vazi_do'+trte.ChildNodes['vazi_od'].NodeValue);
          except
              // ShowMessage(' ����������� ������� �� �������� �� �������� ?!?!');
              errormessage := ' ����������� ������� �� �������� �� �������� ?!?!';
          end;
end;


procedure TfrmPacienti.aNovExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    EMBG.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if Tag = 1 then
       dm.tblPacientiEMBG.Value:=dm.tblPreglediEMBG.Value;
  end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;



procedure TfrmPacienti.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if(Sender = EMAIL)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmPacienti.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
   if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
            begin
               ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
           end
         else if(Sender = EMAIL)then
          ActivateKeyboardLayout($042F042F, KLF_REORDER)
         else if ((Sender = EMBG) and (EMBG.Text<> '') and (proverkaModul11(StrToInt64(EMBG.Text)))) then
          begin
            dm.qCountEMBGPacient.Close;
            dm.qCountEMBGPacient.ParamByName('embg').Value:=EMBG.Text;
            dm.qCountEMBGPacient.ExecQuery;
            if ((dm.qCountEMBGPacient.FldByName['br'].Value >=1)and (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert)) then
              begin
                ShowMessage('������ ����� �� ��������� ������� ��� !');
                EMBG.SetFocus;
              end
            else
              begin
               setDatumPol(dm.tblPacientiEMBG.Value);

          //     actResponseActiveDate.Execute;
              // if StrToDate(activate_date) > Now then
              //    zemiPacientPodatoci_old(dm.tblPacientiEMBG.Value)
            //   else
               zemiPacientPodatoci_new(dm.tblPacientiEMBG.Value)
              end;
          end;

    end

end;

procedure TfrmPacienti.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

procedure TfrmPacienti.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  ZemiImeDvoenKluc(TIP_PARTNER,PARTNER,PARTNERNAZIV);
end;

procedure TfrmPacienti.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmPacienti.EMBGPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  if (EMBG.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then
     if proverkaModul11(StrToInt64(EMBG.Text)) = false then
        begin
          // Error:=true;
           ShowMessage('��������� ������� ��� �� � ������� !!!');
           //ErrorText:='��������� ������� ��� �� � ������� !!!';
           EMBG.SetFocus;
        end
     else
        Error:=false;
end;

procedure TfrmPacienti.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var kom : TWinControl;
begin
  inherited;
  kom := Sender as TWinControl;
  case Key of
       VK_INSERT:
        begin
          if ((Sender = R_MESTO) or (Sender = R_MESTO_NAZIV)) then
             begin
               frmRabotnoMesto:=TfrmRabotnoMesto.Create(self,false);
               frmRabotnoMesto.ShowModal;
               if (frmRabotnoMesto.ModalResult = mrOK) then
                    dm.tblPacientiR_MESTO.Value := StrToInt(frmRabotnoMesto.GetSifra(0));
                frmRabotnoMesto.Free;
             end;
          if (kom = PARTNERNAZIV) then
              begin
                frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
                frmNurkoRepository.kontrola_naziv := kom.Name;
                frmNurkoRepository.ShowModal;

                if (frmNurkoRepository.ModalResult = mrOk) then
                    PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);

                frmNurkoRepository.Free;
              end;
          if ((Sender = MESTO_RAGJANJE) or (Sender = MESTO_RAGJANJE_NAZIV)) then
             begin
               frmMesto:=TfrmMesto.Create(self,false);
               frmMesto.ShowModal;
               if (frmMesto.ModalResult = mrOK) then
                    dm.tblPacientiMESTO_RAGJANJE.Value := StrToInt(frmMesto.GetSifra(0));
                frmMesto.Free;
             end;
          if ((Sender = MESTO_ZIVEENJE) or (Sender = MESTO_ZIVEENJE_NAZIV)) then
             begin
               frmMesto:=TfrmMesto.Create(self,false);
               frmMesto.ShowModal;
               if (frmMesto.ModalResult = mrOK) then
                    dm.tblPacientiMESTO_ZIVEENJE.Value := StrToInt(frmMesto.GetSifra(0));
                frmMesto.Free;
             end
          else if ((Sender = KATEGORIJA_RM) or (Sender = KATEGORIJA_RM_NAZIV)) then
             begin
               frmKategorijaRM:=TfrmKategorijaRM.Create(self,false);
               frmKategorijaRM.ShowModal;
               if (frmKategorijaRM.ModalResult = mrOK) then
                    dm.tblPacientiKATEGORIJA_RM.Value := StrToInt(frmKategorijaRM.GetSifra(0));
                frmKategorijaRM.Free;
             end;
        end;
   end;
end;

procedure TfrmPacienti.FormClose(Sender: TObject; var Action: TCloseAction);
begin
      //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dmRes.FreeRepository(rData);
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dmRes.FreeRepository(rData);
            Action := caFree;
          end
          else Action := caNone;
    end
    else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then dmRes.FreeRepository(rData);
end;

procedure TfrmPacienti.FormCreate(Sender: TObject);
begin
  inherited;
  rData := TRepositoryData.Create();
end;

procedure TfrmPacienti.FormShow(Sender: TObject);
begin
  inherited;

  dm.tblPacienti.Open;
  dm.tblRabotnoMesto.Open;
  dm.tblKategorijaRM.Open;


  ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);

	PARTNERNAZIV.RepositoryItem := dmRes.InitRepository(-1, 'PARTNERNAZIV', Name, rData);

  if Tag = 1 then
     aNov.Execute();


end;

procedure TfrmPacienti.PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
begin
  inherited;
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       if PARTNERNAZIV.Text <> '' then
          begin
            dm.tblPacientiTIP_PARTNER.Value:=PARTNERNAZIV.EditValue[0];
            dm.tblPacientiPARTNER.Value:=PARTNERNAZIV.EditValue[1];
          end
        else
         begin
            PARTNER.Clear;
            TIP_PARTNER.Clear;
         end;
     end;
end;

procedure TfrmPacienti.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

function TfrmPacienti.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;

procedure TfrmPacienti.setDatumPol(matBr:String);
var  datum:string;
begin
     datum:= matBr[1] + matBr[2] + '.' + matBr[3] + matBr[4] + '.';
     if StrToInt(matBr[5]) = 9 then
        datum:=datum + '1'
     else if StrToInt(matBr[5]) in [0,1]then
        datum:=datum + '2';
     datum:=datum + matBr[5] + matBr[6]+ matBr[7];
     dm.tblPacientiDATUM_RAGJANJE.Value:=StrToDate(datum);

end;

procedure TfrmPacienti.prefrli;
begin
    inherited;
    if(not inserting) then
      begin
         ModalResult := mrOk;
         SetSifra(0,IntToStr(dm.tblPacientiID.Value));
      end;
end;


procedure TfrmPacienti.zemiPacientPodatoci_old(mb: String);
var pass:boolean;
 xml: IXMLDOMDocument2;
 xmlNode: IXMLDomNode;
 xmlListNode: IXMLDomNodeList;
 load_url, z_broj, priv, z1, z2, clen, adresa, ime, prezime, mesto:String;
 i, broj:integer;
begin
    try
        xml := CreateOleObject('Msxml2.DOMDocument.6.0') as IXMLDOMDocument2;
        xml.async := False;
        xml.setProperty('ServerHTTPRequest', true);
        pass:=true;

        if dm.session_token_form = '1' then
           begin
             load_url:=dm.osiguruvanje_old+mb+'&token='+dm.token_codex+'&session_token='+dm.session_token;  //danica
             dm.TimerToken.enabled:=false;
             dm.TimerToken.enabled:=true;
           end
        else
             load_url:=dm.osiguruvanje_old+mb+'&token='+dm.token_codex;

        xml.load(load_url);
        if xml.parseError.errorCode <> 0 then
        raise Exception.Create('XML Load error:' + xml.parseError.reason);
        xmlListNode := xml.selectNodes('/patient');
        xmlNode:=xmlListNode.item[0];
      //  danica.Text:=load_url;
    except on Exception do   pass:=false
    end;

    if (pass=true) and (dm.tblPacienti.State in [dsInsert,dsEdit]) then
       begin
            ime:= Copy(xmlNode.SelectSingleNode('ime').text,0,50);
            dm.tblPacientiIME.Value:=ime;
            prezime:= Copy(xmlNode.SelectSingleNode('prezime').text,0,50);
            dm.tblPacientiPREZIME.Value:=prezime;
            adresa:= Copy(xmlNode.SelectSingleNode('adresa').text,0,100);
            dm.tblPacientiADRESA.Value:=adresa;
            mesto:= Copy(xmlNode.SelectSingleNode('place_name').text,0,100);
            dm.qMestoID.Close;
            dm.qMestoID.ParamByName('mesto').Value:=mesto;
            dm.qMestoID.ExecQuery;
            if not dm.qMestoID.FldByName['id'].IsNull then
               dm.tblPacientiMESTO_ZIVEENJE.Value:=dm.qMestoID.FldByName['id'].Value;
       end;
end;

procedure TfrmPacienti.zemiPacientPodatoci_new(mb: String);
var pass:boolean;
 xml: IXMLDOMDocument2;
 xmlNode: IXMLDomNode;
 xmlListNode: IXMLDomNodeList;
 load_url, z_broj, priv, z1, z2, clen, adresa, ime, prezime, mesto:String;
 i, broj:integer;
begin
   // ShowMessage('NEW');

    try
        xml := CreateOleObject('Msxml2.DOMDocument.6.0') as IXMLDOMDocument2;
        xml.async := False;
        xml.setProperty('ServerHTTPRequest', true);
        pass:=true;

        if dm.session_token_form = '1' then
           begin
             load_url:=dm.osiguruvanje+mb+'?token='+dm.token_codex+'&session_token='+dm.session_token;  //danica
           //  danica.Text:=load_url;
             dm.TimerToken.enabled:=false;
             dm.TimerToken.enabled:=true;
           end
        else
             load_url:=dm.osiguruvanje+mb+'&token='+dm.token_codex;

        xml.load(load_url);
        if xml.parseError.errorCode <> 0 then
          raise Exception.Create('XML Load error:' + xml.parseError.reason);
        xmlListNode := xml.selectNodes('/patient');
        xmlNode:=xmlListNode.item[0];

    except on Exception do   pass:=false
    end;

    if (pass=true) and (dm.tblPacienti.State in [dsInsert,dsEdit]) then
       begin
            ime:= Copy(xmlNode.SelectSingleNode('first_name').text,0,50);
            dm.tblPacientiIME.Value:=ime;
            prezime:= Copy(xmlNode.SelectSingleNode('last_name').text,0,50);
            dm.tblPacientiPREZIME.Value:=prezime;
            adresa:= Copy(xmlNode.SelectSingleNode('address').text,0,100);
            dm.tblPacientiADRESA.Value:=adresa;
            mesto:= Copy(xmlNode.SelectSingleNode('place').SelectSingleNode('name').text,0,100);
            dm.qMestoID.Close;
            dm.qMestoID.ParamByName('mesto').Value:=mesto;
            dm.qMestoID.ExecQuery;
            if not dm.qMestoID.FldByName['id'].IsNull then
               dm.tblPacientiMESTO_ZIVEENJE.Value:=dm.qMestoID.FldByName['id'].Value;
       end;
end;

{procedure TfrmPacienti.zemiPacientPodatoci_old(mb: String);
var pass:boolean;
 xml: IXMLDOMDocument2;
 xmlNode: IXMLDomNode;
 xmlListNode: IXMLDomNodeList;
 load_url, z_broj, priv, z1, z2, clen, adresa, ime, prezime, mesto:String;
 i, broj:integer;
begin
    try
        xml := CreateOleObject('Msxml2.DOMDocument.6.0') as IXMLDOMDocument2;
        xml.async := False;
        xml.setProperty('ServerHTTPRequest', true);
        pass:=true;

        if dm.session_token_form = '1' then
           begin
             load_url:=dm.osiguruvanje+mb+'?token='+dm.token_codex+'&session_token='+dm.session_token;  //danica
             //danica.Text:=load_url;
             dm.TimerToken.enabled:=false;
             dm.TimerToken.enabled:=true;
           end
        else
             load_url:=dm.osiguruvanje+mb+'&token='+dm.token_codex;

        xml.load(load_url);
        if xml.parseError.errorCode <> 0 then
          raise Exception.Create('XML Load error:' + xml.parseError.reason);
        xmlListNode := xml.selectNodes('/patient');
        xmlNode:=xmlListNode.item[0];

    except on Exception do   pass:=false
    end;

    if (pass=true) and (dm.tblPacienti.State in [dsInsert,dsEdit]) then
       begin
            ime:= Copy(xmlNode.SelectSingleNode('first_name').text,0,50);
            dm.tblPacientiIME.Value:=ime;
            prezime:= Copy(xmlNode.SelectSingleNode('last_name').text,0,50);
            dm.tblPacientiPREZIME.Value:=prezime;
            adresa:= Copy(xmlNode.SelectSingleNode('address').text,0,100);
            dm.tblPacientiADRESA.Value:=adresa;
            mesto:= Copy(xmlNode.SelectSingleNode('place').SelectSingleNode('name').text,0,100);
            dm.qMestoID.Close;
            dm.qMestoID.ParamByName('mesto').Value:=mesto;
            dm.qMestoID.ExecQuery;
            if not dm.qMestoID.FldByName['id'].IsNull then
               dm.tblPacientiMESTO_ZIVEENJE.Value:=dm.qMestoID.FldByName['id'].Value;
       end;
end;
}

initialization
  RegisterClass(TfrmPacienti);



end.
