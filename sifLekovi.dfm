inherited frmLekovi: TfrmLekovi
  Caption = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1094#1080
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 290
    ExplicitHeight = 290
    inherited cxGrid1: TcxGrid
      Height = 286
      ExplicitHeight = 286
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsLekovi
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 525
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 161
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 215
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 204
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 247
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Height = 114
    ExplicitTop = 416
    ExplicitHeight = 114
    object Label2: TLabel [1]
      Left = 37
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1090#1080#1082
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsLekovi
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
    end
    inherited OtkaziButton: TcxButton
      Top = 74
      TabOrder = 3
      ExplicitTop = 74
    end
    inherited ZapisiButton: TcxButton
      Top = 74
      TabOrder = 2
      ExplicitTop = 74
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1090#1080#1082
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsLekovi
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 470
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 200
  end
  inherited PopupMenu1: TPopupMenu
    Left = 352
    Top = 224
  end
  inherited dxBarManager1: TdxBarManager
    Left = 424
    Top = 232
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 240
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41353.605305578710000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
