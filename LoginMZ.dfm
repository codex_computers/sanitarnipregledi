object frmLoginMZ: TfrmLoginMZ
  Left = 825
  Top = 164
  Caption = #1040#1074#1090#1077#1085#1090#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1052#1086#1112' '#1058#1077#1088#1084#1080#1085
  ClientHeight = 324
  ClientWidth = 575
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 575
    Height = 105
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 521
    object cxLabel1: TcxLabel
      Left = 1
      Top = 1
      Align = alTop
      Caption = #1040#1074#1090#1077#1085#1090#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1052#1086#1112' '#1058#1077#1088#1084#1080#1085
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taCenter
      ExplicitWidth = 519
      AnchorX = 288
    end
    object cxLabel2: TcxLabel
      Left = 56
      Top = 56
      Caption = 
        #1055#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1072#1074#1090#1077#1085#1090#1080#1082#1072#1094#1080#1112#1072' '#1089#1077' '#1087#1086#1090#1088#1077#1073#1085#1080' '#1087#1086#1088#1072#1076#1080' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1089#1077#1088#1074 +
        #1080#1089#1080#1090#1077' '#1086#1076' '#1052#1086#1112' '#1058#1077#1088#1084#1080#1085
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 105
    Width = 575
    Height = 219
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 521
    object edtUsername: TcxTextEdit
      Left = 200
      Top = 24
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      Width = 121
    end
    object edtPass: TcxTextEdit
      Left = 200
      Top = 64
      Properties.EchoMode = eemPassword
      TabOrder = 1
      Width = 121
    end
    object cxButton1: TcxButton
      Left = 187
      Top = 144
      Width = 153
      Height = 25
      Caption = #1053#1072#1112#1072#1074#1072' '#1074#1086' '#1052#1086#1112' '#1058#1077#1088#1084#1080#1085
      TabOrder = 2
      OnClick = cxButton1Click
    end
    object cxLabel3: TcxLabel
      Left = 88
      Top = 24
      Caption = #1050#1086#1088#1080#1089#1085#1080#1095#1082#1086' '#1080#1084#1077
    end
    object cxLabel4: TcxLabel
      Left = 126
      Top = 64
      Caption = #1051#1086#1079#1080#1085#1082#1072
    end
    object cxButton2: TcxButton
      Left = 355
      Top = 144
      Width = 82
      Height = 25
      Caption = #1054#1090#1082#1072#1078#1080
      TabOrder = 5
      OnClick = cxButton2Click
    end
    object cxCBSync: TcxCheckBox
      Left = 39
      Top = 192
      Caption = #1050#1086#1088#1080#1089#1090#1080' '#1089#1077#1089#1080#1089#1082#1080' '#1090#1086#1082#1077#1085' '
      Properties.DisplayChecked = 'False'
      Properties.DisplayUnchecked = 'True'
      Properties.NullStyle = nssUnchecked
      Properties.ValueChecked = '1'
      Properties.ValueUnchecked = '0'
      TabOrder = 6
      Width = 146
    end
  end
  object qUpdSessTokForm: TpFIBQuery
    Transaction = dm.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'update sys_setup set v1=:p_1 where p1='#39'SAN'#39' and p2='#39'SESSIONTOKEN' +
        '_FORM'#39)
    Left = 324
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
