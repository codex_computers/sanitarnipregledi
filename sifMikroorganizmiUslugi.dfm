inherited frmMikroorganizmiUslugi: TfrmMikroorganizmiUslugi
  Caption = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1084#1080'/'#1059#1089#1083#1091#1075#1080
  ExplicitWidth = 741
  ExplicitHeight = 584
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 282
    ExplicitHeight = 282
    inherited cxGrid1: TcxGrid
      Height = 278
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitHeight = 278
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsMikroUslugi
        object cxGrid1DBTableView1USLUGA_ID: TcxGridDBColumn
          DataBinding.FieldName = 'USLUGA_ID'
          Width = 86
        end
        object cxGrid1DBTableView1USLUGANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'USLUGANAZIV'
          Width = 180
        end
        object cxGrid1DBTableView1MIKRO_ID: TcxGridDBColumn
          DataBinding.FieldName = 'MIKRO_ID'
          Width = 137
        end
        object cxGrid1DBTableView1MIKRONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MIKRONAZIV'
          Width = 259
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 163
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 213
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 203
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 241
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 408
    Height = 122
    ExplicitTop = 408
    ExplicitHeight = 122
    inherited Label1: TLabel
      Left = 15
      Top = 53
      Width = 104
      Caption = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084' :'
      ExplicitLeft = 15
      ExplicitTop = 53
      ExplicitWidth = 104
    end
    object Label2: TLabel [1]
      Left = 69
      Top = 26
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1059#1089#1083#1091#1075#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 125
      Top = 50
      Hint = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084' - '#1096#1080#1092#1088#1072
      DataBinding.DataField = 'MIKRO_ID'
      DataBinding.DataSource = dm.dsMikroUslugi
      TabOrder = 2
      ExplicitLeft = 125
      ExplicitTop = 50
      ExplicitWidth = 52
      Width = 52
    end
    inherited OtkaziButton: TcxButton
      Top = 82
      TabOrder = 5
      ExplicitTop = 82
    end
    inherited ZapisiButton: TcxButton
      Top = 82
      TabOrder = 4
      ExplicitTop = 82
    end
    object USLUGA: TcxDBLookupComboBox
      Tag = 1
      Left = 176
      Top = 23
      Hint = #1059#1089#1083#1091#1075#1072' - '#1085#1072#1079#1080#1074' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1059#1089#1083#1091#1075#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'USLUGA_ID'
      DataBinding.DataSource = dm.dsMikroUslugi
      Enabled = False
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end
        item
          Width = 200
          FieldName = 'CENA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsUslugi
      StyleDisabled.TextColor = clBackground
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 369
    end
    object USLUGA_ID: TcxDBTextEdit
      Tag = 1
      Left = 125
      Top = 23
      Hint = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'USLUGA_ID'
      DataBinding.DataSource = dm.dsMikroUslugi
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBackground
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object MIKRO_ID: TcxDBLookupComboBox
      Tag = 1
      Left = 176
      Top = 50
      Hint = 
        #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084' - '#1085#1072#1079#1080#1074' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1052#1080#1082#1088#1086#1086#1088#1075#1072#1085 +
        #1080#1079#1084#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'MIKRO_ID'
      DataBinding.DataSource = dm.dsMikroUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsMikroorganizmi
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 369
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 224
  end
  inherited PopupMenu1: TPopupMenu
    Top = 248
  end
  inherited dxBarManager1: TdxBarManager
    Left = 416
    Top = 248
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Top = 208
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41339.633051192130000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
