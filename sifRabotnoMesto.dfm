inherited frmRabotnoMesto: TfrmRabotnoMesto
  Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
  ClientHeight = 601
  ClientWidth = 755
  ExplicitWidth = 763
  ExplicitHeight = 632
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 755
    Height = 254
    ExplicitWidth = 755
    ExplicitHeight = 254
    inherited cxGrid1: TcxGrid
      Width = 751
      Height = 250
      ExplicitWidth = 751
      ExplicitHeight = 250
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsRabotnoMesto
        OptionsData.Editing = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 256
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = False
          Width = 283
        end
        object cxGrid1DBTableView1ROK: TcxGridDBColumn
          DataBinding.FieldName = 'ROK'
          Options.Editing = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 163
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 213
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 210
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 245
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 380
    Width = 755
    Height = 198
    ExplicitTop = 380
    ExplicitWidth = 755
    ExplicitHeight = 198
    inherited Label1: TLabel
      Left = 17
      Top = 22
      ExplicitLeft = 17
      ExplicitTop = 22
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 76
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1086#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 13
      Top = 100
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 149
      Top = 76
      Width = 44
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1084#1077#1089#1077#1094#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 69
      Top = 19
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsRabotnoMesto
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 69
      ExplicitTop = 19
    end
    inherited OtkaziButton: TcxButton
      Left = 664
      Top = 158
      TabOrder = 5
      ExplicitLeft = 664
      ExplicitTop = 158
    end
    inherited ZapisiButton: TcxButton
      Left = 583
      Top = 158
      TabOrder = 4
      ExplicitLeft = 583
      ExplicitTop = 158
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 46
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 476
    end
    object ROK: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 73
      Hint = #1056#1086#1082' '#1079#1072' '#1087#1086#1074#1090#1086#1088#1085#1086' '#1080#1089#1087#1080#1090#1091#1074#1072#1114#1077' ('#1074#1086' '#1084#1077#1089#1077#1094#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'ROK'
      DataBinding.DataSource = dm.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object OPIS: TcxDBMemo
      Left = 69
      Top = 100
      Hint = #1054#1087#1080#1089' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsRabotnoMesto
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 53
      Width = 476
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 755
    ExplicitWidth = 755
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 578
    Width = 755
    ExplicitTop = 578
    ExplicitWidth = 755
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 240
  end
  inherited PopupMenu1: TPopupMenu
    Left = 368
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 248
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 168
    Top = 240
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41330.506937025460000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
