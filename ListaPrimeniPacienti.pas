﻿unit ListaPrimeniPacienti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxLabel, cxImageComboBox, dxSkinOffice2013White, dxCore, cxDateUtils,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm,  IdMessage, IdSMTP, IdSSLOpenSSL, IdGlobal, IdExplicitTLSClientServerBase,  IdAttachment,IdAttachmentFile, IdAttachmentMemory;

type
//  niza = Array[1..5] of Variant;

  TfrmListaPrimeniPacienti = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    cxLabel4: TcxLabel;
    DatumOd: TcxDateEdit;
    cxLabel5: TcxLabel;
    DatumDo: TcxDateEdit;
    ZapisiButton: TcxButton;
    aPregledaj: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    aListaPriemUslugi: TAction;
    aDizajnListaPriemUslugi: TAction;
    BrojOd: TcxTextEdit;
    BrojDo: TcxTextEdit;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PLACANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1U_LISTA: TcxGridDBColumn;
    cxGrid1DBTableView1BR_KVITANCIJA: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2ID: TcxGridDBColumn;
    cxGrid1DBTableView2USLUGA_ID: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2CENA: TcxGridDBColumn;
    cxGrid1DBTableView2RE: TcxGridDBColumn;
    Godina: TcxComboBox;
    cxLabel2: TcxLabel;
    RadioStatus: TcxRadioGroup;
    cxGrid1DBTableView2ZAVRSENA: TcxGridDBColumn;
    cxGrid1DBTableView2NAOD: TcxGridDBColumn;
    aIsprazniPolinja: TAction;
    cxButton1: TcxButton;
    stringUsugi: TcxLookupComboBox;
    cxLabel6: TcxLabel;
    cxGrid1DBTableView2STAVKA_ID: TcxGridDBColumn;
    cxGrid1DBTableView2BROJ: TcxGridDBColumn;
    aPratiRezultatMailPacient: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    dxBarLargeButton21: TdxBarLargeButton;
    aPratiRezultatMailMoe: TAction;
    Panel2: TPanel;
    CheckBox1: TCheckBox;
    cxGrid1DBTableView1PRATI_EMAIL: TcxGridDBColumn;
    aPratiRezultatMailPartner: TAction;
    cxGrid1DBTableView1PRATI_MAIL_POTVRDA: TcxGridDBColumn;
    cxGrid1DBTableView1PRATI_MAIL_POTVRDA_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_EMAIL: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aPregledajExecute(Sender: TObject);
    procedure aListaPriemUslugiExecute(Sender: TObject);
    procedure aDizajnListaPriemUslugiExecute(Sender: TObject);
    procedure aIsprazniPolinjaExecute(Sender: TObject);
    procedure aPratiRezultatMailPacientExecute(Sender: TObject);
    procedure aPratiRezultatMailMoeExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure aPratiRezultatMailPartnerExecute(
  Sender: TObject);
  procedure EmailAttachment(attachment_file : AnsiString = ''; attachment_stream : TMemoryStream = nil; naziv_fajl : AnsiString = '');
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // приватна променлива за чување на шифрата на селектираниот ред

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }

    Attachment : TIdAttachmentFile;
    AttachmentMem : TIdAttachmentMemory;

    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //пристап до приватната променливата _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;

  end;

var
  frmListaPrimeniPacienti: TfrmListaPrimeniPacienti;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmListaPrimeniPacienti.CheckBox1Click(Sender: TObject);
var i:Integer;
begin
     if CheckBox1.Checked then
       begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index]:=  1;
            end;
        end
      else
        begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index]:=0;
            end;
     end
end;

constructor TfrmListaPrimeniPacienti.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	Означува дали формата е повикана за повеќекратно внесување
//	insert = true -> После секој запис автоматски се влегува во Insert Mode
//  insert = false -> После секој запис фокусот се враќа на гридот. За нареден запис треба да се притесне F5
  inserting := insert;
end;

//	Акција за инсертирање. Се исклучува панелот на гридот, се вклучува панелот со едит објектите,
//	се префрла фокусот на првата колона и се влегува во Insert Mode
procedure TfrmListaPrimeniPacienti.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('Најпрво запишете ги или откажете се од тековните промени,' + sLineBreak + 'а потоа пробајте да внесете нов запис!');
end;

//	Акција за ажурирање. Се исклучува панелот на гридот, се вклучува панелот со едит објектите,
//	се префрла фокусот на првата колона и се влегува во Edit Mode
procedure TfrmListaPrimeniPacienti.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('Најпрво запишете ги или откажете се од тековните промени,' + sLineBreak + 'а потоа пробајте да внесете нов запис!');
end;

//	Акција за бришење на селектираниот запис
procedure TfrmListaPrimeniPacienti.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmListaPrimeniPacienti.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  brisiGridVoBaza(Name,cxGrid1DBTableView2);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); за pivot ако има
  BrisiFormaIzgled(self);
end;

//	Акција за освежување на податоците
procedure TfrmListaPrimeniPacienti.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //ресетирање на филтерот
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	Акција за излез од формата со кликање на копчето Излез
procedure TfrmListaPrimeniPacienti.aIsprazniPolinjaExecute(Sender: TObject);
begin
      BrojOd.Clear;
      BrojDo.Clear;
      DatumOd.Clear;
      DatumDo.Clear;
      RadioStatus.EditValue:=0;
      Godina.EditValue:=dmKon.godina;
      stringUsugi.Clear;
      aPregledaj.Execute();
end;

procedure TfrmListaPrimeniPacienti.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmListaPrimeniPacienti.aListaPriemUslugiExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('SAN',9);
        dmKon.tblSqlReport.ParamByName('param').Value:=RadioStatus.EditValue;
        dmKon.tblSqlReport.ParamByName('godina').Value:=Godina.EditValue;
        dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
        if ((DatumOd.Text <> '') and (DatumDo.Text <> '')) then
           begin
             dmKon.tblSqlReport.ParamByName('param_datum').Value:=2;
             dmKon.tblSqlReport.ParamByName('datum_od').Value:=DatumOd.Date;
             dmKon.tblSqlReport.ParamByName('datum_do').Value:=DatumDo.Date;

             dmRes.frxReport1.Variables.AddVariable('VAR', 'datum_od', QuotedStr(DateToStr(DatumOd.Date)));
             dmRes.frxReport1.Variables.AddVariable('VAR', 'datum_do', QuotedStr(DateToStr(DatumDo.Date)));
           end
        else
             dmKon.tblSqlReport.ParamByName('param_datum').Value:=0;
        if ((BrojOd.Text <> '')and (BrojDo.Text <> '')) then
           begin
             dmKon.tblSqlReport.ParamByName('param_broj').Value:=1;
             dmKon.tblSqlReport.ParamByName('broj_od').Value:=BrojOd.Text;
             dmKon.tblSqlReport.ParamByName('broj_do').Value:=BrojDo.Text;

             dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_od', QuotedStr(BrojOd.Text));
             dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_do', QuotedStr(BrojDo.Text));
           end
        else
             dmKon.tblSqlReport.ParamByName('param_broj').Value:=0 ;

        if stringUsugi.Text <> '' then
          begin
           dmKon.tblSqlReport.ParamByName('u_lista').Value:=stringUsugi.EditValue;
           dmRes.frxReport1.Variables.AddVariable('VAR', 'u_lista', QuotedStr(stringUsugi.Text));
          end
        else
           dmKon.tblSqlReport.ParamByName('u_lista').Value:='%';

        dmRes.frxReport1.Variables.AddVariable('VAR', 'godina', QuotedStr(Godina.EditText));
        if RadioStatus.EditValue = 0 then
           dmRes.frxReport1.Variables.AddVariable('VAR', 'status', QuotedStr('Сите'))
        else if RadioStatus.EditValue = 1 then
           dmRes.frxReport1.Variables.AddVariable('VAR', 'status', QuotedStr('Завршени'))
        else if RadioStatus.EditValue = 2 then
           dmRes.frxReport1.Variables.AddVariable('VAR', 'status', QuotedStr('Незавршени'));
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('Не може да се прочита извештајот!');
      end
end;

//	Акција за снимање на изгледот на гридот во база (Utils.pas)
procedure TfrmListaPrimeniPacienti.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid1DBTableView2);
  ZacuvajFormaIzgled(self);
end;

//	Акција за експорт на гридот во Excel формат (Utils.pas)
procedure TfrmListaPrimeniPacienti.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  Процедура за движење на фокусот во контролите со помош на Enter
procedure TfrmListaPrimeniPacienti.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  за нурко контрола
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	Промена на боја на едитот при добивање на фокус
procedure TfrmListaPrimeniPacienti.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	Промена на боја на едитот при губење на фокус
procedure TfrmListaPrimeniPacienti.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmListaPrimeniPacienti.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  Процедура за чување на вредноста на примарниот клуч од табелата
//  Параметри се: br - реден број на полето (од 0 до 9) во зависност од бројот на полиња во примарниот клуч
//  s - вредноста на полето од клучот (претворена во string во случај на integer со IntToStr функцијата)
procedure TfrmListaPrimeniPacienti.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  Функција за читање на вредноста на примарниот клуч од табелата
//  Параметри се: br - реден број на полето (од 0 до 9) во зависност од бројот на полиња во примарниот клуч
function TfrmListaPrimeniPacienti.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmListaPrimeniPacienti.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  Се користи во формите кои наследуваат од Master-от за сетирање на вредноста на клучот преку SetSifra
procedure TfrmListaPrimeniPacienti.prefrli;
begin
end;

procedure TfrmListaPrimeniPacienti.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	Валидација при затварање на формата
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, 'Незапишани податоци', 'Податоците не се запишани. Дали сакате да се запишат?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmListaPrimeniPacienti.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmListaPrimeniPacienti.FormShow(Sender: TObject);
begin
  //  Прочитај ги евентуалните ограничувања/привилегии на корисникот за оваа форма
    SpremiForma(self);
  //	На појавување на формата одреди која компонента е прва а која последна на панелот
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
//    dm.tblListaPrimeniPacientiStavki.close;
//    dm.tblListaPrimeniPacienti.close;
  //	Прочитај ги подесувањата за изглед на гридот
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid1DBTableView2,false,false);
  //	Прочитај ги подесувањата за печатењето на гридот
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    Godina.EditValue:=dmKon.godina;
    sobrano := true;

    dm.tblUslugiStringPriem.Close;
    dm.tblUslugiStringPriem.Open;

//  Користење на нурко од базата во cxExtLookupComboBox или cxDBExtLookupComboBox контрола
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmListaPrimeniPacienti.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmListaPrimeniPacienti.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  Акција за запис
procedure TfrmListaPrimeniPacienti.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	Акција за откажување на записот
procedure TfrmListaPrimeniPacienti.aOtkaziExecute(Sender: TObject);
begin

      Close();

end;

//----------------------------------------------------------------------------------
// Процедури за сетирање, зачувување на својствата и изгледот на Print системот
//----------------------------------------------------------------------------------

//	Акција за сетирање на страната за печатење
procedure TfrmListaPrimeniPacienti.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	Акција за печатење на гридот
procedure TfrmListaPrimeniPacienti.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  if (Godina.Text <> '') then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Година : ' + Godina.Text);
  if RadioStatus.EditValue = 0 then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Статус : ' +'Сите')
  else if RadioStatus.EditValue = 1 then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Статус : ' +'Завршени')
  else if RadioStatus.EditValue = 2 then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Статус : ' +'Незавршени');
  if (DatumOd.Text <> '') and (DatumDo.Text <> '') then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Период : ' + DateToStr(DatumOd.Date)+' - ' +DateToStr(DatumDo.Date));
  if (BrojOd.Text <> '') and (BrojDo.Text <> '') then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Број : ' + BrojOd.Text+' - ' +BrojDo.Text);
  if (stringUsugi.Text <> '') then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Услуги : ' + stringUsugi.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	Акција за повикување на дизајнерот на печатење
procedure TfrmListaPrimeniPacienti.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;



procedure TfrmListaPrimeniPacienti.aPratiRezultatMailPacientExecute(Sender: TObject);
var i,status:integer;  pdfStream:TFileStream;
    RecipientEmail,attachment_file, SubjectEmail, BodyText,naziv_fajl:string;
    memStream: TMemoryStream;
begin
    status:=0;
    with cxGrid1DBTableView1.DataController do
     for  i:=0 to FilteredRecordCount-1 do
          begin
             if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index] = 1 then
                 begin
                              if ((Values[FilteredRecordIndex[i] , cxGrid1DBTableView1EMAIL.Index] <> null)and(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1EMAIL.Index] <> '')) then
                                  begin
                                    try
                                       dmRes.Spremi('SAN',1);
                                       dmKon.tblSqlReport.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                                       dmKon.tblSqlReport.Open;
                                       dm.tblRezultatAnaliza.Close;
                                       dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                                       dm.tblRezultatAnaliza.Open;
                                       dm.tblFrxAntibiogram.Close;
                                       dm.tblFrxAntibiogram.Open;
                                       dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PACIENT_NAZIV.Index]));
                                       dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])));
                                    except
                                      ShowMessage('Не може да се прочита извештајот!');
                                    end ;

                                    RecipientEmail:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1EMAIL.Index];
                                    SubjectEmail:='Резултати од Санитарни прегледи';
                                    BodyText:='Резултати од Санитарни прегледи за пациент '+ Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PACIENT_NAZIV.Index]+' со број '+ intToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index]);
                                    naziv_fajl:='Rezultati_'+ VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])+'_'+VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GODINA.Index])+'.pdf';
                                    try
                                       memStream := TMemoryStream.Create;
                                       dmRes.frxPDFExport1.Stream := memStream;
                                       memStream.Position := 0;
                                       dmRes.frxReport1.PrepareReport(True);
                                       dmRes.frxReport1.Export(dmRes.frxPDFExport1);

                                       if (dmKon.email_api_active) then
                                          dmRes.SendWebApiEmail(RecipientEmail, SubjectEmail, BodyText, '', memStream, naziv_fajl)
                                        else
                                          dmRes.SendIndyEmail(RecipientEmail, SubjectEmail, BodyText, '', memStream, naziv_fajl);
                                    finally
                                       FreeAndNil(MemStream);
                                       dmRes.frxPDFExport1.Stream := nil;
                                       status:=1;
                                       dm.qUpdateMailPotvrda.Close;
                                       dm.qUpdateMailPotvrda.ParamByName('pregled_id').Value:= Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                                       dm.qUpdateMailPotvrda.ExecQuery;
                                    end;
                                  end
                              else ShowMessage('Немате евидентирано eMail адреса за пациентот '+Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PACIENT_NAZIV.Index]);
                 end

          end;

    if status = 1 then
       aPregledaj.Execute();
end;

//procedure TfrmListaPrimeniPacienti.aPratiRezultatMailPacientExecute(Sender: TObject);
//var i:integer;  pdfStream:TFileStream; status:Boolean;
//    RecipientEmail,attachment_file:string;
//begin
//     dmMat.tblMailSetup.Close;
//     dmMat.tblMailSetup.Open;
//     if (dmMat.tblMailSetup.Locate('RE', dmKon.re, [])) then
//        begin
//          dm.SMTP.Host := dmMat.tblMailSetupSMTP_HOST.Value;
//          dm.SMTP.Port := dmMat.tblMailSetupSMTP_PORT.Value;
//          dm.SMTP.Username := dmMat.tblMailSetupLOGIN.Value;
//          dm.SMTP.Password := dmMat.tblMailSetupLOGIN_PASSWORD.Value;
//
//          dm.SMTP.IOHandler := dm.SSLHandler;
//          dm.SMTP.UseTLS := utUseExplicitTLS;
//          dm.SSLHandler.MaxLineAction := maException;
//          dm.SSLHandler.SSLOptions.Method := sslvTLSv1;
//          dm.SSLHandler.SSLOptions.Mode := sslmUnassigned;
//          dm.SSLHandler.SSLOptions.VerifyMode := [];
//          dm.SSLHandler.SSLOptions.VerifyDepth := 0;
//
//          dm.EmailMessage.From.Address := dmMat.tblMailSetupFROM_MAIL.Value;
//          dm.EmailMessage.Subject := 'Резултати од санитарни прегледи';
//          dm.EmailMessage.Body.Text := 'Во прилог се резултати од санитарни прегледи';
//
//
//          try
//            dm.SMTP.Connect;
//          finally
//              if (dm.SMTP.Connected()) then
//                  if(dm.SMTP.Authenticate()) then
//                      begin
//                        with cxGrid1DBTableView1.DataController do
//                            for  i:=0 to FilteredRecordCount-1 do
//                              begin
//                                if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index] = 1 then
//                                  begin
//                                    status:=true;
//                                      try
//                                        dmRes.Spremi('SAN',1);
//                                        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
//                                        dmKon.tblSqlReport.Open;
//                                        dm.tblRezultatAnaliza.Close;
//                                        dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
//                                        dm.tblRezultatAnaliza.Open;
//                                        dm.tblFrxAntibiogram.Close;
//                                        dm.tblFrxAntibiogram.Open;
//                                        dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PACIENT_NAZIV.Index]));
//                                        dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])));
//                                      except
//                                        ShowMessage('Не може да се прочита извештајот!');
//                                      end ;
//
//                                      //export to pdf  ---------------------------------------------------------------------------------------------
//                                      if FileExists(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3') then
//                                         dm.frxRezultatMail.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3');
//                                      dm.frxRezultatMail.PrepareReport(true);
//
//                                      attachment_file:=dm.mail_pateka+'Rezultati_'+ VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])+'_'+VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GODINA.Index])+'.pdf';
//                                      dm.frxPDFExport1.FileName:=attachment_file;
//                                      try
//                                         pdfStream := TFileStream.Create(attachment_file, fmCreate or fmShareDenyNone);
//                                         try
//                                            dm.frxPDFExport1.Stream := pdfStream;
//                                            try
//                                               dm.frxRezultatMail.Export(dm.frxPDFExport1);
//                                            except on Exception do  status:=false;
//                                            end;
//                                         finally
//                                            FreeAndNil(pdfStream);
//                                            dm.frxPDFExport1.Stream := NIL;
//                                         end;
//                                      except on Exception do
//                                      end;
//
//                                      //-----------------------------------------------------------------------------------------------------------------
//
//                                      if FileExists(attachment_file) then
//                                         begin
//                                           Attachment := TIdAttachmentFile.Create(dm.EmailMessage.MessageParts,attachment_file);
//                                         end;
//
//                                      RecipientEmail:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1EMAIL.Index];
//                                      dm.EmailMessage.Recipients.EMailAddresses:=RecipientEmail;
//
//                                      //send mail
//                                      try
//                                            dm.SMTP.Send(dm.EmailMessage);
//                                            ShowMessage('Пораката е успешно пратена на : ' + RecipientEmail);
//
//                                            dm.qUpdateMailPotvrda.Close;
//                                            dm.qUpdateMailPotvrda.ParamByName('pregled_id').Value:= Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
//                                            dm.qUpdateMailPotvrda.ExecQuery;
//                                      except on E:Exception do
//                                             ShowMessage('ГРЕШКА: ' + E.Message);
//                                      end;
//                                      Attachment.Destroy;
//                                 end;
//                              end;
//                      end;
//              if dm.SMTP.Connected then
//                 dm.SMTP.Disconnect;
//          end;
//        end
//     else  ShowMessage('Грешка! Немате дефинирано email адреса за фирмата/раб.единица');
//end;



procedure TfrmListaPrimeniPacienti.aPratiRezultatMailPartnerExecute(
  Sender: TObject);
var i:integer;  pdfStream:TFileStream; status, kraj, kraj_s:Boolean;
    RecipientEmail:string;
    SubjectEmail, BodyText,naziv_fajl: AnsiString;
    memStream: TMemoryStream;
    re : integer;
begin
    dmMat.tblMailSetup.Close;
    dmMat.tblMailSetup.Open;

    // ако user-от има доделено раб. единица во SYS_USER табелата земи го тоа инаку земи ја фирмата
    if (dmKon.UserRE <> -999) then
      re := dmKon.UserRE
    else
      re := dmKon.re;

    if (dmMat.tblMailSetup.Locate('RE', re, [])) then
      begin
        dmRes.SSLHandler.MaxLineAction := maException;
        dmRes.SSLHandler.SSLOptions.Method := sslvTLSv1;
        dmRes.SSLHandler.SSLOptions.Mode := sslmUnassigned;
        dmRes.SSLHandler.SSLOptions.VerifyMode := [];
        dmRes.SSLHandler.SSLOptions.VerifyDepth := 0;
        dmRes.SMTP.IOHandler := dmRes.SSLHandler;
        dmRes.SMTP.UseTLS := utUseExplicitTLS;
        dmRes.SMTP.Host := dmMat.tblMailSetupSMTP_HOST.Value;
        dmRes.SMTP.Port := dmMat.tblMailSetupSMTP_PORT.Value;
        dmRes.SMTP.Username := dmMat.tblMailSetupLOGIN.Value;
        dmRes.SMTP.Password := dmMat.tblMailSetupLOGIN_PASSWORD.Value;
      end
    else
      begin
         ShowMessage('Грешка! Нема email подесување за фирмата/раб.единица');
         abort
      end;

    //-------------------------------------------------------------------------------

    if (not dmMat.tblMailSetupFROM_MAIL.IsNull) then
      begin
         dm.qUpdatePregledMail_NulL.Close;
         dm.qUpdatePregledMail_NulL.ExecQuery;

         with cxGrid1DBTableView1.DataController do
            for  i:=0 to FilteredRecordCount-1 do
             begin
               if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index] = 1 then
                  begin
                     dm.qUpdatePregledMail.Close;
                     dm.qUpdatePregledMail.ParamByName('pregled_id').Value:= Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                     dm.qUpdatePregledMail.ExecQuery;
                  end;
             end;

         dm.tblPratiMail.Close;
         dm.tblPratiMail.Open;

         if dm.tblPratiMail.IsEmpty then
            kraj:=true
         else
           begin
             kraj := false;
             dmRes.EmailMessage.Clear;
           end;

         if kraj = false then
           begin
             while (kraj = false) do
              begin
               if (not dm.tblPratiMailEMAIL.IsNull) then
                  begin
                       dm.tblPratiMailStavki.Close;
                       dm.tblPratiMailStavki.ParamByName('partner').Value:=dm.tblPratiMailPARTNER.Value;
                       dm.tblPratiMailStavki.ParamByName('tip_partner').Value:=dm.tblPratiMailTIP_PARTNER.Value;
                       dm.tblPratiMailStavki.Open;

                       If dm.tblPratiMailStavki.IsEmpty then  kraj_s:=true
                       else  kraj_s := false;

                      while (kraj_s = false) do
                        begin
                          try
                             dmRes.Spremi('SAN',1);
                             dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPratiMailStavkiID.Value;
                             dmKon.tblSqlReport.Open;
                             dm.tblRezultatAnaliza.Close;
                             dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=dm.tblPratiMailStavkiID.Value;
                             dm.tblRezultatAnaliza.Open;
                             dm.tblFrxAntibiogram.Close;
                             dm.tblFrxAntibiogram.Open;
                             dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(dm.tblPratiMailStavkiPACIENT_NAZIV.Value));
                             dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(dm.tblPratiMailStavkiBROJ.Value)));
                          except
                             ShowMessage('Не може да се прочита извештајот!');
                          end ;

                          try
                            memStream := TMemoryStream.Create;
                            dmRes.frxPDFExport1.Stream := memStream;
                            memStream.Position := 0;
                            dmRes.frxReport1.PrepareReport(True);
                            dmRes.frxReport1.Export(dmRes.frxPDFExport1);

                            if (memStream <> nil) then
                               begin
                                  AttachmentMem := TIdAttachmentMemory.Create(dmRes.EmailMessage.MessageParts);
                                  AttachmentMem.LoadFromStream(memStream);

                                  naziv_fajl:='Rezultati_'+ IntToStr(dm.tblPratiMailStavkiBROJ.Value)+'_'+IntToStr(dm.tblPratiMailStavkiGODINA.Value)+'.pdf';
                                  AttachmentMem.ContentType := 'application/pdf';
                                  AttachmentMem.FileName := naziv_fajl;
                               end;
                          except on E:Exception do
                              ShowMessage('ГРЕШКА: ' + E.Message);
                          end;;

                          dm.tblPratiMailStavki.Next;
                          kraj_s:= dm.tblPratiMailStavki.Eof;
                        end;

                        RecipientEmail:=dm.tblPratiMailEMAIL.Value;
                        SubjectEmail:='Резултати од Санитарни прегледи';
                        BodyText:='Во прилог се резултатаи од Санитарни прегледи за фирма '+dm.tblPratiMailPARTNER_NAZIV.Value;

                        dmRes.EmailMessage.From.Address := dmMat.tblMailSetupFROM_MAIL.Value;
                        dmRes.EmailMessage.CharSet := 'utf-8';
                        dmRes.EmailMessage.AttachmentEncoding := 'UUE';
                        dmRes.EmailMessage.Recipients.EMailAddresses := RecipientEmail;
                        dmRes.EmailMessage.Subject := 'Резултати од санитарни прегледи';
                        dmRes.EmailMessage.Body.Text :=  UTF8Encode('Во прилог се резултати од санитарни прегледи' + sLineBreak  + sLineBreak + dmKon.firma_naziv + sLineBreak + dmKon.firma_adresa + sLineBreak + dmKon.firma_mesto);
                        dmRes.EmailMessage.BccList.Clear;

                        try
                           try
                             if (dmKon.email_api_active) then
                                dmRes.SendWebApiEmail(dmRes.EmailMessage.Recipients.EMailAddresses, dmRes.EmailMessage.Subject, dmRes.EmailMessage.Body.Text, '', memStream, naziv_fajl)
                             else
                             begin
                               dmRes.SMTP.Connect;
                               dmRes.SMTP.Authenticate;
                               dmRes.SMTP.Send(dmRes.EmailMessage);
                               ShowMessage('Пораката е успешно пратена на : ' + RecipientEmail);
                             end;


                             dmRes.EmailMessage.Clear;


                             dm.qUpdateMailPotvrdaPartner.Close;
                             dm.qUpdateMailPotvrdaPartner.ParamByName('partner').Value:= dm.tblPratiMailPARTNER.Value;;
                             dm.qUpdateMailPotvrdaPartner.ParamByName('tip_partner').Value:= dm.tblPratiMailTIP_PARTNER.Value;;
                             dm.qUpdateMailPotvrdaPartner.ExecQuery;
                             aPregledaj.Execute();
                           except on E:Exception do
                              ShowMessage('ГРЕШКА: ' + E.Message);
                           end;
                        finally
                            FreeAndNil(MemStream);
                            dmRes.frxPDFExport1.Stream := nil;
                            dmRes.SMTP.Disconnect;
                        end;
                  end
               else  ShowMessage('Немате подесено еМаil адреса за фирмата '+dm.tblPratiMailPARTNER_naziv.Value);
               dm.tblPratiMail.Next;
               kraj:= dm.tblPratiMail.Eof;
              end;
           end
      end
    else ShowMessage('Немате подесено еМаil адреса за фирмата');
end;


procedure TfrmListaPrimeniPacienti.EmailAttachment(attachment_file : AnsiString = ''; attachment_stream : TMemoryStream = nil; naziv_fajl : AnsiString = '');
var re : integer;
begin

     if (attachment_file <> '') then
      begin
        if (FileExists(attachment_file)) then Attachment := TIdAttachmentFile.Create(dmRes.EmailMessage.MessageParts,attachment_file);
    //    Attachment.FileName := attachment_file;
      end;

      if (attachment_stream <> nil) then
      begin
        AttachmentMem := TIdAttachmentMemory.Create(dmRes.EmailMessage.MessageParts);
        AttachmentMem.LoadFromStream(attachment_stream);

        AttachmentMem.ContentType := 'application/pdf';
        AttachmentMem.FileName := naziv_fajl;
      end;
end;
//procedure TfrmListaPrimeniPacienti.aPratiRezultatMailPartnerExecute(
//  Sender: TObject);
//var i:integer;  pdfStream:TFileStream; status, kraj, kraj_s:Boolean;
//    RecipientEmail,attachment_file:string;
//begin
//     dm.qUpdatePregledMail_NulL.Close;
//     dm.qUpdatePregledMail_NulL.ExecQuery;
//
//     with cxGrid1DBTableView1.DataController do
//       for  i:=0 to FilteredRecordCount-1 do
//          begin
//            if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index] = 1 then
//               begin
//                 dm.qUpdatePregledMail.Close;
//                 dm.qUpdatePregledMail.ParamByName('pregled_id').Value:= Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
//                 dm.qUpdatePregledMail.ExecQuery;
//               end;
//          end;
//
//     dm.tblPratiMail.Close;
//     dm.tblPratiMail.Open;
//
//     if dm.tblPratiMail.IsEmpty then
//        kraj:=true
//     else
//        kraj := false;
//
//     if kraj = false then
//        begin
//          dmMat.tblMailSetup.Close;
//          dmMat.tblMailSetup.Open;
//          if (dmMat.tblMailSetup.Locate('RE', dmKon.re, [])) then
//            begin
//              dm.SMTP.Host := dmMat.tblMailSetupSMTP_HOST.Value;
//              dm.SMTP.Port := dmMat.tblMailSetupSMTP_PORT.Value;
//              dm.SMTP.Username := dmMat.tblMailSetupLOGIN.Value;
//              dm.SMTP.Password := dmMat.tblMailSetupLOGIN_PASSWORD.Value;
//
//              dm.SMTP.IOHandler := dm.SSLHandler;
//              dm.SMTP.UseTLS := utUseExplicitTLS;
//              dm.SSLHandler.MaxLineAction := maException;
//              dm.SSLHandler.SSLOptions.Method := sslvTLSv1;
//              dm.SSLHandler.SSLOptions.Mode := sslmUnassigned;
//              dm.SSLHandler.SSLOptions.VerifyMode := [];
//              dm.SSLHandler.SSLOptions.VerifyDepth := 0;
//
//              dm.EmailMessage.From.Address := dmMat.tblMailSetupFROM_MAIL.Value;
//              dm.EmailMessage.Subject := 'Резултати од санитарни прегледи';
//              dm.EmailMessage.Body.Text := 'Во прилог се резултати од санитарни прегледи';
//
//              try
//                dm.SMTP.Connect;
//              finally
//                  if (dm.SMTP.Connected()) then
//                     if(dm.SMTP.Authenticate()) then
//                       begin
//                          while (kraj = false) do
//                              begin
//                                dm.tblPratiMailStavki.Close;
//                                dm.tblPratiMailStavki.ParamByName('partner').Value:=dm.tblPratiMailPARTNER.Value;
//                                dm.tblPratiMailStavki.ParamByName('tip_partner').Value:=dm.tblPratiMailTIP_PARTNER.Value;
//                                dm.tblPratiMailStavki.Open;
//                                if dm.tblPratiMailStavki.IsEmpty then
//                                   kraj_s:=true
//                                else
//                                   kraj_s := false;
//
//                                while (kraj_s = false) do
//                                  begin
//                                    try
//                                        dmRes.Spremi('SAN',1);
//                                        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPratiMailStavkiID.Value;
//                                        dmKon.tblSqlReport.Open;
//                                        dm.tblRezultatAnaliza.Close;
//                                        dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=dm.tblPratiMailStavkiID.Value;
//                                        dm.tblRezultatAnaliza.Open;
//                                        dm.tblFrxAntibiogram.Close;
//                                        dm.tblFrxAntibiogram.Open;
//                                        dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(dm.tblPratiMailStavkiPACIENT_NAZIV.Value));
//                                        dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(dm.tblPratiMailStavkiBROJ.Value)));
//                                    except
//                                        ShowMessage('Не може да се прочита извештајот!');
//                                    end ;
//
//                                    //export to pdf  ---------------------------------------------------------------------------------------------
//                                    if FileExists(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3') then
//                                       dm.frxRezultatMail.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3');
//                                    dm.frxRezultatMail.PrepareReport(true);
//
//                                    attachment_file:=dm.mail_pateka+'Rezultati_'+ VarToStr(dm.tblPratiMailStavkiBROJ.Value)+'_'+VarToStr(dm.tblPratiMailStavkiGODINA.Value)+'.pdf';
//
//                                    dm.frxPDFExport1.FileName:=attachment_file;
//                                      try
//                                         pdfStream := TFileStream.Create(attachment_file, fmCreate or fmShareDenyNone);
//                                         try
//                                            dm.frxPDFExport1.Stream := pdfStream;
//                                            try
//                                               dm.frxRezultatMail.Export(dm.frxPDFExport1);
//                                            except on Exception do  status:=false;
//                                            end;
//                                         finally
//                                            FreeAndNil(pdfStream);
//                                            dm.frxPDFExport1.Stream := NIL;
//                                         end;
//                                      except on Exception do
//                                      end;
//
//                                      //-----------------------------------------------------------------------------------------------------------------
//
//                                    if FileExists(attachment_file) then
//                                       begin
//                                           Attachment := TIdAttachmentFile.Create(dm.EmailMessage.MessageParts,attachment_file);
//                                       end;
//
//                                    dm.tblPratiMailStavki.Next;
//                                    kraj_s:= dm.tblPratiMailStavki.Eof;
//                                  end;
//
//                                RecipientEmail:=dm.tblPratiMailEMAIL.Value;
//                                dm.EmailMessage.Recipients.EMailAddresses:=RecipientEmail;
//
//                                //send mail
//                                try
//                                   dm.SMTP.Send(dm.EmailMessage);
//                                   ShowMessage('Пораката е успешно пратена на : ' + RecipientEmail);
//
//                                   dm.qUpdateMailPotvrdaPartner.Close;
//                                   dm.qUpdateMailPotvrdaPartner.ParamByName('partner').Value:= dm.tblPratiMailPARTNER.Value;;
//                                   dm.qUpdateMailPotvrdaPartner.ParamByName('tip_partner').Value:= dm.tblPratiMailTIP_PARTNER.Value;;
//                                   dm.qUpdateMailPotvrdaPartner.ExecQuery;
//
//                                   aPregledaj.Execute();
//                                except on E:Exception do
//                                   ShowMessage('ГРЕШКА: ' + E.Message);
//                                end;
//                                Attachment.Destroy;
//                                dm.tblPratiMail.Next;
//                                kraj:= dm.tblPratiMail.Eof;
//                              end;
//                       end;
//                  if dm.SMTP.Connected then
//                     dm.SMTP.Disconnect;
//              end;
//            end
//          else  ShowMessage('Грешка! Немате дефинирано email адреса за фирмата/раб.единица');
//        end;
//
//end;

procedure TfrmListaPrimeniPacienti.aPratiRezultatMailMoeExecute(
  Sender: TObject);
var i:integer;  pdfStream:TFileStream; status:Boolean;
Attachment: TIdAttachmentfile;
begin

 dm.SMTP.IOHandler := dm.SSLHandler;
 dm.SMTP.Host := dm.mail_smtp;
 dm.SMTP.Port := dm.mail_smtp_port;
 dm.SMTP.Username := dm.mail;
 dm.SMTP.Password := dm.mail_pass;
 dm.SMTP.UseTLS := utUseExplicitTLS;

 dm.SSLHandler.MaxLineAction := maException;
 dm.SSLHandler.SSLOptions.Method := sslvTLSv1_2;
 dm.SSLHandler.SSLOptions.Mode := sslmUnassigned;
 dm.SSLHandler.SSLOptions.VerifyMode := [];
 dm.SSLHandler.SSLOptions.VerifyDepth := 0;

 dm.Email.From.Address := dm.mail;
 dm.Email.Recipients.EmailAddresses := 'danicastojkova@yahoo.com';
 dm.Email.Subject := 'Rezultati od sanitarni pregledi';
 dm.Email.Body.Text := 'Rezultati od sanitarni pregledi';
 try
     dm.SMTP.Connect;
 finally
     if (dm.SMTP.Connected()) then
       if(dm.SMTP.Authenticate()) then
          begin
            with cxGrid1DBTableView1.DataController do
               for  i:=0 to FilteredRecordCount-1 do
                 begin
                    if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PRATI_EMAIL.Index] = 1 then
                        begin
                          status:=true;
                          try
                              dmRes.Spremi('SAN',1);
                              dmKon.tblSqlReport.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                              dmKon.tblSqlReport.Open;
                              dm.tblRezultatAnaliza.Close;
                              dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                              dm.tblRezultatAnaliza.Open;
                              dm.tblFrxAntibiogram.Close;
                              dm.tblFrxAntibiogram.Open;
                              dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PACIENT_NAZIV.Index]));
                              dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])));
                          except
                                  ShowMessage('Не може да се прочита извештајот!');
                          end ;

                          if FileExists(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3') then
                              dm.frxRezultatMail.LoadFromFile(ExtractFilePath(Application.ExeName)+'\Reports\RezultatMail.fr3');
                          dm.frxRezultatMail.PrepareReport(true);
                          dm.frxPDFExport1.FileName:='Rezultati_'+ VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])+'_'+VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GODINA.Index])+'.pdf';


                          try
                              pdfStream := TFileStream.Create(dm.mail_pateka+'Rezultati_'+ VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])+'_'+VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GODINA.Index])+'.pdf', fmCreate or fmShareDenyNone);
                              try
                                dm.frxPDFExport1.Stream := pdfStream;
                                try
                                    dm.frxRezultatMail.Export(dm.frxPDFExport1);
                                except on Exception do  status:=false;
                                end;
                              finally
                                    FreeAndNil(pdfStream);
                                    dm.frxPDFExport1.Stream := NIL;
                              end;
                           except on Exception do
                            end;

                          Attachment := TIdAttachmentFile.Create(dm.Email.MessageParts,
                                                                 dm.mail_pateka+'Rezultati_'+ VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1BROJ.Index])+'_'+VarToStr(Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GODINA.Index])+'.pdf');

                          try
                           dm.SMTP.Send(dm.Email);
                           except on Exception do

                           end;

                        end;
                 end;
          end;

 end;
 dm.SMTP.Disconnect;

end;


procedure TfrmListaPrimeniPacienti.aPregledajExecute(Sender: TObject);
begin
    dm.master.Close;
    dm.detail.Close;
    dm.master.ParamByName('param').Value:=RadioStatus.EditValue;
    dm.master.ParamByName('godina').Value:=Godina.EditValue;
    dm.master.ParamByName('re').Value:=dmKon.re;
    dm.master.ParamByName('datum').Value:=Now;
    if ((DatumOd.Text <> '') and (DatumDo.Text <> '')) then
      begin
       dm.master.ParamByName('param_datum').Value:=2;
       dm.master.ParamByName('datum_od').Value:=DatumOd.Date;
       dm.master.ParamByName('datum_do').Value:=DatumDo.Date;
      end
    else
       dm.master.ParamByName('param_datum').Value:=0;
    if ((BrojOd.Text <> '')and (BrojDo.Text <> '')) then
      begin
       dm.master.ParamByName('param_broj').Value:=1;
       dm.master.ParamByName('broj_od').Value:=BrojOd.Text;
       dm.master.ParamByName('broj_do').Value:=BrojDo.Text;
      end
    else
       dm.master.ParamByName('param_broj').Value:=0 ;

    if stringUsugi.Text <> '' then
       dm.master.ParamByName('u_lista').Value:=stringUsugi.EditValue
    else
       dm.master.ParamByName('u_lista').Value:='%';
    dm.master.Open;
    dm.detail.Open;
end;

procedure TfrmListaPrimeniPacienti.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	Акција за спуштање или собирање на редовите во гридот кога има групирање по некоја колона
procedure TfrmListaPrimeniPacienti.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	Акција за бришење на запомнатите сетирања за печатење (Utils.pas)
procedure TfrmListaPrimeniPacienti.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmListaPrimeniPacienti.aDizajnListaPriemUslugiExecute(
  Sender: TObject);
begin
     dmRes.Spremi('SAN',9);
     dmRes.frxReport1.DesignReport();
end;

//  Акција за повик на форма за конфигурирање на изгледот и функционалноста на
//  формите(според логираниот корисник)
procedure TfrmListaPrimeniPacienti.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// КРАЈ -> Процедури за сетирање, зачувување на својствата и изгледот на Print системот
//----------------------------------------------------------------------------------

// Процедура за зачувување на својствата и изгледот на Print системот во .ini датотека
procedure TfrmListaPrimeniPacienti.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// Процедура за читање на својствата и изгледот на Print системот зачувани во .ini датотека
procedure TfrmListaPrimeniPacienti.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
