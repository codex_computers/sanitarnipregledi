unit sifMikroorganizmiUslugi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxSkinOffice2013White, cxNavigator, System.Actions;

type
  TfrmMikroorganizmiUslugi = class(TfrmMaster)
    USLUGA: TcxDBLookupComboBox;
    Label2: TLabel;
    USLUGA_ID: TcxDBTextEdit;
    MIKRO_ID: TcxDBLookupComboBox;
    cxGrid1DBTableView1USLUGA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1USLUGANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MIKRO_ID: TcxGridDBColumn;
    cxGrid1DBTableView1MIKRONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMikroorganizmiUslugi: TfrmMikroorganizmiUslugi;

implementation

{$R *.dfm}

uses dmUnit, sifUslugi, sifMikroorganizmi;

procedure TfrmMikroorganizmiUslugi.aAzurirajExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    Sifra.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');
end;

procedure TfrmMikroorganizmiUslugi.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    Sifra.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblMikroUslugiUSLUGA_ID.Value:=dm.tblUslugiID.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

procedure TfrmMikroorganizmiUslugi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
       VK_INSERT:
        begin
          if ((Sender = USLUGA_ID) or (Sender = USLUGA)) then
             begin
               frmUslugi:=TfrmUslugi.Create(self,false);
               frmUslugi.ShowModal;
               if (frmUslugi.ModalResult = mrOK) then
                    dm.tblMikroUslugiUSLUGA_ID.Value := StrToInt(frmUslugi.GetSifra(0));
                frmUslugi.Free;
             end;
          if ((Sender = MIKRO_ID) or (Sender = Sifra)) then
             begin
               frmMikroorganizmi:=TfrmMikroorganizmi.Create(self,false);
               frmMikroorganizmi.ShowModal;
               if (frmMikroorganizmi.ModalResult = mrOK) then
                    dm.tblMikroUslugiMIKRO_ID.Value := StrToInt(frmMikroorganizmi.GetSifra(0));
                frmMikroorganizmi.Free;
             end;
        end;
   end;
end;

procedure TfrmMikroorganizmiUslugi.FormShow(Sender: TObject);
begin
  inherited;
   dm.tblMikroorganizmi.Open;

  dm.tblMikroUslugi.Close;
  dm.tblMikroUslugi.ParamByName('usluga').Value:=dm.tblUslugiID.Value;
  dm.tblMikroUslugi.Open;
end;

end.
