unit IzvestaiGrupni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxLabel, dxSkinOffice2013White, dxCore, cxDateUtils, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmGrupniIzvestai = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    cxLabel2: TcxLabel;
    cbGodina: TcxComboBox;
    cxLabel1: TcxLabel;
    cbMesec: TcxComboBox;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aMesecenIzvestaj: TAction;
    aDizajnMesecenPoUslugi: TAction;
    PARTNERNAZIV: TcxExtLookupComboBox;
    cxLabel3: TcxLabel;
    aFinansiskaKartica: TAction;
    cxLabel4: TcxLabel;
    DatumOd: TcxDateEdit;
    cxLabel5: TcxLabel;
    DatumDo: TcxDateEdit;
    dxBarLargeButton19: TdxBarLargeButton;
    TIP_PARTNER: TcxTextEdit;
    PARTNER: TcxTextEdit;
    dxBarLargeButton20: TdxBarLargeButton;
    PopupMenu2: TPopupMenu;
    dxBarLargeButton21: TdxBarLargeButton;
    aMesecenPoKategorjaRM: TAction;
    dxBarManager1Bar5: TdxBar;
    aDizajnMesencenPoKategorijaRM: TAction;
    aDizajnMesecenPoUslugi1: TMenuItem;
    aDizajnMesencenPoKategorijaRM1: TMenuItem;
    aPopUp: TAction;
    aPeriodicenIzvestaj: TAction;
    aDizajnPeriodicenIzvestaj: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    N2: TMenuItem;
    dxBarManager1Bar7: TdxBar;
    aPGodisenIzvestajZaRabota: TAction;
    aDGodisenIzvestajZaRabota: TAction;
    aDIzvestuvanjeBacilonositelstvo1: TMenuItem;
    dxBarLargeButton23: TdxBarLargeButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aMesecenIzvestajExecute(Sender: TObject);
    procedure aDizajnMesecenPoUslugiExecute(Sender: TObject);
    procedure aFinansiskaKarticaExecute(Sender: TObject);
    procedure dxBarLargeButton20Click(Sender: TObject);
    procedure PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
    procedure aMesecenPoKategorjaRMExecute(Sender: TObject);
    procedure aDizajnMesencenPoKategorijaRMExecute(Sender: TObject);
    procedure aPopUpExecute(Sender: TObject);
    procedure aPeriodicenIzvestajExecute(Sender: TObject);
    procedure aDizajnPeriodicenIzvestajExecute(Sender: TObject);
    procedure aPGodisenIzvestajZaRabotaExecute(Sender: TObject);
    procedure aDGodisenIzvestajZaRabotaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmGrupniIzvestai: TfrmGrupniIzvestai;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmGrupniIzvestai.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmGrupniIzvestai.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmGrupniIzvestai.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmGrupniIzvestai.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmGrupniIzvestai.aBrisiIzgledExecute(Sender: TObject);
begin
  //brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmGrupniIzvestai.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmGrupniIzvestai.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmGrupniIzvestai.aPGodisenIzvestajZaRabotaExecute(
  Sender: TObject);
begin
      try
        dmRes.Spremi('SAN',13);
        dmKon.tblSqlReport.ParamByName('godina').Value:=cbGodina.EditValue;
        dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
        dmKon.tblSqlReport.ParamByName('param_mesec').Value:=0;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'godina', QuotedStr(cbGodina.Text));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmGrupniIzvestai.aMesecenIzvestajExecute(Sender: TObject);
var mesec:Integer;
begin
if cbMesec.Text <> '' then
   begin

     if cbMesec.Text = '�������' then  begin mesec:=1;   end
     else if cbMesec.Text = '��������' then begin mesec:=2;   end
     else if cbMesec.Text = '����' then  begin mesec:=3;   end
     else if cbMesec.Text = '�����' then  begin mesec:=4;   end
     else if cbMesec.Text = '��' then  begin mesec:=5;   end
     else if cbMesec.Text = '����' then  begin mesec:=6;   end
     else if cbMesec.Text = '����' then begin mesec:=7;   end
     else if cbMesec.Text = '������' then begin mesec:=8;   end
     else if cbMesec.Text = '���������' then  begin mesec:=9;   end
     else if cbMesec.Text = '��������' then begin mesec:=10;   end
     else if cbMesec.Text = '�������' then  begin mesec:=11;   end
     else if cbMesec.Text = '��������' then  begin mesec:=12;   end;

      try
        dmRes.Spremi('SAN',3);
        dmKon.tblSqlReport.ParamByName('mesec').Value:=mesec;
        dmKon.tblSqlReport.ParamByName('godina').Value:=cbGodina.EditValue;
        dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'mesec', QuotedStr(cbMesec.Text));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'godina', QuotedStr(cbGodina.Text));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
   end
else ShowMessage('�������� ����� !');
end;

procedure TfrmGrupniIzvestai.aMesecenPoKategorjaRMExecute(Sender: TObject);
var mesec:Integer;
begin
if cbMesec.Text <> '' then
   begin

     if cbMesec.Text = '�������' then  begin mesec:=1;   end
     else if cbMesec.Text = '��������' then begin mesec:=2;   end
     else if cbMesec.Text = '����' then  begin mesec:=3;   end
     else if cbMesec.Text = '�����' then  begin mesec:=4;   end
     else if cbMesec.Text = '��' then  begin mesec:=5;   end
     else if cbMesec.Text = '����' then  begin mesec:=6;   end
     else if cbMesec.Text = '����' then begin mesec:=7;   end
     else if cbMesec.Text = '������' then begin mesec:=8;   end
     else if cbMesec.Text = '���������' then  begin mesec:=9;   end
     else if cbMesec.Text = '��������' then begin mesec:=10;   end
     else if cbMesec.Text = '�������' then  begin mesec:=11;   end
     else if cbMesec.Text = '��������' then  begin mesec:=12;   end;

      try
        dmRes.Spremi('SAN',5);
        dmKon.tblSqlReport.ParamByName('mesec').Value:=mesec;
        dmKon.tblSqlReport.ParamByName('param_mesec').Value:=1;
        dmKon.tblSqlReport.ParamByName('godina').Value:=cbGodina.EditValue;
        dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'mesec', QuotedStr(cbMesec.Text));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'godina', QuotedStr(cbGodina.Text));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
   end
else ShowMessage('�������� ����� !');
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmGrupniIzvestai.aSnimiIzgledExecute(Sender: TObject);
begin
//  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmGrupniIzvestai.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmGrupniIzvestai.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
         if (kom = PARTNERNAZIV) then
              begin
                frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
                frmNurkoRepository.kontrola_naziv := kom.Name;
                frmNurkoRepository.ShowModal;

                if (frmNurkoRepository.ModalResult = mrOk) then
                    PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);

                frmNurkoRepository.Free;
              end;
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmGrupniIzvestai.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmGrupniIzvestai.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
        begin
          ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
        end
end;

procedure TfrmGrupniIzvestai.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmGrupniIzvestai.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmGrupniIzvestai.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmGrupniIzvestai.PARTNERNAZIVPropertiesEditValueChanged(
  Sender: TObject);
begin
    if PARTNERNAZIV.Text <> '' then
          begin
            TIP_PARTNER.Text:=IntToStr(PARTNERNAZIV.EditValue[0]);
            PARTNER.Text:=IntToStr(PARTNERNAZIV.EditValue[1]);
          end
        else
         begin
            PARTNER.Clear;
             TIP_PARTNER.Clear;
         end;
end;

procedure TfrmGrupniIzvestai.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmGrupniIzvestai.prefrli;
begin
end;

procedure TfrmGrupniIzvestai.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmGrupniIzvestai.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmGrupniIzvestai.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;
    cbGodina.EditValue:=dmKon.godina;
    dmMat.tblNurko.Open;

    PARTNERNAZIV.RepositoryItem := dmRes.InitRepository(-1, 'PARTNERNAZIV', Name, rData);
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmGrupniIzvestai.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmGrupniIzvestai.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGrupniIzvestai.dxBarLargeButton20Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmGrupniIzvestai.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmGrupniIzvestai.aOtkaziExecute(Sender: TObject);
begin

      ModalResult := mrCancel;
      Close();
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmGrupniIzvestai.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmGrupniIzvestai.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmGrupniIzvestai.aPeriodicenIzvestajExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('SAN',6);
        dmKon.tblSqlReport.ParamByName('godina').Value:=cbGodina.EditValue;
        dmKon.tblSqlReport.ParamByName('re').Value:=dmKon.re;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'godina', QuotedStr(cbGodina.Text));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end

end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmGrupniIzvestai.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmGrupniIzvestai.aPopUpExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500);
end;

procedure TfrmGrupniIzvestai.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmGrupniIzvestai.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmGrupniIzvestai.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmGrupniIzvestai.aDizajnMesecenPoUslugiExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',3);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmGrupniIzvestai.aDizajnMesencenPoKategorijaRMExecute(
  Sender: TObject);
begin
      dmRes.Spremi('SAN',5);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmGrupniIzvestai.aDizajnPeriodicenIzvestajExecute(Sender: TObject);
begin
      dmRes.Spremi('SAN',6);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmGrupniIzvestai.aDGodisenIzvestajZaRabotaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('SAN',13);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmGrupniIzvestai.aFinansiskaKarticaExecute(Sender: TObject);
begin
  if ((DatumOd.Text <> '') and (DatumDo.Text <> '') and (TIP_PARTNER.Text <> '') and (PARTNER.Text <> '') and (PARTNERNAZIV.Text <> '')) then
    begin
      try
        dmRes.Spremi('SAN',4);
        dmKon.tblSqlReport.ParamByName('IN_RE').Value:=dmKon.re;
        dmKon.tblSqlReport.ParamByName('IN_GOD').Value:=cbGodina.EditValue;
        dmKon.tblSqlReport.ParamByName('IN_TP').Value:=TIP_PARTNER.Text;
        dmKon.tblSqlReport.ParamByName('IN_P').Value:=Partner.Text;
        dmKon.tblSqlReport.ParamByName('IN_ODDATUM').Value:=DatumOd.EditValue;
        dmKon.tblSqlReport.ParamByName('IN_DODATUM').Value:=DatumDo.EditValue;
        dmKon.tblSqlReport.Open;

//        dmRes.frxReport1.Variables.AddVariable('VAR', 'DATA_OD', QuotedStr(DateToStr(DatumOd.Date)));
//        dmRes.frxReport1.Variables.AddVariable('VAR', 'DATA_DO', QuotedStr(DateToStr(DatumDo.Date)));
//        dmRes.frxReport1.Variables.AddVariable('VAR', 'DATUM', QuotedStr(DateToStr(Now)));
//        dmRes.frxReport1.Variables.AddVariable('VAR', 'INPUT', QuotedStr('Input'));
//        dmRes.frxReport1.Variables.AddVariable('VAR', 'KONTO', QuotedStr('Konto'));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'PARTNER', QuotedStr(PARTNERNAZIV.Text));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
    end
   else  ShowMessage('��������� �� ������� �� ������ � �������!');
end;

procedure TfrmGrupniIzvestai.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmGrupniIzvestai.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmGrupniIzvestai.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmGrupniIzvestai.ZemiImeDvoenKluc(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;


end.
