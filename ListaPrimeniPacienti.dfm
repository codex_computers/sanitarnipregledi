object frmListaPrimeniPacienti: TfrmListaPrimeniPacienti
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1085#1080' '#1087#1072#1094#1080#1077#1085#1090#1080
  ClientHeight = 705
  ClientWidth = 1135
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1135
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 682
    Width = 1135
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F10 - '#1055'e'#1095#1072#1090#1080' '#1083#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1084#1077#1085#1080' '#1087#1072#1094#1080#1077#1085#1090#1080' '#1080' '#1091#1089#1083#1091#1075#1080', Esc - '#1054#1090#1082#1072#1078#1080' / ' +
          #1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1135
    Height = 195
    Align = alTop
    TabOrder = 2
    object cxLabel4: TcxLabel
      Left = 20
      Top = 109
      Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object DatumOd: TcxDateEdit
      Tag = 1
      Left = 93
      Top = 110
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object cxLabel5: TcxLabel
      Left = 240
      Top = 111
      Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object DatumDo: TcxDateEdit
      Tag = 1
      Left = 313
      Top = 110
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object ZapisiButton: TcxButton
      Left = 486
      Top = 132
      Width = 115
      Height = 25
      Action = aPregledaj
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 10
    end
    object BrojOd: TcxTextEdit
      Left = 93
      Top = 139
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object BrojDo: TcxTextEdit
      Left = 313
      Top = 139
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object cxLabel1: TcxLabel
      Left = 30
      Top = 137
      Caption = #1041#1088#1086#1112' '#1086#1076' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 250
      Top = 137
      Caption = #1041#1088#1086#1112' '#1076#1086' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object Godina: TcxComboBox
      Left = 93
      Top = 17
      Properties.DropDownListStyle = lsFixedList
      Properties.Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object cxLabel2: TcxLabel
      Left = 32
      Top = 16
      Caption = #1043#1086#1076#1080#1085#1072' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object RadioStatus: TcxRadioGroup
      Left = 93
      Top = 44
      TabStop = False
      Caption = #1057#1090#1072#1090#1091#1089
      Properties.Columns = 3
      Properties.DefaultValue = 0
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077
          Value = 0
        end
        item
          Caption = #1047#1072#1074#1088#1096#1077#1085#1080
          Value = 1
        end
        item
          Caption = #1053#1077#1079#1072#1074#1088#1096#1077#1085#1080
          Value = 2
        end>
      Properties.WordWrap = True
      ItemIndex = 0
      TabOrder = 1
      Transparent = True
      Height = 53
      Width = 341
    end
    object cxButton1: TcxButton
      Left = 607
      Top = 132
      Width = 122
      Height = 25
      Action = aIsprazniPolinja
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 12
    end
    object stringUsugi: TcxLookupComboBox
      Left = 313
      Top = 17
      Properties.KeyFieldNames = 'U_LISTA'
      Properties.ListColumns = <
        item
          FieldName = 'U_LISTA'
        end>
      Properties.ListSource = dm.dsUslugiStringPriem
      TabOrder = 13
      Width = 121
    end
    object cxLabel6: TcxLabel
      Left = 256
      Top = 16
      Caption = #1059#1089#1083#1091#1075#1080' :'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
    end
    object Panel2: TPanel
      Left = 1
      Top = 166
      Width = 1133
      Height = 28
      Align = alBottom
      Color = clGradientInactiveCaption
      ParentBackground = False
      TabOrder = 15
    end
    object CheckBox1: TCheckBox
      Left = 32
      Top = 172
      Width = 129
      Height = 17
      Caption = #1064#1090#1080#1082#1083#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 16
      OnClick = CheckBox1Click
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 321
    Width = 1135
    Height = 361
    Align = alClient
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
      DataController.DataSource = dm.dsMaster
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.KeyFieldNames = 'ID'
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          Column = cxGrid1DBTableView1BROJ
        end
        item
          Format = '0.00,.'
          Kind = skSum
          Column = cxGrid1DBTableView1CENA
        end>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
      OptionsView.Footer = True
      object cxGrid1DBTableView1PRATI_EMAIL: TcxGridDBColumn
        Caption = #1055#1088#1072#1090#1080' '#1087#1086' '#1077'Mail'
        DataBinding.ValueType = 'Smallint'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.ShowEndEllipsis = True
        Properties.ValueChecked = '1'
        Properties.ValueUnchecked = '0'
      end
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Editing = False
        Width = 43
      end
      object cxGrid1DBTableView1GODINA: TcxGridDBColumn
        DataBinding.FieldName = 'GODINA'
        Visible = False
        Options.Editing = False
        Width = 60
      end
      object cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_PRIEM'
        Options.Editing = False
        Width = 88
      end
      object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_VAZENJE'
        Visible = False
        Options.Editing = False
        Width = 97
      end
      object cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn
        DataBinding.FieldName = 'DATUM_ZAVERKA'
        Visible = False
        Options.Editing = False
        Width = 121
      end
      object cxGrid1DBTableView1BROJ: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ'
        Options.Editing = False
        Width = 91
      end
      object cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn
        DataBinding.FieldName = 'PACIENT_ID'
        Visible = False
        Options.Editing = False
        Width = 98
      end
      object cxGrid1DBTableView1EMBG: TcxGridDBColumn
        DataBinding.FieldName = 'EMBG'
        Options.Editing = False
        Width = 92
      end
      object cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PACIENT_NAZIV'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 195
      end
      object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
        DataBinding.FieldName = 'PAKET_ID'
        Options.Editing = False
        Width = 224
      end
      object cxGrid1DBTableView1U_LISTA: TcxGridDBColumn
        DataBinding.FieldName = 'U_LISTA'
        Options.Editing = False
        Width = 117
      end
      object cxGrid1DBTableView1CENA: TcxGridDBColumn
        DataBinding.FieldName = 'CENA'
        Options.Editing = False
        Width = 75
      end
      object cxGrid1DBTableView1TP: TcxGridDBColumn
        DataBinding.FieldName = 'TP'
        Visible = False
        Options.Editing = False
        Width = 79
      end
      object cxGrid1DBTableView1P: TcxGridDBColumn
        DataBinding.FieldName = 'P'
        Options.Editing = False
        Width = 91
      end
      object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER_NAZIV'
        Options.Editing = False
        Width = 174
      end
      object cxGrid1DBTableView1PLACANJE: TcxGridDBColumn
        DataBinding.FieldName = 'PLACANJE'
        Visible = False
        Options.Editing = False
        Width = 152
      end
      object cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn
        DataBinding.FieldName = 'NACINPLAKANJE'
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1BR_KVITANCIJA: TcxGridDBColumn
        DataBinding.FieldName = 'BR_KVITANCIJA'
        Options.Editing = False
        Width = 107
      end
      object cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn
        DataBinding.FieldName = 'KATEGORIJA_RM'
        Visible = False
        Options.Editing = False
        Width = 142
      end
      object cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'KATEGORIJANAZIV'
        Options.Editing = False
        Width = 251
      end
      object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
        DataBinding.FieldName = 'RABOTNO_MESTO'
        Visible = False
        Options.Editing = False
        Width = 129
      end
      object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'RABOTNOMESTONAZIV'
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1RE: TcxGridDBColumn
        DataBinding.FieldName = 'RE'
        Visible = False
        Options.Editing = False
        Width = 139
      end
      object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'RENAZIV'
        Visible = False
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1PARTNER_EMAIL: TcxGridDBColumn
        Caption = #1055#1072#1088#1090#1085#1077#1088' - eMail'
        DataBinding.FieldName = 'PARTNER_EMAIL'
        Options.Editing = False
        Width = 135
      end
      object cxGrid1DBTableView1PRATI_MAIL_POTVRDA_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'PRATI_MAIL_POTVRDA_PARTNER'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1044#1072
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 163
      end
      object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
        Caption = #1055#1072#1094#1080#1077#1085#1090' - eMail'
        DataBinding.FieldName = 'EMAIL'
        Options.Editing = False
        Width = 179
      end
      object cxGrid1DBTableView1PRATI_MAIL_POTVRDA: TcxGridDBColumn
        Caption = #1055#1088#1072#1090#1077#1085#1086' '#1087#1086' eMail '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        DataBinding.FieldName = 'PRATI_MAIL_POTVRDA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1044#1072
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 149
      end
      object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
        DataBinding.FieldName = 'TS_INS'
        Visible = False
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'TS_UPD'
        Visible = False
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
        DataBinding.FieldName = 'USR_INS'
        Visible = False
        Options.Editing = False
        Width = 200
      end
      object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'USR_UPD'
        Visible = False
        Options.Editing = False
        Width = 200
      end
    end
    object cxGrid1DBTableView2: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsdetail
      DataController.DetailKeyFieldNames = 'PRIEM_ID'
      DataController.KeyFieldNames = 'STAVKA_ID'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView2ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Width = 51
      end
      object cxGrid1DBTableView2STAVKA_ID: TcxGridDBColumn
        DataBinding.FieldName = 'STAVKA_ID'
      end
      object cxGrid1DBTableView2USLUGA_ID: TcxGridDBColumn
        DataBinding.FieldName = 'USLUGA_ID'
        Width = 90
      end
      object cxGrid1DBTableView2NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 253
      end
      object cxGrid1DBTableView2NAOD: TcxGridDBColumn
        DataBinding.FieldName = 'NAOD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1055#1086#1079#1080#1090#1080#1074#1077#1085
            ImageIndex = 3
            Value = 1
          end
          item
            Description = #1053#1077#1075#1072#1090#1080#1074#1077#1085
            ImageIndex = 2
            Value = 0
          end>
        Width = 88
      end
      object cxGrid1DBTableView2ZAVRSENA: TcxGridDBColumn
        DataBinding.FieldName = 'ZAVRSENA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1047#1072#1074#1088#1096#1077#1085#1072
            ImageIndex = 2
            Value = 1
          end
          item
            Description = #1053#1077' '#1077' '#1079#1072#1074#1088#1096#1077#1085#1072
            ImageIndex = 3
            Value = 0
          end>
        Width = 102
      end
      object cxGrid1DBTableView2CENA: TcxGridDBColumn
        DataBinding.FieldName = 'CENA'
        Width = 75
      end
      object cxGrid1DBTableView2RE: TcxGridDBColumn
        DataBinding.FieldName = 'RE'
        Visible = False
        Width = 143
      end
      object cxGrid1DBTableView2BROJ: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
      object cxGrid1Level2: TcxGridLevel
        GridView = cxGrid1DBTableView2
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 384
    Top = 448
  end
  object PopupMenu1: TPopupMenu
    Left = 656
    Top = 424
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 736
    Top = 376
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 362
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 607
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1167
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aListaPriemUslugi
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPratiRezultatMailPacient
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPratiRezultatMailPartner
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 296
    Top = 448
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPregledaj: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1083#1080#1089#1090#1072
      ImageIndex = 22
      OnExecute = aPregledajExecute
    end
    object aListaPriemUslugi: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1088#1080#1077#1084' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080' '#1080' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aListaPriemUslugiExecute
    end
    object aDizajnListaPriemUslugi: TAction
      Caption = 'aDizajnListaPriemUslugi'
      ShortCut = 24697
      OnExecute = aDizajnListaPriemUslugiExecute
    end
    object aIsprazniPolinja: TAction
      Caption = #1048#1089#1087#1088#1072#1079#1085#1080' '#1087#1086#1083#1080#1114#1072
      ImageIndex = 23
      OnExecute = aIsprazniPolinjaExecute
    end
    object aPratiRezultatMailPacient: TAction
      Caption = #1055#1088#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090' '#1087#1086' eMail '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
      ImageIndex = 45
      OnExecute = aPratiRezultatMailPacientExecute
    end
    object aPratiRezultatMailMoe: TAction
      Caption = #1055#1088#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090' '#1087#1086' eMail moe'
      ImageIndex = 45
      OnExecute = aPratiRezultatMailMoeExecute
    end
    object aPratiRezultatMailPartner: TAction
      Caption = #1055#1088#1072#1090#1080' '#1088#1077#1079#1091#1083#1090#1072#1090' '#1087#1086' eMail '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 45
      OnExecute = aPratiRezultatMailPartnerExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 25000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 512
    Top = 448
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
end
