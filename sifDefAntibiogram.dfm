inherited frmSifDefAntibiogram: TfrmSifDefAntibiogram
  Caption = #1044#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 282
    ExplicitHeight = 282
    inherited cxGrid1: TcxGrid
      Height = 278
      ExplicitHeight = 278
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsDefAntibiogram
        object cxGrid1DBTableView1ANTIBIOGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ANTIBIOGRAM_ID'
          Visible = False
          Width = 122
        end
        object cxGrid1DBTableView1ANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ANAZIV'
          Width = 313
        end
        object cxGrid1DBTableView1LEK_ID: TcxGridDBColumn
          DataBinding.FieldName = 'LEK_ID'
          Visible = False
          Width = 80
        end
        object cxGrid1DBTableView1LNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'LNAZIV'
          Width = 274
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 408
    Height = 122
    ExplicitLeft = -8
    ExplicitTop = 402
    ExplicitHeight = 122
    inherited Label1: TLabel
      Width = 82
      Caption = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' :'
      ExplicitWidth = 82
    end
    object Label2: TLabel [1]
      Left = 31
      Top = 48
      Width = 82
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 119
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      DataBinding.DataField = 'ANTIBIOGRAM_ID'
      DataBinding.DataSource = dm.dsDefAntibiogram
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 119
      ExplicitWidth = 52
      Width = 52
    end
    inherited OtkaziButton: TcxButton
      Top = 82
      TabOrder = 5
      ExplicitTop = 82
    end
    inherited ZapisiButton: TcxButton
      Top = 82
      TabOrder = 4
      ExplicitTop = 82
    end
    object LEK: TcxDBLookupComboBox
      Tag = 1
      Left = 171
      Top = 45
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1090#1080#1082' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1059#1089#1083#1091#1075#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'LEK_ID'
      DataBinding.DataSource = dm.dsDefAntibiogram
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLekovi
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 385
    end
    object LEK_ID: TcxDBTextEdit
      Tag = 1
      Left = 119
      Top = 45
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1090#1080#1082
      BeepOnEnter = False
      DataBinding.DataField = 'LEK_ID'
      DataBinding.DataSource = dm.dsDefAntibiogram
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object ANTIBIOGRAM: TcxDBLookupComboBox
      Tag = 1
      Left = 171
      Top = 18
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084')'
      BeepOnEnter = False
      DataBinding.DataField = 'ANTIBIOGRAM_ID'
      DataBinding.DataSource = dm.dsDefAntibiogram
      Enabled = False
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsAntibiogram
      StyleDisabled.TextColor = clBtnText
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 385
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41354.616989803240000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
