inherited frmLekari: TfrmLekari
  Caption = #1051#1077#1082#1072#1088#1080
  ClientWidth = 854
  ExplicitWidth = 870
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 854
    Height = 218
    ExplicitWidth = 854
    ExplicitHeight = 218
    inherited cxGrid1: TcxGrid
      Width = 850
      Height = 214
      ExplicitWidth = 850
      ExplicitHeight = 214
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsLekari
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 59
        end
        object cxGrid1DBTableView1TITULA: TcxGridDBColumn
          DataBinding.FieldName = 'TITULA'
          Width = 58
        end
        object cxGrid1DBTableView1SPECIJALNOST: TcxGridDBColumn
          DataBinding.FieldName = 'SPECIJALNOST'
          Width = 103
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 108
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
        end
        object cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME_IME'
          Width = 105
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 292
        end
        object cxGrid1DBTableView1POTPIS: TcxGridDBColumn
          DataBinding.FieldName = 'POTPIS'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 167
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 208
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 198
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 237
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 344
    Width = 854
    Height = 186
    ExplicitTop = 344
    ExplicitWidth = 854
    ExplicitHeight = 186
    inherited Label1: TLabel
      Left = 55
      ExplicitLeft = 55
    end
    object Label2: TLabel [1]
      Left = 54
      Top = 48
      Width = 47
      Height = 13
      Caption = #1058#1080#1090#1091#1083#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 15
      Top = 75
      Width = 86
      Height = 13
      Caption = #1057#1087#1077#1094#1080#1112#1072#1083#1085#1086#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 46
      Top = 102
      Width = 55
      Height = 13
      Caption = #1055#1088#1077#1079#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 72
      Top = 129
      Width = 29
      Height = 13
      Caption = #1048#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 107
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1083#1077#1082#1072#1088
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsLekari
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 107
    end
    inherited OtkaziButton: TcxButton
      Left = 763
      Top = 146
      TabOrder = 6
      ExplicitLeft = 763
      ExplicitTop = 146
    end
    inherited ZapisiButton: TcxButton
      Left = 682
      Top = 146
      TabOrder = 5
      ExplicitLeft = 682
      ExplicitTop = 146
    end
    object TITULA: TcxDBTextEdit
      Left = 107
      Top = 45
      Hint = #1058#1080#1090#1091#1083#1072' '#1085#1072' '#1083#1077#1082#1072#1088
      BeepOnEnter = False
      DataBinding.DataField = 'TITULA'
      DataBinding.DataSource = dm.dsLekari
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 262
    end
    object SPECIJALNOST: TcxDBTextEdit
      Left = 107
      Top = 72
      Hint = #1057#1087#1077#1094#1080#1112#1072#1083#1085#1086#1089#1090' '#1085#1072' '#1083#1077#1082#1072#1088
      BeepOnEnter = False
      DataBinding.DataField = 'SPECIJALNOST'
      DataBinding.DataSource = dm.dsLekari
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 262
    end
    object PREZIME: TcxDBTextEdit
      Tag = 1
      Left = 107
      Top = 99
      Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1083#1077#1082#1072#1088
      BeepOnEnter = False
      DataBinding.DataField = 'PREZIME'
      DataBinding.DataSource = dm.dsLekari
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 262
    end
    object IME: TcxDBTextEdit
      Tag = 1
      Left = 107
      Top = 126
      Hint = #1048#1084#1077' '#1085#1072' '#1083#1077#1082#1072#1088
      BeepOnEnter = False
      DataBinding.DataField = 'IME'
      DataBinding.DataSource = dm.dsLekari
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 262
    end
    object Button1: TButton
      Left = 435
      Top = 143
      Width = 122
      Height = 25
      Hint = 'Bitmap  image |*.bmp;*.dib'
      Caption = '...'
      TabOrder = 7
      TabStop = False
      OnClick = Button1Click
    end
    object Slika: TcxDBImage
      Left = 422
      Top = 19
      Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090
      TabStop = False
      DataBinding.DataField = 'POTPIS'
      DataBinding.DataSource = dm.dsLekari
      Properties.GraphicClassName = 'TJPEGImage'
      Properties.PopupMenuLayout.MenuItems = [pmiCut, pmiCopy, pmiPaste, pmiDelete, pmiLoad, pmiSave, pmiCustom]
      Properties.Stretch = True
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      Height = 118
      Width = 147
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 854
    ExplicitWidth = 854
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 854
    ExplicitWidth = 854
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 232
  end
  inherited dxBarManager1: TdxBarManager
    Top = 248
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 240
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41358.661801736110000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 120
    Top = 328
  end
end
