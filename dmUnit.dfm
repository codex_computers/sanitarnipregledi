﻿object dm: Tdm
  OldCreateOrder = False
  Height = 1444
  Width = 1261
  object dsUslugi: TDataSource
    DataSet = tblUslugi
    Left = 120
    Top = 24
  end
  object tblUslugi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_USLUGI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    CENA = :CENA,'
      '    RE = :RE,'
      '    STATUS = :STATUS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    TIP = :TIP'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_USLUGI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_USLUGI('
      '    ID,'
      '    NAZIV,'
      '    CENA,'
      '    RE,'
      '    STATUS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    TIP'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CENA,'
      '    :RE,'
      '    :STATUS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :TIP'
      ')')
    RefreshSQL.Strings = (
      ' select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.re,'
      '        mr.naziv as ReNaziv,'
      '        su.status,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd,'
      '        su.tip'
      'from san_uslugi su'
      
        'left outer join san_pregled_s ps on ps.usluga_id = su.id and ps.' +
        'pregled_id = :mas_id'
      'inner join mat_re mr on mr.id = su.re'
      
        'where(  ((ps.id is null and :param = 1)or (:param = 0) ) and (su' +
        '.tip like :tip)'
      '     ) and (     SU.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      ' select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.re,'
      '        mr.naziv as ReNaziv,'
      '        su.status,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd,'
      '        su.tip'
      'from san_uslugi su'
      
        'left outer join san_pregled_s ps on ps.usluga_id = su.id and ps.' +
        'pregled_id = :mas_id'
      'inner join mat_re mr on mr.id = su.re'
      
        'where ((ps.id is null and :param = 1)or (:param = 0) ) and (su.t' +
        'ip like :tip)'
      'order by id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPregledi
    Left = 32
    Top = 24
    object tblUslugiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblUslugiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUslugiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblUslugiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblUslugiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblUslugiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUslugiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUslugiRE: TFIBIntegerField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblUslugiRENAZIV: TFIBStringField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUslugiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblUslugiTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP'
    end
  end
  object qMaxUslugiSifra: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_uslugi s')
    Left = 480
    Top = 32
  end
  object qMaxPaketiSifra: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_paketi s')
    Left = 480
    Top = 96
  end
  object tblPaketi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PAKETI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    CENA = :CENA,'
      '    KONTROLEN_PREGLED = :KONTROLEN_PREGLED,'
      '    STATUS = :STATUS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_PAKETI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_PAKETI('
      '    ID,'
      '    NAZIV,'
      '    CENA,'
      '    KONTROLEN_PREGLED,'
      '    STATUS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CENA,'
      '    :KONTROLEN_PREGLED,'
      '    :STATUS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.kontrolen_pregled,'
      '        su.status,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd'
      'from san_paketi su'
      ''
      ' WHERE '
      '        SU.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.kontrolen_pregled,'
      '        su.status,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd'
      'from san_paketi su'
      'order by id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 96
    object tblPaketiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPaketiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPaketiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPaketiKONTROLEN_PREGLED: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
      FieldName = 'KONTROLEN_PREGLED'
    end
    object tblPaketiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblPaketiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPaketiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPaketiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPaketiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPaketi: TDataSource
    DataSet = tblPaketi
    Left = 120
    Top = 96
  end
  object dsPaketUsluga: TDataSource
    DataSet = tblPaketUsluga
    Left = 120
    Top = 152
  end
  object tblPaketUsluga: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PAKETI_USLUGI'
      'SET '
      '    PAKET_ID = :PAKET_ID,'
      '    USLUGA_ID = :USLUGA_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    PAKET_ID = :OLD_PAKET_ID'
      '    and USLUGA_ID = :OLD_USLUGA_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_PAKETI_USLUGI'
      'WHERE'
      '        PAKET_ID = :OLD_PAKET_ID'
      '    and USLUGA_ID = :OLD_USLUGA_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_PAKETI_USLUGI('
      '    PAKET_ID,'
      '    USLUGA_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :PAKET_ID,'
      '    :USLUGA_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select pu.paket_id,'
      '       p.naziv as paketNaziv,'
      '       pu.usluga_id,'
      '       u.naziv as uslugaNaziv,'
      '       pu.ts_ins,'
      '       pu.ts_upd,'
      '       pu.usr_ins,'
      '       pu.usr_upd'
      'from san_paketi_uslugi pu'
      'inner join san_paketi p on p.id = pu.paket_id'
      'inner join san_uslugi u on u.id = pu.usluga_id'
      'where(  pu.paket_id = :paket'
      '     ) and (     PU.PAKET_ID = :OLD_PAKET_ID'
      '    and PU.USLUGA_ID = :OLD_USLUGA_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pu.paket_id,'
      '       p.naziv as paketNaziv,'
      '       pu.usluga_id,'
      '       u.naziv as uslugaNaziv,'
      '       pu.ts_ins,'
      '       pu.ts_upd,'
      '       pu.usr_ins,'
      '       pu.usr_upd'
      'from san_paketi_uslugi pu'
      'inner join san_paketi p on p.id = pu.paket_id'
      'inner join san_uslugi u on u.id = pu.usluga_id'
      'where pu.paket_id = :paket'
      'order by pu.paket_id, pu.usluga_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 152
    object tblPaketUslugaPAKET_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
    end
    object tblPaketUslugaPAKETNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1082#1077#1090
      FieldName = 'PAKETNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPaketUslugaUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      FieldName = 'USLUGA_ID'
    end
    object tblPaketUslugaUSLUGANAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      FieldName = 'USLUGANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPaketUslugaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPaketUslugaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPaketUslugaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPaketUslugaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblRabotnoMesto: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_RAB_MESTO'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    ROK = :ROK,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_RAB_MESTO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_RAB_MESTO('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    ROK,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :ROK,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select r.id,'
      '       r.naziv,'
      '       r.opis,'
      '       r.rok,'
      '       r.ts_ins,'
      '       r.ts_upd,'
      '       r.usr_ins,'
      '       r.usr_upd'
      'from san_rab_mesto r'
      ''
      ' WHERE '
      '        R.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select r.id,'
      '       r.naziv,'
      '       r.opis,'
      '       r.rok,'
      '       r.ts_ins,'
      '       r.ts_upd,'
      '       r.usr_ins,'
      '       r.usr_upd'
      'from san_rab_mesto r'
      'order by r.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 208
    object tblRabotnoMestoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabotnoMestoNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoROK: TFIBSmallIntField
      DisplayLabel = #1056#1086#1082
      FieldName = 'ROK'
    end
    object tblRabotnoMestoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRabotnoMestoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRabotnoMestoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRabotnoMesto: TDataSource
    DataSet = tblRabotnoMesto
    Left = 120
    Top = 208
  end
  object qMaxRabotnoMestoID: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_rab_mesto s')
    Left = 480
    Top = 160
  end
  object tblPacienti: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PACIENTI'
      'SET '
      '    BROJ_KARTON = :BROJ_KARTON,'
      '    EMBG = :EMBG,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    TATKOVO = :TATKOVO,'
      '    NAZIV = :NAZIV,'
      '    DATUM_RAGJANJE = :DATUM_RAGJANJE,'
      '    ADRESA = :ADRESA,'
      '    TEL = :TEL,'
      '    EMAIL = :EMAIL,'
      '    MESTO_ZIVEENJE = :MESTO_ZIVEENJE,'
      '    MESTO_RAGJANJE = :MESTO_RAGJANJE,'
      '    R_MESTO = :R_MESTO,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    KATEGORIJA_RM = :KATEGORIJA_RM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_PACIENTI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_PACIENTI('
      '    ID,'
      '    BROJ_KARTON,'
      '    EMBG,'
      '    IME,'
      '    PREZIME,'
      '    TATKOVO,'
      '    NAZIV,'
      '    DATUM_RAGJANJE,'
      '    ADRESA,'
      '    TEL,'
      '    EMAIL,'
      '    MESTO_ZIVEENJE,'
      '    MESTO_RAGJANJE,'
      '    R_MESTO,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    KATEGORIJA_RM,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ_KARTON,'
      '    :EMBG,'
      '    :IME,'
      '    :PREZIME,'
      '    :TATKOVO,'
      '    :NAZIV,'
      '    :DATUM_RAGJANJE,'
      '    :ADRESA,'
      '    :TEL,'
      '    :EMAIL,'
      '    :MESTO_ZIVEENJE,'
      '    :MESTO_RAGJANJE,'
      '    :R_MESTO,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :KATEGORIJA_RM,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.broj_karton,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.tatkovo,'
      '       p.naziv,'
      '       p.datum_ragjanje,'
      '       p.adresa,'
      '       p.tel,'
      '       p.email,'
      '       p.mesto_ziveenje,'
      '       mz.naziv as mestoZiveenje,'
      '       p.mesto_ragjanje,'
      '       mr.naziv as MestoRagjanje,'
      '       p.r_mesto,'
      '       rm.naziv as RabotnoMestoNaziv,'
      '       p.tip_partner,'
      '       p.partner,'
      '       mp.naziv as partnerNaziv,'
      '       krm.naziv as kategorijaRMMaziv,'
      '       p.kategorija_rm,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from san_pacienti p'
      'left outer join mat_mesto mz on mz.id = p.mesto_ziveenje'
      'left outer join mat_mesto mr on mr.id = p.mesto_ragjanje'
      'left outer join san_rab_mesto rm on rm.id = p.r_mesto'
      
        'left outer join mat_partner mp on mp.id = p.partner and mp.tip_p' +
        'artner = p.tip_partner'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      ''
      ' WHERE '
      '        P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.broj_karton,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.tatkovo,'
      '       p.naziv,'
      '       p.datum_ragjanje,'
      '       p.adresa,'
      '       p.tel,'
      '       p.email,'
      '       p.mesto_ziveenje,'
      '       mz.naziv as mestoZiveenje,'
      '       p.mesto_ragjanje,'
      '       mr.naziv as MestoRagjanje,'
      '       p.r_mesto,'
      '       rm.naziv as RabotnoMestoNaziv,'
      '       p.tip_partner,'
      '       p.partner,'
      '       mp.naziv as partnerNaziv,'
      '       krm.naziv as kategorijaRMMaziv,'
      '       p.kategorija_rm,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from san_pacienti p'
      'left outer join mat_mesto mz on mz.id = p.mesto_ziveenje'
      'left outer join mat_mesto mr on mr.id = p.mesto_ragjanje'
      'left outer join san_rab_mesto rm on rm.id = p.r_mesto'
      
        'left outer join mat_partner mp on mp.id = p.partner and mp.tip_p' +
        'artner = p.tip_partner'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'order by p.naziv')
    AutoUpdateOptions.UpdateTableName = 'SAN_PACIENTI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_PACIENTI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 240
    Top = 24
    object tblPacientiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPacientiBROJ_KARTON: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1088#1090#1086#1085
      FieldName = 'BROJ_KARTON'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiEMBG: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'EMBG'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiTATKOVO: TFIBStringField
      DisplayLabel = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077
      FieldName = 'TATKOVO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiDATUM_RAGJANJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077
      FieldName = 'DATUM_RAGJANJE'
    end
    object tblPacientiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085#1089#1082#1080' '#1073#1088#1086#1112
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiEMAIL: TFIBStringField
      DisplayLabel = 'eMail '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiMESTO_ZIVEENJE: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO_ZIVEENJE'
    end
    object tblPacientiMESTOZIVEENJE: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTOZIVEENJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiMESTO_RAGJANJE: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1088#1072#1107#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO_RAGJANJE'
    end
    object tblPacientiMESTORAGJANJE: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1088#1072#1107#1072#1114#1077
      FieldName = 'MESTORAGJANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiR_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'R_MESTO'
    end
    object tblPacientiRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblPacientiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblPacientiPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPacientiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPacientiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiKATEGORIJARMMAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'KATEGORIJARMMAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPacientiKATEGORIJA_RM: TFIBSmallIntField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
  end
  object dsPacienti: TDataSource
    DataSet = tblPacienti
    Left = 320
    Top = 24
  end
  object dsPartner: TDataSource
    AutoEdit = False
    DataSet = tblPartner
    Left = 120
    Top = 274
  end
  object tblPartner: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    mp.TIP_PARTNER,'
      '    mp.ID,'
      '    mp.NAZIV,'
      '    mp.naziv_skraten,'
      '    mp.RE,'
      '    mp.adresa,'
      '    mm.naziv as mestoNaziv,'
      '    mp.danocen as MB'
      'from MAT_PARTNER mp'
      'left outer join mat_mesto mm on mm.id = mp.mesto'
      'order by mp.tip_partner, mp.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 274
    poSQLINT64ToBCD = True
    oTrimCharFields = False
    oRefreshAfterPost = False
    object tblPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblPartnerID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1089#1082#1088#1072#1090#1077#1085
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblPartnerADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerMESTONAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTONAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerMB: TFIBStringField
      DisplayLabel = #1045#1052#1041#1043
      FieldName = 'MB'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblPregledi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PREGLED'
      'SET '
      '    GODINA = :GODINA,'
      '    DATUM_PRIEM = :DATUM_PRIEM,'
      '    BROJ = :BROJ,'
      '    PACIENT_ID = :PACIENT_ID,'
      '    TP = :TP,'
      '    P = :P,'
      '    PAKET_ID = :PAKET_ID,'
      '    CENA = :CENA,'
      '    DATUM_ZAVERKA = :DATUM_ZAVERKA,'
      '    DATUM_VAZENJE = :DATUM_VAZENJE,'
      '    PLACANJE = :PLACANJE,'
      '    RE = :RE,'
      '    RABOTNO_MESTO = :RABOTNO_MESTO,'
      '    KATEGORIJA_RM = :KATEGORIJA_RM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    U_LISTA = :U_LISTA,'
      '    BR_KVITANCIJA = :BR_KVITANCIJA,'
      '    DOGOVOR_ID = :DOGOVOR_ID,'
      '    NAPLATENO = :NAPLATENO,'
      '    FISKALNA = :FISKALNA,'
      '    STORNA = :STORNA,'
      '    DATUM_PRESMETKA = :DATUM_PRESMETKA,'
      '    IZNOS_VKUPNO = :IZNOS_VKUPNO,'
      '    PRATI_MAIL_PARTNER = :PRATI_MAIL_PARTNER,'
      '    PRATI_MAIL_B = :PRATI_MAIL_B,'
      '    PRATI_MAIL_PB = :PRATI_MAIL_PB'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_PREGLED'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_PREGLED('
      '    ID,'
      '    GODINA,'
      '    DATUM_PRIEM,'
      '    BROJ,'
      '    PACIENT_ID,'
      '    TP,'
      '    P,'
      '    PAKET_ID,'
      '    CENA,'
      '    DATUM_ZAVERKA,'
      '    DATUM_VAZENJE,'
      '    PLACANJE,'
      '    RE,'
      '    RABOTNO_MESTO,'
      '    KATEGORIJA_RM,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    U_LISTA,'
      '    BR_KVITANCIJA,'
      '    DOGOVOR_ID,'
      '    NAPLATENO,'
      '    FISKALNA,'
      '    STORNA,'
      '    DATUM_PRESMETKA,'
      '    IZNOS_VKUPNO,'
      '    PRATI_MAIL_PARTNER,'
      '    PRATI_MAIL_B,'
      '    PRATI_MAIL_PB'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :DATUM_PRIEM,'
      '    :BROJ,'
      '    :PACIENT_ID,'
      '    :TP,'
      '    :P,'
      '    :PAKET_ID,'
      '    :CENA,'
      '    :DATUM_ZAVERKA,'
      '    :DATUM_VAZENJE,'
      '    :PLACANJE,'
      '    :RE,'
      '    :RABOTNO_MESTO,'
      '    :KATEGORIJA_RM,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :U_LISTA,'
      '    :BR_KVITANCIJA,'
      '    :DOGOVOR_ID,'
      '    :NAPLATENO,'
      '    :FISKALNA,'
      '    :STORNA,'
      '    :DATUM_PRESMETKA,'
      '    :IZNOS_VKUPNO,'
      '    :PRATI_MAIL_PARTNER,'
      '    :PRATI_MAIL_B,'
      '    :PRATI_MAIL_PB'
      ')')
    RefreshSQL.Strings = (
      '--- Se razlikuvaat Select so Insert, Update, Delete, Refresh'
      '--- Selectot e po procedura'
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      
        '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd,p.u_lista,p.BR_K' +
        'VITANCIJA,'
      '       pk.kontrolen_pregled,'
      '       p.dogovor_id,'
      '       p.naplateno,'
      '       p.fiskalna,'
      '       p.storna,'
      '       p.datum_presmetka,'
      '       p.iznos_vkupno,'
      '       p.prati_mail_partner,'
      '       p.PRATI_MAIL_B,'
      '       p.PRATI_MAIL_PB,'
      '       (select count(fp.faktura_id)'
      '        from san_faktura_priem fp'
      '        where fp.priem_id = p.id) brFakturi'
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum, :para' +
        'm_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join san_paketi pk on pk.naziv = p.paket_id'
      'where(  p.re like :re'
      '     ) and (     P.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      '--- Se razlikuvaat Select so Insert, Update, Delete, Refresh'
      '--- Selectot e po procedura'
      'select  p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      
        '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd,p.u_lista,p.BR_K' +
        'VITANCIJA,'
      '       pk.kontrolen_pregled,'
      '       p.dogovor_id,'
      '       p.naplateno,'
      '       p.fiskalna,'
      '       p.storna,'
      '       p.datum_presmetka,'
      '       p.iznos_vkupno,'
      '       p.prati_mail_partner,'
      '       p.PRATI_MAIL_B,'
      '       p.PRATI_MAIL_PB,'
      '       (select count(fp.faktura_id)'
      '        from san_faktura_priem fp'
      '        where fp.priem_id = p.id) brFakturi,'
      
        '        (select first 1 a.lekar from san_analiza a where a.pregl' +
        'ed = p.id) as lekar'
      ''
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum, :para' +
        'm_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join san_paketi pk on pk.naziv = p.paket_id'
      'where p.re like :re'
      'order by p.datum_priem desc')
    AutoUpdateOptions.UpdateTableName = 'SAN_PREGLED'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_PREGLED_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 240
    Top = 96
    object tblPreglediID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPreglediGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPreglediDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object tblPreglediPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object tblPreglediEMBG: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblPreglediP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblPreglediPAKET_ID: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPreglediDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ZAVERKA'
    end
    object tblPreglediDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblPreglediPLACANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PLACANJE'
    end
    object tblPreglediPACIENT_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1085#1072#1079#1080#1074
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblPreglediRABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RABOTNO_MESTO'
    end
    object tblPreglediRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediKATEGORIJA_RM: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
    object tblPreglediKATEGORIJANAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KATEGORIJANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPreglediTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPreglediUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediBR_KVITANCIJA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1112#1072
      FieldName = 'BR_KVITANCIJA'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ'
    end
    object tblPreglediBRFAKTURI: TFIBIntegerField
      DisplayLabel = #1041#1088'. '#1060#1072#1082#1090#1091#1088#1080
      FieldName = 'BRFAKTURI'
    end
    object tblPreglediKONTROLEN_PREGLED: TFIBSmallIntField
      FieldName = 'KONTROLEN_PREGLED'
    end
    object tblPreglediDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DOGOVOR_ID'
    end
    object tblPreglediNAPLATENO: TFIBSmallIntField
      FieldName = 'NAPLATENO'
    end
    object tblPreglediFISKALNA: TFIBSmallIntField
      DisplayLabel = #1060#1080#1089#1082#1072#1083#1085#1072' '#1089#1084#1077#1090#1082#1072
      FieldName = 'FISKALNA'
    end
    object tblPreglediSTORNA: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1072' '#1089#1084#1077#1090#1082#1072
      FieldName = 'STORNA'
    end
    object tblPreglediDATUM_PRESMETKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblPreglediIZNOS_VKUPNO: TFIBBCDField
      DisplayLabel = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089
      FieldName = 'IZNOS_VKUPNO'
      Size = 2
    end
    object tblPreglediLEKAR: TFIBIntegerField
      DisplayLabel = #1051#1077#1082#1072#1088
      FieldName = 'LEKAR'
    end
    object tblPreglediPRATI_MAIL_B: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1090#1077#1085' '#1077#1052#1072#1080#1083' '#1079#1072' '#1048#1079'. '#1041#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      FieldName = 'PRATI_MAIL_B'
    end
    object tblPreglediPRATI_MAIL_PB: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1090#1077#1085' '#1077#1052#1072#1080#1083' '#1079#1072' '#1048#1079'. '#1079#1072' '#1055#1088#1077#1082#1080#1085' '#1085#1072' '#1073#1072#1094#1080#1083#1086#1085#1086#1089#1080#1090#1077#1083#1089#1090#1074#1086
      FieldName = 'PRATI_MAIL_PB'
    end
  end
  object dsPregledi: TDataSource
    DataSet = tblPregledi
    Left = 320
    Top = 96
  end
  object tblTipPlakanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_TIP_PLACANJE'
      'SET '
      '    NAZIV = :NAZIV'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_TIP_PLACANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_TIP_PLACANJE('
      '    ID,'
      '    NAZIV'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV'
      ')')
    RefreshSQL.Strings = (
      'select tp.id, tp.naziv'
      'from sys_tip_placanje tp'
      ''
      ' WHERE '
      '        TP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select tp.id, tp.naziv'
      'from sys_tip_placanje tp'
      'order by tp.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 352
    object tblTipPlakanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipPlakanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipPlakanje: TDataSource
    DataSet = tblTipPlakanje
    Left = 120
    Top = 352
  end
  object qMaxTipPlakanje: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from sys_tip_placanje s')
    Left = 480
    Top = 224
  end
  object dsPreglediStavki: TDataSource
    DataSet = tblPreglediStavki
    Left = 328
    Top = 160
  end
  object tblPreglediStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PREGLED_S'
      'SET '
      '    ID = :ID,'
      '    PREGLED_ID = :PREGLED_ID,'
      '    USLUGA_ID = :USLUGA_ID,'
      '    CENA = :CENA,'
      '    ZAVRSENA = :ZAVRSENA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DOGOVOR_ID = :DOGOVOR_ID'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_PREGLED_S'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_PREGLED_S('
      '    ID,'
      '    PREGLED_ID,'
      '    USLUGA_ID,'
      '    CENA,'
      '    ZAVRSENA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    DOGOVOR_ID'
      ')'
      'VALUES('
      '    :ID,'
      '    :PREGLED_ID,'
      '    :USLUGA_ID,'
      '    :CENA,'
      '    :ZAVRSENA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :DOGOVOR_ID'
      ')')
    RefreshSQL.Strings = (
      'select ps.id,'
      '       ps.pregled_id,'
      '       ps.usluga_id,'
      '       u.naziv as ulugaNaziv,'
      '       ps.cena,'
      '       u.re, '
      '       u.tip,'
      '       mr.naziv as reNaziv,'
      '       case when ps.zavrsena = 1 and ps.naod = 1 then 1'
      '            when ps.zavrsena = 1 and ps.naod = 0 then 0'
      '            when ps.zavrsena = 0 then 3'
      '       end "naod",'
      '       ps.zavrsena,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd,'
      '       dogovor_id,'
      '       ps.m_dogovori_id,'
      '       case when (select count(a.id)'
      '                  from san_analiza a'
      
        '                  inner join san_pregled_s pss on a.pregled_s = ' +
        'pss.id'
      
        '                  where a.pregled_s = ps.id and ps.naod = 1 and ' +
        'ps.zavrsena = 1 and a.mikroorganizam is not null and a.mikroorga' +
        'nizam = 16) > 0 then 1'
      '            else 0'
      '       end br_mrsa'
      ''
      'from san_pregled_s ps'
      'left outer join san_uslugi u on u.id = ps.usluga_id'
      'left outer join mat_re mr on mr.id = u.re'
      'where(  ps.pregled_id = :mas_id'
      '     ) and (     PS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.pregled_id,'
      '       ps.usluga_id,'
      '       u.naziv as ulugaNaziv,'
      '       ps.cena,'
      '       u.re, '
      '       u.tip,'
      '       mr.naziv as reNaziv,'
      '       case when ps.zavrsena = 1 and ps.naod = 1 then 1'
      '            when ps.zavrsena = 1 and ps.naod = 0 then 0'
      '            when ps.zavrsena = 0 then 3'
      '       end "naod",'
      '       ps.zavrsena,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd,'
      '       dogovor_id,'
      '       ps.m_dogovori_id,'
      '       case when (select count(a.id)'
      '                  from san_analiza a'
      
        '                  inner join san_pregled_s pss on a.pregled_s = ' +
        'pss.id'
      
        '                  where a.pregled_s = ps.id and ps.naod = 1 and ' +
        'ps.zavrsena = 1 and a.mikroorganizam is not null and a.mikroorga' +
        'nizam = 16) > 0 then 1'
      '            else 0'
      '       end br_mrsa'
      ''
      'from san_pregled_s ps'
      'left outer join san_uslugi u on u.id = ps.usluga_id'
      'left outer join mat_re mr on mr.id = u.re'
      'where ps.pregled_id = :mas_id')
    AutoUpdateOptions.UpdateTableName = 'SAN_PREGLED_S'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_PREGLED_S_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = tblPreglediStavkiBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPregledi
    Left = 240
    Top = 160
    object tblPreglediStavkiID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblPreglediStavkiPREGLED_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1077#1075#1083#1077#1076' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_ID'
    end
    object tblPreglediStavkiUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA_ID'
    end
    object tblPreglediStavkiULUGANAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072
      FieldName = 'ULUGANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediStavkiNAOD: TFIBSmallIntField
      DisplayLabel = #1053#1072#1086#1076
      FieldName = 'NAOD'
    end
    object tblPreglediStavkiZAVRSENA: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ZAVRSENA'
    end
    object tblPreglediStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPreglediStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPreglediStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediStavkiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPreglediStavkiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblPreglediStavkiRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPreglediStavkiTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
    object tblPreglediStavkiDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'DOGOVOR_ID'
    end
    object tblPreglediStavkiBR_MRSA: TFIBIntegerField
      DisplayLabel = 'STAPHYLOCOCCUS AUREUS (MRSA)'
      FieldName = 'BR_MRSA'
    end
    object tblPreglediStavkiM_DOGOVORI_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1086#1076' '#1052#1086#1076#1091#1083' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '
      FieldName = 'M_DOGOVORI_ID'
    end
  end
  object PROC_SAN_INSERTUSLUGA: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_SAN_INSERTUSLUGA (?PAKET_ID, ?PREGLED_ID,' +
        ' ?FLAG, ?TIP_PARTNER, ?PARTNER, ?DATUM_PRIEM)')
    StoredProcName = 'PROC_SAN_INSERTUSLUGA'
    Left = 768
    Top = 104
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 768
    Top = 32
  end
  object qDeleteUslugiOdPregled: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    SAN_PREGLED_S'
      'WHERE'
      '    PREGLED_ID = :PREGLED_ID')
    Left = 768
    Top = 168
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pInsDelSelektiraniUslugi: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_SAN_KOPIRAJ_USLUGI (?PREGLED_ID, ?USLUGA_' +
        'ID,?TIP_DEL_INS)')
    StoredProcName = 'PROC_SAN_KOPIRAJ_USLUGI'
    Left = 768
    Top = 240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblMikroorganizmi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_MIKROORGANIZMI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_MIKROORGANIZMI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_MIKROORGANIZMI('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select m.id,'
      '       m.naziv,'
      '       m.ts_ins,'
      '       m.ts_upd,'
      '       m.usr_ins,'
      '       m.usr_upd'
      'from san_mikroorganizmi m'
      ''
      ' WHERE '
      '        M.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select m.id,'
      '       m.naziv,'
      '       m.ts_ins,'
      '       m.ts_upd,'
      '       m.usr_ins,'
      '       m.usr_upd'
      'from san_mikroorganizmi m'
      'order by m.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 424
    object tblMikroorganizmiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblMikroorganizmiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMikroorganizmiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblMikroorganizmiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblMikroorganizmiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMikroorganizmiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsMikroorganizmi: TDataSource
    DataSet = tblMikroorganizmi
    Left = 120
    Top = 424
  end
  object tblMikroUslugi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_USLUGA_MIKRO'
      'SET '
      '    USLUGA_ID = :USLUGA_ID,'
      '    MIKRO_ID = :MIKRO_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    USLUGA_ID = :OLD_USLUGA_ID'
      '    and MIKRO_ID = :OLD_MIKRO_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_USLUGA_MIKRO'
      'WHERE'
      '        USLUGA_ID = :OLD_USLUGA_ID'
      '    and MIKRO_ID = :OLD_MIKRO_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_USLUGA_MIKRO('
      '    USLUGA_ID,'
      '    MIKRO_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :USLUGA_ID,'
      '    :MIKRO_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select um.usluga_id, '
      '       u.naziv as uslugaNaziv,'
      '       um.mikro_id,'
      '       m.naziv as mikroNaziv,'
      '       um.ts_ins,'
      '       um.ts_upd,'
      '       um.usr_ins,'
      '       um.usr_upd'
      'from san_usluga_mikro um'
      'inner join san_mikroorganizmi m on m.id = um.mikro_id'
      'inner join san_uslugi u on u.id = um.usluga_id'
      'where(  um.usluga_id = :usluga'
      '     ) and (     UM.USLUGA_ID = :OLD_USLUGA_ID'
      '    and UM.MIKRO_ID = :OLD_MIKRO_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select um.usluga_id, '
      '       u.naziv as uslugaNaziv,'
      '       um.mikro_id,'
      '       m.naziv as mikroNaziv,'
      '       um.ts_ins,'
      '       um.ts_upd,'
      '       um.usr_ins,'
      '       um.usr_upd'
      'from san_usluga_mikro um'
      'inner join san_mikroorganizmi m on m.id = um.mikro_id'
      'inner join san_uslugi u on u.id = um.usluga_id'
      'where um.usluga_id = :usluga'
      'order by um.mikro_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 496
    object tblMikroUslugiUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA_ID'
    end
    object tblMikroUslugiUSLUGANAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072
      FieldName = 'USLUGANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMikroUslugiMIKRO_ID: TFIBIntegerField
      DisplayLabel = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'MIKRO_ID'
    end
    object tblMikroUslugiMIKRONAZIV: TFIBStringField
      DisplayLabel = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084
      FieldName = 'MIKRONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMikroUslugiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblMikroUslugiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblMikroUslugiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMikroUslugiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsMikroUslugi: TDataSource
    DataSet = tblMikroUslugi
    Left = 120
    Top = 496
  end
  object qMaxMikro: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_mikroorganizmi s'
      '')
    Left = 480
    Top = 288
  end
  object qFirmaNaPacient: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select sp.tip_partner, sp.partner, sp.r_mesto, sp.kategorija_rm'
      'from san_pacienti sp'
      'where sp.id = :pacient')
    Left = 480
    Top = 352
  end
  object tblKategorijaRM: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_KATEGORIJA_RM'
      'SET '
      '    NAZIV = :NAZIV,'
      '    ROK = :ROK,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_KATEGORIJA_RM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_KATEGORIJA_RM('
      '    ID,'
      '    NAZIV,'
      '    ROK,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :ROK,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select km.id,'
      '       km.naziv,'
      '       km.rok,'
      '       km.ts_ins,'
      '       km.ts_upd,'
      '       km.usr_ins,'
      '       km.usr_upd'
      'from san_kategorija_rm km'
      ''
      ' WHERE '
      '        KM.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select km.id,'
      '       km.naziv,'
      '       km.rok,'
      '       km.ts_ins,'
      '       km.ts_upd,'
      '       km.usr_ins,'
      '       km.usr_upd'
      'from san_kategorija_rm km')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 568
    object tblKategorijaRMID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKategorijaRMNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorijaRMTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblKategorijaRMTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblKategorijaRMUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorijaRMUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorijaRMROK: TFIBSmallIntField
      DisplayLabel = #1056#1086#1082
      FieldName = 'ROK'
    end
  end
  object dsKategorijaRM: TDataSource
    DataSet = tblKategorijaRM
    Left = 112
    Top = 568
  end
  object qMaxKategorijaRM: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_kategorija_rm s')
    Left = 480
    Top = 416
  end
  object tblLekovi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_LEKOVI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_LEKOVI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_LEKOVI('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select l.id,'
      '       l.naziv,'
      '       l.ts_ins,'
      '       l.ts_upd,'
      '       l.usr_ins,'
      '       l.usr_upd'
      'from san_lekovi l'
      ''
      ' WHERE '
      '        L.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select l.id,'
      '       l.naziv,'
      '       l.ts_ins,'
      '       l.ts_upd,'
      '       l.usr_ins,'
      '       l.usr_upd'
      'from san_lekovi l'
      'order by l.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 624
    object tblLekoviID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLekoviNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblLekoviTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblLekoviUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekoviUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLekovi: TDataSource
    DataSet = tblLekovi
    Left = 112
    Top = 624
  end
  object qMaxLekovi: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_lekovi s')
    Left = 480
    Top = 480
  end
  object tblAntibiogram: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_ANTIBIOGRAM'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    LEGENDA = :LEGENDA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_ANTIBIOGRAM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_ANTIBIOGRAM('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    LEGENDA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :LEGENDA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select sa.id,'
      '       sa.naziv,'
      '       sa.opis,'
      '       sa.legenda,'
      '       sa.ts_ins,'
      '       sa.ts_upd,'
      '       sa.usr_ins,'
      '       sa.usr_upd'
      'from san_antibiogram sa'
      ''
      ' WHERE '
      '        SA.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select sa.id,'
      '       sa.naziv,'
      '       sa.opis,'
      '       sa.legenda,'
      '       sa.ts_ins,'
      '       sa.ts_upd,'
      '       sa.usr_ins,'
      '       sa.usr_upd'
      'from san_antibiogram sa'
      'order by sa.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 688
    object tblAntibiogramID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblAntibiogramTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblAntibiogramTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblAntibiogramUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAntibiogramUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAntibiogramNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAntibiogramOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAntibiogramLEGENDA: TFIBStringField
      DisplayLabel = #1051#1077#1075#1077#1085#1076#1072
      FieldName = 'LEGENDA'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAntibiogram: TDataSource
    DataSet = tblAntibiogram
    Left = 120
    Top = 688
  end
  object qMaxAntibiogram: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_antibiogram s')
    Left = 480
    Top = 544
  end
  object tblDefAntibiogram: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_ANTIBIOGRAM_LEKOVI'
      'SET '
      '    ANTIBIOGRAM_ID = :ANTIBIOGRAM_ID,'
      '    LEK_ID = :LEK_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ANTIBIOGRAM_ID = :OLD_ANTIBIOGRAM_ID'
      '    and LEK_ID = :OLD_LEK_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_ANTIBIOGRAM_LEKOVI'
      'WHERE'
      '        ANTIBIOGRAM_ID = :OLD_ANTIBIOGRAM_ID'
      '    and LEK_ID = :OLD_LEK_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_ANTIBIOGRAM_LEKOVI('
      '    ANTIBIOGRAM_ID,'
      '    LEK_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ANTIBIOGRAM_ID,'
      '    :LEK_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select al.antibiogram_id,'
      '       a.naziv as aNaziv,'
      '       al.lek_id,'
      '       l.naziv as lNaziv,'
      '       al.ts_ins,'
      '       al.ts_upd,'
      '       al.usr_ins,'
      '       al.usr_upd'
      'from san_antibiogram_lekovi al'
      'inner join san_lekovi l on l.id = al.lek_id'
      'inner join san_antibiogram a on a.id = al.antibiogram_id'
      'where(  al.antibiogram_id = :antibiogram_id'
      '     ) and (     AL.ANTIBIOGRAM_ID = :OLD_ANTIBIOGRAM_ID'
      '    and AL.LEK_ID = :OLD_LEK_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select al.antibiogram_id,'
      '       a.naziv as aNaziv,'
      '       al.lek_id,'
      '       l.naziv as lNaziv,'
      '       al.ts_ins,'
      '       al.ts_upd,'
      '       al.usr_ins,'
      '       al.usr_upd'
      'from san_antibiogram_lekovi al'
      'inner join san_lekovi l on l.id = al.lek_id'
      'inner join san_antibiogram a on a.id = al.antibiogram_id'
      'where al.antibiogram_id = :antibiogram_id'
      'order by al.antibiogram_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 752
    object tblDefAntibiogramANTIBIOGRAM_ID: TFIBBCDField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'ANTIBIOGRAM_ID'
      Size = 0
    end
    object tblDefAntibiogramANAZIV: TFIBStringField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084
      FieldName = 'ANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDefAntibiogramLEK_ID: TFIBIntegerField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1082' - '#1096#1080#1092#1088#1072
      FieldName = 'LEK_ID'
    end
    object tblDefAntibiogramLNAZIV: TFIBStringField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1082' '
      FieldName = 'LNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDefAntibiogramTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDefAntibiogramTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDefAntibiogramUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDefAntibiogramUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDefAntibiogram: TDataSource
    DataSet = tblDefAntibiogram
    Left = 120
    Top = 752
  end
  object tblDefNaodi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_DEF_NAODI'
      'SET '
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_DEF_NAODI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_DEF_NAODI('
      '    ID,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select dn.id,'
      '       dn.opis,'
      '       dn.ts_ins,'
      '       dn.ts_upd,'
      '       dn.usr_ins,'
      '       dn.usr_upd'
      'from san_def_naodi dn'
      ''
      ' WHERE '
      '        DN.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select dn.id,'
      '       dn.opis,'
      '       dn.ts_ins,'
      '       dn.ts_upd,'
      '       dn.usr_ins,'
      '       dn.usr_upd'
      'from san_def_naodi dn'
      'order by dn.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 824
    object tblDefNaodiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDefNaodiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDefNaodiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDefNaodiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDefNaodiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDefNaodiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDefNaodi: TDataSource
    DataSet = tblDefNaodi
    Left = 120
    Top = 824
  end
  object qMaxDefNaodi: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_def_naodi s')
    Left = 480
    Top = 608
  end
  object tblAnaliza: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_ANALIZA'
      'SET '
      '    LAB_BR = :LAB_BR,'
      '    PREGLED = :PREGLED,'
      '    PREGLED_S = :PREGLED_S,'
      '    MIKROORGANIZAM = :MIKROORGANIZAM,'
      '    BR_MIKROORGANIZAM = :BR_MIKROORGANIZAM,'
      '    ANTIBIOGRAM = :ANTIBIOGRAM,'
      '    LEKAR = :LEKAR,'
      '    DEF_NAOD = :DEF_NAOD,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_ANALIZA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_ANALIZA('
      '    ID,'
      '    LAB_BR,'
      '    PREGLED,'
      '    PREGLED_S,'
      '    MIKROORGANIZAM,'
      '    BR_MIKROORGANIZAM,'
      '    ANTIBIOGRAM,'
      '    LEKAR,'
      '    DEF_NAOD,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :LAB_BR,'
      '    :PREGLED,'
      '    :PREGLED_S,'
      '    :MIKROORGANIZAM,'
      '    :BR_MIKROORGANIZAM,'
      '    :ANTIBIOGRAM,'
      '    :LEKAR,'
      '    :DEF_NAOD,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select sa.id,'
      '       sa.lab_br,'
      '       sa.pregled,'
      '       sa.pregled_s,'
      '       sa.mikroorganizam,'
      '       sa.br_mikroorganizam,'
      '       m.naziv as mikro_naziv,'
      '       sa.antibiogram,'
      '       a.naziv as antibiogram_opis,'
      '       sa.lekar,'
      '       l.naziv as lekarNaziv,'
      '       sa.def_naod,'
      '       dn.opis as naod_opis,'
      '       sa.opis,'
      '       sa.ts_ins,'
      '       sa.ts_upd,'
      '       sa.usr_ins,'
      '       sa.usr_upd'
      'from san_analiza sa'
      'left outer join san_mikroorganizmi m on m.id = sa.mikroorganizam'
      'left outer join san_antibiogram a on a.id= sa.antibiogram'
      'left outer join san_lekar l on l.id = sa.lekar'
      'left outer join san_def_naodi dn on dn.id = sa.def_naod'
      'where(  sa.pregled_s = :mas_id and sa.pregled = :mas_pregled_id'
      '     ) and (     SA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select sa.id,'
      '       sa.lab_br,'
      '       sa.pregled,'
      '       sa.pregled_s,'
      '       sa.mikroorganizam,'
      '       sa.br_mikroorganizam,'
      '       m.naziv as mikro_naziv,'
      '       sa.antibiogram,'
      '       a.naziv as antibiogram_opis,'
      '       sa.lekar,'
      '       l.naziv as lekarNaziv,'
      '       sa.def_naod,'
      '       dn.opis as naod_opis,'
      '       sa.opis,'
      '       sa.ts_ins,'
      '       sa.ts_upd,'
      '       sa.usr_ins,'
      '       sa.usr_upd'
      'from san_analiza sa'
      'left outer join san_mikroorganizmi m on m.id = sa.mikroorganizam'
      'left outer join san_antibiogram a on a.id= sa.antibiogram'
      'left outer join san_lekar l on l.id = sa.lekar'
      'left outer join san_def_naodi dn on dn.id = sa.def_naod'
      'where sa.pregled_s = :mas_id and sa.pregled = :mas_pregled_id'
      'order by sa.id')
    AutoUpdateOptions.UpdateTableName = 'SAN_ANALIZA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_ANALIZA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPriemLabStavki
    Left = 240
    Top = 232
    object tblAnalizaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblAnalizaPREGLED: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED'
    end
    object tblAnalizaPREGLED_S: TFIBBCDField
      DisplayLabel = #1055#1088#1080#1077#1084' - '#1091#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_S'
      Size = 0
    end
    object tblAnalizaMIKROORGANIZAM: TFIBIntegerField
      DisplayLabel = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'MIKROORGANIZAM'
    end
    object tblAnalizaMIKRO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084
      FieldName = 'MIKRO_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaANTIBIOGRAM: TFIBBCDField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'ANTIBIOGRAM'
      Size = 0
    end
    object tblAnalizaANTIBIOGRAM_OPIS: TFIBStringField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' '
      FieldName = 'ANTIBIOGRAM_OPIS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaLEKAR: TFIBIntegerField
      DisplayLabel = #1051#1077#1082#1072#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'LEKAR'
    end
    object tblAnalizaLEKARNAZIV: TFIBStringField
      DisplayLabel = #1051#1077#1082#1072#1088
      FieldName = 'LEKARNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaDEF_NAOD: TFIBIntegerField
      DisplayLabel = #1053#1072#1086#1076' - '#1096#1080#1092#1088#1072
      FieldName = 'DEF_NAOD'
    end
    object tblAnalizaNAOD_OPIS: TFIBStringField
      DisplayLabel = #1053#1072#1086#1076
      FieldName = 'NAOD_OPIS'
      Size = 2500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblAnalizaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblAnalizaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblAnalizaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaLAB_BR: TFIBStringField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1089#1082#1080' '#1041#1088#1086#1112
      FieldName = 'LAB_BR'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAnalizaBR_MIKROORGANIZAM: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1084#1080#1082#1088#1086#1088#1075#1072#1085#1080#1079#1084#1080
      FieldName = 'BR_MIKROORGANIZAM'
    end
  end
  object dsAnaliza: TDataSource
    DataSet = tblAnaliza
    Left = 328
    Top = 232
  end
  object qPacientNAziv: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select p.id as id'
      'from san_pacienti p'
      'where p.embg = :embg')
    Left = 480
    Top = 672
  end
  object qPacientEMBG: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(p.embg,'#39#39') as embg'
      'from san_pacienti p'
      'where p.id = :id')
    Left = 480
    Top = 736
  end
  object qPaketNaziv: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select sp.naziv'
      'from san_paketi sp'
      'where sp.id = :id')
    Left = 480
    Top = 800
  end
  object tblLekari: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_LEKAR'
      'SET '
      '    TITULA = :TITULA,'
      '    SPECIJALNOST = :SPECIJALNOST,'
      '    NAZIV = :NAZIV,'
      '    PREZIME = :PREZIME,'
      '    IME = :IME,'
      '    PREZIME_IME = :PREZIME_IME,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    POTPIS = :POTPIS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_LEKAR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_LEKAR('
      '    ID,'
      '    TITULA,'
      '    SPECIJALNOST,'
      '    NAZIV,'
      '    PREZIME,'
      '    IME,'
      '    PREZIME_IME,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    POTPIS'
      ')'
      'VALUES('
      '    :ID,'
      '    :TITULA,'
      '    :SPECIJALNOST,'
      '    :NAZIV,'
      '    :PREZIME,'
      '    :IME,'
      '    :PREZIME_IME,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :POTPIS'
      ')')
    RefreshSQL.Strings = (
      'select s.id,'
      '       s.titula,'
      '       s.specijalnost,'
      '       s.naziv,'
      '       s.prezime,'
      '       s.ime,'
      '       s.prezime_ime,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd,'
      '       s.potpis'
      'from san_lekar s'
      ''
      ' WHERE '
      '        S.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select s.id,'
      '       s.titula,'
      '       s.specijalnost,'
      '       s.naziv,'
      '       s.prezime,'
      '       s.ime,'
      '       s.prezime_ime,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd,'
      '       s.potpis'
      'from san_lekar s'
      'order by id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 888
    object tblLekariID: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLekariTITULA: TFIBStringField
      DisplayLabel = #1058#1080#1090#1091#1083#1072
      FieldName = 'TITULA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariSPECIJALNOST: TFIBStringField
      DisplayLabel = #1057#1087#1077#1094#1080#1112#1072#1083#1085#1086#1089#1090
      FieldName = 'SPECIJALNOST'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblLekariTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblLekariUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariPREZIME_IME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077
      FieldName = 'PREZIME_IME'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLekariPOTPIS: TFIBBlobField
      DisplayLabel = #1055#1086#1090#1087#1080#1089
      FieldName = 'POTPIS'
      Size = 8
    end
  end
  object dsLekari: TDataSource
    DataSet = tblLekari
    Left = 120
    Top = 888
  end
  object qMaxLekariID: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(s.id),0)+1 as id'
      'from san_lekar s')
    Left = 480
    Top = 864
  end
  object qCountAnaliza: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(a.id) br'
      'from san_analiza a'
      'where a.pregled_s = :pregled_s')
    Left = 480
    Top = 928
  end
  object tblRezultat: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_REZULTAT'
      'SET '
      '    VREDNOST = :VREDNOST'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      '                        -- samo update ima'
      'select r.id,'
      '       r.pregled_id,'
      '       r.pregled_s_id,'
      '       r.analiza_id,'
      '       r.lek_id,'
      '       l.naziv as lekNaziv,'
      '       r.vrednost,'
      '       r.ts_ins,'
      '       r.ts_upd,'
      '       r.usr_ins,'
      '       r.usr_upd'
      'from san_rezultat r'
      'inner join san_lekovi l on l.id = r.lek_id'
      'where r.analiza_id = :mas_id'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsAnaliza
    Left = 240
    Top = 304
    object tblRezultatID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRezultatPREGLED_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1077#1075#1083#1077#1076' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_ID'
    end
    object tblRezultatPREGLED_S_ID: TFIBBCDField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_S_ID'
      Size = 0
    end
    object tblRezultatANALIZA_ID: TFIBIntegerField
      DisplayLabel = #1040#1085#1072#1083#1080#1079#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ANALIZA_ID'
    end
    object tblRezultatLEK_ID: TFIBIntegerField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1082' - '#1096#1080#1092#1088#1072
      FieldName = 'LEK_ID'
    end
    object tblRezultatVREDNOST: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'VREDNOST'
      OnSetText = tblRezultatVREDNOSTSetText
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRezultatTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRezultatUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatLEKNAZIV: TFIBStringField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1090#1080#1082
      FieldName = 'LEKNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRezultat: TDataSource
    DataSet = tblRezultat
    Left = 328
    Top = 304
  end
  object qCountRezultat: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(r.id)br'
      'from san_rezultat r'
      'where r.analiza_id = :analiza')
    Left = 480
    Top = 992
  end
  object PROC_SAN_INSERTREZULTAT: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_SAN_INSERTREZULTAT (?ANTIBIOGRAM_ID, ?PRE' +
        'GLED_ID, ?PREGLED_S_ID, ?MIKRO_ID, ?ANALIZA_ID)')
    StoredProcName = 'PROC_SAN_INSERTREZULTAT'
    Left = 768
    Top = 320
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteAnaliza: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    SAN_ANALIZA s'
      'WHERE'
      '    s.pregled_s =:pregled_s')
    Left = 768
    Top = 392
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleterezultat: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from san_rezultat'
      'where pregled_s_id =:pregled_s')
    Left = 768
    Top = 456
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object frxDBAntibiogram: TfrxDBDataset
    UserName = 'frxAntibiogram'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LEKNAZIV=LEKNAZIV'
      'VREDNOST=VREDNOST')
    DataSet = tblFrxAntibiogram
    BCDToCurrency = False
    Left = 784
    Top = 888
  end
  object tblPriemLab: TpFIBDataSet
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       mr.naziv as reNaziv,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd, p.u_lista'
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum, :para' +
        'm_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join mat_re mr on mr.id = p.re'
      'where(  p.re like :OLD_re'
      '     ) and (     P.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       mr.naziv as reNaziv,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd, p.u_lista'
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum, :para' +
        'm_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join mat_re mr on mr.id = p.re'
      'where p.re like :re'
      'order by p.datum_priem desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 240
    Top = 368
    object tblPriemLabID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPriemLabGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPriemLabDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object tblPriemLabPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object tblPriemLabEMBG: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabPACIENT_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblPriemLabP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblPriemLabPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1085#1072#1079#1080#1074
      FieldName = 'PARTNER_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabPAKET_ID: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPriemLabDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ZAVERKA'
    end
    object tblPriemLabDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblPriemLabPLACANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PLACANJE'
    end
    object tblPriemLabNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblPriemLabRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabRABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RABOTNO_MESTO'
    end
    object tblPriemLabRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabKATEGORIJA_RM: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
    object tblPriemLabKATEGORIJANAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KATEGORIJANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPriemLabTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPriemLabUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ'
    end
  end
  object dsPriemLab: TDataSource
    DataSet = tblPriemLab
    Left = 328
    Top = 368
  end
  object tblPriemLabStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_PREGLED_S'
      'SET '
      '    NAOD = :NAOD,'
      '    ZAVRSENA = :ZAVRSENA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      '--ima samo update'
      'select ps.id,'
      '       ps.pregled_id,'
      '       ps.usluga_id,'
      '       u.naziv as ulugaNaziv,'
      '       u.cena,'
      '       u.re,'
      '       mr.naziv as reNaziv,'
      '       case when ps.zavrsena = 1 then ps.naod'
      '            when ps.zavrsena = 0 then 3'
      '       end "naodStatus",'
      '       ps.naod,'
      '       ps.zavrsena,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd'
      'from san_pregled_s ps'
      'left outer join san_uslugi u on u.id = ps.usluga_id'
      'left outer join mat_re mr on mr.id = u.re'
      'where(  ps.pregled_id = :mas_id'
      '     ) and (     PS.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      '--ima samo update'
      'select ps.id,'
      '       ps.pregled_id,'
      '       ps.usluga_id,'
      '       u.naziv as ulugaNaziv,'
      '       u.cena,'
      '       u.re,'
      '       mr.naziv as reNaziv,'
      '       case when ps.zavrsena = 1 then ps.naod'
      '            when ps.zavrsena = 0 then 3'
      '       end "naodStatus",'
      '       ps.naod,'
      '       ps.zavrsena,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd'
      'from san_pregled_s ps'
      'left outer join san_uslugi u on u.id = ps.usluga_id'
      'left outer join mat_re mr on mr.id = u.re'
      
        'where ps.pregled_id = :mas_id and ((u.tip = 0) or (u.tip is null' +
        ')) '
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPriemLab
    Left = 240
    Top = 432
    object tblPriemLabStavkiID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblPriemLabStavkiPREGLED_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1077#1075#1083#1077#1076' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_ID'
    end
    object tblPriemLabStavkiUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA_ID'
    end
    object tblPriemLabStavkiULUGANAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072
      FieldName = 'ULUGANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabStavkiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPriemLabStavkiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblPriemLabStavkiNAOD: TFIBSmallIntField
      DisplayLabel = #1053#1072#1086#1076
      FieldName = 'NAOD'
    end
    object tblPriemLabStavkiZAVRSENA: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ZAVRSENA'
    end
    object tblPriemLabStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPriemLabStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPriemLabStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabStavkiRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPriemLabStavkinaodStatus: TFIBIntegerField
      DisplayLabel = #1053#1072#1086#1076
      FieldName = 'naodStatus'
    end
  end
  object dsPriemLabStavki: TDataSource
    DataSet = tblPriemLabStavki
    Left = 328
    Top = 432
  end
  object qCountPravaRe: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(s.id) br'
      'from sys_user_re_app s'
      'where s.app = '#39'SAN'#39' and s.re = :re and s.username = :username')
    Left = 480
    Top = 1056
  end
  object tblPopust: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_POPUST'
      'SET '
      '    P = :P,'
      '    TP = :TP,'
      '    POPUST = :POPUST,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_POPUST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_POPUST('
      '    ID,'
      '    P,'
      '    TP,'
      '    POPUST,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :P,'
      '    :TP,'
      '    :POPUST,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.p,'
      '       p.tp,'
      '       mp.naziv as partnerNaziv,'
      '       p.popust,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from san_popust p'
      
        'inner join mat_partner mp on mp.tip_partner = p.tp and mp.id = p' +
        '.p'
      ''
      ' WHERE '
      '        P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.p,'
      '       p.tp,'
      '       mp.naziv as partnerNaziv,'
      '       p.popust,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from san_popust p'
      
        'inner join mat_partner mp on mp.tip_partner = p.tp and mp.id = p' +
        '.p')
    AutoUpdateOptions.UpdateTableName = 'SAN_POPUST'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_POPUST_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 960
    object tblPopustID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPopustP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblPopustTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblPopustPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPopustPOPUST: TFIBIntegerField
      DisplayLabel = #1055#1086#1087#1091#1089#1090
      FieldName = 'POPUST'
    end
    object tblPopustTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPopustTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPopustUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPopustUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPopust: TDataSource
    DataSet = tblPopust
    Left = 120
    Top = 960
  end
  object tblNefakturirani: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_san_nefakturiran_priem.pregled_id_out,'
      '       proc_san_nefakturiran_priem.iznos_out,'
      '       proc_san_nefakturiran_priem.broj_dnevnik,'
      '       proc_san_nefakturiran_priem.datum_priem,'
      '       proc_san_nefakturiran_priem.datum_zaverka,'
      '       proc_san_nefakturiran_priem.datum_vazenje,'
      '       proc_san_nefakturiran_priem.nacinplakanje,'
      '       proc_san_nefakturiran_priem.embg,'
      '       proc_san_nefakturiran_priem.pacientnaziv,'
      '       proc_san_nefakturiran_priem.tp,'
      '       proc_san_nefakturiran_priem.p,'
      '       proc_san_nefakturiran_priem.partnernaziv,'
      '       proc_san_nefakturiran_priem.rmnaziv,'
      '       proc_san_nefakturiran_priem.krmnaziv'
      'from proc_san_nefakturiran_priem(:godina, :re)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 488
    object tblNefakturiraniPREGLED_ID_OUT: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PREGLED_ID_OUT'
    end
    object tblNefakturiraniIZNOS_OUT: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS_OUT'
      Size = 2
    end
    object tblNefakturiraniDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object tblNefakturiraniDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1082#1072
      FieldName = 'DATUM_ZAVERKA'
    end
    object tblNefakturiraniDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblNefakturiraniNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniEMBG: TFIBStringField
      DisplayLabel = #1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniPACIENTNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090
      FieldName = 'PACIENTNAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblNefakturiraniP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblNefakturiraniPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniRMNAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RMNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniKRMNAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KRMNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNefakturiraniBROJ_DNEVNIK: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ_DNEVNIK'
    end
  end
  object dsNefakturirani: TDataSource
    DataSet = tblNefakturirani
    Left = 336
    Top = 488
  end
  object qUpdatePriemFlagFaktura: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE SAN_PREGLED'
      'SET '
      '    FLAG_FAKTURA = :flag '
      'WHERE'
      '    ID like :ID')
    Left = 768
    Top = 520
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblFakturi: TpFIBDataSet
    DeleteSQL.Strings = (
      ''
      '    ')
    SelectSQL.Strings = (
      'select  fp.priem_id,'
      '        f.broj as brFaktura,'
      '        f.popust, f.iznos, f.iznos_popust,'
      '        f.datum,'
      '        f.godina,'
      '        f.re,'
      '        p.pacient_id,'
      '        p.p,'
      '        p.tp,'
      '        mp.naziv as partnerNaziv,'
      '        fp.faktura_id,'
      '        sp.embg,'
      '        sp.naziv,'
      '        fp.ts_ins,'
      '        fp.ts_upd,'
      '        fp.usr_ins,'
      '        fp.usr_upd'
      'from san_faktura_priem fp'
      'inner join san_pregled p on p.id = fp.priem_id'
      'inner join san_faktura f on f.id = fp.faktura_id'
      'inner join san_pacienti sp on sp.id = p.pacient_id'
      
        'inner join mat_partner mp on mp.tip_partner = f.tip_partner and ' +
        'mp.id = f.partner'
      'where f.godina = :godina and f.re = :re'
      'order by f.broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 552
    object tblFakturiPRIEM_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PRIEM_ID'
    end
    object tblFakturiFAKTURA_ID: TFIBIntegerField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'FAKTURA_ID'
    end
    object tblFakturiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblFakturiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblFakturiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiBRFAKTURA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'BRFAKTURA'
    end
    object tblFakturiPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object tblFakturiP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblFakturiTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblFakturiPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiEMBG: TFIBStringField
      DisplayLabel = #1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090
      FieldName = 'NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiPOPUST: TFIBIntegerField
      DisplayLabel = #1055#1086#1087#1091#1089#1090
      FieldName = 'POPUST'
    end
    object tblFakturiIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblFakturiIZNOS_POPUST: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1087#1086#1087#1091#1089#1090
      FieldName = 'IZNOS_POPUST'
      Size = 2
    end
    object tblFakturiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
    end
    object tblFakturiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblFakturiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RE'
    end
  end
  object dsFakturi: TDataSource
    DataSet = tblFakturi
    Left = 336
    Top = 552
  end
  object PROC_FAKTURIRANJE: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_SAN_FAKTURIRANJE (?GODINA, ?RE)')
    StoredProcName = 'PROC_SAN_FAKTURIRANJE'
    Left = 768
    Top = 576
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteFaktura: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from san_faktura s'
      'where s.id = :faktura_id')
    Left = 768
    Top = 648
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteFakturaPriem: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from san_faktura_priem sp'
      'where sp.faktura_id = :faktura_id')
    Left = 768
    Top = 712
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblFrxAntibiogram: TpFIBDataSet
    SelectSQL.Strings = (
      'select l.naziv as lekNaziv,'
      '       r.vrednost'
      'from san_rezultat r'
      'inner join san_lekovi l on l.id = r.lek_id'
      'where r.analiza_id = :mas_analiza')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsRezultatAnaliza
    Left = 880
    Top = 888
  end
  object frxDBRezultatiAnaliza: TfrxDBDataset
    UserName = 'frxDBRezultatiAnaliza'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PREGLED_S=PREGLED_S'
      'USLUGA=USLUGA'
      'STATUS_NAOD=STATUS_NAOD'
      'ANALIZA=ANALIZA'
      'MIKRO=MIKRO'
      'ANTIB=ANTIB'
      'LEKAR=LEKAR'
      'DEF_NAOD=DEF_NAOD'
      'LAB_BR=LAB_BR'
      'OPIS=OPIS'
      'BR_MIKROORGANIZAM=BR_MIKROORGANIZAM'
      'LEK=LEK'
      'VREDNOST=VREDNOST'
      'USLUGA_ID=USLUGA_ID'
      'POTPIS=POTPIS'
      'ANALIZA_TS_INS=ANALIZA_TS_INS'
      'ANALIZA_TS_UPD=ANALIZA_TS_UPD'
      'DATUM_PRIEM=DATUM_PRIEM'
      'EMBG=EMBG'
      'RE=RE'
      'RENAZIV=RENAZIV'
      'PRIEM_STAVKA_TS_UPD=PRIEM_STAVKA_TS_UPD')
    DataSet = tblRezultatAnaliza
    BCDToCurrency = False
    Left = 784
    Top = 952
  end
  object tblRezultatAnaliza: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_san_rezultatanaliza.pregled_s,'
      '       proc_san_rezultatanaliza.usluga,'
      '       proc_san_rezultatanaliza.status_naod,'
      '       proc_san_rezultatanaliza.analiza,'
      '       proc_san_rezultatanaliza.mikro,'
      '       proc_san_rezultatanaliza.antib,'
      '       proc_san_rezultatanaliza.lekar,'
      '       proc_san_rezultatanaliza.def_naod,'
      '       proc_san_rezultatanaliza.lab_br,'
      '       proc_san_rezultatanaliza.opis,'
      '       proc_san_rezultatanaliza.br_mikroorganizam,'
      '       proc_san_rezultatanaliza.lek,'
      '       proc_san_rezultatanaliza.vrednost,'
      '       proc_san_rezultatanaliza.usluga_id,'
      '       proc_san_rezultatanaliza.potpis,'
      '       proc_san_rezultatanaliza.analiza_ts_ins,'
      '       proc_san_rezultatanaliza.analiza_ts_upd,'
      '       proc_san_rezultatanaliza.datum_priem,'
      '       proc_san_rezultatanaliza.embg,'
      '       proc_san_rezultatanaliza.re,'
      '       proc_san_rezultatanaliza.renaziv,'
      '       proc_san_rezultatanaliza.priem_stavka_ts_upd'
      'from proc_san_rezultatanaliza(:pregled_id)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 888
    Top = 952
    object tblRezultatAnalizaPREGLED_S: TFIBBCDField
      FieldName = 'PREGLED_S'
      Size = 0
    end
    object tblRezultatAnalizaUSLUGA: TFIBStringField
      FieldName = 'USLUGA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaSTATUS_NAOD: TFIBStringField
      FieldName = 'STATUS_NAOD'
      Size = 12
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaANALIZA: TFIBIntegerField
      FieldName = 'ANALIZA'
    end
    object tblRezultatAnalizaMIKRO: TFIBStringField
      FieldName = 'MIKRO'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaANTIB: TFIBIntegerField
      FieldName = 'ANTIB'
    end
    object tblRezultatAnalizaLEKAR: TFIBStringField
      FieldName = 'LEKAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaDEF_NAOD: TFIBStringField
      FieldName = 'DEF_NAOD'
      Size = 2500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaLAB_BR: TFIBStringField
      FieldName = 'LAB_BR'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaOPIS: TFIBBlobField
      FieldName = 'OPIS'
      Size = 8
    end
    object tblRezultatAnalizaBR_MIKROORGANIZAM: TFIBSmallIntField
      FieldName = 'BR_MIKROORGANIZAM'
    end
    object tblRezultatAnalizaLEK: TFIBStringField
      FieldName = 'LEK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaVREDNOST: TFIBStringField
      FieldName = 'VREDNOST'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaUSLUGA_ID: TFIBIntegerField
      FieldName = 'USLUGA_ID'
    end
    object tblRezultatAnalizaPOTPIS: TFIBBlobField
      FieldName = 'POTPIS'
      Size = 8
    end
    object tblRezultatAnalizaANALIZA_TS_INS: TFIBDateField
      FieldName = 'ANALIZA_TS_INS'
    end
    object tblRezultatAnalizaANALIZA_TS_UPD: TFIBDateField
      FieldName = 'ANALIZA_TS_UPD'
    end
    object tblRezultatAnalizaDATUM_PRIEM: TFIBDateField
      FieldName = 'DATUM_PRIEM'
    end
    object tblRezultatAnalizaEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblRezultatAnalizaRENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatAnalizaPRIEM_STAVKA_TS_UPD: TFIBDateField
      FieldName = 'PRIEM_STAVKA_TS_UPD'
    end
  end
  object dsRezultatAnaliza: TDataSource
    DataSet = tblRezultatAnaliza
    Left = 984
    Top = 952
  end
  object PROC_SAN_LISTAUSLUGI: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_SAN_LISTAUSLUGI (?PREGLED_ID)')
    StoredProcName = 'PROC_SAN_LISTAUSLUGI'
    Left = 920
    Top = 96
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblFakturiPoBroj: TpFIBDataSet
    SelectSQL.Strings = (
      'select  distinct'
      '        f.broj as brFaktura,'
      '        f.popust, f.iznos, f.iznos_popust,'
      '        f.datum,'
      '        f.godina,'
      '        f.re,'
      '        f.partner,'
      '        f.tip_partner,'
      '        mp.naziv as partnerNaziv,'
      '        fp.faktura_id'
      'from san_faktura_priem fp'
      'inner join san_faktura f on f.id = fp.faktura_id'
      
        'inner join mat_partner mp on mp.tip_partner = f.tip_partner and ' +
        'mp.id = f.partner'
      
        'where f.broj between :broj_od and :broj_do and f.godina = :godin' +
        'a'
      'order by f.broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 616
    object tblFakturiPoBrojBRFAKTURA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'BRFAKTURA'
    end
    object tblFakturiPoBrojPOPUST: TFIBIntegerField
      DisplayLabel = #1055#1086#1087#1091#1089#1090
      FieldName = 'POPUST'
    end
    object tblFakturiPoBrojIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblFakturiPoBrojIZNOS_POPUST: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1087#1086#1087#1091#1089#1090
      FieldName = 'IZNOS_POPUST'
      Size = 2
    end
    object tblFakturiPoBrojDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
    end
    object tblFakturiPoBrojGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblFakturiPoBrojRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RE'
    end
    object tblFakturiPoBrojPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiPoBrojFAKTURA_ID: TFIBIntegerField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'FAKTURA_ID'
    end
    object tblFakturiPoBrojPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblFakturiPoBrojTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
  end
  object tblFakturiPoData: TpFIBDataSet
    SelectSQL.Strings = (
      'select  distinct'
      '        f.broj as brFaktura,'
      '        f.popust, f.iznos, f.iznos_popust,'
      '        f.datum,'
      '        f.godina,'
      '        f.re,'
      '        f.partner,'
      '        f.tip_partner,'
      '        mp.naziv as partnerNaziv,'
      '        fp.faktura_id'
      'from san_faktura_priem fp'
      'inner join san_faktura f on f.id = fp.faktura_id'
      
        'inner join mat_partner mp on mp.tip_partner = f.tip_partner and ' +
        'mp.id = f.partner'
      'where f.datum between :datum_od and :datum_do'
      'order by f.broj'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 680
    object tblFakturiPoDataBRFAKTURA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'BRFAKTURA'
    end
    object tblFakturiPoDataPOPUST: TFIBIntegerField
      DisplayLabel = #1055#1086#1087#1091#1089#1090
      FieldName = 'POPUST'
    end
    object tblFakturiPoDataIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblFakturiPoDataIZNOS_POPUST: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1087#1086#1087#1091#1089#1090
      FieldName = 'IZNOS_POPUST'
      Size = 2
    end
    object tblFakturiPoDataDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
    end
    object tblFakturiPoDataGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblFakturiPoDataRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RE'
    end
    object tblFakturiPoDataPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakturiPoDataFAKTURA_ID: TFIBIntegerField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'FAKTURA_ID'
    end
    object tblFakturiPoDataPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblFakturiPoDataTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
  end
  object dsFakturiPoBroj: TDataSource
    DataSet = tblFakturiPoBroj
    Left = 344
    Top = 616
  end
  object dsFakturiPoData: TDataSource
    DataSet = tblFakturiPoData
    Left = 352
    Top = 680
  end
  object qDatumVazenje: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select k.rok,'
      '       dateadd (month, k.rok, :datum_zaverka) as datum_vazenje'
      'from san_kategorija_rm k'
      'where k.id = :id')
    Left = 480
    Top = 1120
  end
  object tblPominatRok: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_san_pominat_rok.priem,'
      '       proc_san_pominat_rok.pacient_id,'
      '       proc_san_pominat_rok.tp,'
      '       proc_san_pominat_rok.p,'
      '       proc_san_pominat_rok.datum_vazenje,'
      '       proc_san_pominat_rok.partner_naziv,'
      '       proc_san_pominat_rok.paket_id,'
      '       proc_san_pominat_rok.cena,'
      '       proc_san_pominat_rok.datum_zaverka,'
      '       proc_san_pominat_rok.placanje,'
      '       proc_san_pominat_rok.nacinplakanje,'
      '       proc_san_pominat_rok.rabotno_mesto,'
      '       proc_san_pominat_rok.rabotnomestonaziv,'
      '       proc_san_pominat_rok.kategorija_rm,'
      '       proc_san_pominat_rok.kategorijanaziv,'
      '       proc_san_pominat_rok.u_lista,'
      '       proc_san_pominat_rok.godina,'
      '       proc_san_pominat_rok.datum_priem,'
      '       proc_san_pominat_rok.broj,'
      '       proc_san_pominat_rok.embg,'
      '       proc_san_pominat_rok.pacient_naziv'
      'from proc_san_pominat_rok(:datum,:re, :tip_partner,:partner)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 752
    object tblPominatRokGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPominatRokDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object tblPominatRokPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object tblPominatRokEMBG: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokPACIENT_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblPominatRokP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblPominatRokPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1085#1072#1079#1080#1074
      FieldName = 'PARTNER_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokPAKET_ID: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblPominatRokDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ZAVERKA'
    end
    object tblPominatRokDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblPominatRokPLACANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PLACANJE'
    end
    object tblPominatRokNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokRABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RABOTNO_MESTO'
    end
    object tblPominatRokRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokKATEGORIJA_RM: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
    object tblPominatRokKATEGORIJANAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KATEGORIJANAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPominatRokBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ'
    end
    object tblPominatRokPRIEM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'PRIEM'
    end
  end
  object dsPominatRok: TDataSource
    DataSet = tblPominatRok
    Left = 352
    Top = 752
  end
  object qUpdatePriemFlagFakturaNULL: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE san_pregled p'
      'SET p.flag_faktura = null'
      'where exists (select p.id'
      '              from san_pregled p'
      
        '              inner join san_faktura_priem fp on fp.priem_id = p' +
        '.id'
      '              where fp.faktura_id = :faktura_id)')
    Left = 760
    Top = 784
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qCountEMBGPacient: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id)as br'
      'from san_pacienti p'
      'where p.embg = :embg')
    Left = 480
    Top = 1184
  end
  object qMaxBrojPriem: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(max(p.broj),0)  broj'
      'from san_pregled p'
      'where p.godina = :godina and p.re like :re')
    Left = 480
    Top = 1240
  end
  object tblListaPrimeniPacientiFirst: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       mr.naziv as reNaziv,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      
        '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd,p.u_lista,p.BR_K' +
        'VITANCIJA'
      'from proc_san_filter_analiza(:godina, :param)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join mat_re mr on mr.id = p.re'
      'where p.re = :re and'
      
        '      ((p.datum_priem between :datum_od and :datum_do and :param' +
        '_datum = 1) or (:param_datum = 0)) and'
      
        '      ((p.broj between :broj_od and :broj_do and :param_broj = 1' +
        ') or (:param_broj = 0))'
      ''
      'order by p.broj desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 824
    object tblListaPrimeniPacientiFirstID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblListaPrimeniPacientiFirstGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblListaPrimeniPacientiFirstDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object tblListaPrimeniPacientiFirstBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ'
    end
    object tblListaPrimeniPacientiFirstPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object tblListaPrimeniPacientiFirstEMBG: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstPACIENT_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object tblListaPrimeniPacientiFirstP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblListaPrimeniPacientiFirstPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1085#1072#1079#1080#1074
      FieldName = 'PARTNER_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstPAKET_ID: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblListaPrimeniPacientiFirstDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ZAVERKA'
    end
    object tblListaPrimeniPacientiFirstDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblListaPrimeniPacientiFirstPLACANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PLACANJE'
    end
    object tblListaPrimeniPacientiFirstNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblListaPrimeniPacientiFirstRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstRABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RABOTNO_MESTO'
    end
    object tblListaPrimeniPacientiFirstRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstKATEGORIJA_RM: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
    object tblListaPrimeniPacientiFirstKATEGORIJANAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KATEGORIJANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblListaPrimeniPacientiFirstTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblListaPrimeniPacientiFirstUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiFirstBR_KVITANCIJA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1112#1072
      FieldName = 'BR_KVITANCIJA'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsListaPrimeniPacientiFirst: TDataSource
    DataSet = tblListaPrimeniPacientiFirst
    Left = 360
    Top = 824
  end
  object tblPartneriOdPriem: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct p.tp, p.p, mp.naziv as partnerNaziv'
      'from san_pregled p'
      
        'inner join mat_partner mp on mp.tip_partner = p.tp and mp.id = p' +
        '.p')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 952
    object tblPartneriOdPriemTP: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TP'
    end
    object tblPartneriOdPriemP: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'P'
    end
    object tblPartneriOdPriemPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'PARTNERNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPartneriOdPriem: TDataSource
    DataSet = tblPartneriOdPriem
    Left = 360
    Top = 952
  end
  object dsIzvestuvanjePoMail: TDataSource
    DataSet = tblIzvestuvanjePoMail
    Left = 360
    Top = 1016
  end
  object tblIzvestuvanjePoMail: TpFIBDataSet
    SelectSQL.Strings = (
      'select pnm.id,'
      '       pnm.datum,'
      '       pnm.tip_partner,'
      '       pnm.partner,'
      '       pnm.kontakt,'
      '       pnm.naslov,'
      '       pnm.text,'
      '       pnm.mail_sender,'
      '       pnm.mail_recipient,'
      '       mp.naziv,'
      
        '       (select m.naziv from mat_kontakt m where m.tip_partner = ' +
        'pnm.tip_partner and m.partner = pnm.partner and m.id = pnm.konta' +
        'kt)as kontaktNaziv'
      'from san_izvestuvanje_mail pnm'
      
        'inner join mat_partner mp on mp.tip_partner = pnm.tip_partner an' +
        'd mp.id = pnm.partner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 1016
    object tblIzvestuvanjePoMailID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIzvestuvanjePoMailDATUM: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblIzvestuvanjePoMailTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblIzvestuvanjePoMailPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblIzvestuvanjePoMailKONTAKT: TFIBIntegerField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'KONTAKT'
    end
    object tblIzvestuvanjePoMailNASLOV: TFIBStringField
      DisplayLabel = #1053#1072#1089#1083#1086#1074
      FieldName = 'NASLOV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvestuvanjePoMailTEXT: TFIBBlobField
      DisplayLabel = #1058#1077#1082#1089#1090
      FieldName = 'TEXT'
      Size = 8
    end
    object tblIzvestuvanjePoMailMAIL_SENDER: TFIBStringField
      DisplayLabel = 'MAIL - '#1080#1089#1087#1088#1072#1116#1072#1095
      FieldName = 'MAIL_SENDER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvestuvanjePoMailMAIL_RECIPIENT: TFIBStringField
      DisplayLabel = 'MAIL - '#1087#1088#1080#1084#1072#1095
      FieldName = 'MAIL_RECIPIENT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvestuvanjePoMailNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvestuvanjePoMailKONTAKTNAZIV: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'KONTAKTNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblListaPrimeniPacientiStavkifirst: TpFIBDataSet
    SelectSQL.Strings = (
      ' select ps.id,'
      '       ps.pregled_id,'
      '       ps.usluga_id,'
      '       u.naziv, u.cena, u.re, u.status'
      'from san_pregled_s ps'
      'inner join san_uslugi u on u.id = ps.usluga_id'
      'where ((u.tip = 0) or (u.tip is null))')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 248
    Top = 888
    object tblListaPrimeniPacientiStavkifirstID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblListaPrimeniPacientiStavkifirstPREGLED_ID: TFIBIntegerField
      FieldName = 'PREGLED_ID'
    end
    object tblListaPrimeniPacientiStavkifirstUSLUGA_ID: TFIBIntegerField
      FieldName = 'USLUGA_ID'
    end
    object tblListaPrimeniPacientiStavkifirstNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaPrimeniPacientiStavkifirstCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 2
    end
    object tblListaPrimeniPacientiStavkifirstRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblListaPrimeniPacientiStavkifirstSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
  end
  object dsListaPrimeniPacientiStavkifirst: TDataSource
    DataSet = tblListaPrimeniPacientiStavkifirst
    Left = 352
    Top = 888
  end
  object master: TpFIBDataSet
    UpdateSQL.Strings = (
      ''
      '    ')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       mr.naziv as reNaziv,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      
        '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd,p.u_lista,p.BR_K' +
        'VITANCIJA, pc.email, p.prati_mail_potvrda, p.prati_mail_potvrda_' +
        'partner'
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum,:param' +
        '_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join mat_re mr on mr.id = p.re'
      'where p.re = :re and'
      
        '      ((p.datum_priem between :datum_od and :datum_do and :param' +
        '_datum = 2) or (:param_datum = 0)) and'
      
        '      ((p.broj between :broj_od and :broj_do and :param_broj = 1' +
        ') or (:param_broj = 0)) and'
      '      p.u_lista like :u_lista'
      ''
      'order by p.broj desc')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.godina,'
      '       p.datum_priem,'
      '       p.broj,'
      '       p.pacient_id,'
      '       pc.embg, pc.naziv as pacient_naziv,'
      '       p.tp,'
      '       p.p,'
      '       mp.naziv as partner_naziv,'
      '       p.paket_id,'
      '       p.cena,'
      '       p.datum_zaverka,'
      '       p.datum_vazenje,'
      '       p.placanje,'
      '       s.naziv as nacinPlakanje,'
      '       p.re,'
      '       mr.naziv as reNaziv,'
      '       p.rabotno_mesto,'
      '       rm.naziv as rabotnoMestoNaziv,'
      '       p.kategorija_rm,'
      '       krm.naziv as KategorijaNaziv,'
      
        '       p.ts_ins, p.ts_upd, p.usr_ins, p.usr_upd,p.u_lista,p.BR_K' +
        'VITANCIJA, pc.email, p.prati_mail_potvrda, p.prati_mail_potvrda_' +
        'partner, mp.mail as partner_email'
      
        'from proc_san_filter_analiza(:godina, :param, :re, :datum,:param' +
        '_datum)'
      'inner join san_pregled p on p.id =proc_san_filter_analiza.priem'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'left outer join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      'left outer join sys_tip_placanje s on s.id = p.placanje'
      'left outer join san_rab_mesto rm on rm.id = p.rabotno_mesto'
      
        'left outer join san_kategorija_rm krm on krm.id = p.kategorija_r' +
        'm'
      'left outer join mat_re mr on mr.id = p.re'
      'where p.re = :re and'
      
        '      ((p.datum_priem between :datum_od and :datum_do and :param' +
        '_datum = 2) or (:param_datum = 0)) and'
      
        '      ((p.broj between :broj_od and :broj_do and :param_broj = 1' +
        ') or (:param_broj = 0)) and'
      '      p.u_lista like :u_lista'
      ''
      'order by p.broj desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 1096
    object masterID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object masterGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object masterDATUM_PRIEM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084
      FieldName = 'DATUM_PRIEM'
    end
    object masterBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1074#1086' '#1076#1085#1077#1074#1085#1080#1082
      FieldName = 'BROJ'
    end
    object masterPACIENT_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PACIENT_ID'
    end
    object masterEMBG: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterPACIENT_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterTP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TP'
    end
    object masterP: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'P'
    end
    object masterPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1085#1072#1079#1080#1074
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterPAKET_ID: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_ID'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object masterDATUM_ZAVERKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1074#1077#1088#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ZAVERKA'
    end
    object masterDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object masterPLACANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PLACANJE'
    end
    object masterNACINPLAKANJE: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACINPLAKANJE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object masterRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterRABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'RABOTNO_MESTO'
    end
    object masterRABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterKATEGORIJA_RM: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA_RM'
    end
    object masterKATEGORIJANAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'KATEGORIJANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object masterTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object masterUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterBR_KVITANCIJA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1074#1080#1090#1072#1085#1094#1080#1112#1072
      FieldName = 'BR_KVITANCIJA'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object masterPRATI_MAIL_POTVRDA: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1090#1077#1085#1086' '#1087#1086' eMail'
      FieldName = 'PRATI_MAIL_POTVRDA'
    end
    object masterPRATI_MAIL_POTVRDA_PARTNER: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1090#1077#1085#1086' '#1087#1086' eMail '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'PRATI_MAIL_POTVRDA_PARTNER'
    end
    object masterPARTNER_EMAIL: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - EMAIL'
      FieldName = 'PARTNER_EMAIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsMaster: TDataSource
    DataSet = master
    Left = 352
    Top = 1096
  end
  object detail: TpFIBDataSet
    SelectSQL.Strings = (
      ' select ps.id as stavka_id,'
      '        p.id as priem_id,'
      '        u.id usluga_id,'
      '        u.naziv, '
      '        u.cena, '
      '        u.re,'
      '        ps.zavrsena, '
      '        ps.naod, '
      '        p.broj'
      'from san_pregled_s ps'
      'inner join san_pregled p on p.id = ps.pregled_id'
      'left outer join san_uslugi u on u.id = ps.usluga_id'
      'where u.tip = 0')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 1160
    object detailSTAVKA_ID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'STAVKA_ID'
      Size = 0
    end
    object detailPRIEM_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' - '#1096#1080#1092#1088#1072
      FieldName = 'PRIEM_ID'
    end
    object detailUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA_ID'
    end
    object detailNAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object detailCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object detailRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RE'
    end
    object detailZAVRSENA: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ZAVRSENA'
    end
    object detailNAOD: TFIBSmallIntField
      DisplayLabel = #1053#1072#1086#1076
      FieldName = 'NAOD'
    end
    object detailBROJ: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' - '#1073#1088#1086#1112
      FieldName = 'BROJ'
    end
  end
  object dsdetail: TDataSource
    DataSet = detail
    Left = 352
    Top = 1160
  end
  object qCountZavrseni: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) br'
      'from san_pregled_s p'
      'where p.pregled_id = :pregled_id and p.zavrsena = 1')
    Left = 480
    Top = 1304
  end
  object pZatvoriDatum: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_SAN_ZATVORI_DATUM_ZAVERKA (?PREGLED_ID)')
    StoredProcName = 'PROC_SAN_ZATVORI_DATUM_ZAVERKA'
    Left = 768
    Top = 840
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblUslugiStringPriem: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct p.u_lista'
      'from san_pregled p')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 1232
    object tblUslugiStringPriemU_LISTA: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'U_LISTA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsUslugiStringPriem: TDataSource
    DataSet = tblUslugiStringPriem
    Left = 352
    Top = 1232
  end
  object StyleRepository: TcxStyleRepository
    Left = 584
    Top = 24
    PixelsPerInch = 96
    object Sunny: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clNavy
    end
    object Dark: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWhite
    end
    object Golden: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object Summer: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Autumn: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object Bright: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 16749885
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Cold: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 14872561
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Spring: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Light: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object Winter: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object Title: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clNavy
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object Search: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clRed
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clCream
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 4707838
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12615680
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clRed
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15451300
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clNavy
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clYellow
    end
    object zContent: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clMoneyGreen
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object zHeader: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clTeal
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clYellow
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWhite
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor]
      Color = 15593713
    end
    object cxStyle27: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle28: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle29: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle30: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object stlCheckBox: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle31: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clMoneyGreen
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle32: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle33: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle34: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle35: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle36: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle37: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle38: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle39: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle40: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle41: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle42: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle43: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle44: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle45: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle46: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle47: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle48: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle49: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle50: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle51: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle52: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle53: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle54: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle55: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle56: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle57: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle58: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle59: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle60: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle61: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle62: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle63: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle64: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle65: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle66: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle67: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle68: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle69: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle70: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle71: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle72: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle73: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle74: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle75: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle76: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle77: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle78: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle79: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle80: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle81: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle82: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle83: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle84: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle85: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle86: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle87: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle88: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle89: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle90: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle91: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle92: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle93: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle94: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle95: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle96: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle97: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle98: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle99: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle100: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle101: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle102: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle103: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle104: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle105: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle106: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle107: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle108: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle109: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle110: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle111: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle112: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle113: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle114: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle115: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle116: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle117: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle118: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle119: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle120: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle121: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle122: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle123: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle124: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle125: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle126: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle127: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle128: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle129: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle130: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle131: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle132: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle133: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle134: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle135: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle136: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle137: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle138: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle139: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle140: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle141: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle142: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle143: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle144: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle145: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle146: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle147: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle148: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle149: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle150: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle151: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle152: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle153: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle154: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle155: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle156: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle157: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle158: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle159: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle160: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle161: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle162: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle163: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle164: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle165: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle166: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle167: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle168: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16749885
      TextColor = clWhite
    end
    object cxStyle169: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle170: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle171: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle172: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle173: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle174: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle175: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle176: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle177: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle178: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle179: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle180: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle181: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle182: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle183: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle184: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle185: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle186: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle187: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle188: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle189: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle190: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle191: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle192: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle193: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle194: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle195: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle196: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle197: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle198: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle199: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle200: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle201: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle202: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle203: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle204: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle205: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle206: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle207: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle208: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle209: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle210: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle211: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle212: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
      TextColor = clBlack
    end
    object cxStyle213: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 13816275
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle227: TcxStyle
    end
    object cxChartStyle: TcxStyle
      AssignedValues = [svColor]
      Color = 13149574
    end
    object cxStyle230: TcxStyle
      AssignedValues = [svColor]
      Color = 15389113
    end
    object RedLight: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 8421631
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object MoneyGreen: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
    object cxStyle232: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle233: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle234: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle235: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle236: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle237: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle238: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle239: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle240: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle241: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle242: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle243: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle244: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle245: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle246: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle247: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle248: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle249: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle250: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle251: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle252: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle253: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle254: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle255: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle256: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle257: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle258: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle259: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle260: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle261: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle262: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16247513
      TextColor = clBlack
    end
    object cxStyle263: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 16445924
      TextColor = clBlack
    end
    object cxStyle264: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 15850688
      TextColor = clBlack
    end
    object cxStyle265: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle266: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle267: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle268: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle269: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16711164
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clSilver
    end
    object cxStyle270: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object cxStyle271: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle272: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle273: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle274: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 16247513
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle275: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 14811135
      TextColor = clBlack
    end
    object cxStyle276: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle277: TcxStyle
      AssignedValues = [svColor]
      Color = 14872561
    end
    object cxStyle278: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle279: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12937777
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clWhite
    end
    object cxStyle280: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle281: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 4707838
      TextColor = clBlack
    end
    object cxStyle282: TcxStyle
      AssignedValues = [svColor]
      Color = 15451300
    end
    object cxStyle283: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 14811135
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clNavy
    end
    object cxStyle284: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = 12937777
      TextColor = clWhite
    end
    object LightBlue: TcxStyle
      AssignedValues = [svColor]
      Color = 14405055
    end
    object BoldText: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStylePortokalova: TcxStyle
      AssignedValues = [svColor]
      Color = 6205183
    end
    object cxStyleZelono: TcxStyle
      AssignedValues = [svColor]
      Color = 11337638
    end
    object cxStyleZolto: TcxStyle
      AssignedValues = [svColor]
      Color = 11075583
    end
    object UserStyleSheet: TcxGridTableViewStyleSheet
      Caption = 'User Defined Style Sheet'
      Styles.Background = Dark
      Styles.Content = Spring
      Styles.FilterBox = Sunny
      Styles.Footer = Light
      Styles.Group = Cold
      Styles.GroupByBox = Golden
      Styles.Inactive = Summer
      Styles.Indicator = Autumn
      Styles.Preview = Winter
      Styles.Selection = Bright
      BuiltIn = True
    end
    object DetailStyleSheet: TcxGridTableViewStyleSheet
      Caption = 'Zoko'
      Styles.Background = cxStyle1
      Styles.Content = zContent
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = zHeader
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object MasterStyleSheet: TcxGridTableViewStyleSheet
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = cxStyle3
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object ssPrekuEdno: TcxGridTableViewStyleSheet
      Styles.Background = cxStyle1
      Styles.Content = cxStyle2
      Styles.ContentEven = Light
      Styles.ContentOdd = cxStyle4
      Styles.FilterBox = cxStyle5
      Styles.IncSearch = cxStyle11
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle7
      Styles.GroupByBox = cxStyle8
      Styles.Header = cxStyle9
      Styles.Inactive = cxStyle10
      Styles.Indicator = cxStyle12
      Styles.Preview = cxStyle13
      Styles.Selection = cxStyle14
      BuiltIn = True
    end
    object Banded: TcxGridBandedTableViewStyleSheet
      Caption = 'DevExpress Banded'
      Styles.Background = cxStyle15
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBox = cxStyle21
      Styles.IncSearch = cxStyle27
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.GroupByBox = cxStyle24
      Styles.Header = cxStyle25
      Styles.Inactive = cxStyle26
      Styles.Indicator = cxStyle28
      Styles.Preview = cxStyle29
      Styles.Selection = cxStyle30
      Styles.BandBackground = cxStyle16
      Styles.BandHeader = cxStyle17
      BuiltIn = True
    end
    object TableViewDefaultStyle: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle116
      Styles.Content = cxStyle117
      Styles.ContentEven = cxStyle118
      Styles.ContentOdd = cxStyle119
      Styles.FilterBox = cxStyle120
      Styles.IncSearch = cxStyle126
      Styles.Footer = cxStyle121
      Styles.Group = cxStyle122
      Styles.GroupByBox = cxStyle123
      Styles.Header = cxStyle124
      Styles.Inactive = cxStyle125
      Styles.Indicator = cxStyle127
      Styles.Preview = cxStyle128
      Styles.Selection = cxStyle129
      BuiltIn = True
    end
    object GridBandedTableViewStyleSheetDevExpress: TcxGridBandedTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle144
      Styles.Content = cxStyle147
      Styles.ContentEven = cxStyle148
      Styles.ContentOdd = cxStyle149
      Styles.FilterBox = cxStyle150
      Styles.IncSearch = cxStyle156
      Styles.Footer = cxStyle151
      Styles.Group = cxStyle152
      Styles.GroupByBox = cxStyle153
      Styles.Header = cxStyle154
      Styles.Inactive = cxStyle155
      Styles.Indicator = cxStyle157
      Styles.Preview = cxStyle158
      Styles.Selection = cxStyle159
      Styles.BandBackground = cxStyle145
      Styles.BandHeader = cxStyle146
      BuiltIn = True
    end
    object GridCardViewStyleSheetDevExpress: TcxGridCardViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle160
      Styles.Content = cxStyle163
      Styles.ContentEven = cxStyle164
      Styles.ContentOdd = cxStyle165
      Styles.IncSearch = cxStyle167
      Styles.CaptionRow = cxStyle161
      Styles.CardBorder = cxStyle162
      Styles.Inactive = cxStyle166
      Styles.RowCaption = cxStyle168
      Styles.Selection = cxStyle169
      BuiltIn = True
    end
    object TreeListStyleSheetDevExpress: TcxTreeListStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle170
      Styles.Content = cxStyle174
      Styles.Inactive = cxStyle178
      Styles.Selection = cxStyle182
      Styles.BandBackground = cxStyle171
      Styles.BandHeader = cxStyle172
      Styles.ColumnHeader = cxStyle173
      Styles.ContentEven = cxStyle175
      Styles.ContentOdd = cxStyle176
      Styles.Footer = cxStyle177
      Styles.IncSearch = cxStyle179
      Styles.Indicator = cxStyle180
      Styles.Preview = cxStyle181
      BuiltIn = True
    end
    object PivotGridStyleSheetDevExpress: TcxPivotGridStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle183
      Styles.ColumnHeader = cxStyle184
      Styles.Content = cxStyle185
      Styles.FieldHeader = cxStyle186
      Styles.HeaderBackground = cxStyle187
      Styles.Inactive = cxStyle188
      Styles.Prefilter = cxStyle189
      Styles.RowHeader = cxStyle190
      Styles.Selected = cxStyle191
      BuiltIn = True
    end
    object GridTableViewStyleSheetLogin: TcxGridTableViewStyleSheet
      Caption = 'DevExpress'
      Styles.Background = cxStyle271
      Styles.Content = cxStyle272
      Styles.ContentEven = cxStyle273
      Styles.ContentOdd = cxStyle274
      Styles.FilterBox = cxStyle275
      Styles.IncSearch = cxStyle281
      Styles.Footer = cxStyle276
      Styles.Group = cxStyle277
      Styles.GroupByBox = cxStyle278
      Styles.Header = cxStyle279
      Styles.Inactive = cxStyle280
      Styles.Indicator = cxStyle282
      Styles.Preview = cxStyle283
      Styles.Selection = cxStyle284
      BuiltIn = True
    end
  end
  object qGetParamPartner: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select p.naziv partner_naziv,'
      '       p.naziv_skraten as skraten_partner_naziv,'
      '       p.adresa,'
      '       p.fax,'
      '       m.naziv  as mesto_naziv,'
      '       p.MAIL'
      'from mat_partner p'
      'left outer join mat_mesto m on m.id = p.mesto'
      'where p.tip_partner = :tp and p.id = :p')
    Left = 768
    Top = 1056
  end
  object tblDogvoriCeni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SAN_DOGOVORI_CENI'
      'SET '
      '    ID = :ID,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    PAKET_ID = :PAKET_ID,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    CENA = :CENA,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SAN_DOGOVORI_CENI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SAN_DOGOVORI_CENI('
      '    ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    PAKET_ID,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    CENA,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :PAKET_ID,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :CENA,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select dc.id,'
      '       dc.tip_partner,'
      '       dc.partner,'
      '       mp.naziv as partner_naziv,'
      '       dc.paket_id,'
      '       p.naziv as paket_naziv,'
      '       dc.datum_od,'
      '       dc.datum_do,'
      '       dc.cena,'
      '       dc.opis,'
      '       dc.ts_ins,'
      '       dc.ts_upd,'
      '       dc.usr_ins,'
      '       dc.usr_upd,'
      '       dc.custom1,'
      '       dc.custom2,'
      '       dc.custom3'
      'from san_dogovori_ceni dc'
      'inner join san_paketi p on p.id = dc.paket_id'
      
        'inner join mat_partner mp on mp.tip_partner = dc.tip_partner and' +
        ' mp.id = dc.partner'
      'where(  ((mp.tip_partner like :tp) and (mp.id like :p)) and'
      
        '      (((:param_status = 1)and(current_date between dc.datum_od ' +
        'and dc.datum_do))or(:param_status = 0))'
      '     ) and (     DC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select dc.id,'
      '       dc.tip_partner,'
      '       dc.partner,'
      '       mp.naziv as partner_naziv,'
      '       dc.paket_id,'
      '       p.naziv as paket_naziv,'
      '       dc.datum_od,'
      '       dc.datum_do,'
      '       dc.cena,'
      '       dc.opis,'
      '       dc.ts_ins,'
      '       dc.ts_upd,'
      '       dc.usr_ins,'
      '       dc.usr_upd,'
      '       dc.custom1,'
      '       dc.custom2,'
      '       dc.custom3'
      'from san_dogovori_ceni dc'
      'inner join san_paketi p on p.id = dc.paket_id'
      
        'inner join mat_partner mp on mp.tip_partner = dc.tip_partner and' +
        ' mp.id = dc.partner'
      'where ((mp.tip_partner like :tp) and (mp.id like :p)) and'
      
        '      (((:param_status = 1)and(current_date between dc.datum_od ' +
        'and dc.datum_do))or(:param_status = 0))')
    AutoUpdateOptions.UpdateTableName = 'SAN_DOGOVORI_CENI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_SAN_DOGOVORI_CENI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 232
    Top = 1312
    object tblDogvoriCeniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogvoriCeniTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblDogvoriCeniPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDogvoriCeniDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1086#1076
      FieldName = 'DATUM_OD'
    end
    object tblDogvoriCeniDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblDogvoriCeniCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblDogvoriCeniTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogvoriCeniTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogvoriCeniPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogvoriCeniPAKET_ID: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'PAKET_ID'
    end
    object tblDogvoriCeniPAKET_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090
      FieldName = 'PAKET_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDogvoriCeni: TDataSource
    DataSet = tblDogvoriCeni
    Left = 352
    Top = 1312
  end
  object qDatumBTW: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(dc.id) br'
      'from san_dogovori_ceni dc'
      'where dc.tip_partner = :tp and dc.partner = :p'
      '      and (:datum  between dc.datum_od and dc.datum_do)'
      '      and dc.paket_id = :paket_id')
    Left = 768
    Top = 1120
  end
  object tblSifUslugi: TpFIBDataSet
    SelectSQL.Strings = (
      ' select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.re,'
      '        mr.naziv as ReNaziv,'
      '        su.status,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd,'
      '        su.tip'
      'from san_uslugi su'
      'inner join mat_re mr on mr.id = su.re'
      'where (su.tip like :tip)'
      'order by id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 32
    Top = 1056
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object FIBStringField1: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBBCDField1: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object FIBDateTimeField1: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object FIBDateTimeField2: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object FIBStringField2: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField3: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField2: TFIBIntegerField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object FIBStringField4: TFIBStringField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072
      FieldName = 'RENAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBSmallIntField1: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object FIBSmallIntField2: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP'
    end
  end
  object dsSifUslugi: TDataSource
    DataSet = tblSifUslugi
    Left = 112
    Top = 1056
  end
  object qUpdatePriemDogovorIDNULL: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE san_pregled p'
      'SET p.dogovor_id = null'
      'where exists (select p.id'
      '              from san_pregled p'
      
        '              inner join san_faktura_priem fp on fp.priem_id = p' +
        '.id'
      '              where fp.faktura_id = :faktura_id)')
    Left = 920
    Top = 784
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblAktivniPartneri: TpFIBDataSet
    SelectSQL.Strings = (
      'select P.TIP_PARTNER as "'#1058#1080#1087'",'
      '       P.ID as "'#1064#1080#1092#1088#1072'",'
      '       P.NAZIV as "'#1053#1072#1079#1080#1074'",'
      '       M.NAZIV as "'#1052#1077#1089#1090#1086'",'
      '       P.ADRESA as "'#1040#1076#1088#1077#1089#1072'",'
      ''
      '       P.TIP_PARTNER as PAR_TIP,'
      '       P.ID as PAR_SIF,'
      '       P.NAZIV as PAR_NAZ,'
      '       P.TIP_PARTNER || '#39'/'#39' || P.ID as PAR_TIPSIF,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       P.ADRESA as PAR_ADR'
      'from MAT_PARTNER P'
      'left outer join MAT_MESTO M on M.ID = P.MESTO'
      'where p.aktiven = 1'
      'order by P.NAZIV')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 32
    Top = 1120
    object tblAktivniPartneriТип: TFIBIntegerField
      FieldName = #1058#1080#1087
    end
    object tblAktivniPartneriШифра: TFIBIntegerField
      FieldName = #1064#1080#1092#1088#1072
    end
    object tblAktivniPartneriНазив: TFIBStringField
      FieldName = #1053#1072#1079#1080#1074
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriМесто: TFIBStringField
      FieldName = #1052#1077#1089#1090#1086
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriАдреса: TFIBStringField
      FieldName = #1040#1076#1088#1077#1089#1072
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriPAR_TIP: TFIBIntegerField
      FieldName = 'PAR_TIP'
    end
    object tblAktivniPartneriPAR_SIF: TFIBIntegerField
      FieldName = 'PAR_SIF'
    end
    object tblAktivniPartneriPAR_NAZ: TFIBStringField
      FieldName = 'PAR_NAZ'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriPAR_TIPSIF: TFIBStringField
      FieldName = 'PAR_TIPSIF'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriMESTO_NAZIV: TFIBStringField
      FieldName = 'MESTO_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAktivniPartneriPAR_ADR: TFIBStringField
      FieldName = 'PAR_ADR'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAktivniPartneri: TDataSource
    DataSet = tblAktivniPartneri
    Left = 112
    Top = 1120
  end
  object qSetUpReBroj: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1, s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'SAN'#39' and s.p2 = '#39'RE_BROJ'#39)
    Left = 576
    Top = 96
  end
  object qSetUpRePregled: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1, s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'SAN'#39' and s.p2 = '#39'RE_PREGLED'#39)
    Left = 584
    Top = 160
  end
  object frxMailExport: TfrxMailExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ShowExportDialog = True
    SmtpPort = 25
    UseIniFile = True
    TimeOut = 60
    ConfurmReading = False
    UseMAPI = SMTP
    Left = 912
    Top = 448
  end
  object IdSMTP_Mail: TIdSMTP
    SASLMechanisms = <>
    Left = 896
    Top = 360
  end
  object qSetupMail: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select s.parametar, s.param_vrednost, s.vrednost, s.p1, s.p2, s.' +
        'v1, s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'SAN'#39' and s.p2 = :P2')
    Left = 904
    Top = 560
  end
  object frxRezultatMail: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42990.459990277780000000
    ReportOptions.LastChange = 42990.459990277780000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 992
    Top = 496
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 896
    Top = 1040
  end
  object MainMenu1: TMainMenu
    Left = 992
    Top = 424
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 587
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 920
    Top = 264
  end
  object SMTP: TIdSMTP
    SASLMechanisms = <>
    Left = 1168
    Top = 24
  end
  object SSLHandler: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 952
    Top = 688
  end
  object EmailMessage: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CharSet = 'utf-8'
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 1120
    Top = 24
  end
  object Email: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 960
    Top = 592
  end
  object qUpdatePregledMail: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  san_pregled p'
      'set p.prati_mail_partner = 1'
      'where p.id = :pregled_id')
    Left = 768
    Top = 1184
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblPratiMail: TpFIBDataSet
    SelectSQL.Strings = (
      '  select distinct p.tp as TIP_PARTNER, '
      '                  p.p as PARTNER, '
      '                  mp.mail as EMAIL,'
      '                  mp.naziv as partner_naziv'
      '     from san_pregled p'
      
        '     inner join mat_partner mp on mp.tip_partner = p.tp and mp.i' +
        'd = p.p'
      '     where p.prati_mail_partner = 1'
      '     order by p.tp, p.p, p.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 872
    Top = 1184
    object tblPratiMailTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblPratiMailPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblPratiMailPARTNER_NAZIV: TFIBStringField
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPratiMailEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblPratiMailStavki: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.id,'
      '       p.broj,'
      '       p.godina,'
      '       pc.naziv as pacient_naziv'
      'from san_pregled p'
      'left outer join san_pacienti pc on pc.id = p.pacient_id'
      
        'where p.p = :partner and p.tp = :tip_partner and p.prati_mail_pa' +
        'rtner = 1'
      ''
      'order by p.broj desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1016
    Top = 1184
    object tblPratiMailStavkiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPratiMailStavkiBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object tblPratiMailStavkiPACIENT_NAZIV: TFIBStringField
      FieldName = 'PACIENT_NAZIV'
      Size = 155
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPratiMailStavkiGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
  end
  object qUpdatePregledMail_NulL: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  san_pregled p'
      'set p.prati_mail_partner = 0'
      'where p.prati_mail_partner = 1')
    Left = 768
    Top = 1240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateMailPotvrda: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  san_pregled p'
      'set p.prati_mail_potvrda = 1'
      'where p.id = :pregled_id')
    Left = 888
    Top = 1240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateMailPotvrdaPartner: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  san_pregled p'
      'set p.prati_mail_potvrda_partner = 1'
      
        'where p.prati_mail_partner = 1 and p.p=:partner and p.tp = :tip_' +
        'partner')
    Left = 1016
    Top = 1240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblSetup: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select  s.parametar, s.param_vrednost, s.vrednost, s.p1, s.p2, s' +
        '.v1, s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'SAN'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1112
    Top = 96
    object tblSetupPARAMETAR: TFIBStringField
      FieldName = 'PARAMETAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetupPARAM_VREDNOST: TFIBStringField
      FieldName = 'PARAM_VREDNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetupVREDNOST: TFIBIntegerField
      FieldName = 'VREDNOST'
    end
    object tblSetupP1: TFIBStringField
      FieldName = 'P1'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetupP2: TFIBStringField
      FieldName = 'P2'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetupV1: TFIBStringField
      FieldName = 'V1'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetupV2: TFIBStringField
      FieldName = 'V2'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object DCP_rc61: TDCP_rc6
    Id = 4
    Algorithm = 'RC6'
    MaxKeySize = 2048
    BlockSize = 128
    Left = 1112
    Top = 160
  end
  object DCP_sha11: TDCP_sha1
    Id = 2
    Algorithm = 'SHA1'
    HashSize = 160
    Left = 1112
    Top = 224
  end
  object TimerToken: TTimer
    Enabled = False
    Interval = 3600000
    OnTimer = TimerTokenTimer
    Left = 1112
    Top = 288
  end
  object qMestoID: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 m.id'
      'from mat_mesto m'
      'where upper(m.naziv) like upper(:mesto)')
    Left = 760
    Top = 1312
  end
  object qCenaUslugaPoDogovor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select su.dogovor_id, su.usluga_id, su.cena'
      'from dog_san_uslugi su'
      'inner join dog_dogovori dd on dd.id = su.dogovor_id'
      
        'inner join dog_dogovoren_organ d on d.dogovor_id = dd.id and d.t' +
        'ip_partner = dd.tip_partner and d.partner = dd.partner'
      
        'where d.tip_partner = :tip_partner and d.partner = :partner and ' +
        ':datum_priem between dd.datum_od and dd.datum_do'
      '      and su.usluga_id = :usluga_id')
    Left = 480
    Top = 1368
  end
end
