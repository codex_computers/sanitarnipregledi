unit DogovoriCeni;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit,
  cxCalendar, cxDBExtLookupComboBox, cxGroupBox, cxSplitter, dmResources,
  dxCustomHint, cxHint, dxSkinOffice2013White, cxNavigator, System.Actions,
  cxMemo, cxRadioGroup, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmDogovoriCeni = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    pnl1: TPanel;
    cxGroupBox1: TcxGroupBox;
    lbl1: TLabel;
    PN: TcxExtLookupComboBox;
    P: TcxTextEdit;
    T: TcxTextEdit;
    lbl2: TLabel;
    PAKET_ID: TcxDBTextEdit;
    PAKET_NAZIV: TcxDBLookupComboBox;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;
    lbl3: TLabel;
    lbl4: TLabel;
    CENA: TcxDBTextEdit;
    lbl5: TLabel;
    OPIS: TcxDBMemo;
    lbl6: TLabel;
    lbl7: TLabel;
    PARTNER_NAZIV: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    dxBarManager1Bar5: TdxBar;
    Status: TcxBarEditItem;
    cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PAKET_NAZIV: TcxGridDBColumn;
    actFind: TAction;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aNovExecute(Sender: TObject);
    procedure RefreshDogovoriCeni();
    procedure cxBarEditItem2PropertiesChange(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure proveriDatum(datum: TcxDBDateEdit);
    procedure aAzurirajExecute(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDogovoriCeni: TfrmDogovoriCeni;
  rData : TRepositoryData;
implementation

{$R *.dfm}

uses dmUnit, NurkoRepository, Utils, DaNe, dmMaticni;

procedure TfrmDogovoriCeni.aAzurirajExecute(Sender: TObject);
begin
//  inherited;

end;

procedure TfrmDogovoriCeni.actFindExecute(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.FindPanel.DisplayMode:=fpdmAlways;
end;

procedure TfrmDogovoriCeni.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    if ((P.Text <> '')and (T.Text <> '') and (PN.Text <> '')) then
       begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          prva.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Insert;
          dm.tblDogvoriCeniTIP_PARTNER.Value:=t.EditValue;
          dm.tblDogvoriCeniPARTNER.Value:=P.EditValue;
          PARTNER_NAZIV.EditValue:=PN.Text;
       end
    else  ShowMessage('�������� ������� !!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDogovoriCeni.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    ZapisiButton.SetFocus;

    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
        if dm.tblDogvoriCeniDATUM_OD.Value <= dm.tblDogvoriCeniDATUM_DO.Value then
           begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              dPanel.Enabled:=false;
              lPanel.Enabled:=true;
              cxGrid1.SetFocus;
           end
        else
           begin
              ShowMessage('��������� ������ !!!');
              DATUM_OD.SetFocus;
           end;
      end;
    end;
  end;
end;

procedure TfrmDogovoriCeni.cxBarEditItem2PropertiesChange(Sender: TObject);
begin
  inherited;
  RefreshDogovoriCeni();
end;

procedure TfrmDogovoriCeni.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
begin
  TEdit(Sender).Color:=clWhite;
  kom := Sender as TWinControl;
  if ((kom = T) or (kom = P)) then
      begin
         if ((T.Text <> '')and (P.Text<> '')) then
            begin
              ZemiImeDvoenKluc(T,P, PN);
              RefreshDogovoriCeni();
            end;
      end
  else if (kom = PN) then
      begin
         if PN.Text <> '' then
            begin
              P.EditValue:=PN.EditValue[1];
              T.EditValue:=PN.EditValue[0];
              RefreshDogovoriCeni();
            end
         else
            begin
              T.Text:='';
              P.Text:='';
              RefreshDogovoriCeni();
            end;
      end;
 if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
      begin
       if (kom = DATUM_OD) then
          proveriDatum(DATUM_OD)
       else if (kom = DATUM_DO)then
          proveriDatum(DATUM_DO)
       else if ((kom = PAKET_NAZIV) or (kom = PAKET_ID))then
          proveriDatum(DATUM_DO)
      end;
end;

procedure TfrmDogovoriCeni.FormClose(Sender: TObject; var Action: TCloseAction);
begin
      //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dmRes.FreeRepository(rData);
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dmRes.FreeRepository(rData);
            Action := caFree;
          end
          else Action := caNone;
    end
    else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then dmRes.FreeRepository(rData);
end;

procedure TfrmDogovoriCeni.FormCreate(Sender: TObject);
begin
  inherited;
  rData := TRepositoryData.Create();
end;

procedure TfrmDogovoriCeni.FormShow(Sender: TObject);
begin
  inherited;
  Status.EditValue:=1;
  RefreshDogovoriCeni();
  dm.tblPaketi.Close;
  dm.tblPaketi.Open;
  dmMat.tblNurko.Open;

  PN.RepositoryItem := dmRes.InitRepository(-1, 'PN', Name, rData);
  T.SetFocus;
end;

procedure TfrmDogovoriCeni.ZemiImeDvoenKluc(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmDogovoriCeni.RefreshDogovoriCeni();
begin
  dm.tblDogvoriCeni.Close;
  dm.tblDogvoriCeni.ParamByName('param_status').Value:=Status.EditValue;
  if ((P.Text <> '')and (T.Text <> '') and (PN.Text <> '')) then
     begin
        dm.tblDogvoriCeni.ParamByName('p').Value:=P.Text;
        dm.tblDogvoriCeni.ParamByName('tp').Value:=T.Text;
     end
  else
     begin
        dm.tblDogvoriCeni.ParamByName('p').Value:='%';
        dm.tblDogvoriCeni.ParamByName('tp').Value:='%';
     end;
  dm.tblDogvoriCeni.Open;
end;

procedure TfrmDogovoriCeni.proveriDatum(datum: TcxDBDateEdit);
begin
   if (dm.tblDogvoriCeniPAKET_ID.Value <> Null) then
      begin
        dm.qDatumBTW.Close;
        dm.qDatumBTW.ParamByName('datum').Value:=datum.EditValue;
        dm.qDatumBTW.ParamByName('p').Value:=dm.tblDogvoriCeniPARTNER.Value;
        dm.qDatumBTW.ParamByName('tp').Value:=dm.tblDogvoriCeniTIP_PARTNER.Value;
        dm.qDatumBTW.ParamByName('paket_id').Value:=dm.tblDogvoriCeniPAKET_ID.Value;
        dm.qDatumBTW.ExecQuery;
        if (((dm.qDatumBTW.FldByName['br'].Value = 1) and (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert)) or
            ((dm.qDatumBTW.FldByName['br'].Value > 1)and (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)))then
           begin
             ShowMessage('��� ������������ �� ������ �� ������� �� ��������� ������ � �������!!!');
             PAKET_ID.Clear;
             PAKET_NAZIV.Clear;
             DATUM_OD.Clear;
             DATUM_DO.Clear;
           end;
      end;
end;
end.
