unit GrupnoPecatenjeFakturi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxPCdxBarPopupMenu, FIBDataSet, pFIBDataSet, cxSpinEdit, cxLabel,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu,
  cxNavigator, dxCore, cxDateUtils, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmGrupnoPecatenjeFakturi = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    DatumDo: TcxDateEdit;
    cxLabel2: TcxLabel;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    DatumOd: TcxDateEdit;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Panel2: TPanel;
    cxLabel3: TcxLabel;
    BrojDo: TcxSpinEdit;
    BrojOd: TcxSpinEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    godina: TcxSpinEdit;
    cxLabel1: TcxLabel;
    aPrikaziPoData: TAction;
    aPecatiPoData: TAction;
    aPrikaziPoBroj: TAction;
    aPecatiPoBroj: TAction;
    cxGridDBTableView1BRFAKTURA: TcxGridDBColumn;
    cxGridDBTableView1POPUST: TcxGridDBColumn;
    cxGridDBTableView1IZNOS: TcxGridDBColumn;
    cxGridDBTableView1IZNOS_POPUST: TcxGridDBColumn;
    cxGridDBTableView1DATUM: TcxGridDBColumn;
    cxGridDBTableView1GODINA: TcxGridDBColumn;
    cxGridDBTableView1RE: TcxGridDBColumn;
    cxGridDBTableView1P: TcxGridDBColumn;
    cxGridDBTableView1TP: TcxGridDBColumn;
    cxGridDBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGridDBTableView1FAKTURA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1BRFAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView1POPUST: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_POPUST: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1FAKTURA_ID: TcxGridDBColumn;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarLargeButton18: TdxBarLargeButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aPrikaziPoDataExecute(Sender: TObject);
    procedure aPecatiPoDataExecute(Sender: TObject);
    procedure aPrikaziPoBrojExecute(Sender: TObject);
    procedure aPecatiPoBrojExecute(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmGrupnoPecatenjeFakturi: TfrmGrupnoPecatenjeFakturi;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmGrupnoPecatenjeFakturi.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmGrupnoPecatenjeFakturi.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmGrupnoPecatenjeFakturi.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmGrupnoPecatenjeFakturi.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmGrupnoPecatenjeFakturi.aBrisiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       brisiGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       brisiGridVoBaza(Name,cxGridDBTableView1);
     end;
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmGrupnoPecatenjeFakturi.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmGrupnoPecatenjeFakturi.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmGrupnoPecatenjeFakturi.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
         zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
         zacuvajGridVoBaza(Name,cxGridDBTableView1);
     end;
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmGrupnoPecatenjeFakturi.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       zacuvajVoExcel(cxGrid1, Caption);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       zacuvajVoExcel(cxGrid2, Caption);
     end
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmGrupnoPecatenjeFakturi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmGrupnoPecatenjeFakturi.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmGrupnoPecatenjeFakturi.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmGrupnoPecatenjeFakturi.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmGrupnoPecatenjeFakturi.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmGrupnoPecatenjeFakturi.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmGrupnoPecatenjeFakturi.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmGrupnoPecatenjeFakturi.prefrli;
begin
end;

procedure TfrmGrupnoPecatenjeFakturi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmGrupnoPecatenjeFakturi.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmGrupnoPecatenjeFakturi.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;
    DatumOd.SetFocus;
    godina.Text:=inttostr(dmKon.godina);
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmGrupnoPecatenjeFakturi.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmGrupnoPecatenjeFakturi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGrupnoPecatenjeFakturi.cxGridDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView1);
end;

procedure TfrmGrupnoPecatenjeFakturi.cxPageControl1Change(Sender: TObject);
begin
  if cxPageControl1.ActivePageIndex = 0 then DatumOd.SetFocus
  else BrojOd.SetFocus;
end;

procedure TfrmGrupnoPecatenjeFakturi.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin

end;

//  ����� �� �����
procedure TfrmGrupnoPecatenjeFakturi.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmGrupnoPecatenjeFakturi.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmGrupnoPecatenjeFakturi.aPageSetupExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       dxComponentPrinter1Link1.PageSetup;
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       dxComponentPrinter1Link2.PageSetup;
     end
end;

//	����� �� ������� �� ������
procedure TfrmGrupnoPecatenjeFakturi.aPecatiPoBrojExecute(Sender: TObject);
var zafakt:integer;
    i, vkupno :integer;
begin
  dm.tblFakturiPoBroj.FetchAll;
  vkupno := dm.tblFakturiPoBroj.RecordCount;
  if vkupno > 0 then
  begin
    dm.tblFakturiPoBroj.First;
    for i := 0 to vkupno - 1 do
    begin
      try
        dmRes.Spremi('SAN',2);
        dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblFakturiPoBrojFAKTURA_ID.Value;
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pari_so_bukvi', QuotedStr(PariSoBukvi(dm.tblFakturiPoBrojIZNOS_POPUST.Value)));
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.PrintOptions.ShowDialog := false;
        dmRes.frxReport1.PrepareReport(true);
        dmRes.frxReport1.Print;
        //dmReport.frxReport1.ShowReport;

        dm.tblFakturiPoBroj.Next;
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end;
    end;
  end
  else ShowMessage('���� ��������� ������� �� �������!');
end;

procedure TfrmGrupnoPecatenjeFakturi.aPecatiPoDataExecute(Sender: TObject);
var zafakt:integer;
    i, vkupno :integer;
begin
  dm.tblFakturiPoData.FetchAll;
  vkupno := dm.tblFakturiPoData.RecordCount;
  if vkupno > 0 then
  begin
    dm.tblFakturiPoData.First;
    for i := 0 to vkupno - 1 do
    begin
      try
        dmRes.Spremi('SAN',2);
        dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblFakturiPoDataFAKTURA_ID.Value;
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pari_so_bukvi', QuotedStr(PariSoBukvi(dm.tblFakturiPoDataIZNOS_POPUST.Value)));
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.PrintOptions.ShowDialog := false;
        dmRes.frxReport1.PrepareReport(true);
        dmRes.frxReport1.Print;
        //dmReport.frxReport1.ShowReport;

        dm.tblFakturiPoData.Next;
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end;
    end;
  end
  else ShowMessage('���� ��������� ������� �� �������!');
end;

procedure TfrmGrupnoPecatenjeFakturi.aPecatiTabelaExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       dxComponentPrinter1Link1.ReportTitle.Text := Caption;
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
       dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����� : ' + DatumOd.Text + '-' + DatumDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       dxComponentPrinter1Link2.ReportTitle.Text := Caption;
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
       dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + godina.Text + ', ��� :  ' + BrojOd.Text + '-' + BrojDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
     end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmGrupnoPecatenjeFakturi.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       dxComponentPrinter1Link1.DesignReport();
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       dxComponentPrinter1Link2.DesignReport();
     end
end;

procedure TfrmGrupnoPecatenjeFakturi.aPrikaziPoBrojExecute(Sender: TObject);
begin
 if (Validacija(Panel2) = false) then
  begin
    dm.tblFakturiPoBroj.Close;
    dm.tblFakturiPoBroj.ParamByName('broj_od').Value := BrojOd.Value;
    dm.tblFakturiPoBroj.ParamByName('broj_do').Value := BrojDo.Value;
    dm.tblFakturiPoBroj.ParamByName('godina').Value := godina.Value;
    dm.tblFakturiPoBroj.Open;
  end;
end;

procedure TfrmGrupnoPecatenjeFakturi.aPrikaziPoDataExecute(Sender: TObject);
begin
  if (Validacija(Panel1) = false) then
  begin
    dm.tblFakturiPoData.Close;
    dm.tblFakturiPoData.ParamByName('datum_od').Value := DatumOd.Date;
    dm.tblFakturiPoData.ParamByName('datum_do').Value := DatumDo.Date;
    dm.tblFakturiPoData.Open;
  end;
end;

procedure TfrmGrupnoPecatenjeFakturi.aSnimiPecatenjeExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link2);
     end

end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmGrupnoPecatenjeFakturi.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmGrupnoPecatenjeFakturi.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
    if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
     end
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmGrupnoPecatenjeFakturi.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmGrupnoPecatenjeFakturi.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmGrupnoPecatenjeFakturi.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
