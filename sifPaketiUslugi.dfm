inherited frmPaketiUslugi: TfrmPaketiUslugi
  Caption = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1082#1077#1090
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 274
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Height = 270
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPaketUsluga
        object cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn
          DataBinding.FieldName = 'PAKET_ID'
          Width = 92
        end
        object cxGrid1DBTableView1PAKETNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PAKETNAZIV'
          Width = 271
        end
        object cxGrid1DBTableView1USLUGA_ID: TcxGridDBColumn
          DataBinding.FieldName = 'USLUGA_ID'
          Width = 96
        end
        object cxGrid1DBTableView1USLUGANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'USLUGANAZIV'
          Width = 247
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 162
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 216
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 201
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 239
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Height = 130
    ExplicitTop = 400
    ExplicitHeight = 130
    inherited Label1: TLabel
      Left = 23
      Top = 30
      Width = 42
      Caption = #1055#1072#1082#1077#1090' :'
      ExplicitLeft = 23
      ExplicitTop = 30
      ExplicitWidth = 42
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 57
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1059#1089#1083#1091#1075#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 69
      Top = 27
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      DataBinding.DataField = 'PAKET_ID'
      DataBinding.DataSource = dm.dsPaketUsluga
      Enabled = False
      StyleDisabled.TextColor = clBackground
      ExplicitLeft = 69
      ExplicitTop = 27
      ExplicitWidth = 52
      Width = 52
    end
    inherited OtkaziButton: TcxButton
      Top = 90
      TabOrder = 5
      ExplicitTop = 90
    end
    inherited ZapisiButton: TcxButton
      Top = 90
      TabOrder = 4
      ExplicitTop = 90
    end
    object USLUGA_ID: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 54
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      BeepOnEnter = False
      DataBinding.DataField = 'USLUGA_ID'
      DataBinding.DataSource = dm.dsPaketUsluga
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object PAKET: TcxDBLookupComboBox
      Tag = 1
      Left = 120
      Top = 27
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1082#1077#1090' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1055#1072#1082#1077#1090#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'PAKET_ID'
      DataBinding.DataSource = dm.dsPaketUsluga
      Enabled = False
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end
        item
          Width = 200
          FieldName = 'CENA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsPaketi
      StyleDisabled.TextColor = clBackground
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 385
    end
    object USLUGA: TcxDBLookupComboBox
      Tag = 1
      Left = 120
      Top = 54
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1091#1089#1083#1091#1075#1072' (*Insert - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1059#1089#1083#1091#1075#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'USLUGA_ID'
      DataBinding.DataSource = dm.dsPaketUsluga
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end
        item
          Width = 200
          FieldName = 'CENA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsUslugi
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 385
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited PopupMenu1: TPopupMenu
    Left = 368
    Top = 232
  end
  inherited dxBarManager1: TdxBarManager
    Top = 216
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 224
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41330.458374108800000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 232
    Top = 280
  end
end
