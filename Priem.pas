unit Priem;

interface

uses
//  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
//  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
//  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
//  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
//  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
//  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
//  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
//  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
//  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
//  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
//  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
//  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
//  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
//  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
//  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
//  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
//  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
//  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
//  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
//  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
//  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
//  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
//  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
//  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
//  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
//  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
//  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
//  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
//  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
//  cxSplitter, cxPCdxBarPopupMenu, cxImageComboBox, dxCustomHint, cxHint,
//  dxSkinOffice2013White, cxNavigator, System.Actions, dxCore, cxDateUtils,
//  dxBarBuiltInMenu, dxSkinMetropolis, dxSkinMetropolisDark,
//  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
//  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
//  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, IdAttachment,IdAttachmentFile, IdAttachmentMemory;


 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxLabel, cxImageComboBox, dxSkinOffice2013White, dxCore, cxDateUtils,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm,  IdMessage, IdSMTP, IdSSLOpenSSL, IdGlobal, IdExplicitTLSClientServerBase,  IdAttachment,IdAttachmentFile, IdAttachmentMemory,
  dxBarBuiltInMenu, dxCustomHint, cxHint;

type
//  niza = Array[1..5] of Variant;

  TfrmPriem = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    Panel2: TPanel;
    dxBarManager1Bar5: TdxBar;
    cbGodina: TcxBarEditItem;
    cxPageControlMaster: TcxPageControl;
    cxTabSheetTabelarenM: TcxTabSheet;
    cxTabSheetDeatlenM: TcxTabSheet;
    cxPageControlDetail: TcxPageControl;
    cxTabSheetTabelarenD: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PLACANJE: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn;
    dPanel: TPanel;
    Label4: TLabel;
    ID: TcxDBTextEdit;
    aPrebarajPacient: TAction;
    aIsprazniPacient: TAction;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    GODINA: TcxDBComboBox;
    DATUM_PRIEM: TcxDBDateEdit;
    BROJ: TcxDBTextEdit;
    DATUM_ZAVERKA: TcxDBDateEdit;
    cxGroupBox4: TcxGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    PLACANJE_NAZIV: TcxDBLookupComboBox;
    PLACANJE: TcxDBTextEdit;
    CENA: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    Label7: TLabel;
    Label14: TLabel;
    PACIENT_EMBG: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxBarEditItem2: TcxBarEditItem;
    cbBarGodina: TdxBarCombo;
    Label12: TLabel;
    DATUM_VAZENJE: TcxDBDateEdit;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiUsluga: TAction;
    aBrisiUsluga: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    aSpustiSoberi2: TAction;
    aZacuvajExcel2: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    aPecatiTabela2: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarButton5: TdxBarButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarManager1Bar7: TdxBar;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    aPodesuvanjePecatenje2: TAction;
    aPageSetup2: TAction;
    aSnimiPecatenje2: TAction;
    aBrisiPodesuvanjePecatenje2: TAction;
    aSnimiIzgled2: TAction;
    aBrisiIzgled2: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    PACIENT_NAZIV: TcxDBExtLookupComboBox;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    Label13: TLabel;
    R_MESTO: TcxDBTextEdit;
    R_MESTO_NAZIV: TcxDBLookupComboBox;
    Label1: TLabel;
    KATEGORIJA_RM: TcxDBTextEdit;
    KATEGORIJA_RM_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    tipDodadiUloga: TdxScreenTip;
    tipBrisiUloga: TdxScreenTip;
    Paket_ID: TcxTextEdit;
    cbPaketNaziv: TcxLookupComboBox;
    PAKET_ID_TEXT: TcxDBTextEdit;
    cxBarDatum: TcxBarEditItem;
    dxBarLargeButton28: TdxBarLargeButton;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1PREGLED_ID: TcxGridDBColumn;
    cxGrid2DBTableView1USLUGA_ID: TcxGridDBColumn;
    cxGrid2DBTableView1ULUGANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1NAOD: TcxGridDBColumn;
    cxGrid2DBTableView1ZAVRSENA: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1RENAZIV: TcxGridDBColumn;
    dxBarSubItem4: TdxBarSubItem;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    RadioStatus: TcxBarEditItem;
    dxBarSubItem5: TdxBarSubItem;
    cxBarEditItem3: TcxBarEditItem;
    cxGrid1DBTableView1U_LISTA: TcxGridDBColumn;
    Label2: TLabel;
    BR_KVITANCIJA: TcxDBTextEdit;
    cxGrid1DBTableView1BR_KVITANCIJA: TcxGridDBColumn;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton31: TdxBarLargeButton;
    aPregledajPecati: TAction;
    aDizajnRezultati: TAction;
    dxBarLargeButton32: TdxBarLargeButton;
    dxBarLargeButton33: TdxBarLargeButton;
    aDodadiSanitarnaKniska: TAction;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BRFAKTURI: TcxGridDBColumn;
    aUpatnica: TAction;
    aPotvrda: TAction;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    PopupMenu2: TPopupMenu;
    aDizajnRezultati1: TMenuItem;
    aDizajnPoupUp: TAction;
    aDUpatnica: TAction;
    aDPotvrda: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    dxRibbon1Tab3: TdxRibbonTab;
    dxBarManager1Bar10: TdxBar;
    dxBarLargeButton36: TdxBarLargeButton;
    aPIzvestuvanjeBacilonositelstvo: TAction;
    aDIzvestuvanjeBacilonositelstvo: TAction;
    N4: TMenuItem;
    PanelIzborPartner: TPanel;
    cxGroupBoxIzberiPartner: TcxGroupBox;
    TIP_PARTNER: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    PARTNERNAZIV: TcxExtLookupComboBox;
    I_PARTNER_NAZIV: TcxExtLookupComboBox;
    btnaOK_PIzvestuvanjeBacilonositelstvoPrekin: TcxButton;
    aOK_PIzvestuvanjeBacilonositelstvo: TAction;
    btnOtkazi: TcxButton;
    cxGrid2DBTableView1DOGOVOR_ID: TcxGridDBColumn;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1EMBG: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    I_TIP_PARTNER: TcxTextEdit;
    I_PARTNER: TcxTextEdit;
    cxGrid2DBTableView1BR_MRSA: TcxGridDBColumn;
    dxBarLargeButton37: TdxBarLargeButton;
    cxGridViewRepository1DBTableView2: TcxGridDBTableView;
    cxGridViewRepository1DBTableView2DBColumn: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2DBColumn1: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2DBColumn2: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2DBColumn3: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2DBColumn4: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3: TcxGridDBTableView;
    cxGridViewRepository1DBTableView3DBColumn: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3DBColumn1: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3DBColumn2: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3DBColumn3: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1KONTROLEN_PREGLED: TcxGridDBColumn;
    cxGrid1DBTableView1DOGOVOR_ID: TcxGridDBColumn;
    dxBarManager1Bar11: TdxBar;
    dxBarLargeButton38: TdxBarLargeButton;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    aKontrolenDnevenPregled: TAction;
    aDnevenPregledSoNuliranje: TAction;
    aSkratenPeriodicenIzvestaj: TAction;
    aDetalenPeriodicenIzvestaj: TAction;
    dxBarLargeButton39: TdxBarLargeButton;
    aPecatiFiskalnaSmetka: TAction;
    dxBarLargeButton40: TdxBarLargeButton;
    aStornirajFiskalnaSmetka: TAction;
    cxGrid1DBTableView1FISKALNA: TcxGridDBColumn;
    cxGrid1DBTableView1STORNA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid2DBTableView1TIP: TcxGridDBColumn;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    PanelIzborDatum: TPanel;
    cxGroupBoxIzborDatum: TcxGroupBox;
    fiskalOd: TcxDateEdit;
    Label15: TLabel;
    Label16: TLabel;
    fiskalDo: TcxDateEdit;
    aSkratenPeriodicen: TAction;
    aDetalenPeriodicen: TAction;
    cxButton3: TcxButton;
    aPecatiPeriodicen: TAction;
    cxButton4: TcxButton;
    aOtkaziPeriodicen: TAction;
    dxBarLargeButton41: TdxBarLargeButton;
    actFind: TAction;
    lbl1: TLabel;
    lbl2: TLabel;
    btnaOK_PIzvestuvanjeBacilonositelstvo: TcxButton;
    LekarNaziv: TcxLookupComboBox;
    actOK_PIzvestuvanjeBacilonositelstvoPrekin: TAction;
    actDIzvestuvanjeBacilonositelstvoPrekin: TAction;
    N5: TMenuItem;
    dxBarManager1Bar12: TdxBar;
    aPratiRezMailPacient: TdxBarLargeButton;
    actPratiRezMailPartner: TAction;
    dxBarLargeButton42: TdxBarLargeButton;
    dxBarLargeButton43: TdxBarLargeButton;
    btn_MailBacilonositelstvo: TcxButton;
    btn_MailBacilonositelstvoPrekin: TcxButton;
    actMailZaBacilonositelstvo: TAction;
    actMailZaPrekinNaBacilonositelstvo: TAction;
    actPratiMail: TAction;
    cxGrid1DBTableView1PRATI_MAIL_B: TcxGridDBColumn;
    cxGrid1DBTableView1PRATI_MAIL_PB: TcxGridDBColumn;
    cxGrid2DBTableView1M_DOGOVORI_ID: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure PARTNERNAZIVPropertiesCloseUp(Sender: TObject);
    procedure cxBarEditItem2PropertiesChange(Sender: TObject);
    procedure aDodadiUslugaExecute(Sender: TObject);
    procedure aBrisiUslugaExecute(Sender: TObject);
    procedure cbBarGodinaChange(Sender: TObject);
    procedure aSpustiSoberi2Execute(Sender: TObject);
    procedure aZacuvajExcel2Execute(Sender: TObject);
    procedure aPecatiTabela2Execute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aPageSetup2Execute(Sender: TObject);
    procedure aSnimiPecatenje2Execute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aSnimiIzgled2Execute(Sender: TObject);
    procedure aBrisiIzgled2Execute(Sender: TObject);
    procedure cbPaketNazivPropertiesCloseUp(Sender: TObject);
    procedure cbPaketNazivPropertiesChange(Sender: TObject);
    procedure cxBarDatumChange(Sender: TObject);
    procedure RadioStatusPropertiesEditValueChanged(Sender: TObject);
    procedure aPregledajPecatiExecute(Sender: TObject);
    procedure aDizajnRezultatiExecute(Sender: TObject);
    procedure aDodadiSanitarnaKniskaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aUpatnicaExecute(Sender: TObject);
    procedure aPotvrdaExecute(Sender: TObject);
    procedure aDizajnPoupUpExecute(Sender: TObject);
    procedure aDUpatnicaExecute(Sender: TObject);
    procedure aDPotvrdaExecute(Sender: TObject);
    procedure cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid2DBTableView1NAODStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aPIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
    procedure aDIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
    procedure aOK_PIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
    procedure I_PARTNER_NAZIVPropertiesCloseUp(Sender: TObject);
    procedure ZemiImeDvoenKluc_1(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
    procedure PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure I_PARTNER_NAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure aKontrolenDnevenPregledExecute(Sender: TObject);
    procedure aDnevenPregledSoNuliranjeExecute(Sender: TObject);
    procedure aSkratenPeriodicenIzvestajExecute(Sender: TObject);
    procedure aDetalenPeriodicenIzvestajExecute(Sender: TObject);
    procedure aPecatiFiskalnaSmetkaExecute(Sender: TObject);
    procedure aStornirajFiskalnaSmetkaExecute(Sender: TObject);
    procedure aPecatiPeriodicenExecute(Sender: TObject);
    procedure dxBarButton13Click(Sender: TObject);
    procedure aOtkaziPeriodicenExecute(Sender: TObject);
    procedure aSkratenPeriodicenExecute(Sender: TObject);
    procedure aDetalenPeriodicenExecute(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
    procedure actOK_PIzvestuvanjeBacilonositelstvoPrekinExecute(
      Sender: TObject);
    procedure actDIzvestuvanjeBacilonositelstvoPrekinExecute(Sender: TObject);
    procedure actPratiRezMailPartnerExecute(Sender: TObject);
    procedure actMailZaPrekinNaBacilonositelstvoExecute(Sender: TObject);
    procedure actMailZaBacilonositelstvoExecute(Sender: TObject);
    procedure actPratiMailExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano1, sobrano2 : boolean;
    prva, posledna :TWinControl;
    paket_pom, partner_pom:String;
    procedure prefrli;

  public
    { Public declarations }
    Attachment : TIdAttachmentFile;
    AttachmentMem : TIdAttachmentMemory;

    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;


  end;

var
  frmPriem: TfrmPriem;
  rData : TRepositoryData;
  pom_godina, pom_datum, pom_status, pom_periodicen, tag_mail :Integer;
  F,A: TextFile;prvznak:integer; strCENA,StrLAB,StrDOP,S:string;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository,  sifUslugi,
  dmMaticni, sifRabotnoMesto, sifKategorijaRM, sifTipUplata, Pacienti,
  cxFiscalInterface, cxConstantsMak, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPriem.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPriem.aNovExecute(Sender: TObject);
begin
  if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)) then
  begin
    cxPageControlMaster.ActivePage:=cxTabSheetDeatlenM;
    dPanel.Enabled:=True;
    cxGroupBox3.Enabled:=true;
    DATUM_PRIEM.SetFocus;
    cbPaketNaziv.Clear;
    PAKET_ID_TEXT.Visible:=False;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblPreglediRE.Value:=dmKon.firma_id;
    dm.qMaxBrojPriem.Close;
    if cbBarGodina.Text <> '' then
      begin
       dm.qMaxBrojPriem.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
       dm.tblPreglediGODINA.Value:=StrToInt(cbBarGodina.Text)
      end
    else
      begin
       dm.qMaxBrojPriem.ParamByName('godina').Value:=dmKon.godina;
       dm.tblPreglediGODINA.Value:=dmKon.godina;
      end;
    if dm.re_broj  = '1' then
      begin
       dm.qMaxBrojPriem.ParamByName('re').Value:=dmKon.firma_id;
      end
    else
      begin
       dm.qMaxBrojPriem.ParamByName('re').Value:='%';
      end;
    dm.qMaxBrojPriem.ExecQuery;
    dm.tblPreglediBROJ.Value:=dm.qMaxBrojPriem.FldByName['broj'].Value+1;
    dm.tblPreglediDATUM_PRIEM.Value:=Now;
    Paket_ID.Visible:=true;
    Paket_ID.Text:=''
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPriem.aAzurirajExecute(Sender: TObject);
begin
  if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)) then
     begin

              dm.qCountZavrseni.Close;
              dm.qCountZavrseni.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
              dm.qCountZavrseni.ExecQuery;
              cxPageControlMaster.ActivePage:=cxTabSheetDeatlenM;
              dPanel.Enabled:=True;
              DATUM_ZAVERKA.SetFocus;
              cxGrid1DBTableView1.DataController.DataSet.Edit;
              cbPaketNaziv.Text:=PAKET_ID_TEXT.Text;
              PAKET_ID_TEXT.Visible:=False;
              paket_pom:= dm.tblPreglediPAKET_ID.Value;
              partner_pom:=IntToStr(dm.tblPreglediP.Value) + '|'+IntToStr(dm.tblPreglediTP.Value);
              Paket_ID.Visible:=true;
              if cbPaketNaziv.Text <> '' then
                 begin
                   Paket_ID.Text:=IntToStr(cbPaketNaziv.EditValue);
                   dm.qPaketNaziv.Close;
                   dm.qPaketNaziv.ParamByName('id').Value:=StrToInt(Paket_ID.Text);
                   dm.qPaketNaziv.ExecQuery;
                   dm.tblPreglediPAKET_ID.Value:= dm.qPaketNaziv.FldByName['naziv'].Value;
                  end;
              if ((dm.qCountZavrseni.FldByName['br'].Value <> Null)and (dm.qCountZavrseni.FldByName['br'].Value > 0)) then
                 begin
                   ShowMessage('�������� �� �� ��������� ���������� �� ����� � ������ �� ������� �� ������������� ����� ���弝� ��� �������� ������� !!!');
                   cxGroupBox4.Enabled:=False;
                  // cxGroupBox3.Enabled:=False;
                   PACIENT_EMBG.Enabled:=False;
                   PACIENT_NAZIV.Enabled:=False;
                   R_MESTO.Enabled:=False;
                   R_MESTO_NAZIV.Enabled:=False;
                 end;
              if dm.tblPreglediBRFAKTURI.Value > 0 then
                begin
                  ShowMessage('�������� �� �� ��������� ���������� �� �������, ����� � ������ �� ������� �� ������������� ����� ���弝� ��� ������� !!!');
                  cxGroupBox4.Enabled:=False;
                  cxGroupBox3.Enabled:=False;
                end
     end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPriem.aBrisiExecute(Sender: TObject);
var pregled_id:Integer;

begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
     begin
       pregled_id:=dm.tblPreglediID.Value;
       if dm.tblPreglediBRFAKTURI.Value = 0 then
           begin
              dm.qCountZavrseni.Close;
              dm.qCountZavrseni.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
              dm.qCountZavrseni.ExecQuery;
              if ((dm.qCountZavrseni.FldByName['br'].Value <> Null)and (dm.qCountZavrseni.FldByName['br'].Value = 0)) then
                 begin
                   frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
                   if (frmDaNe.ShowModal <> mrYes) then
                       Abort
                   else
                     begin
                       dm.qDeleteUslugiOdPregled.Close;
                       dm.qDeleteUslugiOdPregled.ParamByName('PREGLED_ID').Value:=pregled_id;
                       dm.qDeleteUslugiOdPregled.ExecQuery;
                       cxGrid1DBTableView1.DataController.DataSet.Delete();
                     end;
                 end
              else ShowMessage('�������� �� �� ������� ������������� ����� ���弝� ��� �������� ������� !!!') ;
           end
       else ShowMessage('�������� �� �� ������� ������������� ����� ���弝� ��� ������� !!!');
     end;
end;

procedure TfrmPriem.aBrisiIzgled2Execute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

procedure TfrmPriem.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmPriem.aRefreshExecute(Sender: TObject);
begin
    dm.tblPregledi.Close;

    dm.tblPregledi.ParamByName('param').Value:=RadioStatus.EditValue;

    if cbBarGodina.Text <> '' then
       dm.tblPregledi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;

    if cxBarDatum.EditValue <> Null then
      begin
       dm.tblPregledi.ParamByName('datum').Value:=cxBarDatum.EditValue;
       dm.tblPregledi.ParamByName('param_datum').Value:=1;
      end
    else
      begin
       dm.tblPregledi.ParamByName('param_datum').Value:=0;
      end;

    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';
    dm.tblPregledi.Open;
    dm.tblPreglediStavki.Close;
    dm.tblPreglediStavki.Open;

    if cxPageControlMaster.ActivePage = cxTabSheetTabelarenM then
       cxGrid1.SetFocus;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPriem.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmPriem.aKontrolenDnevenPregledExecute(Sender: TObject);
begin
     fpKontrolen();
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPriem.aSkratenPeriodicenExecute(Sender: TObject);
begin
    PanelIzborDatum.Visible:=True;
    fiskalDo.Clear;
    fiskalOd.Clear;
    pom_periodicen:=2;
end;

procedure TfrmPriem.aSkratenPeriodicenIzvestajExecute(Sender: TObject);
var datum_od,datum_do,pos :string ;
    datumOd, datumDo:TDate;
begin

end;

procedure TfrmPriem.aSnimiIzgled2Execute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmPriem.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPriem.aZacuvajExcel2Execute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������ �� ������');
end;

procedure TfrmPriem.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� ��������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPriem.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if (kom = PACIENT_NAZIV)  then //or(kom = PACIENT_embg)
          begin
              frmPacienti:=TfrmPacienti.Create(self,false);
              frmPacienti.ShowModal;
              if (frmPacienti.ModalResult = mrOK) then
                 dm.tblPreglediPACIENT_ID.Value := StrToInt(frmPacienti.GetSifra(0));
              frmPacienti.Free;
       	  end
          else if (kom = PARTNERNAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
              PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
              frmNurkoRepository.Free;
       	  end
         else if (kom = I_PARTNER_NAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
              I_PARTNER_NAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            frmNurkoRepository.Free;
       	  end
         else if ((Sender = R_MESTO) or (Sender = R_MESTO_NAZIV)) then
             begin
               frmRabotnoMesto:=TfrmRabotnoMesto.Create(self,false);
               frmRabotnoMesto.ShowModal;
               if (frmRabotnoMesto.ModalResult = mrOK) then
                    dm.tblPreglediRABOTNO_MESTO.Value := StrToInt(frmRabotnoMesto.GetSifra(0));
                frmRabotnoMesto.Free;
             end
          else if ((Sender = KATEGORIJA_RM) or (Sender = KATEGORIJA_RM_NAZIV)) then
             begin
               frmKategorijaRM:=TfrmKategorijaRM.Create(self,false);
               frmKategorijaRM.ShowModal;
               if (frmKategorijaRM.ModalResult = mrOK) then
                    dm.tblPreglediKATEGORIJA_RM.Value := StrToInt(frmKategorijaRM.GetSifra(0));
                frmKategorijaRM.Free;
             end
          else if ((Sender = PLACANJE) or (Sender = PLACANJE_NAZIV)) then
             begin
               frmTipUplata:=TfrmTipUplata.Create(self,false);
               frmTipUplata.ShowModal;
               if (frmTipUplata.ModalResult = mrOK) then
                    dm.tblPreglediPLACANJE.Value := StrToInt(frmTipUplata.GetSifra(0));
                frmTipUplata.Free;
             end;
	end;
	end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPriem.cxBarDatumChange(Sender: TObject);
begin
if pom_datum = 1 then
   begin
    dm.tblPregledi.Close;
    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';

    if RadioStatus.EditValue = 0 then dm.tblPregledi.ParamByName('param').Value:=0
    else if RadioStatus.EditValue = 1 then dm.tblPregledi.ParamByName('param').Value:=1
    else if RadioStatus.EditValue = 2 then dm.tblPregledi.ParamByName('param').Value:=2;


    if cbBarGodina.Text <> '' then
       dm.tblPregledi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;

    if cxBarDatum.EditValue <> Null then
      begin
       dm.tblPregledi.ParamByName('datum').Value:=cxBarDatum.EditValue;
       dm.tblPregledi.ParamByName('param_datum').Value:=1;
      end
    else
      begin
       dm.tblPregledi.ParamByName('param_datum').Value:=0;
      end;

    dm.tblPregledi.Open;
    dm.tblPreglediStavki.FullRefresh;
   end
else
   begin
      pom_datum:= 1
   end;
end;

procedure TfrmPriem.cxBarEditItem2PropertiesChange(Sender: TObject);
begin
     ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
end;

procedure TfrmPriem.RadioStatusPropertiesEditValueChanged(Sender: TObject);
begin
    dm.tblPregledi.Close;

    dm.tblPregledi.ParamByName('param').Value:=RadioStatus.EditValue;

    if cbBarGodina.Text <> '' then
       dm.tblPregledi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;

    if cxBarDatum.EditValue <> Null then
      begin
       dm.tblPregledi.ParamByName('datum').Value:=cxBarDatum.EditValue;
       dm.tblPregledi.ParamByName('param_datum').Value:=1;
      end
    else
      begin
       dm.tblPregledi.ParamByName('param_datum').Value:=0;
      end;

    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';
    dm.tblPregledi.Open;
    dm.tblPreglediStavki.Close;
    dm.tblPreglediStavki.Open;
end;

procedure TfrmPriem.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPriem.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
begin
     TEdit(Sender).Color:=clWhite;
    kom := Sender as TWinControl;
    if ((((Sender as TWinControl)= I_TIP_PARTNER) or ((Sender as TWinControl)= I_PARTNER))and (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)) then
       begin
          ZemiImeDvoenKluc_1(I_TIP_PARTNER,I_PARTNER, I_PARTNER_NAZIV);
       end;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
           begin
               ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
           end
         else if ((Sender = cbPaketNaziv) and (cbPaketNaziv.Text<> ''))then
           begin
             dm.tblPreglediCENA.Value:=dm.tblPaketiCENA.Value;
             dm.tblPreglediIZNOS_VKUPNO.Value:=dm.tblPaketiCENA.Value;
             dm.tblPreglediPAKET_ID.Value:=dm.tblPaketiNAZIV.Value;
           end
         else if ((Sender = cbPaketNaziv) and (cbPaketNaziv.Text= ''))then
           begin
             dm.tblPreglediPAKET_ID.Value:='';
           end
         else if((Sender = PACIENT_EMBG) or (Sender = PACIENT_NAZIV)) then
           begin
             if ((Sender = PACIENT_EMBG) and (PACIENT_EMBG.Text <> '')) then
                 begin
                    dm.qPacientNAziv.Close;
                    dm.qPacientNAziv.ParamByName('embg').Value:=PACIENT_EMBG.Text;
                    dm.qPacientNAziv.ExecQuery;
                    if (not dm.qPacientNAziv.FldByName['id'].IsNull)then
                       dm.tblPreglediPACIENT_ID.Value:=dm.qPacientNAziv.FldByName['id'].Value
                    else
                       begin
                         frmPacienti:=TfrmPacienti.Create(self,false);
                         frmPacienti.Tag:=1;
                         frmPacienti.ShowModal;
                         if (frmPacienti.ModalResult = mrOK) then
                           begin
                            dm.tblPreglediPACIENT_ID.Value := StrToInt(frmPacienti.GetSifra(0));
                           end;

                         frmPacienti.Free;
                       end;
                 end

             else if ((Sender = PACIENT_NAZIV) and (PACIENT_NAZIV.EditValue <> Null)) then
                 begin
                    dm.qPacientEMBG.Close;
                    dm.qPacientEMBG.ParamByName('id').Value:=dm.tblPreglediPACIENT_ID.Value;
                    dm.qPacientEMBG.ExecQuery;
                    dm.tblPreglediEMBG.Value:=dm.qPacientEMBG.FldByName['embg'].Value;
                 end ;
             if ((PACIENT_EMBG.Text <> '') and (PACIENT_NAZIV.Text <> '')) then
               begin
                dm.qFirmaNaPacient.Close;
                dm.qFirmaNaPacient.ParamByName('pacient').Value:=dm.tblPreglediPACIENT_ID.Value;
                dm.qFirmaNaPacient.ExecQuery;
                dm.tblPreglediTP.Value:=dm.qFirmaNaPacient.FldByName['TIP_PARTNER'].Value;
                dm.tblPreglediP.Value:=dm.qFirmaNaPacient.FldByName['PARTNER'].Value;
                if (dm.qFirmaNaPacient.FldByName['r_mesto'].Value <> Null) then
                   dm.tblPreglediRABOTNO_MESTO.Value:= dm.qFirmaNaPacient.FldByName['r_mesto'].Value;
                if (dm.qFirmaNaPacient.FldByName['kategorija_rm'].Value <> Null) then
                   dm.tblPreglediKATEGORIJA_RM.Value:= dm.qFirmaNaPacient.FldByName['kategorija_rm'].Value;
                ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV)
               end

           end
       else if (Sender = Paket_ID)  then
             begin
               if (Paket_ID.Text<> '') then
                  begin
                    dm.qPaketNaziv.Close;
                    dm.qPaketNaziv.ParamByName('id').Value:=StrToInt(Paket_ID.Text);
                    dm.qPaketNaziv.ExecQuery;
                    if dm.qPaketNaziv.FldByName['naziv'].Value <> Null then
                       begin
                          dm.tblPreglediPAKET_ID.Value:= dm.qPaketNaziv.FldByName['naziv'].Value;
                          cbPaketNaziv.EditValue:=StrToInt(Paket_ID.Text);
                       end
                    else
                       Paket_ID.Clear;
                 end
               else
                 begin
                    cbPaketNaziv.Clear;
                 end;
             end
       else if ((sender = KATEGORIJA_RM) or (sender= KATEGORIJA_RM_NAZIV)or (sender = DATUM_ZAVERKA)) then
             begin
               if ((not dm.tblPreglediDATUM_ZAVERKA.IsNull)and (not dm.tblPreglediKATEGORIJA_RM.IsNull)) then
                   begin
                       dm.qDatumVazenje.Close;
                       dm.qDatumVazenje.ParamByName('id').Value:=dm.tblPreglediKATEGORIJA_RM.Value;
                       dm.qDatumVazenje.ParamByName('datum_zaverka').Value:=dm.tblPreglediDATUM_ZAVERKA.Value;
                       dm.qDatumVazenje.ExecQuery;
                       dm.tblPreglediDATUM_VAZENJE.Value:= dm.qDatumVazenje.FldByName['datum_vazenje'].Value;
                   end;
             end;

    end
end;

procedure TfrmPriem.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
      ZemiImeDvoenKluc(TIP_PARTNER,PARTNER,PARTNERNAZIV);
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPriem.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPriem.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPriem.I_PARTNER_NAZIVPropertiesCloseUp(Sender: TObject);
begin
//      if I_PARTNER_NAZIV.Text <> '' then
//         begin
//           I_TIP_PARTNER.EditValue:=I_PARTNER_NAZIV.EditValue[0];
//           I_PARTNER.EditValue:=I_PARTNER_NAZIV.EditValue[1];
//         end
//      else
//         begin
//           I_PARTNER.Clear;
//           I_TIP_PARTNER.Clear;
//         end;
end;

procedure TfrmPriem.I_PARTNER_NAZIVPropertiesEditValueChanged(Sender: TObject);
begin
      if I_PARTNER_NAZIV.Text <> '' then
         begin
           I_TIP_PARTNER.EditValue:=I_PARTNER_NAZIV.EditValue[0];
           I_PARTNER.EditValue:=I_PARTNER_NAZIV.EditValue[1];
         end
      else
         begin
           I_PARTNER.Clear;
           I_TIP_PARTNER.Clear;
         end;
end;

procedure TfrmPriem.PARTNERNAZIVPropertiesCloseUp(Sender: TObject);
begin
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//      if PARTNERNAZIV.Text <> '' then
//         begin
//           dm.tblPreglediTP.Value:=PARTNERNAZIV.EditValue[0];
//           dm.tblPreglediP.Value:=PARTNERNAZIV.EditValue[1];
//         end
//      else
//         begin
//           PARTNER.Clear;
//           TIP_PARTNER.Clear;
//         end;
//    end;
end;

procedure TfrmPriem.PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
      if PARTNERNAZIV.Text <> '' then
         begin
           dm.tblPreglediTP.Value:=PARTNERNAZIV.EditValue[0];
           dm.tblPreglediP.Value:=PARTNERNAZIV.EditValue[1];
         end
      else
         begin
           PARTNER.Clear;
           TIP_PARTNER.Clear;
         end;
    end;
end;

procedure TfrmPriem.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPriem.prefrli;
begin
end;

procedure TfrmPriem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dmRes.FreeRepository(rData);
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dmRes.FreeRepository(rData);
            Action := caFree;
          end
          else Action := caNone;
    end
  else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then dmRes.FreeRepository(rData);
end;
procedure TfrmPriem.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmPriem.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    dmRes.GenerateFormReportsAndActions(dxRibbon1,dxRibbon1Tab1,dxBarManager1,cxGrid1DBTableView1, Name);


    cxPageControlMaster.ActivePage:=cxTabSheetTabelarenM;
    cxPageControlDetail.ActivePage:=cxTabSheetTabelarenD;

    pom_godina:=0;
    pom_datum:=0;
    pom_status:=0;
    cbBarGodina.Text:=IntToStr(dmKon.godina);
    cxBarDatum.EditValue:=Now;
    RadioStatus.EditValue:=0;
    ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);

    dxComponentPrinter1Link1.ReportTitle.Text := '��������';
    dxComponentPrinter1Link1.ReportTitle.Text := '������';

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);

    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
    sobrano1 := true;
    sobrano2 := true;


	 // PACIENT_NAZIV.RepositoryItem := dmRes.InitRepository(-401, 'PACIENT_NAZIV', Name, rData);
   //	procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

   PARTNERNAZIV.RepositoryItem := dmRes.InitRepository(-1, 'PARTNERNAZIV', Name, rData);
   I_PARTNER_NAZIV.RepositoryItem := dmRes.InitRepository(-1, 'I_PARTNER_NAZIV', Name, rData);
   //	procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPregledi.ParamByName('param').Value:=0;
    dm.tblPregledi.ParamByName('datum').Value:=Now;
    dm.tblPregledi.ParamByName('param_datum').Value:=1;
    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';
    dm.tblPregledi.FullRefresh;
    dm.tblPreglediStavki.FullRefresh;

    dm.tblLekari.Open;
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    cxGrid1.SetFocus;
end;
//------------------------------------------------------------------------------

procedure TfrmPriem.SaveToIniFileExecute(Sender: TObject);
begin
   cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
   cxGrid2DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPriem.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPriem.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
      if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1BRFAKTURI.Index] = 1) then
          AStyle := dmRes.RedLight;
end;

procedure TfrmPriem.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmPriem.cxGrid2DBTableView1NAODStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
      if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1NAOD.Index] = 1) then
          AStyle := dm.cxStylePortokalova
      else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1NAOD.Index] = 0) then
          AStyle := dm.cxStyleZelono
      else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 2) then
          AStyle := dm.cxStyleZolto;
end;

procedure TfrmPriem.cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 0) then
          AStyle := dm.cxStylePortokalova
     else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 1) then
          AStyle := dm.cxStyleZelono
     else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 2) then
          AStyle := dm.cxStyleZolto;
end;

procedure TfrmPriem.dxBarButton13Click(Sender: TObject);
begin
    PanelIzborDatum.Visible:=True;
    fiskalDo.Clear;
    fiskalOd.Clear;
    pom_periodicen:=2;
end;

//  ����� �� �����
procedure TfrmPriem.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  pom_id, flag: Integer;
begin
//  ZapisiButton.SetFocus;
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin

      if st in [dsInsert] then
         begin
          dm.qMaxBrojPriem.Close;
          if cbBarGodina.Text <> '' then
             dm.qMaxBrojPriem.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
          else
             dm.qMaxBrojPriem.ParamByName('godina').Value:=dmKon.godina;
          dm.qMaxBrojPriem.ExecQuery;
          dm.tblPreglediBROJ.Value:=dm.qMaxBrojPriem.FldByName['broj'].Value+1;
         end;
      cxGrid1DBTableView1.DataController.DataSet.Post;


      if ((dm.tblPreglediKONTROLEN_PREGLED.Value = 0)or (dm.tblPreglediKONTROLEN_PREGLED.IsNull)) then flag:=1
      else if(dm.tblPreglediKONTROLEN_PREGLED.Value = 1) then flag:=0;

      if (not dm.tblPreglediPAKET_ID.IsNull) then
        begin
           if ((st in [dsEdit])and((paket_pom <> dm.tblPreglediPAKET_ID.Value)or(partner_pom <> IntToStr(dm.tblPreglediP.Value) + '|'+IntToStr(dm.tblPreglediTP.Value)))) then
              begin
                 dm.insert6(dm.PROC_SAN_INSERTUSLUGA, 'PAKET_ID', 'PREGLED_ID', 'FLAG', 'TIP_PARTNER','PARTNER', 'DATUM_PRIEM',cbPaketNaziv.EditValue, dm.tblPreglediID.Value, flag, dm.tblPreglediTP.Value,dm.tblPreglediP.Value, dm.tblPreglediDATUM_PRIEM.Value);
                 dm.insert6(dm.PROC_SAN_LISTAUSLUGI, 'PREGLED_ID', Null, Null, Null,Null, Null,dm.tblPreglediID.Value, Null, Null, Null,Null, Null);
              end
           else if st in [dsInsert] then
              begin
                 dm.insert6(dm.PROC_SAN_INSERTUSLUGA, 'PAKET_ID', 'PREGLED_ID', 'FLAG', 'TIP_PARTNER','PARTNER', 'DATUM_PRIEM',cbPaketNaziv.EditValue, dm.tblPreglediID.Value, flag, dm.tblPreglediTP.Value,dm.tblPreglediP.Value, dm.tblPreglediDATUM_PRIEM.Value);
                 dm.insert6(dm.PROC_SAN_LISTAUSLUGI, 'PREGLED_ID', Null, Null, Null,Null, Null,dm.tblPreglediID.Value, Null, Null, Null,Null, Null);
              end
        end;

      dm.tblPregledi.FullRefresh;
      dm.tblPreglediStavki.Close;
      dm.tblPreglediStavki.Open;
      PAKET_ID_TEXT.Visible:=True;
      Paket_ID.Visible:=False;
      dPanel.Enabled:=false;
      cxPageControlMaster.ActivePage:=cxTabSheetTabelarenM;
      cxGroupBox4.Enabled:=True;
      //cxGroupBox3.Enabled:=True;
      PACIENT_EMBG.Enabled:=True;
      PACIENT_NAZIV.Enabled:=True;
      R_MESTO.Enabled:=True;
      R_MESTO_NAZIV.Enabled:=True;
      cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmPriem.cbBarGodinaChange(Sender: TObject);
begin
if pom_godina = 1 then
   begin
    dm.tblPregledi.Close;
    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';

    if RadioStatus.EditValue = 0 then dm.tblPregledi.ParamByName('param').Value:=0
    else if RadioStatus.EditValue = 1 then dm.tblPregledi.ParamByName('param').Value:=1
    else if RadioStatus.EditValue = 2 then dm.tblPregledi.ParamByName('param').Value:=2;

    if cbBarGodina.Text <> '' then
       dm.tblPregledi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;

    if cxBarDatum.EditValue <> Null then
      begin
       dm.tblPregledi.ParamByName('datum').Value:=cxBarDatum.EditValue;
       dm.tblPregledi.ParamByName('param_datum').Value:=1;
      end
    else
      begin
       dm.tblPregledi.ParamByName('param_datum').Value:=0;
      end;

    dm.tblPregledi.Open;
    dm.tblPreglediStavki.FullRefresh;
   end
else
   begin
      pom_godina:= 1
   end;
end;

procedure TfrmPriem.cbPaketNazivPropertiesChange(Sender: TObject);
var  st: TDataSetState;
begin


  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
    begin
     if (cbPaketNaziv.Text <> '') and (not dm.tblPaketiID.IsNull) then
      begin
        Paket_ID.Text:=IntToStr(dm.tblPaketiID.Value);
        dm.qPaketNaziv.Close;
        dm.qPaketNaziv.ParamByName('id').Value:=StrToInt(Paket_ID.Text);
        dm.qPaketNaziv.ExecQuery;
        dm.tblPreglediPAKET_ID.Value:= dm.qPaketNaziv.FldByName['naziv'].Value;
      end
     else
      Paket_ID.Text:='';
    end;
end;

procedure TfrmPriem.cbPaketNazivPropertiesCloseUp(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmPriem.aOK_PIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
begin
    if (Validacija(PanelIzborPartner) = false) then
       begin
          PanelIzborPartner.Visible:=false;
          dxRibbon1.Enabled:=True;
          Panel1.Enabled:=True;
          Panel2.Enabled:=True;
          try
            dmRes.Spremi('SAN',12);
            dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
            dmKon.tblSqlReport.Open;

            dm.qGetParamPartner.Close;
            dm.qGetParamPartner.ParamByName('p').Value:=I_PARTNER.EditValue;
            dm.qGetParamPartner.ParamByName('tp').Value:=I_TIP_PARTNER.EditValue;
            dm.qGetParamPartner.ExecQuery;

            if dm.qGetParamPartner.FldByName['partner_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv', QuotedStr(dm.qGetParamPartner.FldByName['partner_naziv'].Value));
            if dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv_skraten', QuotedStr(dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value));
            if dm.qGetParamPartner.FldByName['mesto_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_mesto', QuotedStr(dm.qGetParamPartner.FldByName['mesto_naziv'].Value));
            if dm.qGetParamPartner.FldByName['adresa'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_adresa', QuotedStr(dm.qGetParamPartner.FldByName['adresa'].Value));
            if dm.qGetParamPartner.FldByName['fax'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_fax', QuotedStr(dm.qGetParamPartner.FldByName['fax'].Value));
            if LekarNaziv.Text <> '' then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'lekar', QuotedStr(LekarNaziv.Text));
            dmRes.frxReport1.ShowReport();
          except
            ShowMessage('�� ���� �� �� ������� ���������!');
          end
       end;
end;

procedure TfrmPriem.aOtkaziExecute(Sender: TObject);
begin
  if PanelIzborPartner.Visible = true then
     begin
       PanelIzborPartner.Visible:=false;
       dxRibbon1.Enabled:=True;
       Panel1.Enabled:=True;
       Panel2.Enabled:=True;
     end
  else if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)) then
     begin
       ModalResult := mrCancel;
       Close();
     end
  else if (cxGrid1DBTableView1.DataController.DataSource.State in [dsEdit,dsInsert]) then
     begin
       cxGrid1DBTableView1.DataController.DataSet.Cancel;
       RestoreControls(dPanel);
       PAKET_ID_TEXT.Visible:=True;
       Paket_ID.Visible:=False;
       dPanel.Enabled:=false;
       cxPageControlMaster.ActivePage:=cxTabSheetTabelarenM;
       cxGrid1.SetFocus;
       cxGroupBox4.Enabled:=True;
       //cxGroupBox3.Enabled:=True;
       PACIENT_EMBG.Enabled:=True;
       PACIENT_NAZIV.Enabled:=True;
       R_MESTO.Enabled:=True;
       R_MESTO_NAZIV.Enabled:=True;
     end;
end;

procedure TfrmPriem.aOtkaziPeriodicenExecute(Sender: TObject);
begin
      PanelIzborDatum.Visible:=False;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPriem.aPageSetup2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmPriem.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPriem.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPriem.aPIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)) then
       if (cxGrid1DBTableView1.DataController.RecordCount <> 0)then
           begin
              PanelIzborPartner.Visible:=true;
              PanelIzborPartner.Left:=180;
              PanelIzborPartner.Top:=230;
              cxGroupBoxIzberiPartner.Left:=0;
              cxGroupBoxIzberiPartner.Top:=0;
              dxRibbon1.Enabled:=false;
              Panel1.Enabled:=False;
              Panel2.Enabled:=False;
              I_TIP_PARTNER.SetFocus;
              I_TIP_PARTNER.EditValue:=dm.tblPreglediTP.Value;
              I_PARTNER.EditValue:=dm.tblPreglediP.Value;
              ZemiImeDvoenKluc_1(I_TIP_PARTNER,I_PARTNER,I_PARTNER_NAZIV);
              LekarNaziv.EditValue:=dm.tblPreglediLEKAR.Value;
              btnaOK_PIzvestuvanjeBacilonositelstvo.Visible:=true;
              btnaOK_PIzvestuvanjeBacilonositelstvoPrekin.Visible:=true;
              btn_MailBacilonositelstvoPrekin.Visible:=False;
              btn_MailBacilonositelstvo.Visible:=false;
           end
       else ShowMessage('�������� �����/�������')
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPriem.aPodesuvanjePecatenje2Execute(Sender: TObject);
begin
   dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPriem.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPriem.aPotvrdaExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('SAN',10);
        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;



procedure TfrmPriem.aPregledajPecatiExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('SAN',1);
        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
        dmKon.tblSqlReport.Open;
        dm.tblRezultatAnaliza.Close;
        dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
        dm.tblRezultatAnaliza.Open;
        dm.tblFrxAntibiogram.Close;
        dm.tblFrxAntibiogram.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(dm.tblPreglediPACIENT_NAZIV.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(dm.tblPreglediBROJ.Value)));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmPriem.aSnimiPecatenje2Execute(Sender: TObject);
begin
    zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

procedure TfrmPriem.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmPriem.aSpustiSoberi2Execute(Sender: TObject);
begin
  if (sobrano2 = true) then
  begin
    cxGrid2DBTableView1.ViewData.Expand(false);
    sobrano2 := false;
  end
  else
  begin
    cxGrid2DBTableView1.ViewData.Collapse(false);
    sobrano2 := true;
  end;
end;

procedure TfrmPriem.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano1 = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano1 := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano1 := true;
  end;
end;

procedure TfrmPriem.aStornirajFiskalnaSmetkaExecute(Sender: TObject);
begin
if UpperCase(dm.tblPreglediNACINPLAKANJE.AsString) = '�� ������' then
   begin
    if ({(dm.tblPreglediNAPLATENO.Value>0)and} {comentDanica and (zemiPromet>=dm1.qPresmetka1NAPLATENA.Value)}  (dm.tblPreglediFISKALNA.Value >dm.tblPreglediSTORNA.Value))  then
       begin
          strLAB:=dm.tblPreglediPAKET_ID.AsString+':LABOR.USLUGI' ;
          strCENA:=dm.tblPreglediIZNOS_VKUPNO.AsString;
          if dm.PecatiFiskalnaDll(strCENA,StrLAB,1,1,1)=false
          then ShowMessage('������ �� �� �������� �������� ������ !');
       end
    else ShowMessage('�� ����������� ��� ���������� ������ ������ !!!');
   end
else ShowMessage('������ �� �� �������� ������ �������� ������ �� ������� �� ������� !!!');
end;

procedure TfrmPriem.aUpatnicaExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('SAN',11);
        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPriem.aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmPriem.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPriem.aBrisiUslugaExecute(Sender: TObject);
begin
 if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
     begin
       if dm.tblPreglediBRFAKTURI.Value = 0 then
          begin
            if ((dm.tblPreglediPAKET_ID.IsNull) or (dm.tblPreglediStavkiTIP.value = 1)or (dm.tblPreglediKONTROLEN_PREGLED.Value = 1))then
              begin
                if ((dm.tblPreglediStavkiZAVRSENA.Value = 0) or (dm.tblPreglediStavkiZAVRSENA.Value = 2)) then
                   begin
                      cxGrid2DBTableView1.DataController.DataSet.Delete();
                      dm.insert6(dm.PROC_SAN_LISTAUSLUGI, 'PREGLED_ID', Null, Null, Null,Null, Null,dm.tblPreglediID.Value, Null, Null, Null,Null, Null);
                      dm.tblPregledi.Refresh;
                   end
                else ShowMessage('��������� �� ������������� ������ � ��������. �� � ��������� ������ �� �������� !');
              end
            else ShowMessage('����� ������� ����� �� ������������� �����. �� � ��������� ������ � �������� �� ������ !');
          end
       else ShowMessage('�������� �� �� ��������� ������������� ����� ���弝� ��� ������� !!!')
     end;
end;

procedure TfrmPriem.actDIzvestuvanjeBacilonositelstvoPrekinExecute(
  Sender: TObject);
begin
    dmRes.Spremi('SAN',14);
    dmRes.frxReport1.DesignReport();

end;

procedure TfrmPriem.actFindExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.FindPanel.DisplayMode:=fpdmAlways;
end;

procedure TfrmPriem.actMailZaBacilonositelstvoExecute(Sender: TObject);
begin
   tag_mail:=1;
   actPratiMail.Execute;
end;

procedure TfrmPriem.actMailZaPrekinNaBacilonositelstvoExecute(Sender: TObject);
begin
    tag_mail:=2;
    actPratiMail.Execute;
end;

procedure TfrmPriem.actOK_PIzvestuvanjeBacilonositelstvoPrekinExecute(
  Sender: TObject);
begin
    if (Validacija(PanelIzborPartner) = false) then
       begin
          PanelIzborPartner.Visible:=false;
          dxRibbon1.Enabled:=True;
          Panel1.Enabled:=True;
          Panel2.Enabled:=True;
          try
            dmRes.Spremi('SAN',14);
            dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
            dmKon.tblSqlReport.Open;

            dm.qGetParamPartner.Close;
            dm.qGetParamPartner.ParamByName('p').Value:=I_PARTNER.EditValue;
            dm.qGetParamPartner.ParamByName('tp').Value:=I_TIP_PARTNER.EditValue;
            dm.qGetParamPartner.ExecQuery;

            if dm.qGetParamPartner.FldByName['partner_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv', QuotedStr(dm.qGetParamPartner.FldByName['partner_naziv'].Value));
            if dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv_skraten', QuotedStr(dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value));
            if dm.qGetParamPartner.FldByName['mesto_naziv'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_mesto', QuotedStr(dm.qGetParamPartner.FldByName['mesto_naziv'].Value));
            if dm.qGetParamPartner.FldByName['adresa'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_adresa', QuotedStr(dm.qGetParamPartner.FldByName['adresa'].Value));
            if dm.qGetParamPartner.FldByName['fax'].Value <> Null then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_fax', QuotedStr(dm.qGetParamPartner.FldByName['fax'].Value));
            if LekarNaziv.Text <> '' then
               dmRes.frxReport1.Variables.AddVariable('VAR', 'lekar', QuotedStr(LekarNaziv.Text));

            dmRes.frxReport1.ShowReport();
          except
            ShowMessage('�� ���� �� �� ������� ���������!');
          end
       end;
end;

procedure TfrmPriem.actPratiMailExecute(Sender: TObject);
var i,status:integer;  pdfStream:TFileStream;
    RecipientEmail,attachment_file, SubjectEmail, BodyText,naziv_fajl:string;
    memStream: TMemoryStream;
    re : integer;
    MemoryStreams: TArray<TMemoryStream>;
    FileNames: TArray<string>;
begin
   // ---- ���������� �� �����
    dmMat.tblMailSetup.Close;
    dmMat.tblMailSetup.Open;

    // ��� user-�� ��� �������� ���. ������� �� SYS_USER �������� ���� �� ��� ����� ���� �� �������
    if (dmKon.UserRE <> -999) then
      re := dmKon.UserRE
    else
      re := dmKon.re;

    if (dmMat.tblMailSetup.Locate('RE', re, [])) then
      begin
        dmRes.SSLHandler.MaxLineAction := maException;
        dmRes.SSLHandler.SSLOptions.Method := sslvTLSv1_2;
        dmRes.SSLHandler.SSLOptions.Mode := sslmUnassigned;
        dmRes.SSLHandler.SSLOptions.VerifyMode := [];
        dmRes.SSLHandler.SSLOptions.VerifyDepth := 0;
        dmRes.SMTP.IOHandler := dmRes.SSLHandler;
        dmRes.SMTP.UseTLS := utUseExplicitTLS;
        dmRes.SMTP.Host := dmMat.tblMailSetupSMTP_HOST.Value;
        dmRes.SMTP.Port := dmMat.tblMailSetupSMTP_PORT.Value;
        dmRes.SMTP.Username := dmMat.tblMailSetupLOGIN.Value;
        dmRes.SMTP.Password := dmMat.tblMailSetupLOGIN_PASSWORD.Value;
      end
    else
      begin
         ShowMessage('������! ���� email ���������� �� �������/���.�������');
         abort
      end;

    //-------------------------------------------------------------------------------


    // ���������� �� �������� �� �������

     dm.qGetParamPartner.Close;
     dm.qGetParamPartner.ParamByName('p').Value:=I_PARTNER.EditValue;
     dm.qGetParamPartner.ParamByName('tp').Value:=I_TIP_PARTNER.EditValue;
     dm.qGetParamPartner.ExecQuery;

     //-------------------------------------------------------------------------------
     if not dm.qGetParamPartner.FldByName['MAIL'].IsNull then
        begin
          try

            // Define the number of attachments
            SetLength(MemoryStreams, 2);
            SetLength(FileNames, 2);

              if (tag_mail = 1) then
                 dmRes.Spremi('SAN',12)
              else
                 dmRes.Spremi('SAN',14);
              dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
              dmKon.tblSqlReport.Open;

              if dm.qGetParamPartner.FldByName['partner_naziv'].Value <> Null then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv', QuotedStr(dm.qGetParamPartner.FldByName['partner_naziv'].Value));
              if dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value <> Null then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_naziv_skraten', QuotedStr(dm.qGetParamPartner.FldByName['skraten_partner_naziv'].Value));
              if dm.qGetParamPartner.FldByName['mesto_naziv'].Value <> Null then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_mesto', QuotedStr(dm.qGetParamPartner.FldByName['mesto_naziv'].Value));
              if dm.qGetParamPartner.FldByName['adresa'].Value <> Null then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_adresa', QuotedStr(dm.qGetParamPartner.FldByName['adresa'].Value));
              if dm.qGetParamPartner.FldByName['fax'].Value <> Null then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'do_firma_fax', QuotedStr(dm.qGetParamPartner.FldByName['fax'].Value));
              if LekarNaziv.Text <> '' then
                 dmRes.frxReport1.Variables.AddVariable('VAR', 'lekar', QuotedStr(LekarNaziv.Text));
           except
              ShowMessage('�� ���� �� �� ������� ���������!');
           end ;

           try
              memStream := TMemoryStream.Create;
              dmRes.frxPDFExport1.Stream := memStream;
              memStream.Position := 0;
              dmRes.frxReport1.PrepareReport(True);
              dmRes.frxReport1.Export(dmRes.frxPDFExport1);

              if (memStream <> nil) then
              begin
                  naziv_fajl:='Rezultati_B_'+ IntToStr(dm.tblPreglediBROJ.Value)+'_'+IntToStr(dm.tblPreglediGODINA.Value)+'.pdf';
                  if (dmKon.email_api_active) then
                  begin
                    MemoryStreams[0] := TMemoryStream.Create;

                    memStream.Position := 0; // Reset position for reading
                    MemoryStreams[0].CopyFrom(memStream, memStream.Size);
                    MemoryStreams[0].Position := 0; // Reset position for reading

                    FileNames[0] := naziv_fajl;
                  end
                  else
                  begin
                    AttachmentMem := TIdAttachmentMemory.Create(dmRes.EmailMessage.MessageParts);
                    AttachmentMem.LoadFromStream(memStream);

                    AttachmentMem.ContentType := 'application/pdf';
                    AttachmentMem.FileName := naziv_fajl;
                  end;
              end
              else
              begin
                MemoryStreams[0].CopyFrom(memStream, memStream.Size);
                MemoryStreams[0].Position := 0; // Reset position for reading

                FileNames[0] := '';
              end;
           except on E:Exception do
                 ShowMessage('������: ' + E.Message);
           end;


           try
              dmRes.Spremi('SAN',1);
              dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
              dmKon.tblSqlReport.Open;
              dm.tblRezultatAnaliza.Close;
              dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=dm.tblPreglediID.Value;
              dm.tblRezultatAnaliza.Open;
              dm.tblFrxAntibiogram.Close;
              dm.tblFrxAntibiogram.Open;
              dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(dm.tblPreglediPACIENT_NAZIV.Value));
              dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(dm.tblPreglediBROJ.Value)));
           except
              ShowMessage('�� ���� �� �� ������� ���������!');
           end ;

           try
              memStream := TMemoryStream.Create;
              dmRes.frxPDFExport1.Stream := memStream;
              memStream.Position := 0;
              dmRes.frxReport1.PrepareReport(True);
              dmRes.frxReport1.Export(dmRes.frxPDFExport1);

              if (memStream <> nil) then
                 begin
                    naziv_fajl:='Rezultati_'+ IntToStr(dm.tblPreglediBROJ.Value)+'_'+IntToStr(dm.tblPreglediGODINA.Value)+'.pdf';
                    if (dmKon.email_api_active) then
                    begin
                      MemoryStreams[1] := TMemoryStream.Create;

                      memStream.Position := 0; // Reset position for reading
                      MemoryStreams[1].CopyFrom(memStream, memStream.Size);
                      MemoryStreams[1].Position := 0; // Reset position for reading

                      FileNames[1] := naziv_fajl;
                    end
                    else
                    begin
                      AttachmentMem := TIdAttachmentMemory.Create(dmRes.EmailMessage.MessageParts);
                      AttachmentMem.LoadFromStream(memStream);

                      AttachmentMem.ContentType := 'application/pdf';
                      AttachmentMem.FileName := naziv_fajl;
                    end;
                 end
                 else
                  begin
                    MemoryStreams[1].CopyFrom(memStream, memStream.Size);
                    MemoryStreams[1].Position := 0; // Reset position for reading

                    FileNames[1] := '';
                  end;
           except on E:Exception do
                   ShowMessage('������: ' + E.Message);
           end;;


          RecipientEmail:=dm.qGetParamPartner.FldByName['mail'].Value;

          dmRes.EmailMessage.From.Address := dmMat.tblMailSetupFROM_MAIL.Value;
          dmRes.EmailMessage.CharSet := 'utf-8';
          dmRes.EmailMessage.AttachmentEncoding := 'UUE';
          dmRes.EmailMessage.Recipients.EMailAddresses :=RecipientEmail;
          dmRes.EmailMessage.Subject := '��������� �� ��������� ��������';
          dmRes.EmailMessage.Body.Text :=  UTF8Encode('����������'+sLineBreak+'�� ������ �� ��������� �� ��������� �������� �� ������� '+dm.tblPreglediPACIENT_NAZIV.Value+'  �� ��� �� ����� '+ IntToStr(dm.tblPreglediBROJ.Value)+'_'+IntToStr(dm.tblPreglediGODINA.Value)+ sLineBreak  + sLineBreak + dmKon.firma_naziv + sLineBreak + dmKon.firma_adresa + sLineBreak + dmKon.firma_mesto);
          dmRes.EmailMessage.BccList.Clear;

          try
              try
                if (dmKon.email_api_active) then
                begin
                  dmRes.SendWebApiEmailMultipleAttachmentFiles(RecipientEmail, dmRes.EmailMessage.Subject, dmRes.EmailMessage.Body.Text, nil, MemoryStreams, FileNames)
                end
                else
                begin
                 dmRes.SMTP.Connect;
                 dmRes.SMTP.Authenticate;
                 dmRes.SMTP.Send(dmRes.EmailMessage);

                 ShowMessage('�������� � ������� ������� �� : ' + RecipientEmail);
                end;
                dmRes.EmailMessage.Clear;

                 dm.tblPregledi.Edit;
                 if (tag_mail = 1) then
                    dm.tblPreglediPRATI_MAIL_B.Value:=1
                 else
                    dm.tblPreglediPRATI_MAIL_PB.Value:=1;
                 dm.tblPregledi.Post;


              except on E:Exception do
                 ShowMessage('������ dmRes.SMTP.Send: ' + E.Message);
              end;
          finally
             FreeAndNil(MemStream);
             MemoryStreams[0].Free;
             MemoryStreams[1].Free;
             dmRes.frxPDFExport1.Stream := nil;
             dmRes.SMTP.Disconnect;
          end;
        end
     else  ShowMessage('������ ������������ eMail ������ �� ��������� ������� !!!');

end;

procedure TfrmPriem.actPratiRezMailPartnerExecute(Sender: TObject);
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)) then
       if (cxGrid1DBTableView1.DataController.RecordCount <> 0)then
           begin
              PanelIzborPartner.Visible:=true;
              PanelIzborPartner.Left:=180;
              PanelIzborPartner.Top:=230;
              cxGroupBoxIzberiPartner.Left:=0;
              cxGroupBoxIzberiPartner.Top:=0;
              dxRibbon1.Enabled:=false;
              Panel1.Enabled:=False;
              Panel2.Enabled:=False;
              I_TIP_PARTNER.SetFocus;
              I_TIP_PARTNER.EditValue:=dm.tblPreglediTP.Value;
              I_PARTNER.EditValue:=dm.tblPreglediP.Value;
              ZemiImeDvoenKluc_1(I_TIP_PARTNER,I_PARTNER,I_PARTNER_NAZIV);
              LekarNaziv.EditValue:=dm.tblPreglediLEKAR.Value;

              btnaOK_PIzvestuvanjeBacilonositelstvo.Visible:=False;
              btnaOK_PIzvestuvanjeBacilonositelstvoPrekin.Visible:=false;
              btn_MailBacilonositelstvoPrekin.Visible:=True;
              btn_MailBacilonositelstvo.Visible:=True;
           end
       else ShowMessage('�������� �����/�������')
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');;
end;

procedure TfrmPriem.aDetalenPeriodicenExecute(Sender: TObject);
begin
    PanelIzborDatum.Visible:=True;
    fiskalDo.Clear;
    fiskalOd.Clear;
    pom_periodicen:=1;
end;

procedure TfrmPriem.aDetalenPeriodicenIzvestajExecute(Sender: TObject);
var datum_od,datum_do,pos :string ;
begin

end;

procedure TfrmPriem.aDizajnPoupUpExecute(Sender: TObject);
begin
   PopupMenu2.Popup(500, 500);
end;

procedure TfrmPriem.aDizajnRezultatiExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',1);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPriem.aDIzvestuvanjeBacilonositelstvoExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',12);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPriem.aDnevenPregledSoNuliranjeExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �������', '�� ��� ������� �� �� ������ �������� �� ���������.'#13#10'���� ��������� �� ��������� �� ��������� �� ��������� �������.'#13#10'�������� �������� ������� ?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort;

      // if messagedlg ('�� ��� ������� �� �� ������ �������� �� ���������.'#13#10'���� ��������� �� ��������� �� ��������� �� ��������� �������.'#13#10'�������� �������� ������� ?',
                   // mtConfirmation,[mbyes,mbno],0) =mryes then
    begin
       fpFinansiski
    end;
end;

procedure TfrmPriem.aPecatiFiskalnaSmetkaExecute(Sender: TObject);
begin
if UpperCase(dm.tblPreglediNACINPLAKANJE.AsString) = '�� ������' then
   begin
     if {(dm.tblPreglediNAPLATENO.Value>0) and }(dm.tblPreglediFISKALNA.Value = dm.tblPreglediSTORNA.Value)  then
        begin
          strLAB:=dm.tblPreglediPAKET_ID.AsString+':LABOR.USLUGI' ;
          strCENA:=dm.tblPreglediIZNOS_VKUPNO.AsString;
          if dm.PecatiFiskalnaDll(strCENA,StrLAB,0,1,1)=false
          then ShowMessage('������ �� �� �������� �������� ������ !');
        end
     else ShowMessage('������ �� �� �������� �������� ������!!!');
   end
 else ShowMessage('������ �� �� �������� �������� ������ �� ������� �� ������� !!!');
end;

procedure TfrmPriem.aPecatiPeriodicenExecute(Sender: TObject);
begin
     if pom_periodicen = 1 then
        begin
           fpDetalenPoDatum(DateToStr(fiskalOd.Date),DateToStr(fiskalDo.Date));
           PanelIzborDatum.Visible:=False;
        end
     else if pom_periodicen = 2 then
        begin
           fpSkratenPoDatum(DateToStr(fiskalOd.Date),DateToStr(fiskalDo.Date));
           PanelIzborDatum.Visible:=False;
        end
end;

procedure TfrmPriem.aPecatiTabela2Execute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + dm.tblPreglediPACIENT_NAZIV.Value+' - '+dm.tblPreglediEMBG.Value+' - '+intToStr(dm.tblPreglediBROJ.Value));

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmPriem.aDodadiSanitarnaKniskaExecute(Sender: TObject);
begin
if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    begin
      if dm.tblPreglediBRFAKTURI.Value = 0 then
         begin
            frmUslugi:=TfrmUslugi.Create(self,true);
            frmUslugi.Tag:=2;
            frmUslugi.ShowModal();
            frmUslugi.Free;
         end
      else ShowMessage('�������� �� �� ��������� ������������� ����� ���弝� ��� ������� !!!')
    end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPriem.aDodadiUslugaExecute(Sender: TObject);
begin
if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    begin
      if dm.tblPreglediBRFAKTURI.Value = 0 then
         begin
           if ((dm.tblPreglediPAKET_ID.IsNull)or (dm.tblPreglediKONTROLEN_PREGLED.Value = 1)) then
              begin
                frmUslugi:=TfrmUslugi.Create(self,true);
                frmUslugi.Tag:=1;
                frmUslugi.ShowModal();
                frmUslugi.Free;
              end
           else ShowMessage('����� ������� ����� �� ������������� �����. �� � ��������� ������ � �������� �� ������ !');
         end
      else ShowMessage('�������� �� �� ��������� ������������� ����� ���弝� ��� ������� !!!')
    end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPriem.aDPotvrdaExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',10);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPriem.aDUpatnicaExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',11);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPriem.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPriem.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPriem.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmPriem.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmPriem.ZemiImeDvoenKluc_1(tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

end.
