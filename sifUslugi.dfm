inherited frmUslugi: TfrmUslugi
  Caption = #1059#1089#1083#1091#1075#1080
  ClientHeight = 571
  ClientWidth = 850
  ExplicitWidth = 866
  ExplicitHeight = 610
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 850
    Height = 224
    ExplicitWidth = 850
    ExplicitHeight = 224
    inherited cxGrid1: TcxGrid
      Width = 846
      Height = 220
      ExplicitWidth = 846
      ExplicitHeight = 220
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsUslugi
        OptionsSelection.MultiSelect = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 69
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 166
        end
        object cxGrid1DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Width = 71
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Width = 73
        end
        object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Width = 238
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 1
            end
            item
              ImageIndex = 1
              Value = 0
            end>
          Width = 93
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 162
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 212
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 202
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 242
        end
        object cxGrid1DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1059#1089#1083#1091#1075#1072
              ImageIndex = 2
              Value = 0
            end
            item
              Description = #1052#1072#1085#1080#1087#1091#1083#1072#1090#1080#1074#1077#1085' '#1090#1088#1086#1096#1086#1082
              ImageIndex = 4
              Value = 1
            end>
          Width = 107
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 389
    Width = 850
    Height = 159
    ExplicitTop = 389
    ExplicitWidth = 850
    ExplicitHeight = 159
    inherited Label1: TLabel
      Left = 57
      Top = 14
      ExplicitLeft = 57
      ExplicitTop = 14
    end
    object Label2: TLabel [1]
      Left = 53
      Top = 41
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 53
      Top = 95
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1062#1077#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [3]
      Left = 13
      Top = 68
      Width = 90
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 109
      Top = 11
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsUslugi
      ExplicitLeft = 109
      ExplicitTop = 11
    end
    inherited OtkaziButton: TcxButton
      Left = 759
      Top = 119
      TabOrder = 6
      ExplicitLeft = 759
      ExplicitTop = 119
    end
    inherited ZapisiButton: TcxButton
      Left = 678
      Top = 119
      TabOrder = 5
      ExplicitLeft = 678
      ExplicitTop = 119
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 109
      Top = 38
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 476
    end
    object CENA: TcxDBTextEdit
      Tag = 1
      Left = 109
      Top = 92
      Hint = #1062#1077#1085#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      BeepOnEnter = False
      DataBinding.DataField = 'CENA'
      DataBinding.DataSource = dm.dsUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object RE: TcxDBTextEdit
      Tag = 1
      Left = 109
      Top = 65
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1083#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072
      BeepOnEnter = False
      DataBinding.DataField = 'RE'
      DataBinding.DataSource = dm.dsUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object RE_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 160
      Top = 65
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072
      BeepOnEnter = False
      DataBinding.DataField = 'RE'
      DataBinding.DataSource = dm.dsUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsRE
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 425
    end
    object cxDBCheckBox1: TcxDBCheckBox
      Left = 195
      Top = 11
      Caption = #1040#1082#1090#1080#1074#1085#1072
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = dm.dsUslugi
      Properties.Alignment = taLeftJustify
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 7
      Transparent = True
      Width = 67
    end
    object cxDBCheckBox2: TcxDBCheckBox
      Left = 271
      Top = 11
      Caption = #1052#1072#1085#1080#1087#1091#1083#1072#1090#1080#1074#1077#1085' '#1090#1088#1086#1096#1086#1082
      DataBinding.DataField = 'TIP'
      DataBinding.DataSource = dm.dsUslugi
      Properties.Alignment = taLeftJustify
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 8
      Transparent = True
      Width = 145
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 850
    ExplicitWidth = 850
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 548
    Width = 850
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080' '
        Width = 320
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 548
    ExplicitWidth = 850
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 350
    Width = 850
    Height = 39
    Align = alBottom
    TabOrder = 4
    Visible = False
    DesignSize = (
      850
      39)
    object Label4: TLabel
      Left = 231
      Top = 15
      Width = 274
      Height = 13
      AutoSize = False
      Caption = '* '#1052#1086#1078#1077' '#1076#1072' '#1089#1077' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1072#1090' '#1087#1086#1074#1077#1116#1077' '#1089#1090#1072#1074#1082#1080' '#1086#1076#1077#1076#1085#1072#1096
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxButton1: TcxButton
      Left = 13
      Top = 8
      Width = 212
      Height = 25
      Action = aPrevzemiuslugi
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 0
    end
  end
  inherited dxBarManager1: TdxBarManager
    Top = 224
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 290
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 610
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1084#1080
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aMikroorganizmi
      Category = 0
      ScreenTip = tipDodadiMikro
    end
  end
  inherited ActionList1: TActionList
    object aPrevzemiuslugi: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1075#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1090#1077' '#1091#1089#1083#1091#1075#1080
      ImageIndex = 51
      OnExecute = aPrevzemiuslugiExecute
    end
    object aMikroorganizmi: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aMikroorganizmiExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41330.381637546300000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 280
    Top = 232
    PixelsPerInch = 96
    object tipDodadiMikro: TdxScreenTip
      Header.Text = #1044#1086#1076#1072#1076#1080' '#1052#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1072#1084
      Description.Text = 
        #1044#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1084#1080#1082#1088#1086#1086#1088#1075#1072#1085#1080#1079#1084#1080' '#1082#1086#1080' '#1084#1086#1078#1077' '#1076#1072' '#1089#1077' '#1087#1086#1112#1072#1074#1072#1090' '#1087#1088#1080' '#1072#1085#1072#1083#1080#1079#1072' '#1085#1072 +
        ' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1091#1089#1083#1091#1075#1072
    end
  end
end
