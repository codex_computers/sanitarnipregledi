inherited frmPacienti: TfrmPacienti
  ActiveControl = nil
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080
  ClientHeight = 741
  ClientWidth = 1045
  Position = poDesktopCenter
  ExplicitTop = -197
  ExplicitWidth = 1061
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1045
    Height = 259
    ExplicitWidth = 1045
    ExplicitHeight = 259
    inherited cxGrid1: TcxGrid
      Width = 1041
      Height = 255
      ExplicitWidth = 1041
      ExplicitHeight = 255
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsPacienti
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.FooterSummaryItems = <
          item
            Column = cxGrid1DBTableView1BROJ_KARTON
          end
          item
            Kind = skCount
            Column = cxGrid1DBTableView1EMBG
          end>
        Filtering.ColumnFilteredItemsList = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ_KARTON: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_KARTON'
          Width = 93
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Width = 110
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Width = 109
        end
        object cxGrid1DBTableView1TATKOVO: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO'
          Width = 128
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 102
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 156
        end
        object cxGrid1DBTableView1DATUM_RAGJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_RAGJANJE'
          Width = 113
        end
        object cxGrid1DBTableView1MESTO_RAGJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_RAGJANJE'
          Visible = False
          Width = 93
        end
        object cxGrid1DBTableView1MESTORAGJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'MESTORAGJANJE'
          Width = 160
        end
        object cxGrid1DBTableView1MESTO_ZIVEENJE: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_ZIVEENJE'
          Visible = False
          Width = 101
        end
        object cxGrid1DBTableView1MESTOZIVEENJE: TcxGridDBColumn
          DataBinding.FieldName = 'MESTOZIVEENJE'
          Width = 129
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 161
        end
        object cxGrid1DBTableView1TEL: TcxGridDBColumn
          DataBinding.FieldName = 'TEL'
          Width = 107
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 85
        end
        object cxGrid1DBTableView1R_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'R_MESTO'
          Width = 79
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 155
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
          Width = 84
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Width = 90
        end
        object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNERNAZIV'
          Width = 196
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 166
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 211
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 201
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 232
        end
        object cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn
          DataBinding.FieldName = 'KATEGORIJA_RM'
          Visible = False
          Width = 206
        end
        object cxGrid1DBTableView1KATEGORIJARMMAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'KATEGORIJARMMAZIV'
          Width = 221
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 391
    Width = 1045
    Height = 327
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitTop = 391
    ExplicitWidth = 1045
    ExplicitHeight = 327
    inherited Label1: TLabel
      Left = 489
      Top = 70
      ExplicitLeft = 489
      ExplicitTop = 70
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 465
      Top = 89
      TabStop = False
      DataBinding.DataField = 'ID'
      TabOrder = 2
      ExplicitLeft = 465
      ExplicitTop = 89
    end
    inherited OtkaziButton: TcxButton
      Left = 917
      Top = 279
      TabOrder = 5
      ExplicitLeft = 917
      ExplicitTop = 279
    end
    inherited ZapisiButton: TcxButton
      Left = 836
      Top = 279
      TabOrder = 4
      ExplicitLeft = 836
      ExplicitTop = 279
    end
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 6
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1072#1094#1080#1077#1085#1090
      TabOrder = 0
      Height = 227
      Width = 377
      object Label3: TLabel
        Left = 21
        Top = 28
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 21
        Top = 55
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1088#1090#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 80
        Top = 82
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 64
        Top = 109
        Width = 57
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 40
        Top = 136
        Width = 81
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 2
        Top = 163
        Width = 119
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 2
        Top = 190
        Width = 119
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1088#1072#1107#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EMBG: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 25
        Hint = #1045#1076#1080#1085#1089#1090#1074#1077#1085' '#1084#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMBG'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Properties.OnValidate = EMBGPropertiesValidate
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 226
      end
      object BROJ_KARTON: TcxDBTextEdit
        Left = 127
        Top = 52
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1088#1090#1086#1085' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_KARTON'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 106
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 79
        Hint = #1048#1084#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 226
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 106
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 226
      end
      object TATKOVO: TcxDBTextEdit
        Left = 127
        Top = 133
        Hint = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 226
      end
      object DATUM_RAGJANJE: TcxDBDateEdit
        Tag = 1
        Left = 127
        Top = 160
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_RAGJANJE'
        DataBinding.DataSource = dm.dsPacienti
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 106
      end
      object MESTO_RAGJANJE: TcxDBTextEdit
        Left = 127
        Top = 187
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1088#1072#1107#1072#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_RAGJANJE'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 42
      end
      object MESTO_RAGJANJE_NAZIV: TcxDBLookupComboBox
        Left = 168
        Top = 187
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1088#1072#1107#1072#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_RAGJANJE'
        DataBinding.DataSource = dm.dsPacienti
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'ID'
          end
          item
            Width = 300
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 185
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 399
      Top = 6
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      TabOrder = 1
      Height = 143
      Width = 610
      object Label9: TLabel
        Left = 27
        Top = 28
        Width = 119
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 17
        Top = 56
        Width = 129
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 17
        Top = 83
        Width = 129
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1077#1083#1077#1092#1086#1085#1089#1082#1080' '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 17
        Top = 110
        Width = 129
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MESTO_ZIVEENJE: TcxDBTextEdit
        Left = 152
        Top = 25
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_ZIVEENJE'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 42
      end
      object MESTO_ZIVEENJE_NAZIV: TcxDBLookupComboBox
        Left = 193
        Top = 25
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_ZIVEENJE'
        DataBinding.DataSource = dm.dsPacienti
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'ID'
          end
          item
            Width = 300
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 400
      end
      object ADRESA: TcxDBTextEdit
        Left = 152
        Top = 52
        Hint = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 441
      end
      object TEL: TcxDBTextEdit
        Left = 152
        Top = 79
        Hint = #1058#1077#1083#1077#1092#1086#1085#1089#1082#1080' '#1073#1088#1086#1112' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'TEL'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 441
      end
      object EMAIL: TcxDBTextEdit
        Left = 152
        Top = 107
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 441
      end
    end
    object cxGroupBox3: TcxGroupBox
      Left = 399
      Top = 155
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      TabOrder = 3
      DesignSize = (
        610
        118)
      Height = 118
      Width = 610
      object Label13: TLabel
        Left = 40
        Top = 28
        Width = 105
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 55
        Top = 57
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1072#1088#1090#1085#1077#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 28
        Top = 84
        Width = 118
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1056#1052' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object R_MESTO: TcxDBTextEdit
        Tag = 1
        Left = 151
        Top = 25
        Hint = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'R_MESTO'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object R_MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 200
        Top = 25
        Hint = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'R_MESTO'
        DataBinding.DataSource = dm.dsPacienti
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'ID'
          end
          item
            Width = 300
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsRabotnoMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 393
      end
      object TIP_PARTNER: TcxDBTextEdit
        Tag = 1
        Left = 151
        Top = 54
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1090#1080#1087
        BeepOnEnter = False
        DataBinding.DataField = 'TIP_PARTNER'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 40
      end
      object PARTNER: TcxDBTextEdit
        Tag = 1
        Left = 190
        Top = 54
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'PARTNER'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 40
      end
      object PARTNERNAZIV: TcxExtLookupComboBox
        Left = 229
        Top = 54
        Hint = #1060#1080#1088#1084#1072' '#1074#1086' '#1082#1086#1112#1072' '#1088#1072#1073#1086#1090#1080' '#1087#1072#1094#1080#1077#1085#1090#1086#1090' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight]
        Properties.DropDownSizeable = True
        Properties.OnEditValueChanged = PARTNERNAZIVPropertiesEditValueChanged
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 364
      end
      object KATEGORIJA_RM: TcxDBTextEdit
        Tag = 1
        Left = 152
        Top = 81
        Hint = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'KATEGORIJA_RM'
        DataBinding.DataSource = dm.dsPacienti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 49
      end
      object KATEGORIJA_RM_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 200
        Top = 81
        Hint = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'KATEGORIJA_RM'
        DataBinding.DataSource = dm.dsPacienti
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'ID'
          end
          item
            Width = 300
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsKategorijaRM
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 393
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1045
    ExplicitWidth = 1045
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 1045
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Ctrl+F - '#1055#1088#1077#1073#1072#1088#1072#1112', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 718
    ExplicitWidth = 1045
  end
  object cxSplitter1: TcxSplitter [4]
    Left = 0
    Top = 385
    Width = 1045
    Height = 6
    AlignSplitter = salBottom
    MinSize = 257
    Control = dPanel
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 256
  end
  inherited PopupMenu1: TPopupMenu
    Left = 376
    Top = 288
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 336
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1055#1072#1094#1080#1077#1085#1090
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 256
    object actFind: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ShortCut = 16454
      OnExecute = actFindExecute
    end
    object actget_upati_datumExecute: TAction
      Caption = 'actget_upati_datumExecute'
      OnExecute = actget_upati_datumExecuteExecute
    end
    object actget_upati_datum_2Execute: TAction
      Caption = 'actget_upati_datum_2Execute'
    end
    object actResponseActiveDate: TAction
      Caption = 'actResponse'
      OnExecute = actResponseActiveDateExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.Orientation = poPortrait
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 41330.541868252320000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 264
    Top = 88
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 256
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 608
    Top = 262
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsPartner
      DataController.KeyFieldNames = 'PAR_TIP;PAR_SIF'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1DBColumn: TcxGridDBColumn
        DataBinding.FieldName = #1058#1080#1087
      end
      object cxGridViewRepository1DBTableView1DBColumn1: TcxGridDBColumn
        DataBinding.FieldName = #1064#1080#1092#1088#1072
        Width = 51
      end
      object cxGridViewRepository1DBTableView1DBColumn2: TcxGridDBColumn
        DataBinding.FieldName = #1053#1072#1079#1080#1074
        Width = 263
      end
      object cxGridViewRepository1DBTableView1DBColumn3: TcxGridDBColumn
        DataBinding.FieldName = #1052#1077#1089#1090#1086
        Width = 90
      end
      object cxGridViewRepository1DBTableView1DBColumn4: TcxGridDBColumn
        DataBinding.FieldName = #1040#1076#1088#1077#1089#1072
        Width = 96
      end
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 472
    Top = 262
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    HandleRedirects = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.CharSet = 'win1251'
    Request.ContentEncoding = 'win1251'
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.ContentType = 'application/xml'
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.AcceptCharSet = 'win1251'
    Request.AcceptEncoding = 'win1251'
    Request.BasicAuthentication = True
    Request.Password = 'cxhadmin1@'
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Username = 'cxhealth'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoInProcessAuth]
    Left = 640
    Top = 174
  end
  object xml: TXMLDocument
    Left = 600
    Top = 166
    DOMVendorDesc = 'MSXML'
  end
end
