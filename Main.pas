unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxCheckBox, cxCalendar, cxRadioGroup, dxSkinOffice2013White, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxSkinscxPCPainter,
  System.ImageList, cxImageList;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    aLogout: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aSifUslugi: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    aSifPaketi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aSifPaketUsluga: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aSifRabotnoMesto: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    aSifPartner: TAction;
    aSifMesto: TAction;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton14: TdxBarLargeButton;
    aPacienti: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    aPregledi: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    aSifTipUplata: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    aSifMikroorganizmi: TAction;
    aSifMikroUsluga: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    aAnalizi: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aSifKategorijaRM: TAction;
    dxRibbon1TabSifrarnici: TdxRibbonTab;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    aSifLekovi: TAction;
    dxBarLargeButton32: TdxBarLargeButton;
    aSifAntibiogram: TAction;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    aSifDefNaodi: TAction;
    dxBarLargeButton35: TdxBarLargeButton;
    aSifLekari: TAction;
    dxBarLargeButton36: TdxBarLargeButton;
    �SifRE: TAction;
    dxBarLargeButton37: TdxBarLargeButton;
    aPopust: TAction;
    dxBarManager1Bar2: TdxBar;
    aFakturiranje: TAction;
    dxBarLargeButton38: TdxBarLargeButton;
    dxRibbon1TabPreglediReporter: TdxRibbonTab;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton39: TdxBarLargeButton;
    aPominatRok: TAction;
    dxBarLargeButton40: TdxBarLargeButton;
    aPregledajPecati: TAction;
    dxBarLargeButton41: TdxBarLargeButton;
    aFinansiskaKartica: TAction;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton42: TdxBarLargeButton;
    dxBarLargeButton43: TdxBarLargeButton;
    aListaPrimeniPacienti: TAction;
    dxBarLargeButton44: TdxBarLargeButton;
    actDogovoriCeni: TAction;
    dxBarLargeButton45: TdxBarLargeButton;
    aOsveziPartneri: TAction;
    dxBarLargeButton46: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton47: TdxBarLargeButton;
    actLoginMoeZdravje: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aLogoutExecute(Sender: TObject);
    procedure aSifUslugiExecute(Sender: TObject);
    procedure aSifPaketiExecute(Sender: TObject);
    procedure aSifPaketUslugaExecute(Sender: TObject);
    procedure aSifRabotnoMestoExecute(Sender: TObject);
    procedure aSifPartnerExecute(Sender: TObject);
    procedure aSifMestoExecute(Sender: TObject);
    procedure aPacientiExecute(Sender: TObject);
    procedure aPreglediExecute(Sender: TObject);
    procedure aSifTipUplataExecute(Sender: TObject);
    procedure aSifMikroorganizmiExecute(Sender: TObject);
    procedure aAnaliziExecute(Sender: TObject);
    procedure aSifKategorijaRMExecute(Sender: TObject);
    procedure aSifLekoviExecute(Sender: TObject);
    procedure aSifAntibiogramExecute(Sender: TObject);
    procedure aSifDefNaodiExecute(Sender: TObject);
    procedure aSifLekariExecute(Sender: TObject);
    procedure �SifREExecute(Sender: TObject);
    procedure aPopustExecute(Sender: TObject);
    procedure aFakturiranjeExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aPominatRokExecute(Sender: TObject);
    procedure aPregledajPecatiExecute(Sender: TObject);
    procedure aFinansiskaKarticaExecute(Sender: TObject);
    procedure aListaPrimeniPacientiExecute(Sender: TObject);
    procedure actDogovoriCeniExecute(Sender: TObject);
    procedure dxBarLargeButton46Click(Sender: TObject);
    procedure actLoginMoeZdravjeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, sifUslugi, MK, sifPaketi,
  sifPaketiUslugi, sifRabotnoMesto,
  Partner, Mesto, Pacienti, Priem, dmUnit,
  sifTipUplata, sifMikroorganizmi, sifMikroorganizmiUslugi, PreglediMikro,
  sifKategorijaRM, sifLekovi, sifAntibiogram, sifDefAntibiogram,
  sifDefNaodi, sifLekari, RabotniEdinici, sifPopust, Fakturiranje, PominatRok,
  IzvestaiGrupni, ListaPrimeniPacienti, DogovoriCeni, LoginMZ;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aFakturiranjeExecute(Sender: TObject);
begin
   frmFakturiranje:=TfrmFakturiranje.Create(self,false);
   frmFakturiranje.ShowModal();
   frmFakturiranje.Free;
end;

procedure TfrmMain.aFinansiskaKarticaExecute(Sender: TObject);
begin
    //
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aSifLekariExecute(Sender: TObject);
begin
   frmLekari:=TfrmLekari.Create(self,true);
   frmLekari.ShowModal();
   frmLekari.Free;
end;

procedure TfrmMain.aListaPrimeniPacientiExecute(Sender: TObject);
begin
   frmListaPrimeniPacienti:=TfrmListaPrimeniPacienti.Create(self,false);
   frmListaPrimeniPacienti.ShowModal();
   frmListaPrimeniPacienti.Free;
end;

procedure TfrmMain.actLoginMoeZdravjeExecute(Sender: TObject);
begin
   frmLoginMZ:=TfrmLoginMZ.Create(Application);
   frmLoginMZ.ShowModal();
   frmLoginMZ.Free;
end;

procedure TfrmMain.aLogoutExecute(Sender: TObject);
begin
  if (dmKon.Logout = false) then Close
  else
  begin
    SpremiForma(self);

    if dmKon.user = 'SYSDBA' then
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : �������������'
    else
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : ' + dmKon.imeprezime; // ������� �� ���������� ��������

    // Ovde otvori posebni datasetovi koi ne se otvaraat vo OtvoriTabeli()
    OtvoriTabeli();
  end;
end;

procedure TfrmMain.aSifMikroorganizmiExecute(Sender: TObject);
begin
   frmMikroorganizmi:=TfrmMikroorganizmi.Create(self,true);
   frmMikroorganizmi.ShowModal();
   frmMikroorganizmi.Free;
end;

procedure TfrmMain.aPacientiExecute(Sender: TObject);
begin
   frmPacienti:=TfrmPacienti.Create(self,true);
   frmPacienti.ShowModal();
   frmPacienti.Free;
end;

procedure TfrmMain.aPominatRokExecute(Sender: TObject);
begin
   frmPominatRok:=TfrmPominatRok.Create(self,true);
   frmPominatRok.ShowModal();
   frmPominatRok.Free;
end;

procedure TfrmMain.aPopustExecute(Sender: TObject);
begin
   frmPopust:=TfrmPopust.Create(self,true);
   frmPopust.ShowModal();
   frmPopust.Free;
end;

procedure TfrmMain.aPregledajPecatiExecute(Sender: TObject);
begin
   frmGrupniIzvestai:=TfrmGrupniIzvestai.Create(self,false);
   frmGrupniIzvestai.ShowModal();
   frmGrupniIzvestai.Free;
end;

procedure TfrmMain.aPreglediExecute(Sender: TObject);
begin
   frmPriem:=TfrmPriem.Create(self,false);
   frmPriem.ShowModal();
   frmPriem.Free;
end;

procedure TfrmMain.aAnaliziExecute(Sender: TObject);
begin
   frmPreglediMikro:=TfrmPreglediMikro.Create(self,false);
   frmPreglediMikro.ShowModal();
   frmPreglediMikro.Free;
end;

procedure TfrmMain.actDogovoriCeniExecute(Sender: TObject);
begin
   frmDogovoriCeni:=TfrmDogovoriCeni.Create(self,false);
   frmDogovoriCeni.ShowModal();
   frmDogovoriCeni.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSifAntibiogramExecute(Sender: TObject);
begin
   frmAntibiogramSifrarnik:=TfrmAntibiogramSifrarnik.Create(self,true);
   frmAntibiogramSifrarnik.ShowModal();
   frmAntibiogramSifrarnik.Free;
end;

procedure TfrmMain.aSifDefNaodiExecute(Sender: TObject);
begin
   frmDefNaodi:=TfrmDefNaodi.Create(self,true);
   frmDefNaodi.ShowModal();
   frmDefNaodi.Free;
end;

procedure TfrmMain.aSifKategorijaRMExecute(Sender: TObject);
begin
   frmKategorijaRM:=TfrmKategorijaRM.Create(self,true);
   frmKategorijaRM.ShowModal();
   frmKategorijaRM.Free;
end;

procedure TfrmMain.aSifLekoviExecute(Sender: TObject);
begin
   frmLekovi:=TfrmLekovi.Create(self,true);
   frmLekovi.ShowModal();
   frmLekovi.Free;
end;

procedure TfrmMain.aSifMestoExecute(Sender: TObject);
begin
   frmMesto:=TfrmMesto.Create(self,true);
   frmMesto.ShowModal();
   frmMesto.Free;
end;

procedure TfrmMain.aSifPaketiExecute(Sender: TObject);
begin
   frmPaketi:=TfrmPaketi.Create(self,true);
   frmPaketi.ShowModal();
   frmPaketi.Free;
end;

procedure TfrmMain.aSifPaketUslugaExecute(Sender: TObject);
begin
   frmPaketiUslugi:=TfrmPaketiUslugi.Create(self,true);
   frmPaketiUslugi.ShowModal();
   frmPaketiUslugi.Free;
end;

procedure TfrmMain.aSifPartnerExecute(Sender: TObject);
begin
   frmPartner:=TfrmPartner.Create(self,true);
   frmPartner.ShowModal();
   frmPartner.Free;
end;

procedure TfrmMain.aSifRabotnoMestoExecute(Sender: TObject);
begin
   frmRabotnoMesto:=TfrmRabotnoMesto.Create(self,true);
   frmRabotnoMesto.ShowModal();
   frmRabotnoMesto.Free;
end;

procedure TfrmMain.aSifTipUplataExecute(Sender: TObject);
begin
   frmTipUplata:=TfrmTipUplata.Create(self,true);
   frmTipUplata.ShowModal();
   frmTipUplata.Free;
end;

procedure TfrmMain.aSifUslugiExecute(Sender: TObject);
begin
   frmUslugi:=TfrmUslugi.Create(self,true);
   frmUslugi.Tag:=0;
   frmUslugi.ShowModal();
   frmUslugi.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex ����� ��������
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.dxBarLargeButton46Click(Sender: TObject);
begin
     dm.tblAktivniPartneri.Close;
     dm.tblAktivniPartneri.Open;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (dmKon.reporter_dll) then
       begin
        dmRes.FreeDLL;
       end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  cxSetEureka('danica.stojkova@codex.mk');
 // cxSetEureka('danicastojkova@yahoo.com');
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
Var pom_token_codex:String;
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;

  if (dmKon.reporter_dll) then
  begin
    dmRes.ReporterInit;
    dmRes.GenerateReportsRibbon(dxRibbon1,dxRibbon1TabPreglediReporter,dxBarManager1);
  end;


  if dm.tblSetup.Locate('P2','SERVICE_OSIGURENICI',[])  then   //citam url za servisot za proverka na osigurenici
     dm.osiguruvanje:=dm.tblSetupV1.AsString
  else
     dm.osiguruvanje:='';

   if dm.tblSetup.Locate('P2','SERVICE_OSIGURENICI_OLD',[])  then   //citam url za servisot za proverka na osigurenici
     dm.osiguruvanje_old:=dm.tblSetupV1.AsString
  else
     dm.osiguruvanje_old:='';

  if dm.tblSetup.Locate('P2','CODEX',[])  then
     pom_token_codex:=dm.tblSetupV1.AsString
  else
     pom_token_codex:='';

  dm.token_codex:=dm.decrypt(pom_token_codex);

  if  dm.tblSetup.Locate('P2','SESSIONTOKEN_FORM',[])  then
    begin
      dm.session_token_form:=dm.tblSetupV1.Value;
      //dm.session_token:=dm.GetSessionToken('testkorisnik', 'testkorisnik123');
    end
  else
      dm.session_token_form:='';


  {if dm.tblSetup.Locate('P2','GET_DATUM_AKTIVACIJA',[])  then   //citam url za servisot za proverka na osigurenici
     dm.get_datum_aktivacija:=dm.tblSetupV1.AsString
  else
     dm.get_datum_aktivacija:='';
  }
end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���
  dmMat.tblMesto.Open;
  dmMat.tblDrzava.Open;
  dmMat.tblOpstina.Open;
  dmMat.tblRegioni.Open;
  dmMat.tblFzo.Open;
  dmMat.tblTipPartner.Open;
  dmMat.tblPartner.Open;
  dmmat.tblKontakt.Open;
  dmMat.tblRE.Open;
  dmMat.tblNurko.Open;

  dm.tblSetup.Open;

  dm.qSetUpRePregled.ExecQuery;
  dm.re_pregled:=dm.qSetUpRePregled.FldByName['v1'].Value;

    dm.tblPregledi.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPregledi.ParamByName('param').Value:=0;
    dm.tblPregledi.ParamByName('datum').Value:=Now;
    dm.tblPregledi.ParamByName('param_datum').Value:=1;
    if dm.re_pregled = '1' then
       dm.tblPregledi.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPregledi.ParamByName('re').Value:='%';
    dm.tblPregledi.Open;
    dm.tblPreglediStavki.Open;

    dm.tblPaketi.Open;
    dm.tblTipPlakanje.Open;

    dm.tblRabotnoMesto.Open;
    dm.tblKategorijaRM.Open;
    dm.tblPacienti.Open;

    dm.qSetUpReBroj.ExecQuery;
    dm.re_broj:=dm.qSetUpReBroj.FldByName['v1'].Value;

end;


procedure TfrmMain.�SifREExecute(Sender: TObject);
begin
   frmRE:=TfrmRE.Create(self,true);
   frmRE.Caption:='�����������';
   frmRE.ShowModal();
   frmRE.Free;
end;

//TODO


end.
