unit Fakturiranje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxPCdxBarPopupMenu, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu;

type
//  niza = Array[1..5] of Variant;

  TfrmFakturiranje = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxPageControl1: TcxPageControl;
    cxTabSheetNefakturirani: TcxTabSheet;
    cxTabSheetFakturi: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid1DBTableView1PREGLED_ID_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RMNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KRMNAZIV: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    cbGodina: TcxBarEditItem;
    cbBarGodina: TdxBarCombo;
    dxBarManager1BarNefakturirani: TdxBar;
    aFakturiraj: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    Selekcija: TcxGridDBColumn;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    cxGrid2DBTableView1PRIEM_ID: TcxGridDBColumn;
    cxGrid2DBTableView1FAKTURA_ID: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1BRFAKTURA: TcxGridDBColumn;
    cxGrid2DBTableView1PACIENT_ID: TcxGridDBColumn;
    cxGrid2DBTableView1P: TcxGridDBColumn;
    cxGrid2DBTableView1TP: TcxGridDBColumn;
    cxGrid2DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1EMBG: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    dxBarLargeButton19: TdxBarLargeButton;
    aBrisiFaktura: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aPecatiFaktura: TAction;
    aDizajnFaktura: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    cxGrid2DBTableView1POPUST: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS_POPUST: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    dxBarLargeButton24: TdxBarLargeButton;
    aGrupnoPecatenje: TAction;
    dxBarManager1BarFakturirani: TdxBar;
    dxBarManager1Bar1: TdxBar;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGrid1DBTableView1BROJ_DNEVNIK: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cbGodinaChange(Sender: TObject);
    procedure cbBarGodinaChange(Sender: TObject);
    procedure aFakturirajExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure SelekcijaPropertiesEditValueChanged(Sender: TObject);
    procedure aBrisiFakturaExecute(Sender: TObject);
    procedure aPecatiFakturaExecute(Sender: TObject);
    procedure aDizajnFakturaExecute(Sender: TObject);
    procedure aGrupnoPecatenjeExecute(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano1, sobrano2 : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmFakturiranje: TfrmFakturiranje;
  rData : TRepositoryData;
  pom_godina, vk_selekcija, fakturiranje_vlez: Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit,
  GrupnoPecatenjeFakturi;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmFakturiranje.CheckBox1Click(Sender: TObject);
var i:Integer;
    status: TStatusWindowHandle;
begin
  if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
   begin
   status := cxCreateStatusWindow();
   try
    	if CheckBox1.Checked then
       begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
              Values[FilteredRecordIndex[i] , Selekcija.Index]:=  true;
              dm.qUpdatePriemFlagFaktura.Close;
              dm.qUpdatePriemFlagFaktura.ParamByName('id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PREGLED_ID_OUT.Index];
              dm.qUpdatePriemFlagFaktura.ParamByName('flag').Value:=100;
              dm.qUpdatePriemFlagFaktura.ExecQuery;
             end;
        end
      else
        begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , Selekcija.Index]:= false;
               dm.qUpdatePriemFlagFaktura.Close;
               dm.qUpdatePriemFlagFaktura.ParamByName('id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1PREGLED_ID_OUT.Index];
               dm.qUpdatePriemFlagFaktura.ParamByName('flag').Value:=null;
               dm.qUpdatePriemFlagFaktura.ExecQuery;
            end;
     end
      finally
	       cxRemoveStatusWindow(status);
      end;
   end;
end;

constructor TfrmFakturiranje.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmFakturiranje.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmFakturiranje.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmFakturiranje.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmFakturiranje.aBrisiFakturaExecute(Sender: TObject);
begin
if (cxGrid2DBTableView1.DataController.RecordCount <> 0) then
     begin
        frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
        if (frmDaNe.ShowModal = mrYes) then
          begin
             //dm.qUpdatePriemDogovorIDNULL.Close;
             //dm.qUpdatePriemDogovorIDNULL.ParamByName('faktura_id').Value:=dm.tblFakturiFAKTURA_ID.Value;
             //dm.qUpdatePriemDogovorIDNULL.ExecQuery;

             dm.qDeleteFakturaPriem.Close;
             dm.qDeleteFakturaPriem.ParamByName('faktura_id').Value:=dm.tblFakturiFAKTURA_ID.Value;
             dm.qDeleteFakturaPriem.ExecQuery;

             dm.qDeleteFaktura.Close;
             dm.qDeleteFaktura.ParamByName('faktura_id').Value:=dm.tblFakturiFAKTURA_ID.Value;
             dm.qDeleteFaktura.ExecQuery;

             dm.tblFakturi.Close;
             dm.tblNefakturirani.Close;
             if cbBarGodina.Text <> '' then
               begin
                dm.tblNefakturirani.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
                dm.tblFakturi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
               end
             else
               begin
                dm.tblNefakturirani.ParamByName('godina').Value:=dmKon.godina;
                dm.tblFakturi.ParamByName('godina').Value:=dmKon.godina;
               end;
             dm.tblFakturi.ParamByName('re').Value:=dmKon.re;
             dm.tblNefakturirani.ParamByName('re').Value:=dmKon.re;
             dm.tblNefakturirani.Open;
             dm.tblFakturi.Open;
          end
        else  Abort;
     end;
end;

procedure TfrmFakturiranje.aBrisiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       brisiGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
       brisiGridVoBaza(Name,cxGrid2DBTableView1);
     end;
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmFakturiranje.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmFakturiranje.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmFakturiranje.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
         zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
         zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
     end;
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmFakturiranje.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       zacuvajVoExcel(cxGrid1, Caption);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
       zacuvajVoExcel(cxGrid2, Caption);
     end
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmFakturiranje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmFakturiranje.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmFakturiranje.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmFakturiranje.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmFakturiranje.SelekcijaPropertiesEditValueChanged(Sender: TObject);
var i, momentalen:Integer;
begin
     if Selekcija.EditValue = true then
        begin
          dm.qUpdatePriemFlagFaktura.Close;
          dm.qUpdatePriemFlagFaktura.ParamByName('id').Value:=dm.tblNefakturiraniPREGLED_ID_OUT.Value;
          dm.qUpdatePriemFlagFaktura.ParamByName('flag').Value:=100;
          dm.qUpdatePriemFlagFaktura.ExecQuery;
         end
     else if (selekcija.EditValue  = false) then
         begin
           dm.qUpdatePriemFlagFaktura.Close;
           dm.qUpdatePriemFlagFaktura.ParamByName('id').Value:=dm.tblNefakturiraniPREGLED_ID_OUT.Value;
           dm.qUpdatePriemFlagFaktura.ParamByName('flag').Value:=Null;
           dm.qUpdatePriemFlagFaktura.ExecQuery;
         end;
end;

procedure TfrmFakturiranje.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmFakturiranje.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmFakturiranje.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmFakturiranje.prefrli;
begin
end;

procedure TfrmFakturiranje.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if fakturiranje_vlez = 1 then
   begin
    dm.qUpdatePriemFlagFaktura.Close;
    dm.qUpdatePriemFlagFaktura.ParamByName('id').Value:='%';
    dm.qUpdatePriemFlagFaktura.ParamByName('flag').Value:=null;
    dm.qUpdatePriemFlagFaktura.ExecQuery;
   end;
  Action := caFree;

  dmRes.FreeRepository(rData);
end;
procedure TfrmFakturiranje.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmFakturiranje.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    dmRes.GenerateFormReportsAndActions(dxRibbon1,dxRibbon1Tab1,dxBarManager1,cxGrid1DBTableView1, Name);

  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
    sobrano1 := true;
    sobrano2 := true;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    dm.tblNefakturirani.Close;
    dm.tblNefakturirani.ParamByName('re').Value:=dmKon.re;
    dm.tblNefakturirani.ParamByName('godina').Value:=dmKon.godina;
    dm.tblNefakturirani.Open;

    dm.tblFakturi.Close;
    dm.tblFakturi.ParamByName('godina').Value:=dmKon.godina;
    dm.tblFakturi.ParamByName('re').Value:=dmKon.re;
    dm.tblFakturi.Open;

    pom_godina:=0;
    cbBarGodina.Text:=IntToStr(dmKon.godina);

    dxBarManager1BarFakturirani.Visible:=False;
    dxBarManager1BarNefakturirani.Visible:=True;
    fakturiranje_vlez:=0
end;
//------------------------------------------------------------------------------

procedure TfrmFakturiranje.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmFakturiranje.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmFakturiranje.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmFakturiranje.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
   if NewPage =  cxTabSheetNefakturirani then
   begin
       dxBarManager1BarFakturirani.Visible:=False;
       dxBarManager1BarNefakturirani.Visible:=true;
   end
 else if NewPage = cxTabSheetFakturi then
   begin
       dxBarManager1BarFakturirani.Visible:=True;
       dxBarManager1BarNefakturirani.Visible:=False;
   end;
end;

//  ����� �� �����
procedure TfrmFakturiranje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmFakturiranje.cbBarGodinaChange(Sender: TObject);
begin
if pom_godina = 1 then
   begin
    dm.tblFakturi.Close;
    dm.tblNefakturirani.Close;
    if cbBarGodina.Text <> '' then
      begin
       dm.tblNefakturirani.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
       dm.tblFakturi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
      end
    else
      begin
       dm.tblNefakturirani.ParamByName('godina').Value:=dmKon.godina;
       dm.tblFakturi.ParamByName('godina').Value:=dmKon.godina;
      end;
    dm.tblFakturi.ParamByName('re').Value:=dmKon.re;
    dm.tblNefakturirani.ParamByName('re').Value:=dmKon.re;
    dm.tblNefakturirani.Open;
    dm.tblFakturi.Open;
   end
else
   begin
      pom_godina:= 1
   end;
end;

procedure TfrmFakturiranje.cbGodinaChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmFakturiranje.aOtkaziExecute(Sender: TObject);
begin

      ModalResult := mrCancel;
      Close();

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmFakturiranje.aPageSetupExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       dxComponentPrinter1Link1.PageSetup;
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
       dxComponentPrinter1Link2.PageSetup;
     end
end;

//	����� �� ������� �� ������
procedure TfrmFakturiranje.aPecatiFakturaExecute(Sender: TObject);
begin
if (cxGrid2DBTableView1.DataController.RecordCount <> 0) then
  begin
      try
        dmRes.Spremi('SAN',2);
        dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblFakturiFAKTURA_ID.Value;
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pari_so_bukvi', QuotedStr(PariSoBukvi(dm.tblFakturiIZNOS_POPUST.Value)));
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
  end
else ShowMessage('����������� �������!');
end;

procedure TfrmFakturiranje.aPecatiTabelaExecute(Sender: TObject);
begin
   if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       dxComponentPrinter1Link1.ReportTitle.Text := '�������';
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
     //  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����� : ' + DatumOd.Text + '-' + DatumDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi  then
     begin
       dxComponentPrinter1Link2.ReportTitle.Text := '����� �� ������ ��� �� �� �����������';
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
     //  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + godina.Text + ', ��� :  ' + BrojOd.Text + '-' + BrojDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
     end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmFakturiranje.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
   if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       dxComponentPrinter1Link1.DesignReport();
     end
  else if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       dxComponentPrinter1Link2.DesignReport();
     end
end;

procedure TfrmFakturiranje.aSnimiPecatenjeExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
       zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
       zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
     end
 end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmFakturiranje.aSpustiSoberiExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
        if (sobrano1 = true) then
          begin
            cxGrid1DBTableView1.ViewData.Expand(false);
            sobrano1 := false;
          end
        else
          begin
            cxGrid1DBTableView1.ViewData.Collapse(false);
            sobrano1 := true;
          end
     end
 else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
        if (sobrano2 = true) then
          begin
            cxGrid2DBTableView1.ViewData.Expand(false);
            sobrano2 := false;
          end
        else
          begin
            cxGrid2DBTableView1.ViewData.Collapse(false);
            sobrano2 := true;
          end
     end
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmFakturiranje.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
    if cxPageControl1.ActivePage = cxTabSheetNefakturirani then
     begin
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetFakturi then
     begin
       brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
     end
  end;

procedure TfrmFakturiranje.aDizajnFakturaExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',2);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmFakturiranje.aFakturirajExecute(Sender: TObject);
var
  id_mnozestvo,dogovor_string : AnsiString;
  oznaceno, kraj : bool;
  momentalen : integer;
begin

     dogovor_string:='';
     if cbBarGodina.Text <> '' then
      dogovor_string:=dm.zemiString3(dm.PROC_FAKTURIRANJE, 'GODINA', 'RE', Null,StrToInt(cbBarGodina.Text), dmKon.re,Null, 'DOGOVOR_STRING')
     else
      dogovor_string:=dm.zemiString3(dm.PROC_FAKTURIRANJE, 'GODINA', 'RE', Null, dmKon.godina, dmKon.re,Null,'DOGOVOR_STRING');

     CheckBox1.Checked:=false;
     dm.tblNefakturirani.Close;
     dm.tblFakturi.Close;
     if cbBarGodina.Text <> '' then
       begin
        dm.tblNefakturirani.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
        dm.tblFakturi.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
       end
      else
       begin
          dm.tblNefakturirani.ParamByName('godina').Value:=dmKon.godina;
          dm.tblFakturi.ParamByName('godina').Value:=dmKon.godina;
       end;
     dm.tblFakturi.ParamByName('re').Value:=dmKon.re;
     dm.tblNefakturirani.ParamByName('re').Value:=dmKon.re;
     dm.tblNefakturirani.Open;
     dm.tblFakturi.Open;
     if dogovor_string <>'' then
       ShowMessage('������ ��������� �� �������� �� ���� �� ������ ��������� �� ������ �� �������� �� ���: '+dogovor_string);
     fakturiranje_vlez:=1;
end;

procedure TfrmFakturiranje.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmFakturiranje.aGrupnoPecatenjeExecute(Sender: TObject);
begin
   frmGrupnoPecatenjeFakturi:=TfrmGrupnoPecatenjeFakturi.Create(self,false);
   frmGrupnoPecatenjeFakturi.ShowModal();
   frmGrupnoPecatenjeFakturi.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmFakturiranje.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmFakturiranje.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
