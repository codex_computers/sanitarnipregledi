unit sifUslugi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxCheckBox, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxImageComboBox, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmUslugi = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    Label3: TLabel;
    CENA: TcxDBTextEdit;
    Panel1: TPanel;
    Label4: TLabel;
    cxButton1: TcxButton;
    aPrevzemiuslugi: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aMikroorganizmi: TAction;
    tipDodadiMikro: TdxScreenTip;
    Label5: TLabel;
    RE: TcxDBTextEdit;
    RE_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxDBCheckBox1: TcxDBCheckBox;
    cxDBCheckBox2: TcxDBCheckBox;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    procedure aNovExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure prefrli;
    procedure aPrevzemiuslugiExecute(Sender: TObject);
    procedure aMikroorganizmiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUslugi: TfrmUslugi;

implementation

{$R *.dfm}

uses dmUnit, sifMikroorganizmi, sifMikroorganizmiUslugi, dmMaticni, dmResources;

procedure TfrmUslugi.aMikroorganizmiExecute(Sender: TObject);
begin
  inherited;
   frmMikroorganizmiUslugi:=TfrmMikroorganizmiUslugi.Create(self,true);
   frmMikroorganizmiUslugi.ShowModal();
   frmMikroorganizmiUslugi.Free;
end;

procedure TfrmUslugi.aNovExecute(Sender: TObject);
begin
  inherited;
  dm.qMaxUslugiSifra.Close;
  dm.qMaxUslugiSifra.ExecQuery;
  dm.tblUslugiID.Value:=dm.qMaxUslugiSifra.FldByName['id'].Value;
  NAZIV.SetFocus;
end;

procedure TfrmUslugi.aPrevzemiuslugiExecute(Sender: TObject);
var i,flag:integer;
begin
  inherited;
 if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    begin
      if (cxGrid1DBTableView1.DataController.Controller.SelectedRecordCount > 0)  then
        begin
           with cxGrid1DBTableView1.DataController do
           for I := 0 to cxGrid1DBTableView1.DataController.Controller.SelectedRecordCount -1 do  //izvrti gi site stiklirani - ne samo filtered
            begin
              flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;
              dm.insert6(dm.pInsDelSelektiraniUslugi, 'PREGLED_ID','USLUGA_ID','TIP_DEL_INS', Null, Null,Null,dm.tblPreglediID.Value, GetValue(flag,cxGrid1DBTableView1ID.Index),1,Null,Null,Null);
              dm.insert6(dm.PROC_SAN_LISTAUSLUGI, 'PREGLED_ID', Null, Null, Null,Null, Null,dm.tblPreglediID.Value, Null, Null, Null,Null, Null);
            end;
           dm.tblUslugi.FullRefresh;
           dm.tblPregledi.Refresh;
           dm.tblPreglediStavki.FullRefresh;
           close;
        end
      else ShowMessage('����������� ������!');
    end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmUslugi.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
   prefrli;
end;

procedure TfrmUslugi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmUslugi.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 0 then
     begin
      dm.tblUslugi.Close;
      dm.tblUslugi.ParamByName('param').Value:=0;
      dm.tblUslugi.ParamByName('tip').Value:='%';
      dm.tblUslugi.Open;
     end
  else if tag = 1 then
     begin
      dm.tblUslugi.Close;
      dm.tblUslugi.ParamByName('param').Value:=1;
      dm.tblUslugi.ParamByName('tip').Value:=0;
      dm.tblUslugi.Open;

      Panel1.Visible:=true;
      dxBarManager1Bar1.Visible:=False;
      dxBarManager1Bar5.Visible:=False;
      aNov.Enabled:=False;
      aAzuriraj.Enabled:=False;
      aBrisi.Enabled:=False;
      StatusBar1.Panels[0].Visible:=False;
     end
  else if tag = 2 then
     begin
      dm.tblUslugi.Close;
      dm.tblUslugi.ParamByName('param').Value:=1;
      dm.tblUslugi.ParamByName('tip').Value:=1;
      dm.tblUslugi.Open;

      Panel1.Visible:=true;
      dxBarManager1Bar1.Visible:=False;
      dxBarManager1Bar5.Visible:=False;
      aNov.Enabled:=False;
      aAzuriraj.Enabled:=False;
      aBrisi.Enabled:=False;
      StatusBar1.Panels[0].Visible:=False;
     end;
end;

procedure TfrmUslugi.prefrli;
begin
    inherited;
    if(not inserting) then
      begin
         ModalResult := mrOk;
         SetSifra(0,IntToStr(dm.tblUslugiID.Value));
      end;
end;

end.
