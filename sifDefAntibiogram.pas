unit sifDefAntibiogram;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxCheckBox;

type
  TfrmSifDefAntibiogram = class(TfrmMaster)
    LEK: TcxDBLookupComboBox;
    LEK_ID: TcxDBTextEdit;
    Label2: TLabel;
    ANTIBIOGRAM: TcxDBLookupComboBox;
    cxGrid1DBTableView1ANTIBIOGRAM_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1LEK_ID: TcxGridDBColumn;
    cxGrid1DBTableView1LNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSifDefAntibiogram: TfrmSifDefAntibiogram;

implementation

{$R *.dfm}

uses dmUnit, sifLekovi, sifAntibiogram;

procedure TfrmSifDefAntibiogram.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    LEK_ID.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');
end;

procedure TfrmSifDefAntibiogram.aNovExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    LEK_ID.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblDefAntibiogramANTIBIOGRAM_ID.Value:=dm.tblAntibiogramID.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');

end;

procedure TfrmSifDefAntibiogram.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
       VK_INSERT:
        begin
          if ((Sender = Sifra) or (Sender = ANTIBIOGRAM)) then
             begin
               frmAntibiogramSifrarnik:=TfrmAntibiogramSifrarnik.Create(self,false);
               frmAntibiogramSifrarnik.ShowModal;
               if (frmAntibiogramSifrarnik.ModalResult = mrOK) then
                    dm.tblDefAntibiogramANTIBIOGRAM_ID.Value := StrToInt(frmAntibiogramSifrarnik.GetSifra(0));
                frmAntibiogramSifrarnik.Free;
             end;
          if ((Sender = LEK) or (Sender = LEK_ID)) then
             begin
               frmLekovi:=TfrmLekovi.Create(self,false);
               frmLekovi.ShowModal;
               if (frmLekovi.ModalResult = mrOK) then
                    dm.tblDefAntibiogramLEK_ID.Value := StrToInt(frmLekovi.GetSifra(0));
                frmLekovi.Free;
             end;
        end;
   end;
end;

procedure TfrmSifDefAntibiogram.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblLekovi.Open;
  dm.tblDefAntibiogram.Close;
  dm.tblDefAntibiogram.ParamByName('antibiogram_id').Value:=dm.tblAntibiogramID.Value;
  dm.tblDefAntibiogram.Open;
end;

end.
