inherited frmKategorijaRM: TfrmKategorijaRM
  Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
  ClientHeight = 569
  ExplicitWidth = 749
  ExplicitHeight = 607
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 290
    ExplicitHeight = 290
    inherited cxGrid1: TcxGrid
      Height = 286
      ExplicitHeight = 286
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsKategorijaRM
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 71
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 419
        end
        object cxGrid1DBTableView1ROK: TcxGridDBColumn
          DataBinding.FieldName = 'ROK'
          Width = 75
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 161
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 214
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 198
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 252
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Height = 130
    ExplicitTop = 416
    ExplicitHeight = 130
    object Label2: TLabel [1]
      Left = 31
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 27
      Top = 75
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1086#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [3]
      Left = 163
      Top = 75
      Width = 44
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1084#1077#1089#1077#1094#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Hint = #1064#1080#1092#1088#1072' '#1085#1072'  '#1082#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsKategorijaRM
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
    end
    inherited OtkaziButton: TcxButton
      Top = 90
      TabOrder = 4
      ExplicitTop = 90
    end
    inherited ZapisiButton: TcxButton
      Top = 90
      TabOrder = 3
      ExplicitTop = 90
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1082#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsKategorijaRM
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 478
    end
    object ROK: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 72
      Hint = #1056#1086#1082' '#1079#1072' '#1087#1086#1074#1090#1086#1088#1085#1086' '#1080#1089#1087#1080#1090#1091#1074#1072#1114#1077' ('#1074#1086' '#1084#1077#1089#1077#1094#1080')'
      BeepOnEnter = False
      DataBinding.DataField = 'ROK'
      DataBinding.DataSource = dm.dsKategorijaRM
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 546
    ExplicitTop = 546
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 224
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41347.629142430550000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
