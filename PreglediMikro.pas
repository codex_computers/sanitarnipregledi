unit PreglediMikro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxSplitter, cxPCdxBarPopupMenu, cxImageComboBox, dxCustomHint, cxHint,
  dxSkinOffice2013White, cxNavigator, System.Actions, dxBarBuiltInMenu,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmPreglediMikro = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    Panel2: TPanel;
    cxSplitter1: TcxSplitter;
    dxBarManager1Bar5: TdxBar;
    cbGodina: TcxBarEditItem;
    cxPageControlMaster: TcxPageControl;
    cxTabSheetTabelarenM: TcxTabSheet;
    cxPageControlDetail: TcxPageControl;
    cxTabSheetTabelarenD: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    aPrebarajPacient: TAction;
    aIsprazniPacient: TAction;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ADRESA: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTONAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MB: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxBarEditItem2: TcxBarEditItem;
    cbBarGodina: TdxBarCombo;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1PREGLED_ID: TcxGridDBColumn;
    cxGrid2DBTableView1USLUGA_ID: TcxGridDBColumn;
    cxGrid2DBTableView1ULUGANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1NAOD: TcxGridDBColumn;
    cxGrid2DBTableView1ZAVRSENA: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiUsluga: TAction;
    aBrisiUsluga: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    aSpustiSoberi2: TAction;
    aZacuvajExcel2: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    aPecatiTabela2: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarButton5: TdxBarButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarManager1Bar7: TdxBar;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    aPodesuvanjePecatenje2: TAction;
    aPageSetup2: TAction;
    aSnimiPecatenje2: TAction;
    aBrisiPodesuvanjePecatenje2: TAction;
    aSnimiIzgled2: TAction;
    aBrisiIzgled2: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar1: TdxBar;
    aVnesNaRezultat: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    tipAnaliza: TdxScreenTip;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton29: TdxBarLargeButton;
    aStatus: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    aPecatiRezultatiAnaliza: TAction;
    dxBarLargeButton31: TdxBarLargeButton;
    aDizajnRezultatAnaliza: TAction;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    dxBarSubItem4: TdxBarSubItem;
    aZavrseniAnalizi: TAction;
    aNezavrseniAnalizi: TAction;
    aSiteAnalizi: TAction;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRIEM: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1PACIENT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PAKET_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAVERKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PLACANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NACINPLAKANJE: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA_RM: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1U_LISTA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1NAOD1: TcxGridDBColumn;
    actFind: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aDodadiUslugaExecute(Sender: TObject);
    procedure cbBarGodinaChange(Sender: TObject);
    procedure aSpustiSoberi2Execute(Sender: TObject);
    procedure aZacuvajExcel2Execute(Sender: TObject);
    procedure aPecatiTabela2Execute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aPageSetup2Execute(Sender: TObject);
    procedure aSnimiPecatenje2Execute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aSnimiIzgled2Execute(Sender: TObject);
    procedure aBrisiIzgled2Execute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aVnesNaRezultatExecute(Sender: TObject);
    procedure aStatusExecute(Sender: TObject);
    procedure aPecatiRezultatiAnalizaExecute(Sender: TObject);
    procedure aDizajnRezultatAnalizaExecute(Sender: TObject);
    procedure aZavrseniAnaliziExecute(Sender: TObject);
    procedure aNezavrseniAnaliziExecute(Sender: TObject);
    procedure aSiteAnaliziExecute(Sender: TObject);
    procedure cxGrid2DBTableView1NAODStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure actFindExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano1, sobrano2 : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;


    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
   property sifra_kluc[br :integer]: string read GetSifra write SetSifra;

  end;

var
  frmPreglediMikro: TfrmPreglediMikro;
  rData : TRepositoryData;
  pom_godina :Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, sifUslugi,
  Partner, Analizi;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPreglediMikro.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPreglediMikro.aNezavrseniAnaliziExecute(Sender: TObject);
begin
    dm.tblPriemLab.Close;
    if cbBarGodina.Text <> '' then
       dm.tblPriemLab.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPriemLab.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPriemLab.ParamByName('param').Value:=2;
    if dm.re_pregled = '1' then
       dm.tblPriemLab.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPriemLab.ParamByName('re').Value:='%';
    dm.tblPriemLab.Open;
    dm.tblPriemLabStavki.FullRefresh;
end;

procedure TfrmPreglediMikro.aNovExecute(Sender: TObject);
begin
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPreglediMikro.aAzurirajExecute(Sender: TObject);
begin
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPreglediMikro.aBrisiIzgled2Execute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

procedure TfrmPreglediMikro.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmPreglediMikro.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPreglediMikro.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPreglediMikro.aSiteAnaliziExecute(Sender: TObject);
begin
    dm.tblPriemLab.Close;
    if cbBarGodina.Text <> '' then
       dm.tblPriemLab.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPriemLab.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPriemLab.ParamByName('param').Value:=0;
    if dm.re_pregled = '1' then
       dm.tblPriemLab.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPriemLab.ParamByName('re').Value:='%';
    dm.tblPriemLab.Open;
    dm.tblPriemLabStavki.FullRefresh;
end;

procedure TfrmPreglediMikro.aSnimiIzgled2Execute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmPreglediMikro.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPreglediMikro.aZacuvajExcel2Execute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������ �� ������');
end;

procedure TfrmPreglediMikro.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� ��������');
end;

procedure TfrmPreglediMikro.aZavrseniAnaliziExecute(Sender: TObject);
begin
    dm.tblPriemLab.Close;
    if cbBarGodina.Text <> '' then
       dm.tblPriemLab.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPriemLab.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPriemLab.ParamByName('param').Value:=1;
    if dm.re_pregled = '1' then
       dm.tblPriemLab.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPriemLab.ParamByName('re').Value:='%';
    dm.tblPriemLab.Open;
    dm.tblPriemLabStavki.FullRefresh;
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPreglediMikro.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

     	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPreglediMikro.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPreglediMikro.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPreglediMikro.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPreglediMikro.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPreglediMikro.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPreglediMikro.prefrli;
begin
end;

procedure TfrmPreglediMikro.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmPreglediMikro.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

    dmRes.GenerateFormReportsAndActions(dxRibbon1,dxRibbon1Tab1,dxBarManager1,cxGrid1DBTableView1, Name);

    dm.tblPriemLab.Close;
    dm.tblPriemLab.ParamByName('godina').Value:=dmKon.godina;
    dm.tblPriemLab.ParamByName('param').Value:=2;
    dm.tblPriemLab.ParamByName('param_datum').Value:=0;
    dm.tblPriemLab.ParamByName('datum').Value:=Now;
    dm.tblPriemLab.ParamByName('re').Value:='%';
    if dm.re_pregled = '1' then
       dm.tblPriemLab.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPriemLab.ParamByName('re').Value:='%';
    dm.tblPriemLab.Open;
    dm.tblPriemLabStavki.Open;

    cxPageControlMaster.ActivePage:=cxTabSheetTabelarenM;
    cxPageControlDetail.ActivePage:=cxTabSheetTabelarenD;

    pom_godina:=0;
    cbBarGodina.Text:=IntToStr(dmKon.godina);

    dxComponentPrinter1Link1.ReportTitle.Text := '����� �� ��������';
    dxComponentPrinter1Link2.ReportTitle.Text := '������';


    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);

    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
    sobrano1 := true;
    sobrano2 := true;

    cxGrid1.SetFocus;

end;
//------------------------------------------------------------------------------

procedure TfrmPreglediMikro.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
    cxGrid2DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPreglediMikro.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
       VK_RETURN:
        begin
          cxGrid2.SetFocus;
     	  end;
  end;
end;

procedure TfrmPreglediMikro.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPreglediMikro.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
       VK_RETURN:
        begin
          aVnesNaRezultat.Execute();
     	  end;
  end;

end;

procedure TfrmPreglediMikro.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmPreglediMikro.cxGrid2DBTableView1NAODStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
      if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1NAOD.Index] = 1) then
          AStyle := dm.cxStylePortokalova
      else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1NAOD.Index] = 0) then
          AStyle := dm.cxStyleZelono
end;

procedure TfrmPreglediMikro.cxGrid2DBTableView1ZAVRSENAStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 0) then
          AStyle := dm.cxStylePortokalova
     else if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1ZAVRSENA.Index] = 1) then
          AStyle := dm.cxStyleZelono
end;

//  ����� �� �����
procedure TfrmPreglediMikro.cbBarGodinaChange(Sender: TObject);
begin
if pom_godina = 1 then
   begin
    dm.tblPriemLab.Close;
    if cbBarGodina.Text <> '' then
       dm.tblPriemLab.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
    else
       dm.tblPriemLab.ParamByName('godina').Value:=dmKon.godina;
    if dm.re_pregled = '1' then
       dm.tblPriemLab.ParamByName('re').Value:=dmKon.re
    else
       dm.tblPriemLab.ParamByName('re').Value:='%';
    dm.tblPriemLab.Open;
    dm.tblPriemLabStavki.FullRefresh;
   end
else
   begin
      pom_godina:= 1
   end;
end;

//	����� �� ���������� �� �������
procedure TfrmPreglediMikro.aOtkaziExecute(Sender: TObject);
begin
   Close();
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPreglediMikro.aPageSetup2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmPreglediMikro.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPreglediMikro.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����� �� �������� �� : ' + cbBarGodina.Text + ' ������') ;

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPreglediMikro.aPodesuvanjePecatenje2Execute(Sender: TObject);
begin
   dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPreglediMikro.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPreglediMikro.aSnimiPecatenje2Execute(Sender: TObject);
begin
    zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

procedure TfrmPreglediMikro.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmPreglediMikro.aSpustiSoberi2Execute(Sender: TObject);
begin
  if (sobrano2 = true) then
  begin
    cxGrid2DBTableView1.ViewData.Expand(false);
    sobrano2 := false;
  end
  else
  begin
    cxGrid2DBTableView1.ViewData.Collapse(false);
    sobrano2 := true;
  end;
end;

procedure TfrmPreglediMikro.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano1 = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano1 := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano1 := true;
  end;
end;

procedure TfrmPreglediMikro.aStatusExecute(Sender: TObject);
begin
 dm.qCountPravaRe.Close;
 dm.qCountPravaRe.ParamByName('re').Value:=dm.tblPriemLabStavkiRE.Value;
 dm.qCountPravaRe.ParamByName('username').Value:=dmKon.user;
 dm.qCountPravaRe.ExecQuery;
 if dm.qCountPravaRe.FldByName['br'].Value = 1 then
   begin
    frmDaNe := TfrmDaNe.Create(self, '������� �� ������ �� ������', '���� ��������� ������ �� �� ��������� �������� �� �������� ?', 1);
    if (frmDaNe.ShowModal = mrYes) then
      begin
        if (dm.tblPriemLabStavkiZAVRSENA.Value = 1) then
        begin
          dm.tblPriemLabStavki.Edit;
          dm.tblPriemLabStavkiZAVRSENA.Value:=0;
          dm.tblPriemLabStavki.Post;

          tag:=dm.zemi3(dm.pZatvoriDatum,'PREGLED_ID', Null, Null, dm.tblPriemLabID.Value, Null, Null,'TAG');
          if tag = 1 then ShowMessage('������� ����������� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!')
          else if tag = 0 then  ShowMessage('������� ����Ō� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!') ;
        end
        else if (dm.tblPriemLabStavkiZAVRSENA.Value = 0) then
        begin
          dm.tblPriemLabStavki.Edit;
          dm.tblPriemLabStavkiZAVRSENA.Value:=1;
          dm.tblPriemLabStavki.Post;

          tag:=dm.zemi3(dm.pZatvoriDatum,'PREGLED_ID', Null, Null, dm.tblPriemLabID.Value, Null, Null,'TAG');
          if tag = 1 then ShowMessage('������� ����������� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!')
          else if tag = 0 then  ShowMessage('������� ����Ō� �� ����� �� ������� � ����� �� ����� �� ��������� �� ������� !!!') ;
        end;
        dm.tblPriemLab.Refresh;
      end
    else Abort;
   end
  else ShowMessage('������ ����� �� ������� �� ������ �� ������������� ������ !');
end;

procedure TfrmPreglediMikro.aVnesNaRezultatExecute(Sender: TObject);
begin
     dm.qCountPravaRe.Close;
     dm.qCountPravaRe.ParamByName('re').Value:=dm.tblPriemLabStavkiRE.Value;
     dm.qCountPravaRe.ParamByName('username').Value:=dmKon.user;
     dm.qCountPravaRe.ExecQuery;
     if dm.qCountPravaRe.FldByName['br'].Value = 1 then
        begin
          frmAnaliza:=TfrmAnaliza.Create(self,true);
          frmAnaliza.Caption:='������� �� '+dm.tblPriemLabStavkiULUGANAZIV.Value;
          frmAnaliza.ShowModal();
          frmAnaliza.Free;
        end
     else ShowMessage('������ ����� �� ������� � ��������� �� ������� �� ������������� ������ !');
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPreglediMikro.aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmPreglediMikro.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPreglediMikro.actFindExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.FindPanel.DisplayMode:=fpdmAlways;
end;

procedure TfrmPreglediMikro.aPecatiRezultatiAnalizaExecute(Sender: TObject);
begin

       try
        dmRes.Spremi('SAN',1);
        dmKon.tblSqlReport.ParamByName('pregled_id').Value:=dm.tblPriemLabID.Value;
        dmKon.tblSqlReport.Open;
        dm.tblRezultatAnaliza.Close;
        dm.tblRezultatAnaliza.ParamByName('pregled_id').Value:=dm.tblPriemLabID.Value;
        dm.tblRezultatAnaliza.Open;
        dm.tblFrxAntibiogram.Close;
        dm.tblFrxAntibiogram.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pacient', QuotedStr(dm.tblPriemLabPACIENT_NAZIV.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'br_dnevnik', QuotedStr(intToStr(dm.tblPriemLabBROJ.Value)));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
   
end;

procedure TfrmPreglediMikro.aPecatiTabela2Execute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + dm.tblPriemLabPACIENT_NAZIV.Value+' - '+dm.tblPriemLabEMBG.Value+' - '+intToStr(dm.tblPreglediBROJ.Value));

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmPreglediMikro.aDizajnRezultatAnalizaExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',1);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPreglediMikro.aDodadiUslugaExecute(Sender: TObject);
begin

end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPreglediMikro.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPreglediMikro.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPreglediMikro.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

end.
