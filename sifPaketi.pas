unit sifPaketi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, dxSkinOffice2013White, cxNavigator, cxImageComboBox, cxCheckBox,
  System.Actions;

type
  TfrmPaketi = class(TfrmMaster)
    Label3: TLabel;
    NAZIV: TcxDBTextEdit;
    CENA: TcxDBTextEdit;
    Label4: TLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aDefinicijaPaket: TAction;
    tipDefinicijaPaket: TdxScreenTip;
    cxCheckBoxKontrolen: TcxDBCheckBox;
    cxGrid1DBTableView1KONTROLEN_PREGLED: TcxGridDBColumn;
    procedure aNovExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure prefrli;
    procedure aDefinicijaPaketExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPaketi: TfrmPaketi;

implementation

{$R *.dfm}

uses dmUnit, sifPaketiUslugi, dmResources;

procedure TfrmPaketi.aDefinicijaPaketExecute(Sender: TObject);
begin
  inherited;
  frmPaketiUslugi:=TfrmPaketiUslugi.Create(self,true);
  frmPaketiUslugi.ShowModal();
  frmPaketiUslugi.Free;
end;

procedure TfrmPaketi.aNovExecute(Sender: TObject);
begin
  inherited;
  dm.qMaxPaketiSifra.Close;
  dm.qMaxPaketiSifra.ExecQuery;
  dm.tblPaketiID.Value:=dm.qMaxPaketiSifra.FldByName['id'].Value;
  NAZIV.SetFocus;
end;

procedure TfrmPaketi.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

procedure TfrmPaketi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmPaketi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.tblPaketi.Close;
  dm.tblPaketi.Open;
end;

procedure TfrmPaketi.prefrli;
begin
    inherited;
    if(not inserting) then
      begin
         ModalResult := mrOk;
         SetSifra(0,IntToStr(dm.tblPaketiID.Value));
      end;
end;

end.
