unit LoginMZ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, ExtCtrls, cxLabel, cxCheckBox, FIBQuery,
  pFIBQuery, cxGraphics, cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus;

type
  TfrmLoginMZ = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    edtUsername: TcxTextEdit;
    edtPass: TcxTextEdit;
    cxButton1: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxButton2: TcxButton;
    cxCBSync: TcxCheckBox;
    qUpdSessTokForm: TpFIBQuery;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoginMZ: TfrmLoginMZ;

implementation

uses dmKonekcija,  dmMaticni, Dane, dmUnit;

{$R *.dfm}

procedure TfrmLoginMZ.cxButton1Click(Sender: TObject);

begin
      dm.username_st:=  edtUsername.Text;
      dm.password_st:=  edtPass.Text;
      dm.session_token:=dm.GetSessionToken(dm.username_st, dm.password_st);
      if dm.session_token='' then
      begin
          frmDaNe := TfrmDaNe.Create(self, '�������� ��������', '��������� �������� �� ��������. �� ���� �� �� �������� ������� �����. ���� ������ �� ���������� �� ������?', 1);
          if (frmDaNe.ShowModal = mrYes) then
          begin
              dm.username_st:='';
              dm.password_st:= '';
              Close;
          end;

      end
      else
      begin
        if dm.username_st<>'' then
            begin
              dm.TimerToken.enabled:=false;
              dm.TimerToken.enabled:=true;
            end;
        Close;
      end;
    
end;

procedure TfrmLoginMZ.FormShow(Sender: TObject);
begin
     ActivateKeyboardLayout($04090409, KLF_REORDER);
     if dm.tblSetup.Locate('P2','SESSIONTOKEN_FORM',[]) then
      cxCBSync.EditValue:=dm.tblSetupV1.Value;

end;

procedure TfrmLoginMZ.cxButton2Click(Sender: TObject);
begin
      dm.username_st:='' ;
      dm.password_st:='';
      Close;

end;

procedure TfrmLoginMZ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    qUpdSessTokForm.ParamByName('p_1').AsString:=vartostr(cxCBSync.EditValue);
    qUpdSessTokForm.ExecQuery;
end;

end.
